﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Campaign.aspx.cs" Inherits="DeskAlerts.Server.sever.MODULES.CAMPAIGN_MODULE.admin.Campaign" %>
<%@ Import Namespace="DeskAlerts.Server.Utils.Helpers" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="viewport" content="user-scalable=no,initial-scale=1,maximum-scale=1,minimum-scale=1,width=device-width">

    <title>Campaign</title>
    <link href="css/google.fonts.css?family=Roboto:100,300,400,500,700,900|Material+Icons" rel="stylesheet" />
    <link href="css/animate.min.css" rel="stylesheet" />
    <link href="css/quasar.mat.min.css" rel="stylesheet" />
    <style>
        :root {
            --q-color-primary: #066c19;
        }

        .text-search {
            color: black;
        }

        .bg-search {
            background: #f0f0f1;
        }

        [v-cloak] {
            display: none;
        }
    </style>
    <link href="admin" />
</head>

<body>
    <div id="app" v-cloak>
        <q-layout view="hHr LpR lFf">
            <!-- Header -->
            <q-layout-header>
                <q-toolbar>
                    <q-toolbar-title>
                        Campaigns
                        <span slot="subtitle">Manage campaigns</span>
                    </q-toolbar-title>
                </q-toolbar>
            </q-layout-header>

            <!-- page container -->
            <q-page-container>
                <campaign-page></campaign-page>
            </q-page-container>
            <!-- end-page container -->
        </q-layout>
    </div>
    <script src="jscripts/vue/quasar.ie.polyfills.umd.min.js"></script>
    <script src="jscripts/vue/vue.min.js"></script>
    <script src="jscripts/vue/quasar.mat.umd.min.js"></script>
    <script src="jscripts/vue/vue-router.js"></script>
    <script src="jscripts/vue/axios.min.js"></script>
    <script src="jscripts/moment.js"></script>
    <script type="text/x-template" id="campaign-page">
        <q-page padding>
            <template>
                <q-btn class="q-mb-md" color="primary" icon="create" @click="newCampaignClick" label="New campaign" v-if="canCreate" />
                <q-search class="q-mb-md"
                          color="primary"
                          v-model="filter" />
                <!--campaign-table-->
                <q-table class="q-mb-md"
                         :selection="selection"
                         :selected.sync="selected"
                         :data="tableData"
                         :columns="columns"
                         :filter="filter"
                         row-key="id"
                         :pagination.sync="serverPagination"
                         :loading="loading"
                         @request="request"
                         v-if="canView">
                    <template slot="top-selection" slot-scope="props">
                        <div class="col" />
                        <q-btn color="negative" v-if="canDelete" flat round delete icon="delete" @click="deleteRow" />
                    </template>
                    <template slot="body" slot-scope="props">
                        <q-tr :props="props">
                            <q-td auto-width>
                                <q-checkbox color="primary" v-model="props.selected" />
                            </q-td>
                            <q-td key="name" :props="props" @click.native="campaignClick(props.row)" class="cursor-pointer">
                                {{ props.row.name }}
                            </q-td>
                            <q-td key="st_d" :props="props" auto-width>{{ props.row.startDateText }}</q-td>
                            <q-td key="en_d" :props="props" auto-width>{{ props.row.endDateText }}</q-td>
                            <q-td key="stat" :props="props" auto-width>{{ props.row.statusText }}</q-td>
                            <q-td key="actions" :props="props" auto-width>
                                <q-btn color="primary" flat round icon="edit" v-if="canEdit" @click="editCampaignClick(props.row)" />
                                <q-btn color="primary" flat round icon="play_circle_outline" v-if="props.row.startButtonVisible && canStop" @click="startCampaignClick(props.row)" />
                                <q-btn color="primary" flat round icon="stop" v-if="props.row.stopButtonVisible && canStop" @click="stopCampaignClick(props.row.id)" />
                            </q-td>
                        </q-tr>
                    </template>
                </q-table>
                <!--end-campaign-table-->
                <!--newCampaignDialog-->
                <q-modal v-model="newCampaignDialog" :content-css="{minWidth: '30vw', height: '330px'}" minimized position="top">
                    <q-modal-layout>
                        <q-toolbar slot="header">
                            <q-btn flat
                                   round
                                   dense
                                   v-close-overlay
                                   icon="keyboard_arrow_left" />
                            <q-toolbar-title>
                                Create new company
                            </q-toolbar-title>
                        </q-toolbar>
                        <q-toolbar slot="footer" color="search">
                            <q-btn color="primary" icon="create" @click="createCampaignClick" label="Create campaign" />
                        </q-toolbar>
                        <div class="layout-padding">
                            <q-input v-model="newCampaign.name" stack-label="Campaign name" />
                            <q-datetime v-model="newCampaign.startDate" format="<% = DateTimeConverter.QuasarDateTimeFormat %>" type="datetime" stack-label="Start datetime" :default-value="currentDateTime" />
                            <q-datetime v-model="newCampaign.endDate" format="<% = DateTimeConverter.QuasarDateTimeFormat %>" type="datetime" stack-label="End datetime" :default-value="currentDateTime" />
                        </div>
                    </q-modal-layout>
                </q-modal>
                <!--end-newCampaignDialog-->
                <!--editCampaignDialog-->
                <q-modal v-model="editCampaignDialog" :content-css="{minWidth: '30vw', height: '330px'}" minimized position="top">
                    <q-modal-layout>
                        <q-toolbar slot="header">
                            <q-btn flat
                                   round
                                   dense
                                   v-close-overlay
                                   icon="keyboard_arrow_left" />
                            <q-toolbar-title>
                                {{ editCampaign.name }}
                            </q-toolbar-title>
                        </q-toolbar>
                        <q-toolbar slot="footer" color="search">
                            <q-btn color="primary" icon="create" @click="saveChangesClick" label="Save changes" />
                        </q-toolbar>
                        <div class="layout-padding">
                            <q-input v-model="editCampaign.name" stack-label="Campaign name" />
                            <q-datetime v-model="editCampaign.startDate" format="<% = DateTimeConverter.QuasarDateTimeFormat %>" stack-label="Start datetime" />
                            <q-datetime v-model="editCampaign.endDate" format="<% = DateTimeConverter.QuasarDateTimeFormat %>" stack-label="End datetime" />
                        </div>
                    </q-modal-layout>
                </q-modal>
                <!--end-editCampaignDialog-->
            </template>
        </q-page>
    </script>

    <script>
        var router = new VueRouter({
            mode: 'history',
            routes: []
        });

        Vue.component("campaign-page", {
            template: "#campaign-page",
            data: function data() {
                return {
                    currentDateTime: new Date(),
                    filter: "",
                    loading: false,
                    selection: "multiple",
                    selected: [],
                    serverPagination: {
                        page: 1,
                        rowsNumber: 25,
                        rowsPerPage: 25
                    },
                    columns: [
                        {
                            name: "name",
                            required: true,
                            label: "Campaign`s name",
                            align: "left",
                            field: "name",
                            sortable: true
                        },
                        {
                            name: "st_d",
                            label: "Start date",
                            field: "startDateText",
                            sortable: true
                        },
                        {
                            name: "en_d",
                            label: "End date",
                            field: "endDateText",
                            sortable: true
                        },
                        { name: "stat", label: "Status", field: "status", sortable: true },
                        {
                            name: "actions",
                            required: true,
                            label: "Actions",
                            align: "center",
                            field: "name",
                            sortable: false
                        }
                    ],
                    tableData: [],
                    canCreate: false,
                    canView: false,
                    canEdit: false,
                    canDelete: false,
                    canSend: false,
                    canStop: false,
                    newCampaignDialog: false,
                    newCampaign: {
                        name: "",
                        startDate: null,
                        endDate: null
                    },
                    editCampaignDialog: false,
                    editCampaign: {
                        name: "",
                        startDate: null,
                        endDate: null,
                        id: null
                    },
                    url: ""
                };
            },
            methods: {
                request: function request(_ref) {
                    var _this = this;

                    var pagination = _ref.pagination,
                        filter = _ref.filter;

                    pagination.search = filter;
                    this.loading = true;
                    axios
                        .post(this.url + "api/v1/campaigns", pagination)
                        .then(function (_ref2) {
                            var data = _ref2.data.data;

                            _this.serverPagination = pagination;
                            _this.serverPagination.rowsNumber = data.pagination.rowsNumber;
                            _this.loading = false;
                            _this.canCreate = data.canCreate;
                            _this.canView = data.canView;
                            _this.canEdit = data.canEdit;
                            _this.canDelete = data.canDelete;
                            _this.canSend = data.canSend;
                            _this.canStop = data.canStop;

                            function parseDateTime(dateTime) {
                                if (dateTime !== undefined) {
                                    var date = moment(dateTime);
                                    return date.format("<% = DateTimeConverter.QuasarDateTimeFormat %>");
                                }
                            }

                            data.campaigns.forEach(function (i) {
                                i.startDateText = parseDateTime(i.startDate);
                                i.endDateText = parseDateTime(i.endDate);
                                i.alerts = [];
                            });

                            _this.tableData = data.campaigns;
                        })
                        .catch(function (error) { })
                        .then(function () {
                            _this.loading = false;
                        });
                },
                campaignClick: function campaignClick(column) {
                    window.location.href = "CampaignDetails.aspx?campaignId=" + column.id;
                },
                newCampaignClick: function newCampaignClick() {
                    this.newCampaignDialog = !this.newCampaignDialog;
                    if (this.newCampaignDialog) {
                        this.newCampaign = {
                            name: "",
                            startDate: null,
                            endDate: null
                        };
                    }
                },
                editCampaignClick: function editCampaignClick(row) {
                    this.editCampaignDialog = !this.editCampaignDialog;
                    if (this.editCampaignDialog) {
                        this.editCampaign.name = row.name;
                        this.editCampaign.startDate = row.startDate;
                        this.editCampaign.endDate = row.endDate;
                        this.editCampaign.id = row.id;
                    }
                },
                createCampaignClick: function createCampaignClick() {
                    var _this2 = this;

                    if (this.newCampaign.name === "") {
                        this.showMessage("Campaign name can not be empty");
                        return;
                    }

                    if (
                        this.newCampaign.startDate !== null &&
                        this.newCampaign.endDate !== null &&
                        this.newCampaign.startDate >= this.newCampaign.endDate
                    ) {
                        this.showMessage("Start date must be less end date");
                        return;
                    }

                    this.loading = true;
                    axios
                        .post(this.url + "api/v1/campaigns/add", this.newCampaign)
                        .then(function (_ref3) {
                            var data = _ref3.data.data;

                            window.location.href = "CampaignDetails.aspx?campaignId=" + data.id;
                        })
                        .catch(function (error) { })
                        .then(function () {
                            _this2.loading = false;
                            _this2.newCampaignDialog = false;
                        });
                },
                saveChangesClick: function saveChangesClick() {
                    var _this3 = this;

                    if (this.editCampaign.startDate >= this.editCampaign.endDate) {
                        this.showMessage("Start date must be less end date");
                        return;
                    }

                    axios
                        .post(this.url + "api/v1/campaigns/update", this.editCampaign)
                        .then(function (_ref4) {
                            var data = _ref4.data;

                            _this3.request({
                                pagination: _this3.serverPagination,
                                filter: _this3.filter
                            });
                        })
                        .catch(function (error) { })
                        .then(function () {
                            _this3.loading = false;
                            _this3.editCampaignDialog = false;
                        });
                },
                stopCampaignClick: function stopCampaignClick(campaignId) {
                    var _this4 = this;

                    axios
                        .post(this.url + "api/v1/campaigns/" + campaignId + "/stop")
                        .then(function () {
                            _this4.request({
                                pagination: _this4.serverPagination,
                                filter: _this4.filter
                            });
                        })
                        .catch(function (error) { })
                        .then(function () {
                            _this4.loading = false;
                            _this4.editCampaignDialog = false;
                        });
                },
                startCampaignClick: function startCampaignClick(row) {
                    var _this5 = this;

                    if (row.startDate >= row.endDate) {
                        this.showMessage("Start date must be less end date");
                        return;
                    }

                    axios
                        .post(this.url + "api/v1/campaigns/" + row.id + "/start")
                        .then(function () {
                            _this5.request({
                                pagination: _this5.serverPagination,
                                filter: _this5.filter
                            });
                        })
                        .catch(function (error) { })
                        .then(function () {
                            _this5.loading = false;
                            _this5.editCampaignDialog = false;
                        });
                },
                deleteRow: function deleteRow() {
                    var _this6 = this;

                    this.loading = true;
                    axios
                        .post(this.url + "api/v1/campaigns/delete", this.selected)
                        .then(function (_ref5) {
                            var data = _ref5.data;

                            _this6.selected = [];
                            _this6.request({
                                pagination: _this6.serverPagination,
                                filter: _this6.filter
                            });
                        })
                        .catch(function (error) { })
                        .then(function () {
                            _this6.loading = false;
                        });
                },
                getUrl: function getUrl() {
                    return window.location.href.slice(
                        0,
                        window.location.href.length - "admin/Campaign.aspx".length
                    );
                },
                showMessage: function showMessage(text) {
                    this.$q.notify({
                        message: text,
                        position: "center",
                        type: "warning"
                    });
                }
            },
            mounted: function mounted() {
                this.url = this.getUrl();
                this.request({
                    pagination: this.serverPagination,
                    filter: this.filter
                });
            }
        });

        var app = new Vue({
            router: router,
            el: "#app"
        });

    </script>
</body>

</html>