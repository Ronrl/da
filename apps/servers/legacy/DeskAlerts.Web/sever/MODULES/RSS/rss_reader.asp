<%
'@LANGUAGE="VBSCRIPT" CODEPAGE="65001"
%>
<!-- #include file="admin/config.inc" -->
<!-- #include file="admin/db_conn.asp" -->
<!-- #include file="admin/libs/TemplateObj.asp" -->
<!-- #include file="admin/rss_functions.asp" -->
<%
Response.Expires = -1
Server.ScriptTimeout = 300
Conn.CommandTimeout = 300

insertAlertsSQL = ""
alertsRegularList = ""
alertsBroadcastList = ""

function getRandomSkinCaption
    skins = Array()'Array("default")
    
    Set skinsSet = Conn.Execute("SELECT id  as skin_id  FROM alert_captions")
    Set skinksCountSet = Conn.Execute("SELECT count(id) as cnt FROM alert_captions")
    skinsCount = Clng(skinksCountSet("cnt")) '+ 1
    Set skinsArray  = CreateObject("System.Collections.ArrayList")
   ' skinsArray.add "NULL"
    
    do while not skinsSet.EOF
        qoutedSkinId = "'" & skinsSet("skin_id") & "'"
        skinsArray.add qoutedSkinId

        skinsSet.Movenext
    loop
    
     randomize
     randomIndex = CInt(Math.Floor(skinsCount * Rnd()))
     getRandomSkinCaption = skinsArray.item(randomIndex)
end function


Set RS = Conn.Execute("SELECT id, alert_text, ticker, type2, schedule, schedule_type, sms, email, email_sender, from_date, to_date, rss_allow_content FROM alerts WHERE type = 'S' AND class = 4")
Do While Not RS.EOF
	if RS("schedule_type") = "1" AND ( YEAR(RS("to_date")) < 2000  OR ( YEAR(RS("to_date")) > 2000 AND  Now() < CDate(RS("to_date")) ) ) then

    
		alertId = RS("id")
		URLToRSS = ""
		storedHashesStr = ""
		rssTemplate = "{description}<br/>{url}"
		
            
		alertText = RS("alert_text")
		separatorIndex = InStr(alertText,"|")
		lifetime = "0"
		params = ""
		ticker = CLng(RS("ticker"))
		if separatorIndex > 0 then
			URLToRSS = Left(alertText,separatorIndex-1)
			alertText = Mid(alertText,separatorIndex+1)
			separatorIndex = InStr(alertText,"|")
			if separatorIndex > 0 then
				storedHashesStr = Left(alertText,separatorIndex-1)
				alertText = Mid(alertText,separatorIndex+1)
				separatorIndex = InStr(alertText,"|")
				if separatorIndex > 0 then
					params = Left(alertText,separatorIndex-1) 'for other parameters use splitting by separator ";"
					rssTemplate = Mid(alertText,separatorIndex+1)
					separatorIndex = InStr(params,";")
					if separatorIndex > 0 then
						lifetime = Left(params,separatorIndex-1)
						params = Mid(params,separatorIndex+1)
					else
						lifetime = params
					end if
				end if
			end if
		end if

		storedHashes = split(storedHashesStr,",")
		if VarType(storedHashes) <= 1 then
			storedHashes = Array()
		end if
		
		Data = getFeedData(URLToRSS)
		writeLineToRSSLog("got data from RSS feed")
		if Data(0) = "" then 'no error
			writeLineToRSSLog("no error")
			hashStr = Data(2)
			insertRSSNewsSQL = ""
			for each RSSItem in Data(1).Items
				LoadTemplateFromString "templateRSSAlert", rssTemplate
				for each key in RSSItem.Keys
					SetVar key, RSSItem.Item(key)
				next
				SetVar "more", LNG_MORE
				
				Parse "templateRSSAlert", False

				alertTitle = RSSItem.Item("title")
				writeLineToRSSLog("processing item: "&alertTitle)
				alertHTML = PrintVar("templateRSSAlert")
				hash = RSSItem.Item("deskalerts_hash")
			    
			    if ConfIFRMode = 1 then
			    
			        mons = Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "De�")
	                'Tue, 22 Mar 2016 01:00:00 +0000
	               ' Response.Write RSSItem.Item("date") & " "
	                dateParts = Split(RSSItem.Item("date"), " ")
	                
	                pubDay = dateParts(1)
	                pubYear = dateParts(3)
	                pubTime = dateParts(4)
	                
	                pubTimeComponents = Split(pubTime, ":")

	                pubHour = pubTimeComponents(0)
	                pubMinute = pubTimeComponents(1)
	                pubSecond = pubTimeComponents(2)
	                
	                pubMonStr = dateParts(2)

	                
	                pubMon = 1
	                

	               
	               curMon = 1
	               For Each mon in mons
	 	               if mon = pubMonStr then
	                        pubMon = curMon
	                   end if       
	                   curMon = curMon + 1       
	               next
	                
	                 fullPubMon = ""
	                 
	                 if pubMin < 10 then
	                    fullPubMon = "0" & pubMon
	                 else
	                    fullPubMon = pubMon
	                 end if
	                '01/01/1970 00:00:00
	                
	                createdDate = pubDay & "/" & fullPubMon & "/" & pubYear & " " & pubTime

	                difference = DateDiff("d",  createdDate, now)

	                createdDate = "'" & createdDate & "'"

			    else
			        createdDate = "@now"
			    end if
			    
				isLifetime = "1"
				if not isStoredNews(hash,storedHashes) then
					writeLineToRSSLog("hash check: OK")
					if lifetime <> "" and lifetime <> "0" then
						if RS("schedule") <> "1" then
							from_date = "'"& now_db &"'"
							to_date = "'"& DateAdd("s", 1, DateAdd("n", lifetime, now_db)) &"'"
							schedule = "1"
							isLifetime = "1"
						else
							'TODO: calc max and min dates
							schedule = "schedule"
							from_date = "from_date"
							to_date = "to_date"
							isLifetime = "0"
						end if
					elseif RS("schedule") <> "1" and (RS("sms") = "1" or RS("email") = "1") then 'send sms and email by send_recurrence.asp
						schedule = "1"
						from_date = "'"& now_db &"'"
						to_date = "0"
						
					else
						schedule = "schedule"
						from_date = "from_date"
						to_date = "to_date"
					end if
					
					'add target="_blank" to all links
					'alertHTML = "<a href='http:\\google.com'>non_target</a><br>" & "<a target=""_self"" href='http:\\ya.ru'>non_blank</a><br>" & "<a target='_blank' href='http:\\yahoo.ru'>blank</a><br>" 
					
					' target="_blank" adding
					Set regex = CreateObject("VBScript.RegExp")
					regex.Global = true
					regex.Pattern = "<a .*?>"
					
					Set matches = regex.Execute(alertHTML)
					
					for k = 0 to matches.Count - 1
					    Set match = matches.Item(k)
					    value = match.Value
					    
					    newLink = Replace(value, """", "'") 'replace double quotes to single
					    if InStr(newLink, "target=") then
						    Set targetReplacer = CreateObject("VBScript.RegExp") 'change any target attr value to _blank if this attr already exists
						    targetReplacer.Pattern = "target='.*?'"
					        targetReplacer.Global = true
					        newLink= targetReplacer.Replace(newLink, "target='_blank'")
					    else
					        newLink = Replace(newLink, ">", " target='_blank' >")  'add target attribute with _blank value  				    
					    end if
					    
					    alertHTML = Replace(alertHTML, value, newLink)
					      
					next
					' end of target="_blank" adding
					captionValue = "caption_id"
					
					if ConfIFRMode = 1 then
					    if ticker = 0 then
					    
					        'get random skin
				    	    captionValue = getRandomSkinCaption	
				    	    	
				    	elseif RS("rss_allow_content") = "1" then
				    	    alertHTML = ""
				    	    moreLink = RSSItem.Item("url")
				    	    alertTitle = "<a target='_blank' href='" & moreLink &"'>" & alertTitle & "</a>"
				    	end if			    

					end if
					
					if (ConfIFRMode = 1 and clng(difference) < 1) or (ConfIFRMode = 0) then

					    insertRSSNewsSQL = "INSERT INTO alerts (title, alert_text, create_date, type, from_date, to_date, schedule, schedule_type, recurrence, urgent, email, desktop, sms, email_sender, ticker, fullscreen, alert_width,  alert_height, aknown,  autoclose,  toolbarmode, deskalertsmode, template_id, sender_id, caption_id, sent_date, class, type2, group_type, parent_id, resizable,lifetime) OUTPUT INSERTED.ID as newId " & vbCrLf &_
										    "	SELECT N'"&Replace(alertTitle,"'","''")&"', N'"&Replace(alertHTML,"'","''")&"', "&createdDate&", type, "& from_date &", "& to_date &", "& schedule &", schedule_type, recurrence, urgent, email, desktop, sms, email_sender, ticker, fullscreen, alert_width,  alert_height, aknown,  autoclose,  toolbarmode, deskalertsmode, template_id, sender_id, " & captionValue &",'" & Now() & "', 5, type2, group_type, "& alertId &", resizable, "& isLifetime &" FROM alerts WHERE id =" & alertId & " " & vbCrLf &_
										    "SET @id = SCOPE_IDENTITY() " & vbCrLf &_
										    "INSERT INTO alerts_sent (alert_id, user_id) SELECT @id, user_id FROM alerts_sent WHERE alert_id =" & alertId & " " & vbCrLf &_
										    "INSERT INTO alerts_sent_comp (alert_id, comp_id) SELECT @id, comp_id FROM alerts_sent_comp WHERE alert_id =" & alertId & " " & vbCrLf &_
										    "INSERT INTO alerts_sent_group (alert_id, group_id) SELECT @id, group_id FROM alerts_sent_group WHERE alert_id =" & alertId & " " & vbCrLf &_
										    "INSERT INTO alerts_sent_ou (alert_id, ou_id) SELECT @id, ou_id FROM alerts_sent_ou WHERE alert_id =" & alertId & " " & vbCrLf &_
										    "INSERT INTO alerts_sent_stat (alert_id, user_id, date) SELECT @id, user_id, @now FROM alerts_sent_stat WHERE alert_id =" & alertId & " " & vbCrLf &_
										    "INSERT INTO alerts_sent_iprange (alert_id, ip_from, ip_to) SELECT @id, ip_from, ip_to FROM alerts_sent_iprange WHERE alert_id =" & alertId & " "

                          insertRSSNewsSQL = "DECLARE @id BIGINT " & vbCrLf &_
						        "DECLARE @now DATETIME " & vbCrLf &_
						        "SET @now = GETDATE() " & vbCrLf &_
						        insertRSSNewsSQL

                            Set rssSet = Conn.Execute(insertRSSNewsSQL)
                            
                            newId = rssSet("newId")
                            MemCacheUpdate "Add", newId
                            
					end if
				end if
			next
			if insertRSSNewsSQL <> "" then
				updateAlertsSQL = "UPDATE alerts SET alert_text = N'"& URLToRSS &"|"& hashStr &"|"& params &"|"& rssTemplate &"' WHERE id = "& alertId
                Conn.Execute(updateAlertsSQL)
			end if
		end if
	end if
	
	RS.MoveNext
Loop

if insertAlertsSQL <> "" then
	insertAlertsSQL =	"DECLARE @id BIGINT " & vbCrLf &_
						"DECLARE @now DATETIME " & vbCrLf &_
						"SET @now = GETDATE() " & vbCrLf &_
						insertAlertsSQL
	writeLineToRSSLog("inserting messages")
	'Response.Write insertAlertsSQL
	'Conn.Execute(insertAlertsSQL)
  '  RebuildMemCache
    'MEMCACHE
	' Set cacheMgr = CreateObject("DeskAlerts.CacheManager")
     'cacheMgr.RebuildCache(Now())

end if 

RS.Close

Conn.Execute("UPDATE settings SET val = CONVERT(nvarchar, GETUTCDATE(), 9) WHERE name='ConfRSSReaderTaskTimeStamp'")

%>
<!-- #include file="admin/db_conn_close.asp" -->