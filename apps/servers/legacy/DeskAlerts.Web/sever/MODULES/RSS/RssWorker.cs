﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Server.Utils;
using DeskAlertsDotNet.Parameters;
using Resources;

namespace DeskAlertsDotNet.sever.MODULES.RSS
{
    public static class RssWorker
    {
        public static Dictionary<string, object> GetFeedData(string url)
        {
            var rssRequest = WebRequest.Create(url);
            rssRequest.Method = "GET";
            var response = (HttpWebResponse)rssRequest.GetResponse();
            var result = new Dictionary<string, object>();

            if (response.StatusCode != HttpStatusCode.OK)
            {
                result.Add("ERROR", resources.LNG_NOT_VALID_FEED);
            }
            else
            {
                var encoding = Encoding.UTF8;
                string[] descTags = { "summary", "description", "content" };
                using (var reader = new StreamReader(response.GetResponseStream(), encoding))
                {
                    var responseText = reader.ReadToEnd();
                    XmlDocument xmlDocument = new XmlDocument();
                    try
                    {
                        xmlDocument.LoadXml(responseText);
                    }
                    catch (XmlException ex)
                    {
                        Pages.DeskAlertsBasePage.Logger.Error(ex);

                        result.Add("ERROR", resources.LNG_NOT_VALID_FEED);
                        return result;
                    }

                    var list = xmlDocument.GetElementsByTagName("item");
                    var fullHash = string.Empty;
                    var feedData = new List<Dictionary<string, string>>();
                    foreach (XmlNode node in list)
                    {
                        var rssDictionary = new Dictionary<string, string>();
                        var rssDescription = string.Empty;
                        var rssTitle = string.Empty;
                        foreach (XmlElement xmlElement in node)
                        {
                            if (xmlElement.NodeType == XmlNodeType.Element)
                            {
                                if (rssDictionary.ContainsKey(xmlElement.Name))
                                {
                                    continue;
                                }
                                switch (xmlElement.Name)
                                {
                                    case "category":
                                    case "link":
                                        rssDictionary.Add(xmlElement.Name, xmlElement.InnerText);
                                        break;
                                    case "description":
                                        rssDescription = xmlElement.InnerText;
                                        rssDictionary.Add(xmlElement.Name, rssDescription);
                                        break;
                                    case "title":
                                        rssTitle = xmlElement.InnerText;
                                        rssDictionary.Add(xmlElement.Name, rssTitle);
                                        break;
                                    default:
                                        continue;
                                }
                            }
                        }

                        string feedHash = HashingUtils.MD5(rssTitle + rssDescription);
                        rssDictionary.Add("deskalerts_hash", feedHash);
                        fullHash += string.IsNullOrEmpty(fullHash) ? feedHash : "," + feedHash;
                        feedData.Add(rssDictionary);
                    }

                    result.Add("FEED_DATA", feedData);
                    result.Add("HASH", fullHash);
                }
            }

            return result;
        }

        public static string GenerateAlertBodyFromFeedItem(Dictionary<string, string> feeditem)
        {

            string templateHTML = File.ReadAllText(Config.Data.GetString("alertsFolder") + "\\admin\\templates\\template_rss_alert.html");


            if (feeditem.ContainsKey("description"))
                templateHTML = templateHTML.Replace("{description}", feeditem["description"]);

            if (feeditem.ContainsKey("link"))
                templateHTML = templateHTML.Replace("{url}", feeditem["link"]);

            templateHTML = templateHTML.Replace("{more}", resources.LNG_MORE);

            return templateHTML;
        }
    }
}