﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;

using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.ApplicationCore.Repositories;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Server.Utils.Managers;
using DeskAlertsDotNet.Application;
using DeskAlertsDotNet.Managers;
using DeskAlertsDotNet.Models;
using DeskAlertsDotNet.Pages;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Utils.Consts;
using DeskAlertsDotNet.Utils.Factories;
using DeskAlerts.Server.Utils.Interfactes;
using DeskAlerts.Server.Utils.Services;
using DeskAlertsDotNet.Utils.Services;
using Resources;

using DataRow = DeskAlertsDotNet.DataBase.DataRow;
using DataSet = DeskAlertsDotNet.DataBase.DataSet;

namespace DeskAlertsDotNet.sever.MODULES.RSS
{
    public partial class RssSender : DeskAlertsBasePage
    {
        private const string SelectIdFromPushSentByAlertId =
            "SELECT user_id AS id FROM push_sent AS ps WHERE alert_id = @alert_id AND ps.sent_date > DATEADD(d, 0, DATEDIFF(d, 0, GETDATE()))";

        private const string InsertPushSentByAlertIdAndUserId =
            "INSERT INTO push_sent (alert_id,user_id,sent_date, user_instance_id) VALUES  (@alert_id, @user_id, @date, @user_instance_id)";

        private const string SelectIdFromPushSentByAlertIdAndUserId =
            "SELECT ps.id FROM push_sent AS ps JOIN user_instances ins on ins.id = ps.user_instance_id WHERE ps.alert_id = @alertId AND ins.user_id = @userId AND ps.sent_date > DATEADD(d, 0, DATEDIFF(d, 0, GETDATE()))";

        private const string SelectRssAlerts =
            @"SELECT *
            FROM alerts 
            WHERE class = 4 
                AND schedule_type = '1'
                AND [type] = 'S'";

        private readonly CacheManager _cacheManager = Global.CacheManager;

        private IUserInstancesRepository _userInstancesRepository;
        private IUserService _userService;
        private IConfigurationManager _configurationManager;
        private IGroupRepository _groupRepository;
        private IUserRepository _userRepository;
        private IContentReceivedService _contentReceivedService;
        private ICampaignRepository _campaignRepository;

        private Campaign _campaign;
        private IAlertRepository _alertRepository;

        protected override void OnLoad(EventArgs e)
        {
            _configurationManager = new WebConfigConfigurationManager(Config.Configuration);
            _campaignRepository = new PpCampaignRepository(_configurationManager);
            _userInstancesRepository = new PpUserInstancesRepository(_configurationManager);
            _userRepository = new PpUserRepository(_configurationManager);
            _groupRepository = new PpGroupRepository(_configurationManager);
            _userService = new UserService(_userRepository, _groupRepository);
            _contentReceivedService = new ContentReceivedService();
            _alertRepository = new PpAlertRepository(_configurationManager);

            base.OnLoad(e);

            Server.ScriptTimeout = 10000000;
            var rssAlerts = dbMgr.GetDataByQuery(SelectRssAlerts);
            TrySendRss(rssAlerts);
        }

        public void TrySendRss(DataSet rssAlerts)
        {
            foreach (var rssAlert in rssAlerts)
            {
                var rssAlertText = rssAlert.GetString("alert_text");
                var feedSetttings = rssAlertText.Split('|');
                var senderId = rssAlert.GetInt("sender_id");
                var link = feedSetttings[0];
                var template = feedSetttings[3];
                var hashes = feedSetttings[1].Split(',').ToList();
                var rssStartDate = rssAlert.GetDateTime("from_date");
                var rssEndDate = rssAlert.GetDateTime("to_date");
                var schedule = rssAlert.GetInt("schedule");
                var campaignId = rssAlert.GetString("campaign_id");
                var type2 = rssAlert.GetString("type2");
                var isTicker = rssAlert.GetString("ticker") == "1";

                string sendType;
                switch (type2)
                {
                    case "B":
                        sendType = "broadcast";
                        break;
                    case "R":
                        sendType = "recipients";
                        break;
                    case "I":
                        sendType = "iprange";
                        break;
                    default:
                        throw new Exception("Unexpected sendType in TrySendRss method at RssSender page.");
                }

                if (campaignId != "-1")
                {
                    _campaign = _campaignRepository.GetById(long.Parse(campaignId));
                }

                if ((rssStartDate > DateTime.Now || rssEndDate < DateTime.Now) && (schedule == 1))
                {
                    continue;
                }

                if (IsActiveCampaign())
                {
                    object rawFeedData;

                    try
                    {
                        rawFeedData = RssWorker.GetFeedData(link)["FEED_DATA"];
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }

                    if (rawFeedData is List<Dictionary<string, string>> feedData)
                    {
                        foreach (var feedDictionary in feedData)
                        {
                            var hash = feedDictionary["deskalerts_hash"];

                            if (hashes.Contains(hash))
                            {
                                continue;
                            }

                            hashes.Add(hash);

                            var alertText = feedDictionary.Aggregate(template,
                                (current, pair) => current.Replace("{" + pair.Key + "}", pair.Value));

                            var title = feedDictionary["title"];

                            alertText = alertText.Replace("{more}", resources.LNG_MORE);

                            if (feedDictionary.ContainsKey("link"))
                            {
                                alertText = alertText.Replace("{url}", feedDictionary["link"]);
                            }

                            alertText += $"<!-- html_title = '{title}' -->";
                            if (isTicker)
                            {
                                var tickerSpeed = rssAlert.GetInt("ticker_speed");
                                alertText += $"<!-- tickerSpeed='{tickerSpeed}' -->";
                            }

                            var alertStartDate = DateTime.Now;
                            var diffTime = rssEndDate - rssStartDate;
                            var alertEndDate = DateTime.Now.AddMilliseconds(diffTime.TotalMilliseconds);

                            const string captionValue = "NULL";

                            dbMgr.ExecuteQuery("SET LANGUAGE BRITISH");

                            var query =
                                @"INSERT INTO alerts (
                            title, 
                            alert_text, 
                            create_date, 
                            type, 
                            from_date, 
                            to_date, 
                            schedule, schedule_type, recurrence, urgent, email, desktop, sms, text_to_call, email_sender, ticker, ticker_speed, fullscreen, alert_width,  alert_height, aknown,  autoclose,  toolbarmode, deskalertsmode, template_id, sender_id, caption_id, sent_date, class, type2, group_type, parent_id, resizable,lifetime,campaign_id, approve_status) "
                                + " OUTPUT INSERTED.id SELECT N'" + title.EscapeQuotes() + "', N'"
                                + alertText.EscapeQuotes() + "', "
                                + DateTime.Now.ToString(DateTimeFormat.JavaScript, CultureInfo.InvariantCulture).Quotes() +
                                ", type, "
                                + alertStartDate.ToString(DateTimeFormat.JavaScript, CultureInfo.InvariantCulture).Quotes() + ", "
                                + alertEndDate.ToString(DateTimeFormat.JavaScript, CultureInfo.InvariantCulture).Quotes() + ", schedule "
                                + ", schedule_type, recurrence, urgent, email, desktop, sms, text_to_call, email_sender, ticker, ticker_speed, fullscreen, alert_width,  alert_height, aknown,  autoclose,  toolbarmode, deskalertsmode, template_id, sender_id, "
                                + captionValue + "," + DateTime.Now.ToString(DateTimeFormat.JavaScript, CultureInfo.InvariantCulture).Quotes()
                                + ", 5, type2, group_type, " + rssAlert.GetString("id") + ", resizable, lifetime,campaign_id, 1"
                                + " FROM alerts WHERE id =" + rssAlert.GetString("id");

                            var feedId = rssAlert.GetInt("id");
                            var alertId = dbMgr.GetDataByQuery(query).First().GetInt("id");

                            var addRecipientsQuery = "INSERT INTO alerts_sent (alert_id, user_id) SELECT  " + alertId +
                                                     ", user_id FROM alerts_sent WHERE alert_id =" + feedId + " " +
                                                     "INSERT INTO alerts_sent_comp (alert_id, comp_id) SELECT  " +
                                                     alertId +
                                                     ", comp_id FROM alerts_sent_comp WHERE alert_id =" + feedId + " " +
                                                     "INSERT INTO alerts_sent_group (alert_id, group_id) SELECT  " +
                                                     alertId +
                                                     ", group_id FROM alerts_sent_group WHERE alert_id =" + feedId +
                                                     " " +
                                                     "INSERT INTO alerts_sent_ou (alert_id, ou_id) SELECT  " + alertId +
                                                     ", ou_id FROM alerts_sent_ou WHERE alert_id =" + feedId + " " +
                                                     "INSERT INTO alerts_sent_stat (alert_id, user_id, date) SELECT  " +
                                                     alertId +
                                                     ", user_id, GETDATE() FROM alerts_sent_stat WHERE alert_id =" +
                                                     feedId + " " +
                                                     "INSERT INTO alerts_sent_iprange (alert_id, ip_from, ip_to) SELECT  " +
                                                     alertId +
                                                     ", ip_from, ip_to FROM alerts_sent_iprange WHERE alert_id =" +
                                                     feedId + " ";

                            dbMgr.ExecuteQuery(addRecipientsQuery);

                            if (sendType == "iprange")
                            {
                                var alertIdParameter = SqlParameterFactory.Create(
                                    DbType.Int32,
                                    alertId,
                                    "alertId");

                                var ipGroups = dbMgr.GetDataByQuery(@"
                                    SELECT alerts_sent_iprange.ip_from, alerts_sent_iprange.ip_to 
                                    FROM alerts_sent_iprange 
                                    WHERE alert_id = @alertId",
                                    alertIdParameter
                                );

                                _contentReceivedService.CreateRowsInAlertsReceivedForIp(ipGroups, alertId);
                            }
                            else
                            {
                                var alertIdParameter = SqlParameterFactory.Create(
                                    DbType.Int32,
                                    alertId,
                                    "alertId");

                                var recipientInstanceIds = dbMgr.GetDataByQuery(@"
                                    SELECT user_instances.id 
                                    FROM (
                                        SELECT u.id 
                                        FROM users AS u 
                                        INNER JOIN alerts_sent AS a ON u.id = a.user_id WHERE u.role = 'U' AND a.alert_id = @alertId) as ui 
                                    JOIN user_instances ON ui.id = user_instances.user_id",
                                    alertIdParameter
                                ).ToList();

                                var computersAlertIdParameter = SqlParameterFactory.Create(
                                    DbType.Int32,
                                    alertId,
                                    "alertId");

                                var computersInstanceIds = dbMgr.GetDataByQuery(
                                    " SELECT user_instances.id " +
                                    " FROM " +
                                    " (SELECT user_instance_computers.user_instance_guid " +
                                    " FROM(SELECT c.id FROM computers AS c INNER JOIN alerts_sent_comp AS cs ON c.id = cs.comp_id WHERE cs.alert_id = 33915) as ci " +
                                    " JOIN user_instance_computers ON ci.id = user_instance_computers.user_instance_computer_id) as uig " +
                                    " JOIN user_instances ON uig.user_instance_guid = user_instances.clsid ",
                                    computersAlertIdParameter).ToList();

                                var allRecipientsInstanceIds = recipientInstanceIds.Concat(computersInstanceIds);
                                var recipientIds = allRecipientsInstanceIds.Select(dataRow => long.Parse(dataRow.GetString("id")));

                                _contentReceivedService.CreateRowsInAlertsReceived(sendType, alertId, recipientIds, null);
                            }

                            _cacheManager.Add(alertId, DateTime.Now.ToString(DateTimeFormat.MsSqlFormat, CultureInfo.InvariantCulture));

                            SendPushNotifications();

                            var alertSet = dbMgr.GetDataByQuery("SELECT * FROM alerts WHERE id = " + alertId);
                            var isPublisher =
                                dbMgr.GetScalarByQuery<string>("SELECT role FROM users u WHERE u.id =" + senderId) == "E";

                            if (isPublisher)
                            {
                                var policyId = dbMgr.GetScalarByQuery<int>(
                                    "SELECT policy_id FROM [policy_editor] pe WHERE pe.[editor_id] =" + senderId);
                                var users = dbMgr.GetDataByQuery(
                                    "SELECT * FROM [policy_user] p WHERE p.[policy_id] =" + policyId);
                                var isBroadcastEnabled = users.Count == 0;

                                if (!isBroadcastEnabled && sendType == "broadcast")
                                {
                                    sendType = "recipients";
                                }
                            }

                            var isTextToCallAlert = alertSet.GetInt(0, "text_to_call") == 1;
                            var isTextToCallModuleEnabled = Config.Data.HasModule(Modules.TEXTTOCALL);

                            if (isTextToCallModuleEnabled && isTextToCallAlert)
                            {
                                AddRecipients.SendTextToCall(alertText, sendType, alertId, dbMgr, curUser);
                            }
                        }
                    }

                    dbMgr.ExecuteQuery(
                        $"UPDATE alerts SET alert_text = '{link}|{string.Join(",", hashes.ToArray())}||{template}' WHERE id = {rssAlert.GetString("id")}");
                }
            }
        }

        private bool IsActiveCampaign()
        {
            return _campaign == null || (_campaign != null && _campaign.Status == CampaignStatus.Active);
        }

        #region SendPushNotifications

        private void SendPushNotifications()
        {
            var pushAlerts = _cacheManager.GetPushAlertsForRSS();
            SendPush(pushAlerts);
        }

        private void SendPush(IEnumerable<UserAlertsDTO> scheduledPushAlerts)
        {
            foreach (var userAlerts in scheduledPushAlerts)
            {
                if (userAlerts.Alerts.Any())
                {
                    if (userAlerts.UserName == "broadcast")
                    {
                        SendBroadcastPush(userAlerts);
                    }
                    else
                    {
                        SendToUserPush(userAlerts);
                    }
                }
            }
        }

        private void SendBroadcastPush(UserAlertsDTO userAlerts)
        {
            foreach (var alert in userAlerts.Alerts)
            {
                var usersIdToSend = GetUsersIdToPushSend(alert.Id).ToList().Select(x => (long)x.GetInt("id"));
                PushNotificationsManager.Default.AddPushNotificationToDb(alert.Id, usersIdToSend);
            }
        }

        private void SendToUserPush(UserAlertsDTO userAlerts)
        {
            //todo 
            var userId = Convert.ToInt32(_userService.GetUserByName(userAlerts.UserName).Id);
            foreach (var alert in userAlerts.Alerts)
            {
                var user = UserManager.Default.GetUserById(userId);
                var recievedUsers = GetRecievedPushUsers(alert.Id, user.Id);
                var alertEntity = _alertRepository.GetById(alert.Id);
                //PushNotificationsManager.Default.AddPushNotificationToDb(alert.Id, recievedUsers);
                if (!recievedUsers.IsEmpty)
                {
                    var dataRow = recievedUsers.GetRow(0);
                    var pushSentId = dataRow.GetInt("id");
                    if (pushSentId <= 0)
                    {
                        var userDevices = _userInstancesRepository.GetInstances(userId);

                        foreach (var userDevice in userDevices)
                        {
                            SendPush(alertEntity, userDevice);
                            WriteToPushSend(user.Id, alert.Id, userDevice.Clsid);
                        }
                    }
                }
                else
                {
                    var userDevices = _userInstancesRepository.GetInstances(userId);

                    foreach (var userDevice in userDevices)
                    {
                        SendPush(alertEntity, userDevice);
                        WriteToPushSend(user.Id, alert.Id, userDevice.Clsid);
                    }
                }
            }
        }

        private void SendPush(AlertEntity alert, UserInstances instance)
        {
            try
            {
                //todo add AddPushNotificationToDb method
                PushNotificationsManager.Default.SendPushNotification(alert, instance);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        private IEnumerable<DataRow> GetUsersIdToPushSend(int alertId)
        {
            var alertIdParameter = SqlParameterFactory.Create(DbType.Int32, alertId, "alert_id");
            var sendedPushUserId = dbMgr.GetDataByQuery(SelectIdFromPushSentByAlertId, alertIdParameter).ToList();
            var allUsersId = UserManager.Default.GetUsersId().ToList();
            var usersIdToSend = allUsersId.Where(x => sendedPushUserId.All(y => x.GetInt("id") != y.GetInt("id")));
            return usersIdToSend;
        }

        private void WriteToPushSend(int userId, int alertid, Guid userInstanceId)
        {
            var userIdParameter = SqlParameterFactory.Create(DbType.Int32, userId, "user_id");
            var alertIdParameter = SqlParameterFactory.Create(DbType.Int32, alertid, "alert_id");
            var dateParameter = SqlParameterFactory.Create(DbType.DateTime, DateTime.Now, "date");
            var userInstanceIdParameter = SqlParameterFactory.Create(DbType.Guid, userInstanceId, "user_instance_id");
            dbMgr.ExecuteQuery(InsertPushSentByAlertIdAndUserId, userIdParameter, alertIdParameter, dateParameter, userInstanceIdParameter);
        }

        private DataSet GetRecievedPushUsers(int alertId, int userid)
        {
            var alertIdParam = SqlParameterFactory.Create(DbType.Int32, userid, "userId");
            var userIdParam = SqlParameterFactory.Create(DbType.Int32, alertId, "alertId");
            var dataSet = dbMgr.GetDataByQuery(SelectIdFromPushSentByAlertIdAndUserId, alertIdParam, userIdParam);
            return dataSet;
        }

        #endregion
    }
}