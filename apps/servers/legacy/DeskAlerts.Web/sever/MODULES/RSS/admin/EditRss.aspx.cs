﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Services;
using System.Web.UI.WebControls;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Extensions.Enums;
using DeskAlerts.Server.Utils;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Pages;
using DeskAlertsDotNet.Parameters;
using Newtonsoft.Json;
using Resources;

namespace DeskAlertsDotNet.sever.MODULES.RSS.admin
{
    public partial class EditRss : DeskAlertsBasePage
    {
        //use curUser to access to current user of DA control panel      
        //use dbMgr to access to database

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!IsPostBack)
            {

                lifetimeFactor.Items.Add(new ListItem(resources.LNG_MINUTES, "1"));
                lifetimeFactor.Items.Add(new ListItem(resources.LNG_HOURS, "60"));
                lifetimeFactor.Items.Add(new ListItem(resources.LNG_DAYS, "1440"));

                lifetimeFactor.Items[2].Selected = true;

                startDateAndTime.Value = DateTimeConverter.ToUiDateTime(DateTime.Now);
                endDateAndTime.Value = DateTimeConverter.ToUiDateTime(DateTime.Now.AddMinutes(10));
            }

            LifetimeAdditionalDescription.InnerText = resources.LNG_RSS_TIME_EXPIRED_CANT_RECEIVED;

            if (!string.IsNullOrEmpty(Request["alert_campaign"]))
            {
                nonCampaignDiv.Visible = false;

                string campaignName =
                    dbMgr.GetScalarQuery<string>("SELECT name FROM campaigns WHERE id = " + Request["alert_campaign"]);

                campaignNameLabel.InnerText = campaignName;
                campaignSelect.Value = Request["alert_campaign"];
            }
            else
            {
                campaignDiv.Visible = false;
            }

            sms.Visible = Config.Data.HasModule(Modules.SMS) && !Config.Data.IsDemo;
            smsDemo.Visible = Config.Data.HasModule(Modules.SMS) && Config.Data.IsDemo;

            textToCall.Visible = Config.Data.HasModule(Modules.TEXTTOCALL);

            if (!string.IsNullOrEmpty(Request["id"]))
                LoadRss(Request["id"]);
            else {
                int lifeTime = Convert.ToInt32(Settings.Content["ConfRSSMessageExpire"]);
                alertWidth.Value = Settings.Content["ConfAlertWidth"];
                alertHeight.Value = Settings.Content["ConfAlertHeight"];
                foreach (ListItem item in lifetimeFactor.Items)
                {
                    item.Selected = false;
                }
                
                for (int i = 2; i >= 0; i--)
                {
                    ListItem listItem = lifetimeFactor.Items[i];
                    int itemValue = Convert.ToInt32(listItem.Value);

                    int lifeTimeValue = lifeTime / itemValue;
                  
                    if (lifeTimeValue % itemValue == 0)
                    {
                        listItem.Selected = true;
                        
                        lifetime.Value = lifeTimeValue.ToString();
                        lifetimemode.Checked = true;
                        break;
                    }
                }
            }
        }

        void LoadRss(string id)
        {
            DataRow rssRow = dbMgr.GetDataByQuery("SELECT * FROM alerts WHERE id = " + id).First();


            string link = rssRow.GetString("alert_text").Split('|')[0];
            string title = rssRow.GetString("title");

            string startDateStr = rssRow.GetString("from_date");
            string endDateStr = rssRow.GetString("to_date");

            DateTime startDate = rssRow.GetDateTime("from_date");
            DateTime endDate = rssRow.GetDateTime("to_date");
            int autoclose = rssRow.GetInt("autoclose");

            alertId.Value = rssRow.GetString("id");
            
            rssFeed.Value = link;
            this.title.Value = title;


            if (startDate.Year != 1900 && rssRow.GetInt("schedule") == 1)
            {
                startAndEndDateAndTimeCheckbox.Checked = true;
                startDateAndTime.Value = startDateStr;
            }

            if (endDate.Year != 1900 && rssRow.GetInt("schedule") == 1)
            {
                startAndEndDateAndTimeCheckbox.Checked = true;
                endDateAndTime.Value = endDateStr;
            }


            if (autoclose != 0)
            {
                autocloseCheckbox.Checked = true;
                this.autoclose.Disabled = false;
                this.autoclose.Value = Math.Abs(autoclose/60).ToString();

                if (autoclose < 0)
                {
                    manualCloseCheckBox.Checked = true;
                }

                manualCloseCheckBox.Disabled = false;
            }


            int fullscreen = rssRow.GetInt("fullscreen");

            if (fullscreen == 1)
            {
                size2.Checked = true;
                size1.Checked = false;

                alertWidth.Value = "500";
                alertHeight.Value = "400";
            }
            else
            {
                size1.Checked = true;
                size2.Checked = false;

                alertWidth.Value = rssRow.GetString("alert_width");
                alertHeight.Value = rssRow.GetString("alert_height");

            }

            if (rssRow.GetInt("ticker") == 1)
            {
                ticker1.Checked = false;
                ticker2.Checked = true;
                size2.Checked = true;
                size1.Checked = false;
                alertWidth.Value = "500";
                alertHeight.Value = "400";
            }
            else
            {
                ticker1.Checked = true;
                ticker2.Checked = false;
            }

            if (rssRow.GetInt("desktop") == 1)
            {
                desktopCheck.Checked = true;
            }

            if (rssRow.GetInt("sms") == 1)
            {
                smsCheckbox.Checked = true;
            }

            if (rssRow.GetInt("text_to_call") == 1)
            {
                textToCallCheckbox.Checked = true;
            }

            if (rssRow.GetInt("email") == 1)
            {
                emailCheck.Checked = true;
            }

            if (rssRow.GetInt("lifetime") == 1)
            {

                int lifeTime = (int)(endDate - startDate).TotalMinutes;

                foreach (ListItem item in lifetimeFactor.Items)
                {
                    item.Selected = false;
                }
                for (int i = 2; i >= 0; i--)
                {
                    ListItem listItem = lifetimeFactor.Items[i];
                    int itemValue = Convert.ToInt32(listItem.Value);

                    int lifeTimeValue = lifeTime / itemValue;
                    if (lifeTimeValue % itemValue == 0)
                    {
                        listItem.Selected = true;
                        lifetimemode.Checked = true;
                        lifetime.Value = lifeTimeValue.ToString();
                        break;
                    }
                }
            }


        }


        [WebMethod]
        public static string GetFeedData(string url)
        {
            try
            {
                List<Dictionary<string, string>> data = RssWorker.GetFeedData(url)["FEED_DATA"] as List<Dictionary<string, string>>;


                if(data == null || data.Count == 0)
                    return "";

                var firstItem = data[0];

                string alertHtml = RssWorker.GenerateAlertBodyFromFeedItem(firstItem);

                firstItem.Add("body", alertHtml);
                return JsonConvert.SerializeObject(firstItem);
            }
            catch (UriFormatException ex)
            {
                Logger.Debug(ex);
                return "";
            }
        }


        [WebMethod]
        public static bool ValidateFeed(string url)
        {
            try
            {
                var data = RssWorker.GetFeedData(url);

                return !data.ContainsKey("ERROR");
            }
            catch (Exception ex)
            {
                Logger.Debug(ex);
                return false;
            }
        }
    }
}