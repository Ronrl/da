﻿using System;
using System.Web.UI.WebControls;
using Castle.MicroKernel.Lifestyle;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Models.Permission;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlerts.Server.Utils.Interfaces.Services;
using DeskAlertsDotNet.Application;
using Resources;

namespace DeskAlertsDotNet.Pages
{
    public partial class RssList : DeskAlertsBaseListPage
    {
        private bool shouldDisplayApprove = false;
        protected string pageTitle;

        private readonly ICacheInvalidationService _cacheInvalidationService;

        public RssList()
        {
            using (Global.Container.BeginScope())
            {
                _cacheInvalidationService = Global.Container.Resolve<ICacheInvalidationService>();
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (!IsPostBack)
            {
                searchTermBox.Value = Request["searchTermBox"];
            }

            searchButton.Text = searchButton.Text.Replace("{Search}", resources.LNG_SEARCH);
            headerTitle.Text = resources.LNG_RSS;
            titleCell.Text = GetSortLink(resources.LNG_TITLE, "title", Request["sortBy"]);
            creationDateCell.Text = GetSortLink(resources.LNG_CREATION_DATE, "create_date", Request["sortBy"]);
            scheduledCell.Text = resources.LNG_SCHEDULED;

            base.Table = contentTable;
            pageTitle = !string.IsNullOrEmpty(Request["draft"]) && Request["draft"].Equals("1") ? resources.LNG_DRAFT : resources.LNG_CURRENT;

            addButton.Visible = string.IsNullOrEmpty(Request["draft"]) && string.IsNullOrEmpty(Request["sent"]);

            senderCell.Text = resources.LNG_SENDER;
            actionsCell.Text = resources.LNG_ACTIONS;

            headerSelectAll.Visible = curUser.HasPrivilege || curUser.Policy[Policies.RSS].CanDelete;
            selectAll.Visible = curUser.HasPrivilege || curUser.Policy[Policies.RSS].CanDelete;
            bottomDelete.Visible = curUser.HasPrivilege || curUser.Policy[Policies.RSS].CanDelete;
            upDelete.Visible = curUser.HasPrivilege || curUser.Policy[Policies.RSS].CanDelete;

            scheduledCell.Visible = string.IsNullOrEmpty(Request["sent"]) || !Request["sent"].Equals("1");

            if (!curUser.HasPrivilege && !curUser.Policy[Policies.RSS].CanCreate)
                addButton.Visible = false;

            string getUrlSearchTermBox = Request.QueryString["searchTermBox"] ?? string.Empty;

            bool isSearchTermBoxValueChanged = searchTermBox.Value != getUrlSearchTermBox;
            if (isSearchTermBoxValueChanged)
            {
                Offset = 0;
            }
            int cnt = this.Content.Count < (Offset + Limit) ? this.Content.Count : Offset + Limit;

            for (int i = Offset; i < cnt; i++)
            {
                DataRow row = this.Content.GetRow(i);
                int reccurence = row.GetInt("recurrence");
                int schedule = row.GetInt("schedule");
                var toDate = row.GetDateTime("to_date");
                var fromDate = row.GetDateTime("from_date");
                bool hasLifetime = row.GetInt("lifetime") == 1;

               bool isScheduled = !hasLifetime && (toDate - fromDate).TotalSeconds % 60 == 0 && toDate.Year > 1900;

                TableRow tableRow = new TableRow();
                if (curUser.HasPrivilege || curUser.Policy[Policies.RSS].CanDelete)
                {
                    CheckBox chb = GetCheckBoxForDelete(row);
                    TableCell checkBoxCell = new TableCell();
                    checkBoxCell.HorizontalAlign = HorizontalAlign.Center;
                    checkBoxCell.Controls.Add(chb);
                    tableRow.Cells.Add(checkBoxCell);
                }

                if (string.IsNullOrEmpty(Request["id"]))
                {
                    TableCell titleContentCell = new TableCell();
                    titleContentCell.Text = string.Format("<a href='RssList.aspx?sent=1&id={0}'>{1}</a>",
                        row.GetString("id"), row.GetString("title"));
                    tableRow.Cells.Add(titleContentCell);
                }
                else
                {
                    TableCell titleContentCell = new TableCell();
                    titleContentCell.Text = row.GetString("title");
                    tableRow.Cells.Add(titleContentCell);
                }

                var createDateContentCell = new TableCell
                {
                    Text = DateTimeConverter.ToUiDateTime(row.GetDateTime("create_date")),
                    HorizontalAlign = HorizontalAlign.Center
                };

                tableRow.Cells.Add(createDateContentCell);

                if (string.IsNullOrEmpty(Request["sent"]) || !Request["sent"].Equals("1"))
                {
                    TableCell scheduleCell = new TableCell();
                    if (reccurence == 1 || isScheduled)
                    {
                        scheduleCell.Text =
                            string.Format(
                                "<a href=\"#\" onclick=\"openDialogUI('form', 'frame', 'rescheduler.aspx?id=" +
                                row.GetString("id") + "',600,550, '');\">{0}</a>",
                                resources.LNG_YES);
                    }
                    else
                    {
                        scheduleCell.Text = resources.LNG_NO;
                    }

                    scheduleCell.HorizontalAlign = HorizontalAlign.Center;
                    tableRow.Cells.Add(scheduleCell);
                }

                var senderContentCell = new TableCell
                {
                    HorizontalAlign = HorizontalAlign.Center,
                    Text = string.IsNullOrEmpty(row.GetString("name")) ? resources.SENDER_WAS_DELETED : row.GetString("name")
                };

                tableRow.Cells.Add(senderContentCell);

                LinkButton duplicateButton = GetActionButton(row.GetString("id"), "images/action_icons/duplicate.png", "LNG_RESEND_RSS",
                    delegate
                    {
                        Response.Redirect("EditRss.aspx?id=" + row.GetString("id"), true);
                    }
                );

                int scheduleType = row.GetInt("schedule_type");
                LinkButton changeScheduleType = GetActionButton(row.GetString("id"), scheduleType == 1 ? "images/action_icons/stop.png" : "images/action_icons/start.png", scheduleType == 1 ? "LNG_STOP_SCHEDULED_ALERT" : "LNG_START_SCHEDULED_ALERT",
                    delegate
                    {
                        string parameters = "";
                        if (!string.IsNullOrEmpty(Request["id"]))
                            parameters = "?id=" + Request["id"];

                        Response.Redirect("ChangeScheduleType.aspx?id=" + row.GetString("id") + "&type=" + Math.Abs(scheduleType - 1) + "&return_page=RssList.aspx" + parameters, true);
                    }

                );

                LinkButton previewButton = GetActionButton(row.GetString("id"), "images/action_icons/preview.png", "LNG_PREVIEW", "showAlertPreview(" + row.GetString("id") + ")");
                TableCell actionsContentCell = new TableCell();

                actionsContentCell.HorizontalAlign = HorizontalAlign.Center;
                if (string.IsNullOrEmpty(Request["id"]) && (curUser.HasPrivilege || curUser.Policy[Policies.RSS].CanEdit))
                    actionsContentCell.Controls.Add(duplicateButton);
                else
                {
                    actionsContentCell.Controls.Add(previewButton);
                }

                if (curUser.HasPrivilege || curUser.Policy[Policies.RSS].CanStop)
                    actionsContentCell.Controls.Add(changeScheduleType);
                //actionsContentCell.Controls.Add(previewButton);
                //actionsContentCell.Controls.Add(directLinkButton);
                //actionsContentCell.Controls.Add(detailsButton);

                //if (isScheduled)
                //    actionsContentCell.Controls.Add(rescheduleButton);

                tableRow.Cells.Add(actionsContentCell);

                contentTable.Rows.Add(tableRow);
            }
        }

        #region DataProperties

        protected override string DataTable => "alerts";

        protected override string[] Collumns => new string[] { };

        protected override string PageParams
        {
            get
            {
                if (!string.IsNullOrEmpty(Request["id"]))
                    return "id=" + Request["id"] + "&";

                return "";
            }
        }

        protected override string DefaultOrderCollumn => "id";

        protected override void OnDeleteRows(string[] ids)
        {
            if (!string.IsNullOrEmpty(Request["id"]))
                return;

            foreach (var id in ids)
            {
                _cacheInvalidationService.CacheInvalidation(AlertType.RssAlert, Convert.ToInt64(id));
            }

            string idString = string.Join(",", ids);
            dbMgr.GetDataByQuery("DELETE FROM alerts WHERE parent_id IN (" + idString + ")");
        }

        protected override string CustomQuery
        {
            get
            {
                string viewListStr = "";

                int classId = !string.IsNullOrEmpty(Request["id"]) /*&& Request["sent"].Equals("1")*/ ? 5 : 4;
                string alertType = !string.IsNullOrEmpty(Request["draft"]) && Request["draft"].Equals("1") ? "D" : "S";

                if (!curUser.HasPrivilege && !curUser.Policy.CanViewAll)
                    viewListStr = string.Join(",", curUser.Policy.SendersViewList.ToArray());

                string query = "";
                if (!curUser.HasPrivilege && !curUser.Policy.CanViewAll)
                {
                    query = "SELECT a.id, a.lifetime, a.approve_status, a.schedule, schedule_type, recurrence, alert_text, title, a.type, sent_date, a.create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp, u.name FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id LEFT JOIN users u on u.id = a.sender_id WHERE video = 0 AND param_temp_id IS NULL AND class = " + classId + " AND a.type = '" + alertType + "' AND a.sender_id IN (" + viewListStr + ") ";

                    if (!string.IsNullOrEmpty(Request["id"]))
                        query += " AND parent_id = " + Request["id"];
                }
                else
                {
                    query = "SELECT a.id, a.lifetime, a.approve_status, a.schedule, schedule_type, recurrence, alert_text, title, a.type, sent_date, a.create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp, u.name FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id LEFT JOIN users u on u.id = a.sender_id WHERE video = 0 AND param_temp_id IS NULL AND class = " + classId + " AND a.type = '" + alertType + "' ";

                    if (!string.IsNullOrEmpty(Request["id"]))
                        query += " AND parent_id = " + Request["id"];
                }

                if (IsPostBack && SearchTerm.Length > 0)
                    query += " AND title LIKE '%" + SearchTerm + "%'";

                if (!String.IsNullOrEmpty(Request["sortBy"]))
                    query += " ORDER BY " + Request["sortBy"];
                else
                {
                    query += " ORDER BY " + DefaultOrderCollumn + " DESC ";
                }

                return query;
            }
        }

        protected override string CustomCountQuery
        {
            get
            {
                string viewListStr = "";

                if (!curUser.HasPrivilege && !curUser.Policy.CanViewAll)
                    viewListStr = string.Join(",", curUser.Policy.SendersViewList.ToArray());

                int classId = !string.IsNullOrEmpty(Request["id"]) /*&& Request["sent"].Equals("1")*/ ? 5 : 4;
                string alertType = !string.IsNullOrEmpty(Request["draft"]) && Request["draft"].Equals("1") ? "D" : "S";

                string query = "";
                if (!curUser.HasPrivilege && !curUser.Policy.CanViewAll)
                {
                    query = "SELECT count(1) FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id LEFT JOIN users u on u.id = a.sender_id WHERE video = 0 AND param_temp_id IS NULL AND class = " + classId + " AND a.type = '" + alertType + "' AND a.sender_id IN (" + viewListStr + ") ";

                    if (!string.IsNullOrEmpty(Request["id"]))
                        query += " AND parent_id = " + Request["id"];
                }
                else
                {
                    query = "SELECT  count(1) FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id LEFT JOIN users u on u.id = a.sender_id WHERE video = 0 AND param_temp_id IS NULL AND class = " + classId + " AND a.type = '" + alertType + "' ";

                    if (!string.IsNullOrEmpty(Request["id"]))
                        query += " AND parent_id = " + Request["id"];
                }

                if (IsPostBack && SearchTerm.Length > 0)
                    query += " AND title LIKE '%" + SearchTerm + "%'";

                return query;
            }
        }

        /*
         *
         * If you want to add condition for data selection override property ConditionSqlString
         * Also you can implement your custom query by overriding properties CustomQuery and CustomCountQuery
         *
         */

        #endregion DataProperties

        #region Interface properties

        protected override IButtonControl[] DeleteButtons
        {
            get
            {
                return new IButtonControl[] { upDelete, bottomDelete };
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl NoRowsDiv
        {
            get { return NoRows; }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TableDiv
        {
            get { return mainTableDiv; }
        }

        protected override string ContentName
        {
            get
            {
                //return content name here
                return resources.LNG_OBJECTS;
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl BottomPaging
        {
            get
            {
                return bottomRanging;
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TopPaging
        {
            get
            {
                return topPaging;
            }
        }

        #endregion Interface properties
    }
}