﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditRss.aspx.cs" Inherits="DeskAlertsDotNet.sever.MODULES.RSS.admin.EditRss" EnableEventValidation="false" %>
<%@ Import Namespace="DeskAlerts.Server.Utils.Helpers" %>
<%@ Import Namespace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="css/style9.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript" src="jscripts/jquery.jqplot.min.js"></script>
    <script type="text/javascript" src="jscripts/jqplot.highlighter.min.js"></script>
    <script type="text/javascript" src="jscripts/CreateAlert.js"></script>
    <script type="text/javascript" src="jscripts/jqplot.pieRenderer.min.js"></script>
    <script type="text/javascript" src="jscripts/jqplot.barRenderer.min.js"></script>
    <script type="text/javascript" src="jscripts/jqplot.categoryAxisRenderer.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.jqplot.css" />
    <script type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
    <script type="text/javascript" src="jscripts/json2.js"></script>
    <script type="text/javascript" src="jscripts/date.js"></script>
    <script type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>
    <script src="jscripts/FusionCharts.js"></script>
    <script type="text/javascript" src="functions.js"></script>
    
    <script>

        var serverDate;
        var settingsDateFormat = "<% = DateTimeConverter.JsDateFormat %>";
        var settingsTimeFormat = "<% = DateTimeConverter.JsTimeFormat %>";

        var saveDbFormat = "dd/MM/yyyy HH:mm";

        var uiFormat = "<% = DateTimeConverter.JsDateTimeFormat %>";

        function check_value() {
            var alertWidth = document.getElementById('alertWidth').value;
            var alertHeight = document.getElementById('alertHeight').value;
            var sizeMessage = document.getElementById('sizeMessage');

            if (alertWidth > 1024 && alertHeight > 600) {
                sizeMessage.innerHTML = '<font color="red"><% =resources.LNG_RESOLUTION_DESC1 %></font>';
                return;
            }

            if (alertWidth < 150 || alertHeight < 150) {
                sizeMessage.innerHTML = "<font color='red'><% =resources.LNG_RESOLUTION_DESC2 %>. <A href='#' onclick='set_default_size();'><% =resources.LNG_CLICK_HERE %></a> <% =resources.LNG_RESOLUTION_DESC3 %>.</font>";
                return;
            }
            sizeMessage.innerHTML = "";
        }

        function set_default_size() {
            var alertWidth = document.getElementById('alertWidth');
            if (alertWidth) alertWidth.value = 150;
            var alertHeight = document.getElementById('alertHeight');
            if (alertHeight) alertHeight.value = 150;
        }

        function check_size_input(myfield, e, dec) {
            var key;
            var keychar;

            if (window.event)
                key = window.event.keyCode;
            else if (e)
                key = e.which;
            else
                return true;

            keychar = String.fromCharCode(key);

            if ((key == null) || (key == 0) || (key == 8) ||
                (key == 9) || (key == 13) || (key == 27)) {
                return true;
            }
            else if (myfield.value.length > 6) {
                return false;
            }
            else if ((("0123456789").indexOf(keychar) > -1)) {
                return true;
            }
            else if (dec && (keychar == ".")) {
                myfield.form.elements[dec].focus();
                return false;
            }
            else
                return false;
        }

        function check_size() {
            var elem = document.getElementById('ticker1');
            if (elem) {
                toggleDisable(elem, '#size_div');
                toggleDisable(elem, '#sizeMessage');
                check_message();
            }
        }

        function toggleAllowContent() {
            var tickerContentCheckBox = $("#allowTickerContent");
            var isDisabled = tickerContentCheckBox.attr('disabled') == "disabled";
            if (isDisabled) {
                tickerContentCheckBox.removeAttr("disabled");
                $(tickerContentCheckBox).val(1);
            }
            else {
                tickerContentCheckBox.attr("disabled", true);
                $(tickerContentCheckBox).val(0);
            }
        }

        function check_message() {
            var elem = document.getElementById('size2');
            var first_elem = document.getElementById('size1');
            var alertWidth = document.getElementById('alertWidth');
            var alertHeight = document.getElementById('alertHeight');
            if (elem.disabled == true) {
                first_elem.checked == true;
                document.getElementById("sizeMessage").style.display = "none";
                alertWidth.disabled = true;
                alertHeight.disabled = true;
            }
            else {
                if (elem) {
                    var disabled = elem.checked;
                    if (disabled) {
                        document.getElementById("sizeMessage").style.display = "none";
                        alertWidth.disabled = true;
                        alertHeight.disabled = true;
                    }
                    else {
                        document.getElementById("sizeMessage").style.display = "";
                        alertWidth.disabled = false;
                        alertHeight.disabled = false;
                    }
                }
            }
        }

        function check_email(Click) {
            var elem = document.getElementById('emailCheck');

            if (elem) {
                var disabled = !elem.checked;
                var el = document.getElementById('email_div');
                toggleDisabled(el, disabled);

                if (!disabled && Click) {
                    validateEmail();
                } else {
                    $("#mailErrorDiv").html("");
                }
            }
        }

        function check_desktop() {
            var elem = document.getElementById('desktopCheck');
            if (elem) {
                toggleDisable(elem, '#tickerDiv');
            }
            check_size();
        }



        function check_aknown() {
            var elem = document.getElementById('aknown');
            if (elem) {
                var disabled = !elem.checked;
                if (disabled) {
                    document.getElementById("autocloseCheckbox").disabled = false;
                    document.getElementById("autoclose").disabled = false;
                }
                else {
                    document.getElementById("autocloseCheckbox").disabled = true;
                    document.getElementById("autocloseCheckbox").checked = false;
                    document.getElementById("autoclose").disabled = true;

                }
            }
            if (document.getElementById('autocloseCheckbox').checked == true) {
                document.getElementById("autoclose").disabled = false;
                document.getElementById("manualCloseCheckBox").disabled = false;
            }
            else {
                document.getElementById("manualCloseCheckBox").disabled = true;
                document.getElementById("manualCloseCheckBox").checked = false;
                document.getElementById("autoclose").disabled = true;
            }
        }

        function isPositiveInteger(n) {
            return (/^\d+$/.test(n + ''));
        }

        function toggleDisable(checkbox, selector) {
            if (!$(checkbox).is(':checked')) {
                $(selector + ' :input').prop('disabled', true);
            } else {
                $(selector + ' :input').removeProp('disabled');
            }
        }

        function check_lifetime() {
            var enabled = document.getElementById('lifetimemode').checked;
            document.getElementById('lifetime').disabled = !enabled;
            document.getElementById('lifetimeFactor').disabled = !enabled;
        }

        function getRssContent(url) {
            var result;
            var dataString = "{url: \"" + url + "\"}";
            $.ajax({
                type: 'POST',
                url: 'EditRss.aspx/GetFeedData',
                data: dataString,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    result = JSON.parse(data.d);
                },
                async: false
            });
            return result;
        }

        function showAlertPreview() {
            var rssContent = getRssContent($("#rssFeed").val());
            var data = new Object();
            data.create_date = formatDate(new Date(), uiFormat);
            var fullscreen = ($("#size2").prop("checked")) ? 1 : 0;
            var ticker = ($("#ticker2").prop("checked")) ? 1 : 0;
            var acknowledgement = 0;
            var alert_width = fullscreen && !ticker ? $(".main_cell").width() : (!ticker ? $("#alertWidth").val() : $(".main_cell").width());
            var alert_height = fullscreen && !ticker ? $(".main_cell").height() : (!ticker ? $("#alertHeight").val() : "");
            var top_template;
            var bottom_template;
            var templateId = $("#template").val();

            data.top_template = top_template;
            data.bottom_template = bottom_template;
            data.alert_width = alert_width;
            data.alert_height = alert_height;
            data.ticker = ticker;
            data.fullscreen = fullscreen;
            data.acknowledgement = acknowledgement;
            if (ticker === 1) {
                data.alert_html =
                    rssContent.body +
                    "<!-- ticker_pro --><!-- tickerSpeed=4 --><!-- html_title = '" +
                    rssContent.title +
                    "' -->";
            } else {
                data.alert_title = rssContent.title;
                data.alert_html = rssContent.body;
            }

            initAlertPreview(data);

            return false;
        }
        function saveRSS() {
            var fromDate = $.trim($("#startDateAndTime").val());
            var toDate = $.trim($("#endDateAndTime").val());
            if ($("#startAndEndDateAndTimeCheckbox").is(':checked')) {
                if (!isDate(fromDate, uiFormat)) {
                    showPrompt("<% =resources.LNG_FROM_DATE_AND_TIME_IS_NOT_VALID %>");
                    return;
                }
                else {
                    if (fromDate != "") {
                        fromDate = new Date(getDateFromFormat(fromDate, uiFormat));
                        $("#from_date").val(formatDate(fromDate, saveDbFormat));
                    }
                }

                if (!isDate(toDate, uiFormat)) {
                    showPrompt("<% =resources.LNG_TO_DATE_AND_TIME_IS_NOT_VALID %>");
                    return;
                }
                else {
                    if (toDate != "") {
                        toDate = new Date(getDateFromFormat(toDate, uiFormat));
                        $("#to_date").val(formatDate(toDate, saveDbFormat));
                    }
                }
                if (fromDate < serverDate && toDate < serverDate) {
                    showPrompt("<% =resources.LNG_DATE_IN_THE_PAST %>");
                    return;
                }
                if (fromDate >= toDate) {
                    showPrompt("<% =resources.LNG_REC_DATE_ERROR %>");
                    return;
                }
            }

            var desktop = document.getElementById("desktopCheck");
            var email_check = document.getElementById("emailCheck");
            var rss = document.getElementById("rss");
            if ((email_check || rss) && !(desktop.checked || (email_check && email_check.checked) || (rss && rss.checked))) {
                alert("<% =resources.LNG_YOU_SHOULD_SELECT_RSS_TYPE %>.");
                return;
            }
            if (document.getElementById('autocloseCheckbox').checked == true) {
                if (document.getElementById("autoclose").value == "0") {
                    alert("<% =resources.LNG_EDIT_ALERT_DB_ALERT1 %>.");
                    return false;
                }
                if (document.getElementById("autoclose").value.length < 1) {
                    alert("<% =resources.LNG_EDIT_ALERT_DB_ALERT1%>.");
                    return false;
                }
            }

            var feedUrl = $("#rssFeed").val();

            $.ajax({
                type: "POST",
                url: "EditRss.aspx/ValidateFeed",
                data: '{url: "' + feedUrl + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    var validated = response.d;

                    if (!validated) {
                        showPrompt("<% =resources.LNG_NOT_VALID_FEED %>");
                    } else {
                        $("#form1").submit();
                    }
                }
            });

        }
        $(function () {

            $("#startAndEndDateAndTimeCheckbox")
                .change(function() {
                    var enabled = $(this).prop("checked");
                    $("#lifetimeDiv > input[type=text]").prop("disabled", enabled);
                    $("#lifetimeDiv > input[type=checkbox]").prop("checked", !enabled);
                    $("#lifetimeDiv > select").prop("disabled", enabled);
                });

            $("#lifetimemode")
                .change(function() {

                    var enabled = $(this).prop("checked");
                    $("#startAndEndDateAndTimeCheckbox").prop("checked", !enabled);
                    $(".date_time_param_input").prop("disabled", enabled);
                });

            $("#save_and_next_button").button().click(function () {
                saveRSS();
            });

            $("#preview_button").button().click(function () {
                showAlertPreview();
            });

            serverDate = new Date("<% = DateTimeConverter.ToUiDateTime(DateTime.Now) %>");
            setInterval(function () { serverDate.setSeconds(serverDate.getSeconds() + 1); }, 1000);

            $(".date_time_param_input")
                .datetimepicker({ dateFormat: settingsDateFormat, timeFormat: settingsTimeFormat });

            $("#startAndEndDateAndTimeCheckbox").change(function () {
                toggleDisable(this, "#start_and_endDateAndTime_div");
            }).change();
            check_email();
            check_desktop();
        });


        function showPrompt(content) {
            $("#dialog-modal > p").empty();
            $("#dialog-modal > p").append(content);
            $("#dialog-modal").dialog({
                height: 150,
                modal: true,
                resizable: false,
                draggable: false
            });
        }

function check_sms(Click) {
    if (Click) {
        checkMessagingChannelsSettings("GetSMSSettings", "#smsCheckbox", "#sms");
    }
}

function checkMessagingChannelsSettings(action, checkbox, responseDiv) {
    if ($(checkbox).prop("checked")) {

        $.post("get_asp_config.asp", { "action": action }, function (data) {
            var SMTPSetting = JSON.parse(data);
            var nullFieldError = "";

            for (var key in SMTPSetting) {
                if (SMTPSetting[key] == "") {
                    nullFieldError += key + ", ";
                }
            }

            nullFieldError = nullFieldError.substring(0, nullFieldError.length - 2); //Remove last ", " from error message.

            if (nullFieldError != "") {
                $(responseDiv).append("<div style='color: red;'>Please fill this fields (" + nullFieldError + ") in settings.</div>");
                $(checkbox).prop("checked", false);
                $(checkbox).attr("disabled", "");
            } 
        });
    }
}

function validateEmail() {
    $.ajax({
        type: "POST",
        url: "CreateAlert.aspx/ValidateEmailSettings",
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var message = response.d;

            if (message != "") {
                $("#emailCheck").attr("disabled", true);
                $("#emailCheck").attr("checked", false);
                $("#emailCheckError").append("<div style='color: red;'>" + message + "</div>");
            }
        }
    });
}
</script>
    <style>
            .body {
	            height : 100%;
	            border-spacing: 0px;
	            border-collapse:collapse;
            }

            html, body {
	            border: 0px;
	            overflow: none;
	            height : 100%;
	            padding:0px;
	            margin:0px;
            }

            #RSS_name, #rssFeed {width: 350px;}

            #main_content_cell
            {
	            padding:10px;
	            height: 100%;
            }

            .ui-tabs-panel {
                overflow-y: auto;
                overflow-x: hidden;
            }

            .mceIframeContainer{
	            height:100%
            }

            .date_time_param_input{
	            width:150px
            }

            fieldset {
               margin: 10px;
               border: solid 1px black;
               position: relative;
               padding: .5em;
            }

            .legend {
               left: 0.5em;
               top: -0.6em;
               color: black;
               position: absolute;
               background-color: white;
               padding: 0 0.25em 0 0.25em;
            }

            </style>
            <link href="css/form_style.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <form id="form1" runat="server" action="AddRss.aspx" method="POST">
    <div>
            <input runat="server" name="rejectedEdit" id="rejectedEdit" value="" type="hidden"/>
	    <input runat="server" name="shouldApprove" id="shouldApprove" value="" type="hidden"/>
        <input runat="server" name="alert_id" id="alertId" type="hidden"/>
         <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
         <tr>
             <td valign="top">
                <table width="100%" cellspacing="0" cellpadding="0" class="main_table">
                         <tr>
   			                <td width="100%" height="31" class="main_table_title"><img src="images/menu_icons/rss_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
		                    <a href="EditRss.aspx" class="header_title" style="position:absolute;top:15px"><%=resources.LNG_RSS %></a></td>             
                         </tr>
                          <tr>
                              <td class="main_cell" id="main_content_cell" height="100%" >
 	                                 <table style="width:100%; height:10%;  border-collapse:collapse; border:0px; margin:0px; padding:0px; ">
	                                      
		                                    <tr style="height:20px">
			                                    <td class="RSS_name_td" style="text-align:left; padding: 0px;">

				                                    <div style="padding:2px 0px">
                                                     <div id="campaignDiv" runat="server">
                                                         <b> <%=resources.LNG_CAMPAIGN %>  :</b>
                                                         <label><b runat="server" id="campaignNameLabel"></b></label>
                                                         <input  type='hidden' runat="server" id='campaignSelect' name='campaignSelect' value=''/>
                                                     </div>
                                                    <div runat="server" id="nonCampaignDiv">
                                                        <input  type='hidden' name='campaignSelect' value='-1' runat="server" />
                                                    </div>
                                                    </div><br />
                       
                                                    <div style="padding:2px 0px">
                                                    <% =resources.LNG_STEP %> <strong>1</strong> <% =resources.LNG_OF %> &nbsp;2 : <strong><% =resources.LNG_ADD_RSS %>:</strong>
                                                    </div>
			                                    </td>
		                                    </tr>
                                            <tr style="">
                                                
                                                <table style="border-collapse:collapse; border:0px;margin-top:10px;">
							                        <tr>
							                            <td width="650" valign="top">
							                              		<table style="border-collapse:collapse; border:0px; margin:0px; padding:0px;">
									                                    <tr>
										                                    <td><label for="title"><% =resources.LNG_TITLE %>:</label></td>
										                                    <td><input runat="server" type="text" name="title" id="title" value=""/></td>
									                                    </tr>
									                                    <tr>
										                                    <td><label for="rssFeed"><% =resources.LNG_RSS_FEED%>:</label></td>
										                                    <td><input runat="server" type="text" name="rssFeed" id="rssFeed" value=""/>
											                                    <img src="images/help.png" title="<% =resources.LNG_RSS_FEED_TITLE%>."/>
										                                    </td>
									                                    </tr>
									                                    <tr>
										                                    <td></td>
										                                    <td>
											                                    <div style="color:red"></div>
										                                    </td>
									                                    </tr>
									                            </table> 
                                                            
                                                            	<div id="lifetimeDiv" style="padding-top:30px">
	                                                                <input runat="server" type="checkbox" id="lifetimemode" name="lifetimemode" value="1" onclick="check_lifetime();" checked="True"/> <label for="lifetimemode"><%=resources.LNG_LIFETIME_IN %></label> 
                                                                    <input runat="server" type="text"  id="lifetime" runat="server" name="lifetime" style="width: 50px"  onkeypress="return check_size_input(this, event);"/>
                                                                    <select runat="server" id="lifetimeFactor" name="lifetime_factor">
                                                                                        </select>
                                                                    &nbsp;
                                                                    <label id ="LifetimeAdditionalDescription" runat="server"></label>
									                            </div>
									                            <div style="padding-top:2px">
										                            <input runat="server" type="checkbox" id="autocloseCheckbox" name="autocloseCheckbox" value="1" onclick="check_aknown()"/> <label for="autocloseCheckbox"><%=resources.LNG_AUTOCLOSE_IN%></label> 
                                                                    <input runat="server" type="text" size="3" name="autoclose" disabled="True" id="autoclose" onkeypress="return check_size_input(this, event);" value=""/> <%=resources.LNG_MINUTES %> 
                                                                    <img src="images/help.png" title="<%=resources.LNG_AUTOCLOSE_TITLE%>."/>
                                                                     <span id="manualCloseCheckBox_div">
                                                                            <input runat="server" disabled="True"  type="checkbox" id="manualCloseCheckBox" name="manualCloseCheckBox" value="1"/> 
                                                                            <label for="manualCloseCheckBox"><% =resources.LNG_ALLOW_MANUAL_CLOSE %></label> <img src="images/help.png" title="<%=resources.LNG_ALLOW_MANUAL_CLOSE_DESC %>."/> 
                                                                     </span>
									                            </div> 

									                            <div style="padding-top:80px" >
										                            <input runat="server" id="startAndEndDateAndTimeCheckbox" name="schedule" value="1" type="checkbox" />&nbsp;<label for="startAndEndDateAndTimeCheckbox"><% =resources.LNG_START_AND_END_DATE_AND_TIME %></label>
										                            <div id="start_and_endDateAndTime_div" style="padding-top:5px">
											                            <table style="width: 300px; height:50px; border-collapse:collapse; border:0px; margin:0px; padding:0px; ">
												                            <tr>
													                            <td style="text-align:right">
														                            <label for="startDateAndTime"><%=resources.LNG_START_DATE_AND_TIME %>:</label>
													                            </td>
													                            <td>
														                            <input runat="server" id="startDateAndTime" class="date_time_param_input" name="startDateAndTime" type="text" value=""></input>
													                            </td>
												                            </tr>
												                            <tr>
													                            <td style="text-align:right">
														                            <label for="endDateAndTime"><%=resources.LNG_END_DATE_AND_TIME %>:</label>
													                            </td>
													                            <td>
														                            <input runat="server" id="endDateAndTime" class="date_time_param_input" name="endDateAndTime" type="text" value=""></input>
													                            </td>
												                            </tr>
											                            </table>
										                            </div>
									                            </div>
                                                                </td>
                                                                <td valign="top" width="260px" style="padding-top:40px">
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <div id="sms"  runat="server">
                                                                                    <input runat="server" type="checkbox" name="sms" id="smsCheckbox" value="1" onclick="check_sms(true);">
                                                                                    <label for="sms"><%=resources.LNG_RSS_TO_SMS%></label>
											                                    </div>
										                                    </td>
									                                    </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div id="textToCall"  runat="server">
                                                                                    <input runat="server" type="checkbox" name="sms" id="textToCallCheckbox" value="1">
                                                                                    <label for="textToCall"><%=resources.LNG_RSS_TO_TEXT_TO_CALL%></label>
											                                    </div>
										                                    </td>
									                                    </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div id='smsDemo' runat="server">
												                                    <input runat="server" type='checkbox'  name='sms' value='' disabled title="Demo version is not configured to send rss."/>
                                                                                    <label for="sms" title="Demo version is not configured to send rss."> <%=resources.LNG_RSS_TO_SMS %></label>
                                                                                    <br>
											                                    </div>
										                                    </td>
									                                    </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div>
                                                                                    <div>
                                                                                        <input runat="server" type="checkbox" name="email" id="emailCheck" value="1" onclick="check_email(true);"/>
                                                                                        <label for="emailCheck"><%=resources.LNG_RSS_TO_ENAIL %></label>
											                                        </div>
											                                        <div id="emailDiv">
                                                                                       <div id="mailErrorDiv">
                                                                                                
                                                                                      </div>
											                                        </div>
                                                                                </div>
                                                                                <div id="emailCheckError"></div>
										                                    </td>
									                                    </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div>
                                                                                    <input runat="server" type="checkbox" id="desktopCheck" name="desktop" value="1" checked="True" onclick="check_desktop();"/>
                                                                                    <label for="desktopCheck"><%=resources.LNG_RSS_TO_DESKTOPS%></label>
											                                    </div>
                                                                                <div id="atc">
                                                                                    <input runat="server" type='hidden' id='allow_ticker_content' name='allow_ticker_content'   value=''/> 
                                                                                    <label for='allow_ticker_content'> <%=resources.LNG_RSS_ALLOW_TICKER_CONTENT %></label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
								                                        <!--EndDesktopBlock-->
									                                    <tr>
                                                                            <td>
                                                                                <div id="ticker_div">
                                                                                    <div id="sizeMessage"></div>
                                                                                    <table border="0" cellspacing=0 cellpadding=0>
													                                            <tr>
														                                            <td valign="top" style="width: 110px; padding:5px 5px 5px 0px">
															                                            <fieldset class="reccurence_pattern" style="width: 100px;height:105px; padding:0px; margin:0px">
																                                            <legend style="left:10px"><% =resources.LNG_DISPLAY_TYPE %></legend>
																                                            <br/>
																                                            <div style="padding-left:5px;">
																	                                            <table border="0" cellpadding="1" cellspacing="1">
																	                                            <tr><td valign="top">
																		                                            <input runat="server" type="radio" id="ticker1" name="ticker" value="0" checked="True" onclick="check_size(); toggleAllowContent();"/>
																	                                            </td><td valign="top">
																		                                            <label for="ticker1"><%=resources.LNG_POPUP %></label>
																	                                            </td></tr>
																	                                            <tr><td valign="top">
																		                                            <input runat="server" type="radio" id="ticker2" name="ticker" value="1"  onclick="check_size(); toggleAllowContent();"/>
																	                                            </td><td valign="top">
																		                                            <label for="ticker2"><%=resources.LNG_SCROLL_IN_TICKER %></label>
																	                                            </td></tr>
																	                                            </table>
																                                            </div>
															                                            </fieldset>
														                                            </td>
														                                            <td valign="top" style="padding:5px">
															                                            <!--BeginSizeBlock-->
															                                            <fieldset class="reccurence_pattern" style="height:105px; padding:0px; margin:0px; ">
																                                            <legend style="left:10px"><%=resources.LNG_SIZE%></legend>
																                                            <div id="size_div" style="padding-left:10px;">
																	                                            <table border="0">
																		                                            <tr>
																		                                                <td valign="middle"><input runat="server" type="radio" id="size1" name="fullscreen" value="0" checked="" onclick="check_message()" /></td>
																			                                            <td valign="middle"><label for="size1"><%=resources.LNG_WIDTH %>:</label></td>
																		                                                <td valign="middle"><input runat="server" type="text" name="alert_width" id="alertWidth"  size="4"  onkeypress="return check_size_input(this, event);" onKeyUp="check_value(this)" /></td>
																			                                            <td valign="middle"><%=resources.LNG_PX %></td>
																		                                            </tr>
																		                                            <tr>
																			                                            <td>&nbsp;</td>
																			                                            <td valign="middle"><label for="size1"><%=resources.LNG_HEIGHT %>:</label></td>
																		                                                <td valign="middle"><input runat="server" type="text" size="4" name="alert_height"  id="alertHeight"  onkeypress="return check_size_input(this, event);" onKeyUp="check_value(this)" /></td>
																			                                            <td valign="middle"><%=resources.LNG_PX %></td>
																		                                            </tr>
																		                                            <tr>
																		                                                <td valign="middle"><input runat="server" type="radio" id="size2" name="fullscreen" value="1"   onclick="check_message()" /></td>
																			                                            <td valign="middle" colspan="3"> <label for="size2"><%=resources.LNG_FULLSCREEN %></label><br/></td>
																		                                            </tr>
																	                                            </table>
																                                            </div>
															                                            </fieldset>
															                                            <!--EndSizeBlock-->
														                                            </td>
													                                            </tr>
												                                            </table>
											                                            </div>
										                                            </td>
									                                            </tr>
								                                    </table> 
                                                                </td>

									                        </tr>
		                                                    <tr >
		                                                        <td></td>
			                                                    <td class="RSS_name_td" style="text-align:right; vertical-align:top; padding: 0px; padding-top: 10px; ">
				                                                    <a id="preview_button"><% =resources.LNG_PREVIEW %></a>
				                                                    <a id="save_and_next_button"><% =resources.LNG_SAVE_AND_NEXT %></a>
                                                                    <br/><br/>
			                                                    </td>
		                                                    </tr>

                                                        
							                        
                                                </table>
                                           </tr>
	                                  </table>
                </table>
         </table>


    </div>
</form>
<div id="dialog-modal" title="" style='display: none'>
    <p></p>
</div>
</body>
</html>
