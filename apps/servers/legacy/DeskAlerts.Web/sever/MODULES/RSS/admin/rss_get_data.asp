<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="libs/TemplateObj.asp" -->
<!-- #include file="rss_functions.asp" -->
<%
Function jsEncode(str)
	Dim charmap(127), haystack()
	charmap(8)  = "\b"
	charmap(9)  = "\t"
	charmap(10) = "\n"
	charmap(12) = "\f"
	charmap(13) = "\r"
	charmap(34) = "\"""
	charmap(47) = "\/"
	charmap(92) = "\\"

	Dim strlen : strlen = Len(str) - 1
	ReDim haystack(strlen)

	Dim i, charcode
	For i = 0 To strlen
		haystack(i) = Mid(str, i + 1, 1)

		charcode = AscW(haystack(i)) And 65535
		If charcode < 127 Then
			If Not IsEmpty(charmap(charcode)) Then
				haystack(i) = charmap(charcode)
			ElseIf charcode < 32 Then
				haystack(i) = "\u" & Right("000" & Hex(charcode), 4)
			End If
		Else
			haystack(i) = "\u" & Right("000" & Hex(charcode), 4)
		End If
	Next

	jsEncode = Join(haystack, "")
End Function



URLToRSS = Request("url")
rssTemplate = Request("rssTemplate")
Response.Expires = -1
sRSSTemplateFileName = "template_rss_alert.html"


if rssTemplate = "" then
	rssTemplate = readFile(alerts_dir & templateFolder & "\" & sRSSTemplateFileName)
	if rssTemplate = "" then
		rssTemplate = "{description}<br/>{url}"
	end if
end if

responseText = ""

Data = getFeedData(URLToRSS)
if Data(0) = "" then 'no error

	items = Data(1).Items
	set RSSItem = items(0)
	LoadTemplateFromString "templateRSSAlert", rssTemplate
	for each key in RSSItem.Keys
			SetVar key, RSSItem.Item(key)
	next
	SetVar "more", LNG_MORE
			
	Parse "templateRSSAlert", False

	alertTitle = RSSItem.Item("title")
	alertHTML = PrintVar("templateRSSAlert")
	
	
end if

responseText = "{""title"": """&jsEncode(alertTitle)&""", ""body"": """&jsEncode(alertHTML)&""" }"

response.Write responseText

%>