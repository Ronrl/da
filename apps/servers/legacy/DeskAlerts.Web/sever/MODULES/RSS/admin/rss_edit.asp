<!-- #include file="config.inc" -->
<!-- #include file="demo.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
check_session()
%>
<!-- #include file="Common.asp" -->
<!-- #include file="header.asp" -->
<!-- #include file="footer.asp" -->
<!-- #include file="rss_functions.asp" -->
<!-- #include file="functions_inc.asp" -->
<%
'On Error Resume Next
' index CustomIncludes End
'-------------------------------

'===============================
' Save Page and File Name available into variables
'-------------------------------
Dim sFileName
Dim sTemplateFileName
Dim intSessionUserId

sFileName = "rss_edit.asp"
sListFileName = "RssList.aspx"
sTemplateFileName = "rss_edit.html"
sRSSTemplateFileName = "template_rss_alert.html"
'===============================

if (request("action") = "checkRSSLink") then
    Dim data, httpRequest, postResponse
  
    data = ""
    Response.Write(request("RSS"))
    Set httpRequest = Server.CreateObject("MSXML2.ServerXMLHTTP")
    httpRequest.open "POST", request("RSS"), False
    httpRequest.setRequestHeader "Content-Type", "application/x-ww-form-urlencoded"
    httpRequest.send data

    postResponse = httpRequest.statusText

    Response.Write(postResponse)
    Response.End
end if

function updateAlert(id, alertText, alertTitle, fromDate, toDate, schedule, scheduleType, recurrence, urgent, email, desktop, sms, emailSender, ticker, fullscreen, alertWidth, alertHeight, aknown, autoclose, toolbarmode, deskalertsmode, template, uid, paramTemplateId, rss_allow_content)
	sqlRequest = "UPDATE alerts SET alert_text=N'"& Replace(alertText, "'", "''") &"', title=N'"& Replace(alertTitle, "'", "''") &"', type='D', from_date='"&fromDate&"', to_date='"&toDate&"', schedule='"&schedule&"', schedule_type='"&scheduleType&"', recurrence='"&recurrence&"', urgent='"&urgent&"', email='"&email&"', desktop='"&desktop&"', sms='"&sms&"', email_sender='"& Replace(emailSender, "'", "''") &"', ticker='"&ticker&"', fullscreen='"&fullscreen&"', alert_width="&alertWidth&",  alert_height="&alertHeight&", aknown='"&aknown&"',  autoclose="&autoclose&",  toolbarmode='"&toolbarmode&"', deskalertsmode='"&deskalertsmode&"', template_id="&template&", sender_id="&uid&", param_temp_id="&paramTemplateId&", rss_allow_content=" & rss_allow_content &"WHERE id = " & Clng(id)
	Set RS_alert_update = Conn.Execute(sqlRequest)
        'MEMCACHE
	'Set cacheMgr = CreateObject("DeskAlerts.CacheManager")
   ' cacheMgr.Update(clng(id), now())
    MemCacheUpdate "Update", id 
	updateAlert = id
end function

'alertClass 1 - usual alert, 2 - RSS alert, 4 - ...

function addAlertToDataBase(alertText, alertTitle, fromDate, toDate, schedule, scheduleType, recurrence, urgent, email, desktop, sms, emailSender, ticker, fullscreen, alertWidth, alertHeight, aknown, autoclose, toolbarmode, deskalertsmode, template, uid, paramTemplateId, alertClass, campaignid, approve_status, allow_ticker_content)
	sqlRequest = "INSERT INTO alerts (alert_text, title, create_date, type, from_date, to_date, schedule, schedule_type, recurrence, urgent, email, desktop, sms, email_sender, ticker, fullscreen, alert_width,  alert_height, aknown,  autoclose,  toolbarmode, deskalertsmode, template_id, sender_id, param_temp_id, class, campaign_id, approve_status, rss_allow_content) VALUES (N'"& Replace(alertText, "'", "''") &"', N'"& Replace(alertTitle, "'", "''") &"', GETDATE(), 'D', '"&fromDate&"','"&toDate&"','"&schedule&"', '"&scheduleType&"', '"&recurrence&"', '"&urgent&"', '"&email&"', '"&desktop&"', '"&sms&"', '"& Replace(emailSender, "'", "''") &"',  '"&ticker&"', '"&fullscreen&"', "&alertWidth&",  "&alertHeight&", '"&aknown&"', "&autoclose&", '"&toolbarmode&"', '"&deskalertsmode&"',"&template&", "&uid&","&paramTemplateId&","&alertClass&","& campaignid & "," & approve_status & "," & allow_ticker_content &"); select @@IDENTITY as 'newID'"
	Set RS_alert = Conn.Execute(sqlRequest)
	Set RS_alert  = RS_alert.NextRecordset
	addAlertToDataBase=RS_alert("newID")
        'MEMCACHE
	''Set cacheMgr = CreateObject("DeskAlerts.CacheManager")
   ' cacheMgr.Add(clng(addAlertToDataBase), now())
       MemCacheUpdate "Add", addAlertToDataBase
	RS_alert.Close
end function

uid = Session("uid")
if uid <> "" then

rss_arr = Array ("","","","","","","","")
emails_arr = Array ("","","","","","","","")
sms_arr = Array ("","","","","","","","")
templates_arr = Array ("","","","","","","","")

Set RS = Conn.Execute("SELECT id FROM users WHERE id="&uid&" AND role='E'")
if Not RS.EOF then
	gid = 1
	Set rs_policy = Conn.Execute("SELECT type FROM policy INNER JOIN policy_editor ON policy.id=policy_editor.policy_id WHERE editor_id=" & uid)
	if(Not rs_policy.EOF) then
		if(rs_policy("type")="A") then
			gid = 0
		end if
	end if
	if(gid=1) then
		editor_id = RS("id")
		policy_ids = editorGetPolicyIds(editor_id)
		rss_arr = editorGetPolicyList(policy_ids, "rss_val")
		emails_arr = editorGetPolicyList(policy_ids, "emails_val")
		sms_arr = editorGetPolicyList(policy_ids, "sms_val")
		templates_arr = editorGetPolicyList(policy_ids, "templates_val")
	end if
else
	gid = 0
end if
RS.Close

errMsg = ""
alertId = Request("id")
title = Request("title")
feed = Request("rss_feed")
dateFrom = Request("from_date")
dateTo = Request("to_date")
aut_close = Request("aut_close")
autoclose = Request("autoclose")
man_close = Request("man_close")
schedule = Request("schedule")
lifetime = Request("lifetime")
lifetime_factor = Request("lifetime_factor")
schedule_type = 1

recurrence = 0
urgent = 0
email = Request("email")
desktop = Request("desktop")
sms = Request("sms")
emailSender = Request("email_sender")
ticker = Request("ticker")
fullscreen = Request("fullscreen")
alertWidth = Request("alert_width")
alertHeight = Request("alert_height")
template = Request("template")
rssTemplate = Request("rssTemplate")
save = Request("save")
campaign_id = Request("campaign_select")

if(campaign_id <> "-1") then
schedule_type = 0
end if

lifetime = 0
if Request("lifetimemode") = "1" and Request("lifetime") <> "" and Request("lifetime_factor") <> "" then
	lifetime = Request("lifetime") * Request("lifetime_factor")
end if
if schedule <> "1" then schedule = 0
if email <> "1" then
	email = 0
	emailSender = ""
end if
if sms <> "1" then sms = 0
if desktop <> "1" then desktop = 0
if alertWidth = "" then alertWidth = ConfAlertWidth
if alertHeight = "" then alertHeight = ConfAlertHeight
if template = "" then template = 1

if save = "1" then 'save & next
	aknown = 0
	autoclose_val = autoclose
	if(aut_close <> 1) then autoclose_val = 0 end if
	if(autoclose = "") then autoclose_val = 0 end if
	if(man_close = 1) then autoclose_val = -1*autoclose_val end if
	autoclose_val = autoclose_val*60
	
	toolbarmode = 0
	deskalertsmode = 0
	paramTemplateId = "NULL"
	alertClass = 4
	
	if rssTemplate = "" then
		rssTemplate = readFile(alerts_dir & "\" & templateFolder & "\" & sRSSTemplateFileName)
		if rssTemplate = "" then
			rssTemplate = "{description}<br/>{url}"
		end if
	end if
	

    
	storedHashesStr = ""
	if alertId <> "" and Request("edit") <> "" then
		' for be sure get current hashes list
		Response.Write "SELECT type, alert_text FROM alerts WHERE id = "& alertId
		Set RS = Conn.Execute("SELECT type, alert_text FROM alerts WHERE id = "& CLng(alertId))
		if not RS.EOF then
			if RS("type") = "S" then 'if edit sended alert - get 'storedHashesStr'
				html = RS("alert_text")
				separatorIndex = InStr(html,"|")
				if separatorIndex > 0 then
					html = Mid(html,separatorIndex+1)
					separatorIndex = InStr(html,"|")
					if separatorIndex > 0 then
						storedHashesStr = Left(html,separatorIndex-1)
					end if
				end if
			end if
		end if
	end if
	result = getFeedData(feed)
	
	'Response.Write "|"& result(0) &"|"& result(1) &"|"& result(2) &"|"
	
	
   

	if result(0) = "" then
		if storedHashesStr = "" then
			storedHashesStr = result(2)
		end if

		html = feed &"|"& storedHashesStr &"|"& lifetime &"|"& rssTemplate
		
		'for using other parameters with lifetime use splitting by separator ";"
		
		approve_status = 1
		  if(APPROVE = 1) then
                 Set enabled = Conn.execute("SELECT val FROM settings WHERE name = 'ConfAllowApprove'") 
         
                 if(enabled("val") = "1" and  gid <> 0 and rss_arr(7) <> "checked"  and instant_message <> 1) then
                      approve_status = 0
                 end if

          end if
 		  rss_allow_content = "0"
		    
		  if(ConfIFRMode = 1 and request("allow_ticker_content") = "1") then
		      rss_allow_content = "1"
		  end if
		                        
		if alertId = "" or Request("edit") = "" then		    
			alertId = addAlertToDataBase(html, title, dateFrom, dateTo, schedule, schedule_type, recurrence, urgent, email, desktop, sms, emailSender, ticker, fullscreen, alertWidth, alertHeight, aknown, autoclose_val, toolbarmode, deskalertsmode, template, uid, paramTemplateId, alertClass, campaign_id, approve_status, rss_allow_content)
		else
		

		    'if(Request("rejectedEdit") = "1") then
		    '    Conn.Execute("UPDATE alerts SET approve_status = 0 WHERE id = " & alertId)
		   ' elseif(Request("shouldApprove") = "1") then
		    '    Conn.Execute("UPDATE alerts SET approve_status = 1 WHERE id = " & alertId)
		   ' end if 
		    senderUserId = uid
		    if  Request("rejectedEdit") = "1" or Request("shouldApprove") = "1" then
		         Set senderIdSet = Conn.Execute("SELECT sender_id FROM alerts WHERE id = " & alertId)
		         senderUserId = senderIdSet("sender_id")
	    	end if
			alertId = updateAlert(alertId, html, title, dateFrom, dateTo, schedule, schedule_type, recurrence, urgent, email, desktop, sms, emailSender, ticker, fullscreen, alertWidth, alertHeight, aknown, autoclose_val, toolbarmode, deskalertsmode, template, senderUserId, paramTemplateId, rss_allow_content)
			'Conn.Execute("DELETE FROM alerts_received WHERE alert_id="&alertId)
		end if 
		
		approve = "0"
		
		if(gid = 0 or rss_arr(7) = "checked") then
		    approve = "1"
		end if
		
		
		if(campaign_id = "-1") then
		    Response.Redirect "alert_users.asp?id="& CStr(alertId) &"&return_page="& sListFileName &"&header="& LNG_RSS & "&title=" &LNG_ADD_RSS & "&approve=" & approve & "&shouldApprove=" & request("shouldApprove") & "&rejectedEdit=" & request("rejectedEdit")
		else
		      Response.Redirect "alert_users.asp?id="& CStr(alertId) &"&return_page=CampaignDetails.aspx?campaignid="& campaign_id &"&header="& LNG_RSS & "&title=" &LNG_ADD_RSS & "&campaign_id=" & campaign_id & "&approve=" & approve & "&shouldApprove=" & request("shouldApprove") & "&rejectedEdit=" & request("rejectedEdit")
		end if
	else
		errMsg = result(0)
	end if
end if


'===============================
' Display page
'-------------------------------
' Load HTML template for this page
'-------------------------------
LoadTemplate alerts_dir & "\" & templateFolder & "\" & sTemplateFileName, "main"

'-------------------------------
' Load HTML template of Header and Footer
'-------------------------------
sHeaderFileName=Replace(sHeaderFileName,"header.html","header-old.html")
LoadTemplate sHeaderFileName, "Header"
LoadTemplate sFooterFileName, "Footer"
'-------------------------------
SetVar "sFileName", sFileName
SetVar "strPageName", LNG_RSS

SetVar "LNG_LIFETIME_IN", LNG_LIFETIME_IN
SetVar "LNG_MINUTES", LNG_MINUTES
SetVar "LNG_RSS_TIME_EXPIRED_CANT_RECEIVED", LNG_RSS_TIME_EXPIRED_CANT_RECEIVED
SetVar "LNG_AUTOCLOSE_IN", LNG_AUTOCLOSE_IN
SetVar "LNG_AUTOCLOSE_TITLE", LNG_AUTOCLOSE_TITLE
SetVar "LNG_ALLOW_MANUAL_CLOSE", LNG_ALLOW_MANUAL_CLOSE
SetVar "LNG_ALLOW_MANUAL_CLOSE_DESC", LNG_ALLOW_MANUAL_CLOSE_DESC

SetVar "LNG_STEP", LNG_STEP
SetVar "LNG_OF", LNG_OF
SetVar "LNG_SELECT_TEMPLATE", LNG_SELECT_TEMPLATE
SetVar "LNG_DISPLAY_TYPE", LNG_DISPLAY_TYPE
SetVar "LNG_POPUP", LNG_POPUP
SetVar "LNG_SCROLL_IN_TICKER", LNG_SCROLL_IN_TICKER
SetVar "LNG_SIZE", LNG_SIZE
SetVar "LNG_WIDTH", LNG_WIDTH
SetVar "LNG_HEIGHT", LNG_HEIGHT
SetVar "LNG_PX", LNG_PX
SetVar "LNG_FULLSCREEN", LNG_FULLSCREEN
SetVar "LNG_RSS_TO_SMS", LNG_RSS_TO_SMS
SetVar "LNG_RSS_TO_ENAIL", LNG_RSS_TO_ENAIL
SetVar "LNG_RSS_TO_DESKTOPS", LNG_RSS_TO_DESKTOPS
SetVar "LNG_SENDER_NAME", LNG_SENDER_NAME
SetVar "LNG_RESOLUTION_DESC1", LNG_RESOLUTION_DESC1
SetVar "LNG_RESOLUTION_DESC2", LNG_RESOLUTION_DESC2
SetVar "LNG_RESOLUTION_DESC3", LNG_RESOLUTION_DESC3
SetVar "LNG_CLICK_HERE", LNG_CLICK_HERE

SetVar "LNG_RSS", LNG_RSS
SetVar "LNG_ADD_RSS", LNG_ADD_RSS
SetVar "LNG_RSS_FEED", LNG_RSS_FEED
SetVar "LNG_RSS_FEED_TITLE", LNG_RSS_FEED_TITLE
SetVar "LNG_TITLE", LNG_TITLE
SetVar "LNG_YOU_SHOULD_SELECT_RSS_TYPE", LNG_YOU_SHOULD_SELECT_RSS_TYPE
SetVar "LNG_EDIT_ALERT_DB_ALERT1", LNG_EDIT_ALERT_DB_ALERT1
SetVar "LNG_SCHEDULE_EXT_DESC", LNG_SCHEDULE_EXT_DESC
SetVar "LNG_LIFETIME_EXT_DESC", LNG_LIFETIME_EXT_DESC

SetVar "default_lng", lcase(default_lng)
SetVar "timezone_db", timezone_db

SetVar "FromDateNotValidStr", LNG_FROM_DATE_AND_TIME_IS_NOT_VALID
SetVar "ToDateNotValidStr", LNG_TO_DATE_AND_TIME_IS_NOT_VALID
SetVar "ToDateEarlyError", LNG_REC_DATE_ERROR
SetVar "TimeToDisplayNotValidStr", LNG_TIME_TO_DISPLAY_IS_NOT_VALID

SetVar "ContentsStr", LNG_CONTENTS
SetVar "PreviewStr", LNG_PREVIEW

SetVar "StartAndEndDateAndTimeStr", LNG_START_AND_END_DATE_AND_TIME
SetVar "StartDateAndTimeStr", LNG_START_DATE_AND_TIME
SetVar "EndDateAndTimeStr", LNG_END_DATE_AND_TIME
SetVar "SaveAndNextStr", LNG_SAVE_AND_NEXT

SetVar "errMsg", errMsg

SetVar "rejectedEdit", request("rejectedEdit")
SetVar "shouldApprove", request("shouldApprove")
SetVar "edit", request("edit")

if CAMPAIGN = 1 and Request("alert_campaign") <> "" then
SetVar "Campaign", "<b>" & LNG_CAMPAIGN & ":</b>"
else
SetVar "Campaign", ""   
end if

if ConfIFRMode <> 1 then 
SetVar "allowTickerContentType", "hidden"
SetVar "allowTickerContentEnabled", ""
SetVar "allowTickerContentChecked", ""
SetVar "LNG_RSS_ALLOW_TICKER_CONTENT", ""
else
    SetVar "allowTickerContentType", "checkbox"
    SetVar "LNG_RSS_ALLOW_TICKER_CONTENT", LNG_RSS_ALLOW_TICKER_CONTENT
   if request("id") = "" then
        SetVar "allowTickerContentEnabled", " disabled"
        SetVar "allowTickerContentValue", "0"
          SetVar "allowTickerContentChecked", ""
        'SetVar "allowTickerContentEnabled", ""   
   end if
    
end if
'--------------------


if CAMPAIGN = 1 then


                                                                    
 if Request("alert_campaign") = "" then
 campaignSelector = "<input type='hidden' name='campaign_select' value='-1'>"
 else
 
    SQL = "SELECT name FROM campaigns WHERE id = " & Request("alert_campaign")
    Set RS2 = Conn.Execute(SQL)
    if not RS2.EOF then
       campaignSelector =  "<label><b>"&HtmlEncode(RS2("name"))&"</b></label><br>"
       campaignSelector = campaignSelector &  "<input type='hidden' name='campaign_select' value='"&Request("alert_campaign")&"'>"                                
    end if
  
 end if
   
else
 campaignSelector = "<input type='hidden' name='campaign_select' value='-1'>"
end if

SetVar "CampaignSelector", campaignSelector & "<br>"



nosms = false
noemail = false
set fs=Server.CreateObject("Scripting.FileSystemObject")
if not fs.FileExists(Server.MapPath("3sdfm98a3nz73h825.key")) or (gid = 1 and sms_arr(3)="") then
	LoadTemplateFromString "SmsBlock", ""
	LoadTemplateFromString "SmsDemoBlock", ""
	nosms = true
else
	if DEMO = 1 then
		LoadTemplateFromString "SmsBlock", ""
	else
		LoadTemplateFromString "SmsDemoBlock", ""
	end if
end if

if not fs.FileExists(Server.MapPath("m93sndf8a3z723h85.key")) or (gid = 1 and emails_arr(3)="") then
	LoadTemplateFromString "EmailBlock", ""
	noemail = true
end if

if nosms and noemail  then
	LoadTemplateFromString "DesktopBlock", "<input type=""hidden"" name=""desktop"" value=""1""/>"
end if

if gid = 1 AND rss_arr(6)="" then
	LoadTemplateFromString "SizeBlock", ""
end if

SetVar "alert_id", alertId
SetHtmlVar "title", title
SetHtmlVar "feed", feed
SetVar "fromDate", ""
SetVar "toDate", ""
SetVar "saveFromDate", ""
SetVar "saveToDate", ""
SetVar "alert_width", ConfAlertWidth
SetVar "alert_height", ConfAlertHeight
if schedule = "1" or (save = "" and ConfScheduleByDefault) then
	SetVar "start_and_end_date_and_time_checked", " checked"
else
	SetVar "start_and_end_date_and_time_checked", ""
end if
if email = "1" or (save = "" and ConfEmailByDefault) then
	SetVar "emailChecked", " checked"
else
	SetVar "emailChecked", ""
end if
if  sms = "1" or (save = "" and ConfSMSByDefault) then
	SetVar "smsChecked", " checked"
else
	SetVar "smsChecked", ""
end if
if desktop = "1" or (save = "" and ConfDesktopAlertByDefault) then
	SetVar "desktopChecked", " checked"
else
	SetVar "desktopChecked", ""
end if
if fullscreen = "1" or (save = "" and ConfPopupType = "F") then
	SetVar "fullscreenChecked", " checked"
	SetVar "sizedChecked", ""
	SetVar "sizedDisabled", " disabled"
else
	SetVar "fullscreenChecked", ""
	SetVar "sizedChecked", " checked"
	SetVar "sizedDisabled", ""
end if
if ticker = "1" or (save = "" and ConfDesktopAlertType = "T") then
	SetVar "tickerChecked", " checked"
	SetVar "popupChecked", ""
	SetVar "sizedDisabled", " disabled"
	SetVar "allSizesDisabled", " disabled"
else
	SetVar "tickerChecked", ""
	SetVar "popupChecked", " checked"
	SetVar "allSizesDisabled", ""
	
end if

if aut_close = "1" or (save = "" and ConfAutocloseByDefault = "1") then
	SetVar "aut_close", " checked"
	SetVar "autoclose_dis", ""
	if save = "" then
		SetVar "autoclose", ConfAutocloseValue
	else
		SetVar "autoclose", autoclose
	end if
	if man_close = "1" or (save = "" and ConfAllowManualCloseByDefault = "1") then
		SetVar "man_close", " checked"
	else
		SetVar "man_close", ""
	end if
else
	SetVar "aut_close", ""
	SetVar "autoclose", ""
	SetVar "autoclose_dis", " disabled"
	SetVar "man_close", " disabled"
end if

if emailSender = "" then
	SetHtmlVar "email_sender", ConfDefaultEmail
	if ConfDefaultEmail="" then
		Set RSUser = Conn.Execute("SELECT name FROM users WHERE id="&uid)
		if not RSUser.EOF then
			SetHtmlVar "email_sender", RSUser("name")
		end if
	end if
else
	SetHtmlVar "email_sender", emailSender
end if

if save = "1" then
	SetVar "saveFromDate", dateFrom
	SetVar "saveToDate", dateTo
	if alertWidth <> "" then
		SetVar "alert_width", alertWidth
	end if
	if alertHeight <> "" then
		SetVar "alert_height", alertHeight
	end if
else
	lifetime = ConfRSSMessageExpire
end if

if alertId <> "" and save <> "1" then
	Set RS = Conn.Execute("SELECT title, alert_text, from_date, to_date, autoclose, schedule, email_sender, email, sms, desktop, template_id, ticker, fullscreen, alert_width, alert_height, rss_allow_content FROM alerts WHERE id = "& CLng(alertId))
	if not RS.EOF then
		html = RS("alert_text")
		params = ""
		if html <> "" then
			separatorIndex = InStr(html,"|")
			if separatorIndex > 0 then
				SetHtmlVar "feed", Left(html,separatorIndex-1)
				html = Mid(html,separatorIndex+1)
				separatorIndex = InStr(html,"|")
				if separatorIndex > 0 then
					'storedHashesStr = Left(html,separatorIndex-1)
					html = Mid(html,separatorIndex+1)
					separatorIndex = InStr(html,"|")
					if separatorIndex > 0 then
						params = Left(html,separatorIndex-1) 'for other parameters use splitting by separator ";"
						'rssTemplate = Mid(html,separatorIndex+1)
						separatorIndex = InStr(params,";")
						if separatorIndex > 0 then
							lifetime = Left(params,separatorIndex-1)
							params = Mid(params,separatorIndex+1)
						else
							lifetime = params
						end if
					end if
				end if
			end if
		end if
		
		if RS("title") <> "" then 
			SetHtmlVar "title", RS("title")
		end if
		
		if RS("schedule") = "1" then
			SetVar "start_and_end_date_and_time_checked", " checked"
		end if
		
		SetVar "aut_close", ""
		SetVar "autoclose", ""
		SetVar "man_close", ""
		if(Not IsNull (RS("autoclose"))) then
			autoclose = Abs(CLng(RS("autoclose")))/60
			if(autoclose > 0) then
				SetVar "autoclose", autoclose
				SetVar "aut_close", " checked"
				SetVar "autoclose_dis", ""
				if(CLng(RS("autoclose")) < 0) then SetVar "man_close", " checked"
			end if
		end if

		fromDate = RS("from_date")
		if fromDate <> "" and Year(fromDate) <> 1900 then
			SetVar "fromDate", FullDateTime(fromDate)
			
			toDate = RS("to_date")
			if toDate <> "" and Year(toDate) <> 1900 then
				SetVar "toDate", FullDateTime(toDate)
			end if
		else
			SetVar "start_and_end_date_and_time_checked", ""
		end if
		
		if RS("email") = "1" then 
			SetVar "emailChecked", " checked"
		end if
		
		if RS("sms") = "1" then 
			SetVar "smsChecked", " checked"
		end if
		
		if RS("desktop")="1" or (RS("email") = "" and RS("sms") = "") then
			SetVar "desktopChecked", " checked"
		end if
		
		if RS("fullscreen") = "1" then 
			SetVar "fullscreenChecked", " checked"
			SetVar "sizedChecked", ""
			SetVar "sizedDisabled", " disabled"
		end if
		
		if RS("ticker") = "1" then 
			SetVar "tickerChecked", " checked"
			SetVar "popupChecked", ""
			SetVar "allSizesDisabled", " disabled"
			SetVar "sizedDisabled", " disabled"
			
			'if ConfIFRMode = 1 then
			SetVar "allowTickerContentEnabled", " "
			
			
	       ' else
	        'SetVar "allowTickerContentEnabled", " "
	       ' end if
	    else
	        if ConfIFRMode = 1 then
	        SetVar "allowTickerContentEnabled", " disabled" 
		    else
	        SetVar "allowTickerContenEnabled", " "
	        end if
		end if
		
		if ConfIFRMode = 1 then
		    
		    allowContent = RS("rss_allow_content")
		    
		    if( allowContent = "1") then
		        SetVar "allowTickerContentChecked", " checked" 
		        SetVar "allowTickerContentValue", "1" 
		     else
		        SetVar "allowTickerContentChecked", " " 
		        SetVar "allowTickerContentValue", "0" 		     
		    end if
		    
		end if
		
		
		
		if RS("alert_width") <> "" then 
			SetVar "alert_width", RS("alert_width")
		end if
		
		if RS("alert_height") <> "" then 
			SetVar "alert_height", RS("alert_height")
		end if
		
		SetHtmlVar "email_sender", RS("email_sender")&""
		
		if RS("template_id") <> "" then
			template = RS("template_id")
		end if
	end if
	RS.Close
end if

SetVar "lifetime_control", GetLifetimeControl(lifetime, "lifetime")
if lifetime <> "" and lifetime <> "0" then
	SetVar "lifetimemode", " checked"
else
	SetVar "lifetimemode", ""
end if

SetVar "TemplatesSelect", ""
if gid = 0 or templates_arr(3) <> "" then
	SetVar "TemplatesList", ""
	Set RS = Conn.Execute("SELECT id, name FROM templates")
	do while not RS.EOF
		SetVar "template_id", RS("id")
		SetHtmlVar "template_name", RS("name")
		if CLng(template) = CLng(RS("id")) or (template = "" and CLng(RS("id")) = 1) then
			SetVar "template_selected", " selected"
		else
			SetVar "template_selected", ""
		end if
		Parse "TemplatesList", True
		RS.MoveNext
	loop
	Parse "TemplatesSelect", False
end if
'-------------------------------
' Step through each form
'-------------------------------
Header_Show
Footer_Show
'-------------------------------
' Process page templates
'-------------------------------
Parse "Header", False
Parse "Footer", False
Parse "RSSHtml", False
Parse "main", False
'-------------------------------
' Output the page to the browser
'-------------------------------

Response.write PrintVar("main")

' index Show End

'-------------------------------
' Destroy all object variables
'-------------------------------

' index Close Event begin
' index Close Event End

UnloadTemplate
'===============================

else

Login(Request("intErrorCode"))

end if

'===============================
' Display Grid Form
'-------------------------------
%><!-- #include file="db_conn_close.asp" -->