<%
RSS = 1
rssProxy="%rssProxy%" ' "0" - System predefined, "1" - Direct (without proxy), "2" - Use proxy
rssProxyServer="%rssProxyServer%" ' The name of a proxy server or a list of proxy server names. For example: localhost:8080
rssProxyBypassList="%rssProxyBypassList%" ' The list of locally known host names or IP addresses for which you want to permit bypass of the proxy server.

rssProxyUseAuth="%rssProxyUseAuth%" ' "0" - Don't use, "1" - Use proxy authorization
rssProxyUser="%rssProxyUser%"
rssProxyPass="%rssProxyPass%"
%>