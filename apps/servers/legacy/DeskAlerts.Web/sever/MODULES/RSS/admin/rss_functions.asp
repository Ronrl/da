<!-- #include file="lang.asp" -->
<!-- #include file="md5.asp" -->
<%
function isStoredNews(newsHash,storedHashes)
	for hashIndex = Lbound(storedHashes) to Ubound(storedHashes)
		if storedHashes(hashIndex) = newsHash then
			isStoredNews = true
			exit function
		end if
	next
	isStoredNews = false
end function

function writeLineToRSSLog(str)
	if ConfEnableRSSLogs = 1 then
		if not isObject(fs) then
			set fs=Server.CreateObject("Scripting.FileSystemObject")
		end if
		if isEmpty(logFile) then
			fname = Server.MapPath("logs\RSS-" & Year(now_db) &"-"& TwoDigits(Month(now_db)) &"-"& TwoDigits(Day(now_db)) &"_"& TwoDigits(Hour(now_db)) &"-"& TwoDigits(Minute(now_db)) &"-"& TwoDigits(Second(now_db)) &".log")
			if (InStr(fname,"\admin\logs\")=0) then
				fname = Replace(fname,"\logs\","\admin\logs\")
			end if
			set logFile = fs.OpenTextFile(fname, 8, True, TristateTrue)
		end if
		logFile.WriteLine str
	end if
End function

function getFeedData(URLToRSS)
	hashStr = ""
	ErrorMessage = ""
	Set RSSFeedsDictionary=Server.CreateObject("Scripting.Dictionary")
	
	' ================================================
	Set xmlHttp = Server.CreateObject("MSXML2.ServerXMLHTTP.6.0")
	On error resume next
	Err.Clear
	
	'Dim fso, tf
    'Set fso = CreateObject("Scripting.FileSystemObject")
    'path = Server.MapPath("logs/log.inf")
    'Set tf = fso.OpenTextFile(path, 8, True)
    'tf.WriteLine("RSS LOG") 

    if rssProxy <> "" and rssProxy <> "%rssProxy%" then
		if rssProxyBypassList <> "" and rssProxyBypassList <> "%rssProxyBypassList%" and _
			rssProxyServer <> "" and rssProxyServer <> "%rssProxyServer%" _
		then
			xmlHttp.setProxy rssProxy, rssProxyServer, rssProxyBypassList
		elseif rssProxyServer <> "" and rssProxyServer <> "%rssProxyServer%" then
			xmlHttp.setProxy rssProxy, rssProxyServer
		else
			xmlHttp.setProxy rssProxy
		end if
	end if



	if rssProxyUseAuth = "1" then
			xmlHttp.setProxyCredentials rssProxyUser, rssProxyPass
	end if


	xmlHttp.Open "GET", URLToRSS, false
	if Err.Number <> 0 then
	    'tf.WriteLine("Status = " & "ERROR OPEN URL To RSS")        
		ErrorMessage = LNG_NOT_VALID_FEED
	end if 
	xmlHttp.Send()
	if Err.Number <> 0 then
	    'tf.WriteLine("Status = " & "ERROR SEND URL To RSS")        
	    'tf.WriteLine("Error Number = " & Cstr(xmlHttp.status)) 
		ErrorMessage = LNG_NOT_VALID_FEED
	elseif xmlHttp.status <> 200 then
	    'tf.WriteLine("Status = " & "ERROR RECIEVE DATA FROM RSS") 
		ErrorMessage = LNG_NOT_VALID_FEED
	end if 
	set RSSXml = xmlHttp.ResponseStream ' for avoid encoding issues
	if xmlHttp.ResponseText = "" then
	    'tf.WriteLine("Status = " & "ERROR EMPTY DATA RECIEVED") 
		ErrorMessage = LNG_NOT_VALID_FEED
	end if
	on error goto 0
    
    if ErrorMessage = "" then
        'tf.WriteLine("Status = " & "SUCCESS") 
    end if
    'tf.Close
    
	if ErrorMessage = "" and VarType(RSSXml) = 13 then
		Set xmlDOM = Server.CreateObject("Msxml2.DOMDocument.6.0")
		xmlDOM.async = False
		xmlDOM.validateOnParse = False
		xmlDom.resolveExternals = False
		xmlDOM.SetProperty "SelectionNamespaces", "xmlns:media='http://search.yahoo.com/mrss/'"
		xmlDOM.SetProperty "ProhibitDTD", False

		on error resume next
		Err.Clear
		isParsed = xmlDOM.Load(RSSXml)
		if Err.Number <> 0 then
			isParsed = false
		end if
		on error goto 0

		if not isParsed then
			ErrorMessage = LNG_CAN_NOT_LOAD_RSS & vbCRLF & xmlDOM.parseError.reason & vbCRLF & ErrorMessage
		else
			Set RSSItems = xmlDOM.getElementsByTagName("*") ' collect all "items" from downloaded RSS
			RSSItemsCount = RSSItems.Length-1
			For index = 0 To RSSItemsCount
				Set RSSItem = RSSItems.Item(index)
				if StrComp(RSSItem.tagName, "item", 1) = 0 or StrComp(RSSItem.tagName, "entry", 1) = 0 then
					Set RSSFeedDic = Server.CreateObject("Scripting.Dictionary")
					RSSLink = ""
					RSSTitle = ""
					RSSDescription = ""
					RSSCommentsLink = ""
					RSSCategory = ""
					RSSMediaUrl = ""
					RSSMediaType = ""
					RSSMediaWidth = "100%"
					RSSMediaHeight = "100%"
					RSSMediaTitle = ""
					for each child in RSSItem.childNodes
						select case lcase(child.nodeName)
							case "title"
								RSSTitle = child.text
							case "link"
								if child.Attributes.length>0 then
									RSSLink = child.GetAttribute("href")
									if(RSSLink <> "") then
										if child.GetAttribute("rel") <> "alternate" then
											RSSLink = ""
										end if
									end if
								end if ' if has attributes
								if RSSLink = "" then
									RSSlink = child.text
								end if
							case "description"
								RSSDescription = child.text
							case "content" ' atom format
								RSSDescription = child.text
							case "summary" ' atom format
								RSSDescription = child.text
							case "published"' atom format
								RSSDate = child.text
							case "pubdate"
								RSSDate = child.text
							case "comments"
								RSSCommentsLink = child.text
							case "category"
								Set  CategoryItems = RSSItem.getElementsByTagName("category")
								RSSCategory = ""
								for each categoryitem in CategoryItems
									if RSSCategory <> "" then
										RSSCategory = RSSCategory & ", "
									end if
									RSSCategory = RSSCategory & categoryitem.text
								next
							case else
								if not RSSFeedDic.Exists("custom_"&child.nodeName) then
									RSSFeedDic.Add "custom_"&child.nodeName, child.text
								end if
						end select
					next
					
					Set RSSMediaItems = RSSItem.selectNodes("media:*")
			
					RSSMediaItemsCount =  RSSMediaItems.Length-1
					For mediaIndex = 0 To RSSMediaItemsCount
						Set RSSMediaItem = RSSMediaItems.Item(mediaIndex)
						select case lcase(RSSMediaItem.nodeName)
							case "media:content"
								RSSMediaType = RSSMediaItem.getAttribute("type")
								RSSMediaUrl = RSSMediaItem.getAttribute("url")
								RSSMediaWidth = RSSMediaItem.getAttribute("width")
								RSSMediaHeight = RSSMediaItem.getAttribute("height")
							case "media:title"
								RSSMediaTitle = RSSMediaItem.text
						end select
					next
					
					if RSSMediaUrl <> "" AND InStr(1,RSSDescription,RSSMediaUrl,1) then
						if (InStr(1,RSSMediaType,"video",1) = 1) then
							if LCase(Right(RSSMediaUrl,4)) = ".flv" then
								src = alerts_folder + "admin/jscripts/tiny_mce/plugins/media/img/flv_player.swf?flvToPlay=" & RSSMediaUrl & "&autoStart=false"
								RSSDescription = "<object codebase=""http://active.macromedia.com/flash5/cabs/swflash.cab#version=5,0,0,0"" classid=""clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"" width=""" & RSSMediaWidth & """ height=""" & RSSMediaHeight & """><param name=""movie"" value=""" & src & """/><param name=""width"" value=""" & RSSMediaWidth & """/><param name=""height"" value=""" & RSSMediaHeight & """/><embed type=""application/x-shockwave-flash"" src=""" & src & """ width=""" & RSSMediaWidth & """ height=""" & RSSMediaHeight & """></embed></object>"
							else
								RSSDescription = "<OBJECT  ID='mediaPlayer' width='" & RSSMediaWidth & "' height='" & RSSMediaHeight & "'  CLASSID='CLSID:6BF52A52-394A-11d3-B153-00C04F79FAA6' CODEBASE='http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=6,4,5,715' STANDBY='Loading Microsoft Windows Media Player components...'  TYPE='application/x-oleobject'><PARAM NAME='url' VALUE='" & RSSMediaUrl &"'><PARAM NAME='autoStart' VALUE='true'><PARAM NAME='showControls' VALUE='true'><PARAM NAME='TransparentatStart' VALUE='false'><PARAM NAME='AnimationatStart' VALUE='true'><PARAM NAME='StretchToFit' VALUE='true'><PARAM NAME='PlayCount' VALUE='999999999'><PARAM NAME='loop' VALUE='true'><EMBED src='" & RSSMediaUrl & "' type='application/x-mplayer2' name='mediaPlayer' autostart='1' showcontrols='1' showstatusbar='1' autorewind='1' width='" & RSSMediaWidth & "' height='" & RSSMediaHeight & "' playcount='true' loop='true'></EMBED></OBJECT>"	  
							end if
						else
							RSSDescription = "<img src='"&RSSMediaUrl&"' width='" & RSSMediaWidth & "' height='" & RSSMediaHeight & "'/>"
						end if 
					end if
					
					if RSSTitle <> "" or RSSDescription <> "" then

						if hashStr <> "" then
							hashStr = hashStr & ","
						end if
						hash = md5(md5(RSSTitle) & md5(RSSDescription))
						hashStr = hashStr & hash

						RSSFeedDic.Add "title", RSSTitle
						RSSFeedDic.Add "description", RSSDescription
						RSSFeedDic.Add "url", RSSLink
						RSSFeedDic.Add "date", RSSDate
						RSSFeedDic.Add "category", RSSCategory
						RSSFeedDic.Add "comments", RSSCommentsLink
						RSSFeedDic.Add "deskalerts_hash", hash
						RSSFeedsDictionary.Add index, RSSFeedDic
					end if
				end if
			next
		end if 'parse
	end if 'load
	getFeedData = Array(ErrorMessage,RSSFeedsDictionary,hashStr)
end function
%>