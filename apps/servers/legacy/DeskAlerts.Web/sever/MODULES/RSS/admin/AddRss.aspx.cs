﻿using System;
using System.Collections.Generic;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlertsDotNet.Pages;

namespace DeskAlertsDotNet.sever.MODULES.RSS.admin
{
    public partial class AddRss : DeskAlertsBasePage
    {
        private const int RssApprovedStatusCode = 1;
        private const int DefaultTickerSpeed = 4;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            Dictionary<string, object> rssDictionary = new Dictionary<string, object>();

            string title = Request["title"];
            rssDictionary.Add("title", title);

            string rssTemplate =
                @"|2880|<table style=""margin:0px; padding:0px; border-collapse: collapse; border:0px"">
                    <tr style=""margin:0px; padding:0px; border:0px"">
	                    <td style=""margin:0px; padding:0px; border:0px; text-align:right"">
		                    <a href=""{url}"" target=""_blank"">{more}</a>
	                    </td>
                    </tr>
                    <tr style=""margin:0px; padding:0px; border:0px"">
	                    <td style=""margin:0px; padding:0px; border:0px"">
		                    {description}
	                    </td>
                    </tr>
                    <tr style=""margin:0px; padding:0px; border:0px"">
	                    <td style=""margin:0px; padding:0px; border:0px; text-align:right"">
		                    <a href=""{url}"" target=""_blank"">{more}</a>
	                    </td>
                    </tr>
                    </table>";

            string link = Request["rssFeed"];
            string feedHash = RssWorker.GetFeedData(link)["HASH"].ToString();

            string alertText = string.Format("{0}|{1}{2}", link, feedHash, rssTemplate);
            rssDictionary.Add("alert_text", alertText);

            string toDateStr = "01.01.1900 00:00:00";
            string fromDateStr = "01.01.1900 00:00:00";
            if (!string.IsNullOrEmpty(Request["lifetimemode"]))
            {
                string lifeTimeValueStr = Request["lifetime"];
                string lifeTimeFactorStr = Request["lifetimeFactor"];

                int lifeTimeValue = 0;
                int lifeTimeFactor = 0;

                if (int.TryParse(lifeTimeFactorStr, out lifeTimeFactor) &&
                    int.TryParse(lifeTimeValueStr, out lifeTimeValue))
                {
                    var fromDate = DateTime.Now;
                    fromDateStr = DateTimeConverter.ToDbDateTime(fromDate);
                    var toDate = lifeTimeValue == 0 ? fromDate.AddYears(100) : fromDate.AddMinutes(lifeTimeValue * lifeTimeFactor);
                    toDateStr = DateTimeConverter.ToDbDateTime(toDate);
                }

                rssDictionary.Add("lifetime", 1);
                rssDictionary.Add("schedule", 0);
            }
            else if (!string.IsNullOrEmpty(Request["startAndEndDateAndTimeCheckbox"]))
            {
                fromDateStr = DateTimeConverter.ConvertFromUiToDb(Request["startDateAndTime"]);
                toDateStr = DateTimeConverter.ConvertFromUiToDb(Request["endDateAndTime"]);

                rssDictionary.Add("lifetime", 0);
                rssDictionary.Add("schedule", 1);
            }
            else
            {

                rssDictionary.Add("lifetime", 1);
                rssDictionary.Add("schedule", 0);
            }

            rssDictionary.Add("from_date", fromDateStr);
            rssDictionary.Add("to_date", toDateStr);
            rssDictionary.Add("create_date", DateTimeConverter.ToDbDateTime(DateTime.Now));
            int autoClose = 0;
            if (!string.IsNullOrEmpty(Request["autocloseCheckBox"]))
            {
                if (int.TryParse(Request["autoclose"], out autoClose))
                {
                    autoClose *= 60;

                    if (!string.IsNullOrEmpty(Request["manualCloseCheckBox"]))
                        autoClose = -autoClose;
                }
            }

            rssDictionary.Add("autoclose", autoClose);

            if (!string.IsNullOrEmpty(Request["desktopCheck"]))
                rssDictionary.Add("desktop", 1);

            if (!string.IsNullOrEmpty(Request["smsCheckbox"]))
            {
                rssDictionary.Add("sms", 1);
            }

            if (!string.IsNullOrEmpty(Request["textToCallCheckbox"]))
            {
                rssDictionary.Add("text_to_call", 1);
            }

            if (!string.IsNullOrEmpty(Request["emailCheck"]))
            {
                rssDictionary.Add("email", 1);

                if (!string.IsNullOrEmpty(Request["emailSender"]))
                    rssDictionary.Add("email_sender", Request["emailSender"]);
            }

            if (Request["ticker"] == "1")
            {
                rssDictionary.Add("ticker", 1);
                rssDictionary.Add("ticker_speed", DefaultTickerSpeed);
            }
            else
            {
                rssDictionary.Add("ticker", 0);

                if (Request["fullscreen"] != "1")
                {
                    rssDictionary.Add("fullscreen", 6);
                    rssDictionary.Add("alert_width", Request["alertWidth"]);
                    rssDictionary.Add("alert_height", Request["alertHeight"]);
                }
                else
                {
                    rssDictionary.Add("fullscreen", 1);
                }
            }


            string rejectedEdit = "";

            if (!string.IsNullOrEmpty(Request["rejectedEdit"]))
                rejectedEdit = Request["rejectedEdit"];

            string shouldApprove = "";
            if (!string.IsNullOrEmpty(Request["shouldApprove"]))
                shouldApprove = Request["shouldApprove"];

            string dupAlertId = "";

            if (!string.IsNullOrEmpty(Request["alertId"]))
                dupAlertId = Request["alertId"];

            rssDictionary.Add("approve_status", RssApprovedStatusCode);
            rssDictionary.Add("class", 4);
            rssDictionary.Add("type", "D");
            rssDictionary.Add("sender_id", curUser.Id);
            rssDictionary.Add("schedule_type", 1);
            var campaignId = Request["campaignSelect"];
            if (!string.IsNullOrEmpty(campaignId))
            {
                rssDictionary.Add("campaign_id", campaignId);
            }

            int alertId = dbMgr.Insert("alerts", rssDictionary, "id");
            var redirectUrl = string.Empty;
            if (string.IsNullOrEmpty(campaignId))
            {

                redirectUrl = "RecipientsSelection.aspx?id=" + alertId
                                                                + "&desktop=1&return_page=RssList.aspx&campaign_id="
                                                                + campaignId + "&rejectedEdit=" + rejectedEdit
                                                                + "&shouldApprove=" + shouldApprove + "&dup_id="
                                                                + dupAlertId
                                                                + "&device=2"; // 2 - All devices
            }
            else
            {
                redirectUrl = $"RecipientsSelection.aspx?id={alertId}&desktop=1&return_page=CampaignDetails.aspx?campaignId={campaignId}";
            }



            if (rssDictionary.ContainsKey("sms") && rssDictionary["sms"].ToString().Equals("1"))
            {
                redirectUrl += "&sms=1";
            }

            if (rssDictionary.ContainsKey("email") && rssDictionary["email"].ToString().Equals("1"))
            {
                redirectUrl += "&email=1";
            }

            if (rssDictionary.ContainsKey("text_to_call") && rssDictionary["text_to_call"].ToString().Equals("1"))
            {
                redirectUrl += "&textToCall=1";
            }

            if (!string.IsNullOrEmpty(Request["edit"]) && Request["edit"].Equals("1"))
            {
                redirectUrl += "&edit=1";
            }
            Response.Redirect(redirectUrl);

            //Add your main code here
        }
    }
}