<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
check_session()
%>
<!-- #include file="Common.asp" -->
<!-- #include file="header.asp" -->
<!-- #include file="footer.asp" -->


<%
'On Error Resume Next
' index CustomIncludes End
'-------------------------------

'===============================
' Save Page and File Name available into variables
'-------------------------------
Dim sFileName
Dim sTemplateFileName
Dim sPaginateFileName
Dim intSessionUserId

sFileName = "rss_list.asp"
sTemplateFileName = "rss_list.html"
sPaginateFileName = "paginate.html"
sEditFileName = "rss_edit.asp"
'===============================
  

intSessionUserId = Session("uid")
uid=intSessionUserId

if (intSessionUserId <> "") then


' index Show begin

'===============================
' Display page
'-------------------------------
' Load HTML template for this page
'-------------------------------
LoadTemplate alerts_dir & "\" & templateFolder & "\" & sTemplateFileName, "main"
SetVar "DListRSS", ""
'-------------------------------
' Load HTML template of Header and Footer
'-------------------------------
sHeaderFileName=Replace(sHeaderFileName,"header.html","header-old.html")
LoadTemplate sHeaderFileName, "Header"
LoadTemplate sFooterFileName, "Footer"

'-------------------------------

LoadTemplate sMenuFileName, "Menu"


if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
else 
	offset = 0
end if	

if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
else 
	limit=25
end if

alertType = "S"
if Request("draft") = "1" then
	alertType = "D"
	SetVar "pageHeader",LNG_DRAFT
else
	SetVar "pageHeader",LNG_CURRENT
end if

if Request("parent_id") <> "" then
	parent_id_where = " AND parent_id = " & CLng(Request("parent_id"))
else
	parent_id_where = ""
end if

if Request("sent") = "1" then
	classid = "5"
else
	classid = "4"
end if

if Request("parent_id") <> "" then
	parent_id_where = " AND parent_id = " & CLng(Request("parent_id"))
else
	parent_id_where = ""
end if

SetVar "RSSCurListLink", sFileName
SetVar "RSSCurListStr", LNG_RSS &" - "& LNG_CURRENT

SetVar "RSSDraftListLink", sFileName &"?draft=1"
SetVar "RSSDraftListStr", LNG_RSS &" - "& LNG_DRAFT

SetVar "RSSSentListLink", sFileName &"?sent=1"
SetVar "RSSSentStr", LNG_VIEW_RSS_ALERTS_LIST


SetVar "draftSendLink", "alert_users.asp?return_page=" & sFileName & "&header="&LNG_RSS&"&title="&LNG_ADD_RSS
SetVar "ExpiredStr", LNG_RSS_FEED_IS_EXPIRED
SetVar "ExpiredEditStr", LNG_EDIT
SetVar "ExpireEditLink", "rss_edit.asp"
SetVar "curTime", now_db

SetVar "type", Request("type")
SetVar "limit", limit

'--- rights ---
set rights = getRights(uid, true, false, true, "rss_val")
gid = rights("gid")
gviewall = rights("gviewall")
gviewlist = rights("gviewlist")
rss_arr = rights("rss_val")
'--------------

'---------------------------------

if(Request("sortby") <> "") then 
	sortby = Request("sortby")
else 
	sortby = "sent_date desc"
end if

if gviewall <> 1 then
	SQL = "SELECT COUNT(1) as mycnt, 1 as mycnt1 FROM alerts WHERE param_temp_id IS NULL AND class = "& classid &" AND type='"& alertType &"' AND sender_id IN ("& Join(gviewlist,",") &")"& parent_id_where
	Set RS = Conn.Execute(SQL)
else
	SQL = "SELECT COUNT(1) as mycnt, 1 as mycnt1 FROM alerts WHERE param_temp_id IS NULL AND class = "& classid &" AND type='"& alertType &"'"& parent_id_where
	Set RS = Conn.Execute(SQL)
end if

cnt=0
do while Not RS.EOF
	cnt = cnt + Clng(RS("mycnt"))
	RS.MoveNext
Loop
RS.Close
j=cnt/limit



if(DA=1) then
else
	SetVar "strLogout", "Logout"
end if

SetVar "strPageName", LNG_RSS
SetVar "strMenu", ""
SetVar "RSSNameStr", LNG_TITLE
SetVar "RSSCreationDateStr", LNG_CREATION_DATE
SetVar "RSSScheduledStr", LNG_SCHEDULED
SetVar "RSSSenderStr", LNG_SENT_BY
SetVar "RSSActionStr", LNG_ACTIONS
if rss_arr(2) = "checked" then
	SetVar "StartDeleteForm", "<A href='#' class='delete_button' onclick=""javascript: return LINKCLICK('"&linkclick_str&"');"">"&LNG_DELETE&"</a><br/><br/><form action='delete_objects.asp' name='delete_objs' id='delete_objs' method='post'><input type='hidden' name='objType' value='alerts'/><input type='hidden' name='child' value='1'/>"
	SetVar "EndDeleteForm", "<input type='hidden' name='back' value='"& sFileName &"?draft="& Request("draft") &"&sent="& Request("sent") &"&parent_id="& Request("parent_id") &"'></form><br/><A href='#' class='delete_button' onclick=""javascript: return LINKCLICK('"&linkclick_str&"');"">"&LNG_DELETE&"</a><br><br>"
else
	SetVar "StartDeleteForm", ""
	SetVar "EndDeleteForm", ""
end if
SetVar "RSSAddButton", ""
if classid = "5" then
	SetVar "RSSAddLink", sFileName
	SetVar "RSSAddStr", LNG_BACK
	Parse "RSSAddButton", False
elseif rss_arr(0) = "checked" then
	SetVar "RSSAddLink", sEditFileName
	SetVar "RSSAddStr", LNG_ADD_RSS
	Parse "RSSAddButton", False
end if

Set enabled = Conn.execute("SELECT val FROM settings WHERE name = 'ConfAllowApprove'") 
if APPROVE = 1 and  enabled("val") = "1" then		
    
    collumnHtml = "<td class=""table_title"">" & LNG_APPROVE_STATUS & "</td>"
    SetVar "RSS_Status_Str", collumnHtml
    
else
    SetVar "RSS_Status_Str", ""

end if 


if(cnt>0) then

	page = sFileName &"?draft="& Request("draft") &"&sent="& Request("sent") &"&"
	name = LNG_RSS
	
	SetVar "RSSNameStr", sorting(LNG_NAME,"title", sortby, offset, limit, page)
	SetVar "RSSCreationDateStr", sorting(LNG_CREATION_DATE,"create_date", sortby, offset, limit, page)
	
	SetVar "paging", make_pages(offset, cnt, limit, page, name, sortby) 

'show main table
	if gviewall <> 1 then
		SQL = "SELECT alerts.id, alert_text, title, approve_status, create_date, schedule, recurrence, schedule_type, from_date, to_date, u.name FROM alerts, users u WHERE sender_id = u.id AND param_temp_id IS NULL AND class = "& classid &" AND type='"& alertType &"' AND sender_id IN ("& Join(gviewlist,",") &")"& parent_id_where &" AND campaign_id = -1 ORDER BY "&sortby
		Set RS = Conn.Execute(SQL)
	else
		SQL = "SELECT alerts.id, alert_text, title, approve_status, create_date, schedule, recurrence, schedule_type, from_date, to_date, u.name FROM alerts, users u WHERE sender_id = u.id AND param_temp_id IS NULL AND class = "& classid &" AND type='"& alertType &"'"& parent_id_where &" AND campaign_id = -1 ORDER BY "&sortby
		Set RS = Conn.Execute(SQL)
	end if
	num=offset
	if(Not RS.EOF) then RS.Move(offset) end if
	Do While Not RS.EOF
		num=num+1
		if((offset+limit) < num) then Exit Do end if
		if(num > offset AND offset+limit >= num) then

			schedule_type=RS("schedule_type")
		
			if classid <> "5" and Request("draft") <> "1" then
				SetVar "RSSName", "<a href=""" & sFileName &"?sent=1&parent_id="& RS("id") &""">" & HtmlEncode(RS("title")) & "</a>"
			else
				SetVar "RSSName", RS("title")
			end if
			
			
			approve_status = RS("approve_status")
		            
		            approveString = LNG_APPROVED
		            
		            Select Case approve_status
		                Case "0"
		                    approveString = LNG_PENDING_APPROVAL
		                Case "1"
		                    approveString = LNG_APPROVED
		                Case "2"
		                    approveString = LNG_REJECTED
		            
		      End Select
		        Set enabled = Conn.execute("SELECT val FROM settings WHERE name = 'ConfAllowApprove'") 
                if APPROVE = 1 and  enabled("val") = "1" then	
		            SetVar "RSS_Status", "<td>" & approveString &  "</td>"
		        else
		            SetVar "RSS_Status", ""
		        end if
			SetVar "RSSCreationDate", RS("create_date")
			SetVar "RSSScheduled", getScheduledText(RS("schedule"), RS("from_date"), RS("to_date"))
			SetVar "RSSSender", RS("name")
			
			actionsHtml = ""
			if classid = "4" then
				if (gid = 0 OR gid = 1) AND alertType="S" then
					if gid = 0 or rss_arr(0) <> "" then
						actionsHtml = "<a href=""" & sEditFileName & "?id="&RS("id")&"""><img src=""images/action_icons/duplicate.png"" alt="""&LNG_RESEND_RSS&""" title="""&LNG_RESEND_RSS&""" width=""16"" height=""16"" border=""0"" hspace=5></a>"
					end if
					if gid = 0 or rss_arr(5) <> "" then
						if schedule_type = "1" then
							actionsHtml = actionsHtml & "<a href=""change_schedule_type.asp?id="&RS("id")&"&type=0&child=1&return_page="& Server.URLEncode(page) &"""><img src=""images/action_icons/stop.png"" alt="""&LNG_STOP_SCHEDULED_RSS&""" title="""&LNG_STOP_SCHEDULED_RSS&""" width=""16"" height=""16"" border=""0"" hspace=5></a>"
						else
							actionsHtml = actionsHtml & "<a href=""change_schedule_type.asp?id="&RS("id")&"&type=1&child=1&return_page="& Server.URLEncode(page) &"""><img src=""images/action_icons/start.png"" alt="""&LNG_START_SCHEDULED_RSS&""" title="""&LNG_START_SCHEDULED_RSS&""" width=""16"" height=""16"" border=""0"" hspace=5></a>"
						end if
					end if
					'actionsHtml = actionsHtml & "<a href=""" & sFileName &"?sent=1&parent_id="& RS("id") &"""><img src=""images/read.gif"" alt="""&LNG_VIEW_RSS_ALERTS_LIST&""" title="""&LNG_VIEW_RSS_ALERTS_LIST&""" width=""16"" height=""16"" border=""0"" hspace=5></a>"
				end if
				
				if (gid = 0 OR gid = 1) AND alertType="D" then
					if gid = 0 or rss_arr(3) <> "" then
								
						actionsHtml = "<a href="""
						
						jsSend = 0
						if(Not IsNull(RS("schedule"))) then
							schedule = RS("schedule")
							if(schedule="1") then
								start_date = RS("from_date")
								end_date = RS("to_date")
								if start_date <> "" and end_date <> "" then
									if year(end_date) <> 1900 then
										if (MyMod(DateDiff("s", start_date, end_date),60)) <> 1 then 'if lifetime - update time
											actionsHtml = actionsHtml & "javascript: sendAlert('" & RS("id") & "','" & end_date & "');"
											jsSend = 1 
										end if
									end if
								end if
							end if
						end if
						if (jsSend = 0) then
							actionsHtml = actionsHtml & "alert_users.asp?id=" & RS("id")&"&return_page="& sFileName &"&header="& LNG_RSS &"&title="& LNG_ADD_RSS
						end if
						
						actionsHtml = actionsHtml & """><img src=""images/action_icons/send.png"" alt="""&LNG_SEND&""" title="""&LNG_SEND&""" width=""16"" height=""16"" border=""0"" hspace=5></a>"
					end if
					if gid = 0 or rss_arr(1) <> "" then
						actionsHtml = actionsHtml & "<a href=""" & sEditFileName & "?id="&RS("id")&"""><img src=""images/action_icons/edit.png"" alt="""&LNG_EDIT&""" title="""&LNG_EDIT&""" width=""16"" height=""16"" border=""0"" hspace=5></a>"
					end if
				end if
			elseif classid = "5" then
				actionsHtml = "<a href=""javascript: showAlertPreview(" & CStr(RS("id")) & ")"" ><img src=""images/action_icons/preview.png"" alt="""& LNG_PREVIEW &""" title="""& LNG_PREVIEW &""" width=""16"" height=""16"" border=""0"" hspace=""5""/></a>"
				if (gid = 0 OR gid = 1) AND alertType="S" AND (gid = 0 or rss_arr(5) <> "") then
					if schedule_type = "1" then
						actionsHtml = actionsHtml & "<a href=""change_schedule_type.asp?id="&RS("id")&"&type=0&child=1&return_page="& Server.URLEncode(page) &"""><img src=""images/action_icons/stop.png"" alt="""&LNG_STOP_SCHEDULED_RSS&""" title="""&LNG_STOP_SCHEDULED_RSS&""" width=""16"" height=""16"" border=""0"" hspace=5></a>"
					else
						actionsHtml = actionsHtml & "<a href=""change_schedule_type.asp?id="&RS("id")&"&type=1&child=1&return_page="& Server.URLEncode(page) &"""><img src=""images/action_icons/start.png"" alt="""&LNG_START_SCHEDULED_RSS&""" title="""&LNG_START_SCHEDULED_RSS&""" width=""16"" height=""16"" border=""0"" hspace=5></a>"
					end if
				end if
			end if
			
			SetVar "RSSActions", actionsHtml
			SetVar "RSSId", RS("id")

			Parse "DListRSS", True
	end if

	RS.MoveNext
	Loop
	RS.Close

else

'Response.Write "<center><b>There are no alerts.</b></center>"

end if


'-------------


'-------------------------------
' Step through each form
'-------------------------------
Header_Show
Footer_Show
'-------------------------------
' Process page templates
'-------------------------------
Parse "Header", False
Parse "Footer", False
Parse "main", False
'-------------------------------
' Output the page to the browser
'-------------------------------
Response.write PrintVar("main")

' index Show End

'-------------------------------
' Destroy all object variables
'-------------------------------

' index Close Event begin
' index Close Event End

UnloadTemplate
'===============================

else

Login(Request("intErrorCode"))

end if

'===============================
' Display Grid Form
'-------------------------------
%>
<!-- #include file="db_conn_close.asp" -->