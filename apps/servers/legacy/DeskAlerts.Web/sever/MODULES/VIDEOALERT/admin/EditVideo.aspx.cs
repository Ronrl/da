﻿using System;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using Castle.MicroKernel.Lifestyle;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Extensions.Enums;
using DeskAlerts.Server.Utils.Interfaces;
using DeskAlerts.Server.Utils;
using DeskAlertsDotNet.Application;
using DeskAlertsDotNet.Controls;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Models.Permission;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Utils.Factories;
using Resources;

namespace DeskAlertsDotNet.Pages
{
    public partial class EditVideo : DeskAlertsBasePage
    {
        protected string VideoUploadFolderPath = Config.Data.GetString("alertsDir") + "/admin/images/upload_video/";
        protected string SkinsJson;
        protected string IsInstantVal = string.Empty;
        protected int ShouldCheckSize;
        protected string DigitalSignage;
        protected string Description = string.Empty;
        protected string VideoLinkString;
        protected string VideoName;
        protected string MaxFileSize;
        protected string Content;
        protected string EmbedContent;
        protected int ActiveTab;
        private Int32 MinAlertHeight = 290;
        private Int32 MinAlertWidth = 340;

        private readonly ISkinService _skinService;

        public EditVideo()
        {
            using (Global.Container.BeginScope())
            {
                _skinService = Global.Container.Resolve<ISkinService>();
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            Content = videoLink.Value;
            EmbedContent = link_embed.Value;
            SkinsJson = _skinService.GetSkinsFromDbByPolicyGuidList(curUser.Policy.SkinList);

            if (curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanResize)
            {
                ShouldCheckSize = 1;
            }

            if (!string.IsNullOrEmpty(Request["webplugin"]))
            {
                DigitalSignage = Request["webplugin"];
            }

            if (System.Configuration.ConfigurationManager.GetSection("system.web/httpRuntime") is HttpRuntimeSection
                runTime)
            {
                MaxFileSize = (runTime.MaxRequestLength / 1024).ToString();
            }

            if (IsInstantVal == "1")
            {
                saveNextButton.Text = resources.LNG_NEXT;
                bottomSaveNextButton.Text = resources.LNG_NEXT;
            }
            else
            {
                saveNextButton.Text = resources.LNG_SAVE_AND_NEXT;
                bottomSaveNextButton.Text = resources.LNG_SAVE_AND_NEXT;
            }

            saveButton.Text = resources.LNG_SAVE;
            bottomSaveButton.Text = resources.LNG_SAVE;

            saveNextButton.Click += saveNextButton_Click;
            saveButton.Click += saveButton_Click;
            bottomSaveButton.Click += saveButton_Click;
            bottomSaveNextButton.Click += saveNextButton_Click;

            scriptManager.RegisterAsyncPostBackControl(saveNextButton);
            scriptManager.RegisterAsyncPostBackControl(saveButton);
            scriptManager.RegisterAsyncPostBackControl(bottomSaveButton);
            scriptManager.RegisterAsyncPostBackControl(bottomSaveNextButton);

            if (!curUser.HasPrivilege && !curUser.Policy[Policies.ALERTS].CanSend)
            {
                saveNextButton.Visible = false;
                bottomSaveNextButton.Visible = false;
            }

            lifetimeFactor.Items.Add(new ListItem(resources.LNG_MINUTES, "1"));
            lifetimeFactor.Items.Add(new ListItem(resources.LNG_HOURS, "60"));
            lifetimeFactor.Items.Add(new ListItem(resources.LNG_DAYS, "1440"));

            lifetimeFactor.SelectedIndex = 2;

            if (Settings.IsTrial)
            {
                resizable.Visible = false;
                resizableLabel.Attributes["title"] = resources.TRIAL_MODE_DISABLED_FUNCTIONALITY;
                resizableLabel.Attributes["class"] = ContentCustomClass.AdditionalDescription.GetStringValue();
            }

            LifetimeAdditionalDescription.InnerText = resources.LNG_TIME_EXPIRED_CANT_RECEIVED;
            LifeTimeHelpImage.Attributes["title"] = resources.LNG_LIFETIME_EXT_DESC;

            if (!Config.Data.HasModule(Modules.CAMPAIGN) || string.IsNullOrEmpty(Request["alert_campaign"]) || Request["alert_campaign"].Equals("-1"))
            {
                campaignLabel.Visible = false;
                campaignSelect.Value = "-1";
            }
            else
            {
                var campaignIdParam = SqlParameterFactory.Create(System.Data.DbType.String, Request["alert_campaign"], "campaignId");
                var campName = dbMgr.GetScalarByQuery<string>("SELECT name FROM campaigns WHERE id = @campaignId", campaignIdParam);
                campaignName.InnerText = campName;
                campaignSelect.Value = Request["alert_campaign"];
                saveButton.Visible = false;
                bottomSaveButton.Visible = false;
            }

            if (!string.IsNullOrEmpty(Request["id"]))
            {
                var editingAlertId = Convert.ToInt32(Request["id"]);
                LoadVideo(editingAlertId);
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (!ValidateData())
            {
                return;
            }

            titleHidden.Value = htmlTitle.InnerText;

            SubmitForm(true);
        }

        private bool ValidateData()
        {
            var videoLinkContent = Content;
            var videoEmbedContent = EmbedContent;

            if (videoLinkContent.Length == 0 && videoEmbedContent.Length == 0)
            {
                ShowPromt(Response, resources.LNG_SELECT_FILE);
                return false;
            }

            var title = titleHidden.Value;

            if (autoCloseCheckBox.Checked)
            {
                if (autoClosebox.Value.Equals("0") || autoClosebox.Value.Length < 1)
                {
                    ShowPromt(Response, resources.LNG_EDIT_ALERT_DB_ALERT1);
                    return false;
                }
            }

            if (lifeTimeCheckBox.Checked)
            {
                if (lifetime.Value.Equals("0") || lifetime.Value.Length < 1)
                {
                    ShowPromt(Response, resources.LNG_YOU_SHOULD_ENTER_LIFETIME);
                    return false;
                }
            }

            if (title.Length > 255)
            {
                ShowPromt(Response, resources.LNG_TITLE_SHOULD_BE_LESS_THEN);
                return false;
            }

            if (title.Length == 0)
            {
                ShowPromt(Response, resources.LNG_YOU_SHOULD_ENTER_ALERT_TITLE);
                return false;
            }

            if ((Convert.ToInt32(alertWidth.Value) < MinAlertWidth) || (Convert.ToInt32(alertHeight.Value) < MinAlertHeight))
            {
                ShowPromt(Response, resources.LNG_RESOLUTION_DESC2);
                return false;
            }

            bool.TryParse(recurrenceCheckboxValue.Value, out var recurrenceCheckboxChecked);

            if (recurrenceCheckboxChecked)
            {
                var err = ((ReccurencePanel)schedulePanel).ValidateInputs();

                switch (err)
                {
                    case ReccurencePanelError.TO_DATE_LESS_THAN_FROM_DATE:
                        ShowPromt(Response, resources.LNG_REC_DATE_ERROR);
                        return false;
                    case ReccurencePanelError.TIME_VALUE_IS_EMPTY:
                        ShowPromt(Response, resources.LNG_TIME_VALUE_IS_INCORRECT);
                        return false;
                    case ReccurencePanelError.TO_DATE_LESS_THAN_CURRENT_DATETIME:
                        ShowPromt(Response, resources.LNG_TO_DATE_LESS_THAN_CURRENT_DATE);
                        return false;
                }
            }

            if (!checkBoxAutoplay.Checked && !checkBoxControls.Checked)
            {
                ShowPromt(Response, resources.YOU_MUST_CHOOSE_CONTROL_OR_AUTOPLAY);
                return false;
            }

            return true;
        }

        private void saveNextButton_Click(object sender, EventArgs e)
        {
            if (!ValidateData())
            {
                return;
            }

            if (Settings.Content["ConfEnableHtmlAlertTitle"].Equals("1"))
            {
                titleHidden.Value = htmlTitle.InnerText;
            }

            SubmitForm(false);
        }

        public void ShowPromt(HttpResponse r, string text)
        {
            ScriptManager.RegisterStartupScript(updatePanel, updatePanel.GetType(), "SHOW_PROMT", "showPrompt('" + text + "')", true);
        }

        private void SubmitForm(bool isDraft)
        {
            var isDraftStr = isDraft.ToString().ToLower();

            form1.Action = "AddAlert.aspx?isVideoAlert=1&edit=" + Request["edit"];
            form1.Method = "POST";

            ScriptManager.RegisterClientScriptBlock(this, GetType(), "SUBMIT", "submitForm(" + isDraftStr + ")", true);
        }

        private void LoadVideo(int videoId)
        {
            if (IsPostBack)
            {
                return;
            }

            alertId.Value = videoId.ToString();

            var alertSet = dbMgr.GetDataByQuery("SELECT * FROM alerts WHERE id = " + videoId);
            var alertRow = alertSet.GetRow(0);
            var title = alertRow.GetString("title");
            var currentAlertText = alertRow.GetString("alert_text");
            string videoDescription;

            if (currentAlertText.Contains("%video%="))
            {
                videoDescription = currentAlertText.Substring(0, currentAlertText.LastIndexOf("%video%=", StringComparison.Ordinal) - 1);

                var videAlertSettings = DeskAlertsUtils.ParseVideoAlertSettings(currentAlertText);

                checkBoxMute.Checked = videAlertSettings.Mute;
                checkBoxControls.Checked = videAlertSettings.Controls;
                checkBoxLoop.Checked = videAlertSettings.Loop;
                checkBoxAutoplay.Checked = videAlertSettings.Autoplay;

                var videoAlertSettings = DeskAlertsUtils.ParseVideoAlertSettings(currentAlertText);

                VideoLinkString = videoAlertSettings.Link;

                var videoNameArray = VideoLinkString.Split('/');

                VideoName = videoNameArray[videoNameArray.Length - 1];
                videoLink.Value = VideoLinkString;
            }
            else
            {
                videoDescription = currentAlertText.Substring(0, currentAlertText.LastIndexOf("<iframe", StringComparison.Ordinal) - 1);
            }

            miracleText.InnerText = videoDescription;

            if (currentAlertText.Contains("<iframe"))
            {
                var startFrameIndex = currentAlertText.IndexOf("<iframe", StringComparison.Ordinal);
                var endFrameIndex = currentAlertText.IndexOf("</iframe>", startFrameIndex, StringComparison.Ordinal);

                ActiveTab = 1;

                const int iframeClosingTagLength = 9;
                var iframeLength = endFrameIndex - startFrameIndex + iframeClosingTagLength;

                link_embed.Value = currentAlertText.Substring(startFrameIndex, iframeLength);
            }

            if (Settings.Content["ConfEnableHtmlAlertTitle"].Equals("1"))
            {
                var currentAlertHtmlTitle = title;
                var matches = Regex.Matches(currentAlertText, "<!-- html_title -->");

                if (matches.Count > 0)
                {
                    if (matches[0].Groups.Count > 0)
                    {
                        currentAlertHtmlTitle = matches[0].Groups[2].Value;
                    }
                }

                htmlTitle.InnerHtml = currentAlertHtmlTitle;
                titleHidden.Value = currentAlertHtmlTitle;
            }
            else
            {
                simpleTitle.Value = title;
            }

            if (currentAlertText.IndexOf("<!-- self-deletable -->", StringComparison.Ordinal) >= 0 && alertRow.GetInt("self_deletable") == 1)
            {
                selfDeletable.Checked = true;
            }

            if (alertRow.GetInt("urgent") == 1)
            {
                urgentCheckBox.Checked = true;
            }

            if (alertRow.GetInt("aknown") == 1)
            {
                aknown.Checked = true;
                unobtrusiveCheckbox.Disabled = true;
            }

            if (alertRow.GetInt("class") == 16)
            {
                unobtrusiveCheckbox.Checked = true;
                autoCloseCheckBox.Disabled = true;
                aknown.Disabled = true;
            }

            var fromDateTime = alertRow.GetDateTime("from_date");
            var toDateTime = alertRow.GetDateTime("to_date");
            var ticker = alertRow.GetInt("ticker");

            lifeTimeCheckBox.Checked = false;

            if (alertRow.GetInt("lifetime") == 1)
            {
                lifeTimeCheckBox.Checked = true;

                var lifeTime = (int)(toDateTime - fromDateTime).TotalMinutes;

                foreach (ListItem item in lifetimeFactor.Items)
                {
                    item.Selected = false;
                }

                for (var i = 2; i >= 0; i--)
                {
                    var listItem = lifetimeFactor.Items[i];
                    var itemValue = Convert.ToInt32(listItem.Value);
                    var lifeTimeValue = lifeTime / itemValue;

                    if (lifeTimeValue > 0)
                    {
                        listItem.Selected = true;
                        lifeTimeCheckBox.Checked = true;
                        lifetime.Value = lifeTimeValue.ToString();
                        break;
                    }
                }
            }
            else if (alertRow.GetInt("schedule") == 1)
            {
                recurrenceCheckbox.Checked = true;
                ((ReccurencePanel) schedulePanel).AlertId = videoId;
            }

            if (alertRow.GetInt("desktop") == 1)
            {
                desktopCheck.Checked = true;
            }

            FillSkinsData(alertRow);

            if (alertRow.GetInt("fullscreen") == 1)
            {
                size2.Checked = true;
            }
            else
            {
                size2.Checked = false;
                size1.Checked = true;

                if (ticker == 0)
                {
                    positionBottomRight.Checked = false;
                    positionBottomLeft.Checked = false;
                    positionCenter.Checked = false;
                    positionTopRight.Checked = false;
                    positionTopLeft.Checked = false;

                    switch (alertRow.GetInt("fullscreen"))
                    {
                        case 2:
                            {
                                positionTopLeft.Checked = true;
                                break;
                            }
                        case 3:
                            {
                                positionTopRight.Checked = true;
                                break;
                            }
                        case 4:
                            {
                                positionCenter.Checked = true;
                                break;
                            }
                        case 5:
                            {
                                positionBottomLeft.Checked = true;
                                break;
                            }
                        case 6:
                            {
                                positionBottomRight.Checked = true;
                                break;
                            }
                    }

                    alertWidth.Value = alertRow.GetString("alert_width");
                    alertHeight.Value = alertRow.GetString("alert_height");

                    if (alertRow.GetInt("resizable") == 1)
                    {
                        resizable.Checked = true;
                    }
                }
            }

            if (!Config.Data.HasModule(Modules.CAMPAIGN) || alertRow.GetInt("campaign_id") == -1)
            {
                campaignLabel.Visible = false;
                campaignSelect.Value = "-1";
            }
            else
            {
                var campaignId = alertRow.GetInt("campaign_id");
                var campName = dbMgr.GetScalarByQuery("SELECT name FROM campaigns WHERE id = " + campaignId).ToString();

                campaignName.InnerText = campName;
                campaignSelect.Value = campaignId.ToString();
            }
        }

        private void FillSkinsData(DataRow alertRow)
        {
            var skinId = "default";

            if (!alertRow.IsNull("caption_id"))
            {
                skinId = alertRow.GetString("caption_id");
            }

            var temporarySkinId = string.Equals(skinId, "default", StringComparison.CurrentCultureIgnoreCase)
                ? skinId
                : $"{{{skinId}}}";

            skinId = _skinService.CheckSkinIdInPolicy(skinId, curUser.Policy.SkinList);

            if (!string.Equals(skinId, temporarySkinId, StringComparison.CurrentCultureIgnoreCase))
            {
                Response.ShowJavaScriptAlert(resources.LNG_POLICY_CHANGE_SKINS_TO_DEFAULT, true);
            }

            skin_id.Value = skinId;
            skinPreviewImg.Src = $"skins/{skinId}/thumbnail.png";
        }
    }
}