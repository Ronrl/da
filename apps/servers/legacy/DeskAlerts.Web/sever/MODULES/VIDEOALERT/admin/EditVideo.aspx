﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditVideo.aspx.cs" Inherits="DeskAlertsDotNet.Pages.EditVideo" %>

<%@ Import Namespace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<%@ Register Src="ReccurencePanel.ascx" TagName="ReccurencePanel"
    TagPrefix="da" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />

    <link href="css/dropzone.css" rel="stylesheet" type="text/css" />
    <link href="css/style9.css" rel="stylesheet" type="text/css" />
    <link href="css/form_style.css" rel="stylesheet" type="text/css" />
    <style>
        .checkbox_div {
            padding: 1px;
        }

        .img-hint {
            width: 350px;
            height: 350px;
        }

        .DisableRecordVideo {
            visibility: hidden;
        }
    </style>

    <script type="text/javascript" src="functions.js"></script>

    <!-- TinyMCE -->

    <script type="text/javascript" src="jscripts/tiny_mce/tinymce.min.js"></script>

    <!-- /TinyMCE -->
    <script type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript" src="jscripts/shortcut.js"></script>
    <script type="text/javascript" src="jscripts/CreateAlert.js"></script>
    <script type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>
    <script type="text/javascript" src="jscripts/json2.js"></script>
    <script type="text/javascript" src="jscripts/dropzone.js"></script>

    <script type="text/javascript">
        Dropzone.autoDiscover = false;
        Dropzone.options.videoUpload = {
            addRemoveLinks: 'dictCancelUploadConfirmation ',
            maxFiles: 1,
        };


        function check_size() {

            var shouldCheckSize = <% =ShouldCheckSize%>;

            if (shouldCheckSize == 0)
                return;

            var elem = document.getElementById('tickerTypeTab');
            if (elem) {
                var disabled = elem.checked;
                var el = document.getElementById('size_div');
                toggleDisabled(el, disabled);
                el = document.getElementById('size_message');
                toggleDisabled(el, disabled);
            }
            check_message();
        }


        function check_message() {

            var elem = document.getElementById('size2');
            var msg = document.getElementById('size_message');
            var al_width = document.getElementById('alertWidth');
            var al_height = document.getElementById('alertHeight');
            var resizable = document.getElementById('resizable');
            var position_fieldset = document.getElementById('position_fieldset');
            var webplugin = '<%=DigitalSignage%>';
            if (elem && al_width && al_height) {
                if (msg) msg.style.display = elem.checked || webplugin == "1" ? "none" : "";
                al_width.disabled = elem.checked;
                al_height.disabled = elem.checked;

                if (position_fieldset) {
                    position_fieldset.disabled = elem.checked;
                    $("#ticker_position_fieldset").disabled = elem.checked;
                }
                if (resizable) resizable.disabled = elem.checked;
            }
        }

        $('#files').load(function () {

            $(this).contents().find('.files_uploaded').on('click', function (event) {
                event.preventDefault();
                create_wrong_item($(this).attr('href'), $(this).text());
            });
        });

        var myDropzone;

        $(document).ready(function () {
            $("#alertScheduleHeader").click(function () {
                $(".reccurenceControl input").prop('disabled', true);
                $(".reccurenceControl").prop('disabled', true);

            });

            var height = $(document).height();
            window.parent.parent.onChangeBodyHeight(height, false);

            $("#dialog-recorder").dialog({
                autoOpen: false,
                modal: true,
                resizable: false,
                draggable: false,
                width: 'auto',
                resize: 'auto',
                margin: 'auto'
            });

            $("#recordVideo").button().click(function () {
                $("#dialog-recorder").dialog("open");
            });

            $('#select_tabs').tabs();

            var activeTab = <% = ActiveTab %>;

            $("#select_tabs").tabs({ active: activeTab });

            if (activeTab == 1)
                check_alert_type("embed");

            $(document).tooltip({
                items: "img[title],a[title],[custom-tooltip]",
                content: function () {
                    var element = $(this);
                    if (element.is("[custom-tooltip]")) {
                        return "<p style='width:350px'><%=resources.LNG_VIDEO_EMBED_HINT %></p><img class='img-hint' src='images/video_hint.png' width='350' height='350'/>";
                    }
                    if (element.is("a[title]")) {
                        return element.attr("title");
                    }
                    if (element.is("img[title]")) {
                        return element.attr("title");
                    }
                },
                position: {
                    my: "center bottom-20",
                    at: "center top",
                    using: function (position, feedback) {
                        $(this).css(position);
                        $("<div>")
                            .addClass("arrow")
                            .addClass(feedback.vertical)
                            .addClass(feedback.horizontal)
                            .appendTo(this);
                    }
                }
            });

            $('#link_embed').change(function () {
                $('#videoLink').val($(this).val());
            });

            if (activeTab != 1) {
                $('.select').show();
                $('.embed').hide();
                $('.record').hide();
            }

            $("#recordVideo").button().click(function () {
                $("#dialog-recorder").dialog("open");
            });

            shortcut.add("Alt+s", function (e) {
                e.preventDefault();
                if (my_sub(2)) { document.my_form.submit(); }
            });

            shortcut.add("right", function (e) {
                e.preventDefault();
                if (my_sub(3)) { document.my_form.submit(); }
            });

            shortcut.add("Alt+P", function (e) {
                e.preventDefault();
                showAlertPreview();
            });

            $(".save_button").button();
            $(".preview_button").button();
            $(".save_and_next_button").button({
                icons: {
                    secondary: "ui-icon-carat-1-e"
                }
            });

            if (skins.length > 0) {
                $(".tile").hover(function () {
                    $(this).switchClass("", "tile_hovered", 200);
                }, function () {
                    $(this).switchClass("tile_hovered", "", 200);
                }).click(function () {

                    showSkinsTilesDialog();
                })
            }
            else {
                $(".tile").hide();
            }

            $(function () {
                $(".cancel_button").button();
                $(".save_button").button();
                $("#accordion").accordion({
                    active: 0,
                    heightStyle: "auto",
                    collapsible: true
                });
            });

            $(document).on('click', '#remove_wrong', function () {
                $('#wrong_elem').remove();
                $('#video_upload').removeClass('dz-started');
                $('#video_upload').removeClass('dz-max-files-reached');
                $('#videoLink').val('');
            });

            myDropzone = new Dropzone("#video_upload",
            {
                url: "UploadFile.aspx?uploadedContentType=_video&no_redirect=1",
                maxFilesize: <% = MaxFileSize %>,
                acceptedFiles: '.wmv,.avi,.mp4,.ogg,.flv',
                dictInvalidFileType: 'Please use .wmv, .avi, .mp4, .ogg or.flv files'
            });

            <% if (!string.IsNullOrEmpty(Request["id"])) { %>

                myDropzone.removeAllFiles(true);
                if ($("#wrong_elem").length) {
                    $('#remove_wrong').trigger('click');
                }
                $('#videoLink').val('<%=VideoLinkString%>');
                $('#video_upload').addClass('dz-started');
                $('#video_upload').addClass('dz-max-files-reached');
                $('#video_upload').append('<div id="wrong_elem" class="dz-preview dz-file-preview dz-processing dz-success">  <div class="dz-details">    <div class="dz-filename"><span data-dz-name=""><% =VideoName%></span></div>    <div class="dz-size" data-dz-size=""><strong></strong></div>    <img data-dz-thumbnail="">  </div>  <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress="" style="width: 100%;"></span></div>  <div class="dz-success-mark"><span>✔</span></div>  <div class="dz-error-mark"><span>✘</span></div>  <div class="dz-error-message"><span data-dz-errormessage=""></span></div><a id="remove_wrong" class="dz-remove" href="#" data-dz-remove="">Remove file</a></div>');
            <% } %>

            myDropzone.on("success", function (file) {
                $("#video_overlay").hide();

                const src = $('#files').attr('src');

                $('#files').attr('src', src);

                $('#files').load(function () {
                    const uploadedFileName = file.name.replace(/ /g, '_').replace(/\.[^/.]+$/, '') + ".mp4";
                    const href = '<% =VideoUploadFolderPath%>' + uploadedFileName;

                    $('#videoLink').val(href);
                });
            });

            myDropzone.on("sending", function () {
                $("#video_overlay").hide();
                if ($("#wrong_elem").length) {
                    $('#remove_wrong').trigger('click');
                    $('#video_upload').addClass('dz-started');
                    $('#video_upload').addClass('dz-max-files-reached');
                }
            });

            myDropzone.on("removedfile", function () {
                $('#videoLink').val('');
            });
        });

        function create_wrong_item(link, name) {
            myDropzone.removeAllFiles(true);

            if ($("#wrong_elem").length) {
                $('#remove_wrong').trigger('click');
            }
            $('#videoLink').val(link);
            $('#video_upload').addClass('dz-started');
            $('#video_upload').addClass('dz-max-files-reached');
            $('#video_upload').append('<div id="wrong_elem" class="dz-preview dz-file-preview dz-processing dz-success">  <div class="dz-details">    <div class="dz-filename"><span data-dz-name="">' + name + '</span></div>    <div class="dz-size" data-dz-size=""><strong></strong></div>    <img data-dz-thumbnail="">  </div>  <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress="" style="width: 100%;"></span></div>  <div class="dz-success-mark"><span>✔</span></div>  <div class="dz-error-mark"><span>✘</span></div>  <div class="dz-error-message"><span data-dz-errormessage=""></span></div><a id="remove_wrong" class="dz-remove" href="#" data-dz-remove="">Remove file</a></div>');

        }


        function check_alert_type(type, clearFields) {
            if (clearFields) {

                $('#videoLink').val('');
                $('#link_embed').val('');
            }

            $('#remove_wrong').trigger('click');
            if (type == "select") {
                $('.select').show();
                $('.embed').hide();
            }
            else if (type == "embed") {
                $('.select').hide();
                $('.embed').show();
            }
        }

        function check_tab(type) {
            check_alert_type(type, true);

        }

    </script>

    <script language="javascript">
        var skinId;
        var confShowSkinsDialog = <%= Settings.Content["ConfShowSkinsDialog"]  %>;
        var confShowTypesDialog = <%= Settings.Content["ConfShowTypesDialog"]  %>;

        var isInstant = '<%=IsInstantVal %>';
        var skins = <% =SkinsJson%>;

        function CreateVideoAlertSettings(link, width, height) {
            return {
                link: link,
                width: width,
                height: height,
                mute: $('#checkBoxMute').prop('checked'),
                controls: $('#checkBoxControls').prop('checked'),
                loop: $('#checkBoxLoop').prop('checked'),
                autoplay: $('#checkBoxAutoplay').prop('checked')
            };
        }

        function showAlertPreview(type) {
            if (!type)
                type = "desktop";
            var data = new Object();

            data.create_date = "<%=DateTime.Now.ToString(("dd/MM/yyyy HH:mm:ss"))%>";

            var ticker = ($("#tickerTypeTab").prop("checked")) ? 1 : 0;
            var fullscreen = "9";
            if ($("#size2").prop("checked")) { fullscreen = "1" }
            else if (ticker == 0) { fullscreen = $("input[name='position']:checked").attr("value"); }
            else { fullscreen = $("input[name='ticker_position']:checked").attr("value"); }
            var acknowledgement = ($("#aknown").prop("checked")) ? 1 : 0;
            var self_deletable = ($("#selfDeletable").prop("checked")) ? 1 : 0;
            var alert_width = $("#alertWidth").val();
            var alert_height = $("#alertHeight").val();
            var print_alert = ($("#printAlertCheckBox").prop("checked")) ? 1 : 0;

            var activeTab = $("#select_tabs").tabs("option", "active");
            var link = activeTab == 0 ? $('#videoLink').val() : $('#link_embed').val();

            if (link.length > 0) {
                if (activeTab === 0) {
                    var videoAlertSettings = CreateVideoAlertSettings(link, '100%', '100%');
                    $('#text_value').val('%video%=' + JSON.stringify(videoAlertSettings));
                } else {
                    $('#text_value').val(link);
                }

                link = $('#text_value').val();
            }

            $('#text_value').val($('#miracleText').val() + ' ' + link);
            var alert_html = $("#text_value").val();


            var html_title_ed = tinyMCE.get("htmlTitle");
            var html_title = html_title_ed.getContent();
            if (html_title)
                alert_html += "<!-- html_title = '" + html_title.replace(/&/g, '&amp;').replace(/"/g, '&quot;').replace(/'/g, '&#39;').replace(/</g, '&lt;').replace(/>/g, '&gt;') + "' -->";



            var alert_title = html_title;


            var top_template;
            var bottom_template;
            var templateId = $("#template_select").val();

            if (print_alert == 1) {
                if (alert_html.indexOf("<!-- printable_alert -->") == -1)
                    alert_html += "<!-- printable_alert -->";
            }
            else {
                if (alert_html.match(/<!-- printable_alert -->/img))
                    alert_html = alert_html.replace(/<!-- printable_alert -->/img, "");
            }


            if ($("#top_template_area").length > 0) {
                top_template = tinyMCE.get('top_template_area').getContent();
            }
            else {
                getTemplateHTML(templateId, "top", false, function (data) {
                    top_template = data;
                });
            }

            if ($("#bottom_template_area").length > 0) {
                bottom_template = tinyMCE.get('bottom_template_area').getContent();
            }
            else {
                getTemplateHTML(templateId, "bottom", false, function (data) {
                    bottom_template = data;
                });
            }
            data.top_template = top_template;
            data.bottom_template = bottom_template;
            data.alert_width = alert_width;
            data.alert_height = alert_height;
            data.ticker = ticker;
            data.fullscreen = fullscreen;
            data.acknowledgement = acknowledgement;
            data.self_deletable = self_deletable;
            data.alert_title = alert_title;
            data.alert_html = alert_html;
            data.caption_href = "";

            var skinId = $("#skin_id").val();

            if (skinId != "default" && skinId != "")
                skinId = "{" + skinId + "}";

            data.skin_id = skinId;
            data.type = type;
            data.need_question = type != 'sms' && $("#rsvpTypeTab").prop('checked');
            data.question = $("#question").val();
            data.question_option1 = $("#question_option1").val();
            data.question_option2 = $("#question_option2").val();
            data.need_second_question = $("#need_second_question").prop('checked');
            data.second_question = $("#second_question").val();

            initAlertPreview(data);

            return false;
        }

        function check_value() {
            if ($("#alertWidth").length > 0) {
                var al_width = document.getElementById('alertWidth').value;
                var al_height = document.getElementById('alertHeight').value;
                var size_message = document.getElementById('size_message');
                var fullscreen = document.getElementById('size2').checked;

                if ((al_width > 1024 && al_height > 1080) & !fullscreen) {
                    size_message.innerHTML = '<font color="red"><%=resources.LNG_RESOLUTION_DESC1 %></font>';
                    return;
                }

                if ((al_width < 340 || al_height < 290) & !fullscreen) {
                    size_message.innerHTML = "<font color='red'><%=resources.LNG_RESOLUTION_DESC2 %>. <A href='#' onclick='set_default_size();'><%=resources.LNG_CLICK_HERE %></a> <%=resources.LNG_RESOLUTION_DESC3 %>.</font>";
                    return;
                }
                size_message.innerHTML = "";
            }
        }

        function check_recurrence() {
            var isChecked = $("#recurrenceCheckbox").prop('checked');
            $("#recurrenceCheckboxValue").val(isChecked);
            if (isChecked) {
                $("#lifeTimeCheckBox").prop('checked', false);
            }
            $(".reccurenceControl input").prop('disabled', !isChecked);
            $(".reccurenceControl").prop('disabled', !isChecked);

            var displayValue = $(".count_down_delete_img").css('display') == "none" ? "block" : "none";

            $(".count_down_delete_img").prop('display', displayValue);
        }
        function createSkinsTilesHtml() {
            var thumbnail;
            if ($("#tickerTypeTab").prop("checked")) {
                thumbnail = "thumbnail_tick.png";
            }
            else {
                thumbnail = "thumbnail.png";
            }
            var list = $('<ul style="list-style:none; margin:0px; padding:0px"></ul>');

            for (var i = 0; i < skins.length; i++) {

                var skinsId = "";
                if (skins[i].id.toUpperCase() === "default".toUpperCase()) {
                    skinsId = "default";
                } else {
                    skinsId = "{" + skins[i].id + "}";
                }

                var li = '<li id="' + skins[i].id + '" class="tile" style="float:left; margin:10px; padding:10px">' +
                    '<div style="padding-bottom:5px"><span style="margin:0px" class="header_title">' + skins[i].name + '</span></div>' +
                    '<div>' +
                    '<img src="skins/' + skinsId + '/' + thumbnail + '"/>' +
                    '</div>' +
                    '</li>';
                $(list).append(li);
            }

            $(list)
                .find(".tile")
                .hover(function () {
                        $(this).switchClass("", "tile_hovered", 200);
                    },
                    function () {
                        $(this).switchClass("tile_hovered", "", 200);
                    })
                .click(function () {
                    $("#skins_tiles_dialog").dialog("close");
                    skinId = $(this).attr("id");
                    $("#skin_id").val(skinId);
                    if (!skinId || skinId.toUpperCase() === "default".toUpperCase()) {
                        skinId = "default";
                    } else {
                        skinId = "{" + skinId + "}";
                    }

                    var imgSrc = "skins/" + skinId + "/" + thumbnail;
                    $("#skinPreviewImg").attr("src", imgSrc);

                });

            return list;
        }


        function submitForm(isDraft) {

            $(function () {

                if (isDraft)
                    $("#IsDraft").val("1");

                var activeTab = $("#select_tabs").tabs("option", "active");
                var link = activeTab == 0 ? $('#videoLink').val() : $('#link_embed').val();

                if (link.length > 0) {
                    if (activeTab === 0) {
                        var videoAlertSettings = CreateVideoAlertSettings(link, '100%', '95%');
                        $('#text_value').val("%video%=" + JSON.stringify(videoAlertSettings));
                    } else {
                        $('#text_value').val(link);
                    }

                    link = $('#text_value').val();
                }

                $('#text_value').val($('#miracleText').val() + ' ' + link);
                $("#alertContent").val($('#text_value').val());
                $("#form1").submit();
            });
        }

        function showPrompt(text) {
            $(function () {
                $("#dialog-modal > p").text(text);
                $("#dialog-modal")
                    .dialog({
                        height: 180,
                        modal: true,
                        resizable: false,
                        buttons: {
                            "<%=resources.LNG_CLOSE %>": function () {
                                $(this).dialog("close");
                            }
                        }
                    });
            });
        }


    </script>

    <script type="text/javascript">

        var readOnly = { readonly: false };

        tinyMCE.baseURL = window.location.href.substring(0, window.location.href.lastIndexOf("/")) + "/jscripts/tiny_mce/";
        tinymce.init({
            selector: 'textarea#htmlTitle',
            menubar: false,
            statusbar: false,
            resize: false,
            setup: function (ed) {
                ed.on('change', function (e) {
                    $("#titleHidden").val(ed.getContent());
                });
                ed.on('init',
                    function(ed) {
                        if (window.location.href.indexOf('id=') == -1) {
                            ed.target.editorCommands.execCommand("fontName", false, "Arial");
                        }
                    });
            },
            plugins: "autoresize textcolor autolink",
            toolbar: "bold italic underline strikethrough | fontselect fontsizeselect | forecolor",
            autoresize_bottom_margin: 0,
            autoresize_min_height: 20,
            autoresize_max_height: 80,
            height: 20,
            width: 600,
            fontsize_formats: '8pt 10pt 11pt 12pt 14pt 16pt 18pt',
            forced_root_block: false,
            valid_elements: "span[*],font[*],i[*],b[*],u[*],strong[*],s[*],em[*]",
            content_style: "body{color:#000 !important; font-family:Arial, Times New Roman, Helvetica, sans-serif !important; font-size:14px !important}",
            font_formats: 'Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Calibri=calibri;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Tahoma=tahoma,arial,helvetica,sans-serif;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats',
            branding: false
        });

        var hiddenTitle = tinyMCE.get("htmlTitle");

        if (hiddenTitle != null) {
            $("#titleHidden").val(hiddenTitle.getContent());
        } else {
            $("#titleHidden").val("");
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
                <tr>
                    <td>
                        <input runat="server" type="hidden" id="videoLink" value="" />
                        <input runat="server" type="hidden" id="alertContent" value="" />
                        <input type="hidden" id="video_text" value="" />
                        <input type="hidden" id="IsDraft" name="IsDraft" value="0" />
                        <input type="hidden" runat="server" id="skin_id" name="skin_id" value="" />
                        <input type="hidden" runat="server" id="webAlert" name="webAlert" value="0" />
                        <input type="hidden" runat="server" id="IsInstant" name="IsInstant" value="0" />
                        <input type='hidden' runat="server" id="alertId" name='alertId' value='' />
                        <table id="dialog_dock" width="100%" height="100%" cellspacing="0" cellpadding="0"
                            class="main_table">
                            <tr>
                                <td width="100%" height="31" class="main_table_title">
                                    <a href="#" class="header_title">
                                        <%=resources.LNG_VIDEO_MODULE %></a>
                                </td>
                            </tr>
                            <tr>
                                <td height="100%" class="main_table_body">

                                    <div id="select_tabs" style="padding: 0px; border: none">
                                        <ul>
                                            <li id="select_tab"><a href="#select_tab" onclick="check_tab('select');">
                                                <%=resources.LNG_SELECT_FILE %></a></li>
                                            <li id="embed_tab"><a href="#select_tab" onclick="check_tab('embed');">
                                                <%=resources.LNG_EMBED %></a></li>
                                        </ul>
                                        <div id="select_tab" style="padding: 0.5em 0.5em;">
                                            <div style="padding: 10px;">
                                                <div>
                                                    <div class="videoAlert_info">
                                                        <%=resources.LNG_VIDEO_ALERT_INFO %>
                                                    </div>
                                                    <div>
                                                        <%=resources.LNG_STEP %>
                                                        <strong>1</strong>
                                                        <%=resources.LNG_OF %>&nbsp;2
                                                    : <strong><%=resources.LNG_ENTER_TEXT_OF_YOUR_VIDEO_ALERT %>:</strong>
                                                    </div>
                                                    <table height="100%" style="width: 1000px; margin: auto; border-spacing: 10px 0px;" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td height="auto" style="width: 50%" class="main_table_body">
                                                                <div style="width: 100%; margin: auto;">

                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td rowspan="1" style="vertical-align: top">

                                                                                <div runat="server" id="campaignLabel">
                                                                                    <b><%=resources.LNG_CAMPAIGN %>: </b>
                                                                                    <label><b runat="server" id="campaignName"></b></label>
                                                                                    <br>
                                                                                    <br />
                                                                                </div>
                                                                                <input runat="server" id='campaignSelect' type='hidden' name='campaign_select' value='' />

                                                                                <div>
                                                                                    <%=resources.LNG_TITLE %>:&nbsp;&nbsp;
                                                                                              <div runat="server" id="htmlTitleDiv">
                                                                                                  <div class="html_title_div">
                                                                                                      <textarea runat="server" id="htmlTitle" name="html_title" style="width: 310px"></textarea>
                                                                                                      <input type="hidden" runat="server" id="titleHidden" name="alert_title" value=""
                                                                                                          maxlength="255" style="width: 304px" />
                                                                                                  </div>
                                                                                              </div>

                                                                                    <!--   <div runat="server" id="simpleTitleDiv">
                                                                                                <input type="text" runat="server" id="simpleTitle" name="alert_title" value=""
                                                                                                    maxlength="255" style="width: 300px" />
                                                                                                <input type="hidden" runat="server" id="htmlTitleHidden" name="html_title" value="" />
                                                                                              </div> -->

                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div align="right" style="margin-bottom: 5px;">
                                                                                    <script language="javascript">
                                                                                        check_aknown();
                                                                                        check_email();
                                                                                        check_desktop();
                                                                                        check_size();
                                                                                    </script>
                                                                                    <div>
                                                                                        <a runat="server" id="previewButton" class="preview_button" onclick="showAlertPreview()" title="Hotkey: Alt+P">
                                                                                            <%=resources.LNG_PREVIEW %></a>

                                                                                        <asp:LinkButton runat="server" CssClass="save_button" ID="saveButton" title="Hotkey: Alt+S" />
                                                                                        <asp:LinkButton runat="server" CssClass="save_and_next_button" ID="saveNextButton" title="Hotkey: →" />

                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <span style="color: red;"><%=resources.PLEASE_DO_NOT_USE_FILES_WITH_NAMES_MORE_THAN_50_CHARACTERS %></span>
                                                                                <br/>
                                                                                <span style="color: red;"><%=resources.PLEASE_USER_ONLY_FILE_NAME_ALLOWED_CHARACTERS %></span>
                                                                            </td>
                                                                            <td></td>
                                                                        </tr>
                                                                        <tr>

                                                                            <td class="select" style="text-align: center;">
                                                                                <div style="width: 100%; position: relative">
                                                                                    <div id="video_overlay" style="display: none; position: absolute; width: 100%; height: 100%; z-index: 1000">
                                                                                        <p><%= resources.LNG_VIDEO_UPLOAD_OVERLAY %></p>
                                                                                        <img src="images/loader.gif" style="margin-top: 200px" />
                                                                                    </div>
                                                                                    <div id="formats_overlay" style="padding-top: 250px; position: absolute; width: 100%; height: 20%; z-index: 0">
                                                                                        <b>Supported formats: *.avi, *.flv, *.mp4, *.ogg, *.wmv, </b>

                                                                                    </div>
                                                                                    <div>

                                                                                        <br />
                                                                                        <div id="video_upload" style="width: 100%; margin: auto;" class="dropzone">
                                                                                            <div class="fallback dz-default dz-message">
                                                                                            </div>
                                                                                        </div>

                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            <td class="embed" style="text-align: center;" colspan="2">
                                                                                <div class="embed">
                                                                                    <div style="margin: 30px 0px 30px 50px">
                                                                                        <div style="font-size: 15px;">
                                                                                            <%=resources.LNG_VIDEO_EMBED_DESC %>
                                                                                        </div>
                                                                                        <input type="text" name="embed" id="link_embed" runat="server" style="width: 400px; margin-top: 5px" value='' />&nbsp;<img src="images/help.png" custom-tooltip="" />
                                                                                        <div style="color: #C0C0C0; margin-top: 5px; font-size: 13px;">
                                                                                            Example:&nbsp;&nbsp;&lt;iframe width=&quot;560&quot; height=&quot;315&quot; src=&quot;https://www.youtube.com/embed/uJ7dNKTBI9s?rel=0&quot; frameborder=&quot;0&quot; allowfullscreen&gt;&lt;/iframe&gt;
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </td>
                                                                            <td class="select" style="vertical-align: top;">
                                                                                <div style="margin-left: 10px; margin-top: 15px">
                                                                                    <span style="font-size: 14px"><%=resources.LNG_SELECT_FROM_SERVER %></span>
                                                                                    <div style="margin-top: 10px; width: 100%; height: 330px; border: 1px solid #C0C0C0;">
                                                                                        <iframe id="files" src="uploadform.aspx?size=0&edit=1&no_upload=1" width="100%" height="100%"
                                                                                            style="border: none"></iframe>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr class="select">
                                                                            <td class="DisableRecordVideo" colspan="2" style="text-align: center">
                                                                                <div style="padding: 10px">
                                                                                    <span style="font-size: 14px; margin-right: 10px"><%=resources.LNG_VIDEO_NOT_HAVE %></span><a class="green" style="margin-right: 10px" id="recordVideo" href="#">Record using your webcam</a>&nbsp;&nbsp;<img src="images/help.png" title="<%=resources.LNG_VIDEO_RECORDING_HINT %>" />
                                                                                </div>
                                                                                <hr />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <div style="float: left; width: 745px">
                                                                                    <span style="font-size: 14px; margin-right: 10px;"><%=resources.LNG_VIDEO_DESCRIPTION %></span>
                                                                                    <textarea form="my_form" id="text_value" name="elm" style="visibility: hidden"></textarea>
                                                                                    <textarea runat="server" name="elm2" id="miracleText" style="width: 100%; height: 160px; resize: vertical"></textarea>
                                                                                </div>
                                                                                <div runat="server" class="tile" id="default" style="float: right; margin: 10px 0px; padding: 10px; margin-left: 0px; width: 200px;">
                                                                                    <div class="hints" style="font-size: 12px; text-align: center; padding-bottom: 2px">
                                                                                        <%= resources.LNG_CLICK_TO_CHANGE_SKIN %>
                                                                                    </div>
                                                                                    <div style="font-size: 1px">
                                                                                        <img runat="server" id="skinPreviewImg" src="skins/default/thumbnail.png" />
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <div id="accordion" style="width: 100%">
                                                                                    <h3 runat="server" id="alertSettingsHeader"><%= resources.LNG_ALERT_SETTING_ACC %></h3>
                                                                                    <table runat="server" id="alertSettingsTable" border="0" style="width: 100%">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <div class="checkbox_div">
                                                                                                    <input type="checkbox" runat="server" id="checkBoxMute" />
                                                                                                    <label for="checkBoxMute">
                                                                                                        <%=resources.LNG_MUTE_VIDEOALERT %></label>
                                                                                                    <img src="images/help.png" title="<%=resources.LNG_MUTE_VIDEOALERT_DESC %>" />
                                                                                                    <br />
                                                                                                </div>

                                                                                                <div class="checkbox_div">
                                                                                                    <input type="checkbox" runat="server" id="checkBoxControls" checked/>
                                                                                                    <label for="checkBoxControls">
                                                                                                        <%=resources.LNG_CONTROLS_VIDEOALERT %></label>
                                                                                                    <img src="images/help.png" title="<%=resources.LNG_CONTROLS_VIDEOALERT_DESC %>" />
                                                                                                    <br />
                                                                                                </div>

                                                                                                <div class="checkbox_div">
                                                                                                    <input type="checkbox" runat="server" id="checkBoxLoop" checked/>
                                                                                                    <label for="checkBoxLoop">
                                                                                                        <%=resources.LNG_LOOP_VIDEOALERT %></label>
                                                                                                    <img src="images/help.png" title="<%=resources.LNG_LOOP_VIDEOALERT_DESC %>" />
                                                                                                    <br />
                                                                                                </div>

                                                                                                <div class="checkbox_div">
                                                                                                    <input type="checkbox" runat="server" id="checkBoxAutoplay" checked />
                                                                                                    <label for="checkBoxAutoplay">
                                                                                                        <%=resources.LNG_AUTOPLAY_VIDEOALERT %></label>
                                                                                                    <img src="images/help.png" title="<%=resources.LNG_AUTOPLAY_VIDEOALERT_DESC %>" />
                                                                                                    <br />
                                                                                                </div>

                                                                                                <div class="checkbox_div" id="urgent_div">
                                                                                                    <input type="checkbox" runat="server" id="urgentCheckBox" name="urgent" value="1" onclick="check_aknown();" />
                                                                                                    <label for="urgentCheckBox">
                                                                                                        <%=resources.LNG_URGENT_ALERT %></label>
                                                                                                    <img src="images/help.png" title="<%=resources.LNG_URGENT_ALERT_DESC %>" />
                                                                                                    <br />
                                                                                                </div>
                                                                                                <div class="checkbox_div" id="aknown_div">
                                                                                                    <input type="checkbox" runat="server" id="aknown" name="aknown" onclick="check_aknown();" value="1" />
                                                                                                    <label for="aknown">
                                                                                                        <%=resources.LNG_ACKNOWLEDGEMENT %></label>

                                                                                                    <img src="images/help.png" title="<%=resources.LNG_ACKNOWLEDGEMENT_DESC %>." />
                                                                                                    <br />
                                                                                                </div>
                                                                                                <div class="checkbox_div" id="unobtrusive_div" style="display: none;">
                                                                                                    <input type="checkbox" runat="server" id="unobtrusiveCheckbox" name="unobtrusive" onclick="check_aknown();" value="1" />

                                                                                                    <label for="unobtrusiveCheckbox">
                                                                                                        <%=resources.LNG_UNOBTRUSIVE %></label>
                                                                                                    <img src="images/help.png" title="<%=resources.LNG_UNOBTRUSIVE_HINT %>." />
                                                                                                    <br />
                                                                                                </div>
                                                                                                <div class="checkbox_div" id="self_deletable_div">
                                                                                                    <input type="checkbox" runat="server" id="selfDeletable" name="self_deletable" value="1" />

                                                                                                    <label for="selfDeletable">
                                                                                                        <%=resources.LNG_SELF_DELETABLE %></label>
                                                                                                    <img src="images/help.png" title="<%=resources.LNG_SELF_DELETABLE_HINT %>." />
                                                                                                    <br />
                                                                                                </div>
                                                                                                <div class="checkbox_div" id="aut_close_div">
                                                                                                    <input type="checkbox" runat="server" id="autoCloseCheckBox" name="aut_close" onclick="check_aknown();"
                                                                                                        value="1" />
                                                                                                    <label for="autoCloseCheckBox">
                                                                                                        <%=resources.LNG_AUTOCLOSE_IN %></label>
                                                                                                    <input type="number" size="3" runat="server" name="autoclose" id="autoClosebox" onkeypress="return check_size_input(this, event);"
                                                                                                           value="0" disabled="True" />
                                                                                                    <%=resources.LNG_MINUTES %>
                                                                                                    <img src="images/help.png" title="<%=resources.LNG_AUTOCLOSE_TITLE %>." />
                                                                                                    <span id="man_close_div">
                                                                                                        <input type="checkbox" runat="server" id="manualClose" name="man_close" value="1" />
                                                                                                        <label for="manualClose">
                                                                                                            <%=resources.LNG_ALLOW_MANUAL_CLOSE %></label>
                                                                                                        <img src="images/help.png" title="<%=resources.LNG_ALLOW_MANUAL_CLOSE_DESC %>." />
                                                                                                    </span>
                                                                                                    <br />
                                                                                                </div>
                                                                                                <div class="checkbox_div" id="lifeTimeAlert">
                                                                                                    <input type="checkbox" id="lifeTimeCheckBox" runat="server" name="lifetimemode" value="1" onclick="check_lifetime();" checked="True" />
                                                                                                    <label for="lifeTimeCheckBox">
                                                                                                        <%=resources.LNG_LIFETIME_IN %></label>
                                                                                                    <input type="text" id="lifetime" runat="server" name="lifetimeval" style="width: 50px" value="1" onkeypress="return check_size_input(this, event);" />
                                                                                                    <select runat="server" id="lifetimeFactor" name="lifetime_factor">
                                                                                                    </select>
                                                                                                    &nbsp;
                                                                                                    <label id ="LifetimeAdditionalDescription" runat="server"></label>
                                                                                                    <img src="images/help.png" id="LifeTimeHelpImage" runat="server" />
                                                                                                    <br />
                                                                                                </div>

                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                    <h3 runat="server" id="alertScheduleHeader"><%= resources.LNG_SCHEDULE_ALERT %></h3>
                                                                                    <table runat="server" border="0" style="width: 100%" id="schedulingMenu">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <div runat="server" class="checkbox_div" id="scheduleAlert">
                                                                                                    <input type="checkbox" runat="server" id="recurrenceCheckbox" name="recurrence" value="1" onclick="check_recurrence();" />
                                                                                                    <asp:HiddenField runat="server" ID="recurrenceCheckboxValue" Value="" />

                                                                                                    <label for="recurrenceCheckbox"><%= resources.LNG_SCHEDULE_ALERT %> </label>

                                                                                                    <img src="images/help.png" title="<%=resources.LNG_SCHEDULE_HINT %>." /><br />
                                                                                                </div>

                                                                                                <div runat="server" id="recurrenceDiv">
                                                                                                    <da:ReccurencePanel ID="schedulePanel" runat="server"></da:ReccurencePanel>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>

                                                                                    <h3><%= resources.LNG_ALERT_APPER_ACC %></h3>
                                                                                    <table border="0" style="width: 100%" runat="server" id="appearanceMenu">

                                                                                        <tr>
                                                                                            <td style="width: 200px;">
                                                                                                <div id="buttons_to_display">
                                                                                                    <div id="desktopCheckLabel">
                                                                                                        <input type="checkbox" id="desktopCheck" runat="server" name="desktop" value="1" onclick="check_desktop();" checked="True" />
                                                                                                        <label for="desktopCheck"><%= resources.LNG_DESKTOP_ALERT %></label>
                                                                                                    </div>
                                                                                                    <div id="ticker_div">
                                                                                                        <div id="size_message">
                                                                                                        </div>
                                                                                                        <table border="0" cellspacing="0" cellpadding="0" style="width: auto;" id="tableAlertAppear">
                                                                                                            <tr>
                                                                                                                <td valign="top" runat="server" id="sizeDiv">
                                                                                                                    <fieldset class="reccurence_pattern" style="height: 110px">
                                                                                                                        <legend><%=resources.LNG_SIZE %></legend>
                                                                                                                        <div id="size_div" style="padding-left: 10px;">
                                                                                                                            <table border="0">
                                                                                                                                <tr>
                                                                                                                                    <td valign="middle">
                                                                                                                                        <input type="radio" runat="server" id="size1" name="fullscreen" value="0" onclick="check_message()" />
                                                                                                                                    </td>

                                                                                                                                    <td valign="middle">
                                                                                                                                        <%=resources.LNG_WIDTH %>:
                                                                                                                                    </td>
                                                                                                                                    <td valign="middle">
                                                                                                                                        <input type="text" runat="server" name="alert_width" id="alertWidth" size="4"
                                                                                                                                            value="500" onkeypress="return check_size_input(this, event);"
                                                                                                                                            onkeyup="check_value(this)" />
                                                                                                                                    </td>
                                                                                                                                    <td valign="middle">
                                                                                                                                        <%=resources.LNG_PX %>
                                                                                                                                    </td>

                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td>&nbsp;
                                                                                                                                    </td>
                                                                                                                                    <td valign="middle">
                                                                                                                                        <%=resources.LNG_HEIGHT %>:
                                                                                                                                    </td>
                                                                                                                                    <td valign="middle">
                                                                                                                                        <input type="text" runat="server" name="alert_height" id="alertHeight" size="4"
                                                                                                                                            value="400" onkeypress="return check_size_input(this, event);"
                                                                                                                                            onkeyup="check_value(this)" />
                                                                                                                                    </td>
                                                                                                                                    <td valign="middle">
                                                                                                                                        <%=resources.LNG_PX %>
                                                                                                                                    </td>

                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td>&nbsp;
                                                                                                                                    </td>
                                                                                                                                    <td valign="middle" colspan="3">
                                                                                                                                        <input type="checkbox" runat="server" id="resizable" name="resizable" value="1" />
                                                                                                                                        <label runat="server" id="resizableLabel" for="resizable">
                                                                                                                                            <%=resources.LNG_RESIZABLE %></label>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td valign="middle">
                                                                                                                                        <input type="radio" runat="server" id="size2" name="fullscreen" value="1" onclick="check_message()" checked />
                                                                                                                                    </td>
                                                                                                                                    <td valign="middle" colspan="3">
                                                                                                                                        <label for="size2">
                                                                                                                                            <%=resources.LNG_FULLSCREEN %></label><br />
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </div>

                                                                                                                    </fieldset>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </td>
                                                                                            <td runat="server" id="positionCell" valign="middle" style="height: 130px; width: 200px;">
                                                                                                <br />
                                                                                                <fieldset id="position_fieldset" class="reccurence_pattern" style="height: 105px;">
                                                                                                    <legend>
                                                                                                        <%=resources.LNG_POSITION %></legend>
                                                                                                    <div id="position_div">
                                                                                                        <table border="0" cellspacing="0" cellpadding="5" width="100%">
                                                                                                            <tr>
                                                                                                                <td valign="middle" style="text-align: center">
                                                                                                                    <input type="radio" runat="server" id="positionTopLeft" name="position" value="2" />
                                                                                                                </td>
                                                                                                                <td></td>
                                                                                                                <td valign="middle" style="text-align: center">
                                                                                                                    <input type="radio" runat="server" id="positionTopRight" name="position" value="3" />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td></td>
                                                                                                                <td valign="middle" style="text-align: center">
                                                                                                                    <input runat="server" id="positionCenter" type="radio" name="position" value="4" />
                                                                                                                </td>
                                                                                                                <td></td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td valign="middle" style="text-align: center">
                                                                                                                    <input type="radio" runat="server" id="positionBottomLeft" name="position" value="5" />
                                                                                                                </td>
                                                                                                                <td></td>
                                                                                                                <td valign="middle" style="text-align: center">
                                                                                                                    <input runat="server" id="positionBottomRight"
                                                                                                                        type="radio" name="position" value="6" checked />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>

                                                                                                    </div>

                                                                                                </fieldset>

                                                                                            </td>
                                                                                            <td></td>
                                                                                        </tr>

                                                                                    </table>
                                                                                </div>

                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <div colspan="2" align="right" style="margin-top: 10px;">

                                                                                    <script language="javascript">
                                                                                        check_aknown();
                                                                                        check_email();
                                                                                        check_desktop();
                                                                                        check_size();
                                                                                    </script>

                                                                                    <div>

                                                                                        <a runat="server" id="bottomPreview" class="preview_button" onclick="showAlertPreview()" title="Hotkey: Alt+P">
                                                                                            <%=resources.LNG_PREVIEW %></a>
                                                                                        <asp:LinkButton runat="server" CssClass="save_button" ID="bottomSaveButton" title="Hotkey: Alt+S" />
                                                                                        <asp:LinkButton runat="server" CssClass="save_and_next_button" ID="bottomSaveNextButton" title="Hotkey: →" />

                                                                                        <asp:ScriptManager runat="server" ID="scriptManager"></asp:ScriptManager>
                                                                                        <asp:UpdatePanel runat="server" ID="updatePanel">
                                                                                            <ContentTemplate>
                                                                                            </ContentTemplate>
                                                                                        </asp:UpdatePanel>

                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <div id="skins_tiles_dialog" title="<%=resources.LNG_SELECT_SKIN %>" style='display: none'>
                <p>
                </p>


                <div id="dialog-modal" style="display: none">
                    <p style="vertical-align: middle">
                    </p>
                </div>

                <div id="dialog-recorder" style="display: none" title="<%=resources.LNG_VIDEO_RECORDER %>">
                    <iframe seamless src="video_recorder.html" width="340" height="440"></iframe>
                </div>

            </div>
        </div>
    </form>
</body>
</html>
