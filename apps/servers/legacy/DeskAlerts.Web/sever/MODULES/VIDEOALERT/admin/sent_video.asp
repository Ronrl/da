﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%

check_session()

uid = Session("uid")
if uid <>"" then
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/shortcut.js"></script>
<script type="text/JavaScript" language="JavaScript" src="jscripts/json2.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>
<script language="javascript" type="text/javascript" src="functions.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
</head>
<script language="javascript" type="text/javascript">

    function openDialogUI(_form, _frame, _href, _width, _height, _title) {
        $("#dialog-" + _form).dialog('option', 'height', _height);
        $("#dialog-" + _form).dialog('option', 'width', _width);
        $("#dialog-" + _form).dialog('option', 'title', _title);
        $("#dialog-" + _frame).attr("src", _href);
        $("#dialog-" + _frame).css("height", _height);
        $("#dialog-" + _frame).css("display", 'block');
        if ($("#dialog-" + _frame).attr("src") != undefined) {
            $("#dialog-" + _form).dialog('open');
            $("#dialog-" + _form).contents().find('#table_border').css('border', 'none');
        }
    }



    $(document).ready(function() {

        $("#dialog-form").dialog({
            autoOpen: false,
            height: 100,
            width: 100,
            modal: true
        });
		
		shortcut.add("Alt+N",function(e) {
			e.preventDefault();
			location.href = "edit_videos.asp";
		});
		
		shortcut.add("delete",function(e) {
			e.preventDefault();
			return LINKCLICK('Video Alerts');
		});

		$(document).tooltip({
			items: "img[title],a[title]",
			position: {
				my: "center bottom-20",
				at: "center top",
				using: function (position, feedback) {
					$(this).css(position);
					$("<div>")
				   .addClass("arrow")
				   .addClass(feedback.vertical)
				   .addClass(feedback.horizontal)
				   .appendTo(this);
				}
			}
		});

        $('#dialog-frame').on('load', function() {
            $("#dialog-frame").contents().find('.prelink_stats').on('click', function(e) {
                e.preventDefault();
                var link = $(this).attr('href');
                var title = $(this).attr('title');
                var last_id = $('.dialog_forms').size();
                var elem = '<div id="dialog-form-' + last_id + '" class="dialog_forms" style="overflow:hidden;display:none;"><iframe id="dialog-frame-' + last_id + '" style="display:none;width:100%;height:auto;overflow:hidden;border : none;"></iframe></div>';
                $('#secondary').append(elem);
                $("#dialog-form-" + last_id).dialog({
                    autoOpen: false,
                    height: 100,
                    width: 100,
                    modal: true
                });
                openDialogUI("form-" + last_id, "frame-" + last_id, link, 350, 350, title);
            });
        });
    });

    var settingsDateFormat = "<%=GetDateFormat()%>";
    var settingsTimeFormat = "<%=GetTimeFormat()%>";

    var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
    var uiFormat = getUiDateTimeFormat(settingsDateFormat, settingsTimeFormat);

    function openDialog(alertID) {
        var url = "<%=jsEncode(alerts_folder)%>preview.asp?id=" + alertID;
        $('#linkValue').val(url);
        $('#linkValue').focus(function() {
            $('#linkValue').select().mouseup(function(e) {
                e.preventDefault();
                $(this).unbind("mouseup");
            });
        });
        $('#linkDialog').dialog('open');
    }

    function showAlertPreview(alertId) {
        var data = new Object();

        getAlertData(alertId, false, function(alertData) {
            data = JSON.parse(alertData);
        });

        var fullscreen = parseInt(data.fullscreen);
        var ticker = parseInt(data.ticker);
        var acknowledgement = data.acknowledgement;

        var alert_width = (fullscreen == 1) && !ticker ? $(".main_table_body").width() : (!ticker ? data.alert_width : $(".main_table_body").width());
        var alert_height = (fullscreen == 1) && !ticker ? $(window).height() - document.body.clientHeight + $(".main_table_body").height() : (!ticker ? data.alert_height : "");
        var alert_title = data.alert_title;
        var alert_html = data.alert_html;

        var top_template;
        var bottom_template;
        var templateId = data.template_id;

        if (templateId >= 0) {
            getTemplateHTML(templateId, "top", false, function(data) {
                top_template = data;
            });

            getTemplateHTML(templateId, "bottom", false, function(data) {
                bottom_template = data;
            });
        }

        data.top_template = top_template;
        data.bottom_template = bottom_template;
        data.alert_width = alert_width;
        data.alert_height = alert_height;
        data.ticker = ticker;
        data.fullscreen = fullscreen;
        data.acknowledgement = acknowledgement;
        data.alert_title = alert_title;
        data.alert_html = alert_html;
        data.caption_href = data.caption_href;

        initAlertPreview(data);

    }


    $(function() {
        $(".delete_button").button({
    });

    $(".add_alert_button").button({
        icons: {
            primary: "ui-icon-plusthick"
        }
    });

    $("#linkDialog").dialog({
        autoOpen: false,
        height: 80,
        width: 550,
        modal: true,
        resizable: false
    });

    $(document).ready(function() {
        var width = $(".data_table").width();
        var pwidth = $(".paginate").width();
        if (width != pwidth) {
            $(".paginate").width("100%");
        }
    });
})
</script>
<%

   if(Request("offset") <> "") then 
	offset = clng(Request("offset"))
   else 
	offset = 0
   end if	

   if(Request("sortby") <> "") then 
	sortby = Request("sortby")
   else 
	sortby = "sent_date DESC"
   end if	

'counting pages to display
   if(Request("limit") <> "") then 
	limit = clng(Request("limit"))
   else 
	limit=25
   end if

'  limit=10

	'--- rights ---
	set rights = getRights(uid, true, false, true, "alerts_val")
	gid = rights("gid")
	gviewall = rights("gviewall")
	gviewlist = rights("gviewlist")
	alerts_arr = rights("alerts_val")
	'--------------

	if(alerts_arr(2)="") then
		linkclick_str = "not"
	else
		linkclick_str = LNG_ALERTS
	end if
   ' Response.Write "gviewall = " & gviewall
   ' Response.Write "gviewlist = " & Join(gviewlist,",")

	if  Join(gviewlist,",") <> "" then
		SQL = "SELECT COUNT(1) as mycnt, 1 as mycnt1 FROM alerts WHERE (type='S' OR type='L') AND video = 1 AND param_temp_id IS NULL AND (class = 1 OR class = 16) AND sender_id IN ("& Join(gviewlist,",") &") AND id NOT IN (SELECT alert_id FROM instant_messages) AND campaign_id = -1"
		SQL = SQL & " UNION ALL "
		SQL = SQL & "SELECT COUNT(1) as mycnt, 2 as mycnt1 FROM archive_alerts WHERE (type='S' OR type='L') AND video = 1 AND param_temp_id IS NULL AND (class = 1 OR class = 16) AND sender_id IN ("& Join(gviewlist,",") &") AND id NOT IN (SELECT alert_id FROM instant_messages) AND campaign_id = -1"
		
		Set RS = Conn.Execute(SQL)

	else
		SQL = "SELECT COUNT(1) as mycnt, 1 as mycnt1 FROM alerts WHERE (type='S' OR type='L') AND video = 1 AND param_temp_id IS NULL AND (class = 1 OR class = 16)  AND id NOT IN (SELECT alert_id FROM instant_messages) AND campaign_id = -1"
		SQL = SQL & " UNION ALL "
		SQL = SQL & "SELECT COUNT(1) as mycnt, 2 as mycnt1 FROM archive_alerts WHERE (type='S' OR type='L') AND video = 1 AND param_temp_id IS NULL AND (class = 1 OR class = 16) AND id NOT IN (SELECT alert_id FROM instant_messages) AND campaign_id = -1"
		Set RS = Conn.Execute(SQL)

	end if

	cnt=0

	do while Not RS.EOF
		if(Not IsNull(RS("mycnt"))) then cnt=cnt+clng(RS("mycnt"))
		RS.MoveNext
	loop

	RS.Close

  j=cnt/limit

%><body style="margin:0" class="body">
  <div id="dialog-form" style="overflow: hidden; display: none;">
        <iframe id='dialog-frame' style="width: 100%; height: 100%; overflow: hidden; border: none;
            display: none"></iframe>
    </div>
    <div id="secondary" style="overflow: hidden; display: none;">
    </div>
<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width="100%" height="31" class="main_table_title"><a href="sent_video.asp" class="header_title"><%=LNG_ALERTS %></a></td>
		</tr>
		<tr>
		<td height="100%" valign="top" class="main_table_body">
		<div style="margin:10px">
		<span class="work_header"><%=LNG_SENT_VIDEO %></span>

		<% if(Request("rss")="1") then Response.Write "<br><center><b><font color=""red"">Error while sending rss!</font></b></center>" end if %>
		<% if(Request("rss")="2") then Response.Write "<br><center><b><font color=""red"">Error while sending rss (wrong alert text)!</font></b></center>" end if %>
		<table width="100%"><tr><td align="right">
		<% if(gid = 0 OR (gid = 1 AND alerts_arr(0)<>"")) then %>
		<a class="add_alert_button" title="Hotkey: Alt+N" href="edit_videos.asp?skin_id="><%=LNG_ADD_VIDEO_ALERT%></a>
		<% end if%>
		</td></tr></table>

<%

if(cnt>0) then
	page="sent_video.asp?"
	name=LNG_ALERTS 
	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 

	response.write "<br><A href='#' class='delete_button'  title='Hotkey: Delete' onclick=""javascript: return LINKCLICK('"&linkclick_str&"');"">"&LNG_DELETE&"</a><br/><br/>"
	response.write "<form action='delete_objects.asp' name='delete_objs' id='delete_objs' method='post'><input type='hidden' name='objType' value='alerts'>"

%>
		<table width="100%" height="100%" cellspacing="0" cellpadding="3" class="data_table">
		<tr class="data_table_title">
		<td width="10"><input type='checkbox' name='select_all' id='select_all' onclick='javascript: select_all_objects();'></td>
		<td width="20"><%= LNG_TYPE%></td>
		<td class="table_title"><%=sorting(LNG_ALERT_TITLE,"title", sortby, offset, limit, page) %></td>
		<td class="table_title" style="width:135px"><%=sorting(LNG_CREATION_DATE,"sent_date", sortby, offset, limit, page) %></td>
		<td class="table_title"><%=LNG_SCHEDULED %></td>
		<td class="table_title" width="140"><%=LNG_ACTIONS %></td>
		</tr>
<%

'show main table

	if  Join(gviewlist,",") <> "" then
		SQL = "SELECT * FROM (SELECT a.id, a.schedule, schedule_type, recurrence, alert_text, title, a.type, sent_date, convert(varchar,a.create_date) as create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A'  and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND video = 1 AND param_temp_id IS NULL AND (class = 1 OR class = 16) AND a.campaign_id = -1 AND a.sender_id IN ("& Join(gviewlist,",") &") AND a.id NOT IN (SELECT alert_id FROM instant_messages) "
		SQL = SQL & " UNION ALL "
		SQL = SQL & "SELECT a.id, a.schedule, schedule_type, recurrence, alert_text, title, a.type, sent_date, convert(varchar,a.create_date) as create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp  FROM archive_alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND video = 1  AND param_temp_id IS NULL AND (class = 1 OR class = 16) AND a.sender_id IN ("& Join(gviewlist,",") &")  AND a.campaign_id = -1 AND a.id NOT IN (SELECT alert_id FROM instant_messages)) res ORDER BY res."&sortby
        Set RS = Conn.Execute(SQL)
	else
		SQL = "SELECT * FROM (SELECT a.id, a.schedule, schedule_type, recurrence, alert_text, title, a.type, sent_date, convert(varchar,a.create_date) as create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND video = 1 AND param_temp_id IS NULL AND (class = 1 OR class = 16) AND a.id NOT IN (SELECT alert_id FROM instant_messages) AND a.campaign_id = -1"
		SQL = SQL & " UNION ALL "
		SQL = SQL & "SELECT a.id, a.schedule, schedule_type, recurrence, alert_text, title, a.type, sent_date, convert(varchar,a.create_date) as create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp  FROM archive_alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id WHERE (a.type='S' OR a.type='L') AND video = 1 AND param_temp_id IS NULL AND (class = 1 OR class = 16) AND a.campaign_id = -1 AND a.id NOT IN (SELECT alert_id FROM instant_messages))  res ORDER BY res."&sortby
	    'Response.Write SQL	
         Set RS = Conn.Execute(SQL)
	end if

	num=offset
	if(Not RS.EOF) then RS.Move(offset) end if
	Do While Not RS.EOF
		num=num+1
		if((offset+limit) < num) then Exit Do end if
		if(num > offset AND offset+limit >= num) then
		strObjectID=RS("id")
		schedule_type=RS("schedule_type")
		reschedule = 0
		if RS("schedule")="1" or RS("recurrence")="1" then
			start_date = RS("from_date")
			end_date = RS("to_date")
			if start_date <> "" then
				if end_date <> "" then
					if year(end_date) <> 1900 then
						if (MyMod(DateDiff("s", start_date, end_date),60)) <> 1 then 'if not lifetime
							reschedule = 1
						end if
					else
						reschedule = 1
					end if
				else
					reschedule = 1
				end if
			end if
		end if
		%>
		<tr>
		<td><input type="checkbox" name="objects" value="<%=strObjectID%>"></td>
		<td align="center">
			<% 
				dim isUrgent
				if RS("urgent") = "1" then
					isUrgent = true
				else
					isUrgent = false
				end if
				if RS("class") = "16" then
					if isUrgent = true then
						Response.Write "<img src=""images/alert_types/urgent_unobtrusive.png"" title=""Urgent Unobtrusive"">"
					else
						Response.Write "<img src=""images/alert_types/unobtrusive.png"" title=""Unobtrusive"">"
					end if
				elseif not isNull(RS("is_rsvp")) then
					if isUrgent = true then
						Response.Write "<img src=""images/alert_types/urgent_rsvp.png"" title=""Urgent RSVP"">"
					else
						Response.Write "<img src=""images/alert_types/rsvp.png"" title=""RSVP"">"
					end if
				elseif RS("ticker")="1" then
					if isUrgent = true then
						Response.Write "<img src=""images/alert_types/urgent_ticker.png"" title=""Urgent Ticker"">"
					else
						Response.Write "<img src=""images/alert_types/ticker.png"" title=""Ticker"">"
					end if
				elseif RS("fullscreen")="1" then
					if isUrgent = true then
						Response.Write "<img src=""images/alert_types/urgent_fullscreen.png"" title=""Urgent Fullscreen"">"
					else
						Response.Write "<img src=""images/alert_types/fullscreen.png"" title=""Fullscreen"">"
					end if
				elseif isUrgent = true then
					Response.Write "<img src=""images/alert_types/urgent_alert.gif"" title=""Urgent Alert"">"
				elseif  RS("type")="L" then
				        Response.Write "<img src=""images/alert_types/linkedin.png"" title=""LinkedIn Alert"">"
				else
					Response.Write "<img src=""images/alert_types/alert.png"" title=""Usual Alert"">"
				end if
			%>
		</td>
		<td>
		<% 
			Response.Write "<a href=""javascript: showAlertPreview('" & CStr(RS("id")) & "')"">" & HtmlEncode(RS("title")) & "</a>"
		%>
		</td><td  style="width:135px">
		<script language="javascript">
		    document.write(getUiDateTime('<%=RS("sent_date")%>'));
		</script>
		</td>
		<td style="text-align: center">
			<% 
			if (RS("recurrence")="1") or reschedule = 1 then
				'Response.Write LNG_YES
			%>
			<a href="#" onclick="openDialogUI('form','frame','reschedule.asp?id=<%=RS("id") %>&ro=1',600,550,'');"><%=LNG_YES%></a>	
			<%
			else
				Response.Write LNG_NO
			end if
			%>
		</td>
		<td align="left" nowrap="nowrap">
		
	<% if(gid = 0 OR (gid = 1 AND alerts_arr(0)<>"") AND alerts_arr(1)="checked") then %>
		<a href="edit_videos.asp?id=<%= RS("id") %>"><img src="images/action_icons/duplicate.png" alt="<%=LNG_RESEND_ALERT %>" title="<%=LNG_RESEND_ALERT %>" width="16" height="16" border="0" hspace=5></a>
	<% end if
	   if(gid = 0 OR (gid = 1 AND alerts_arr(1)<>"")) then %>
		<%
		if schedule_type="1" then
			%>
				<a href="change_schedule_type.asp?id=<%=RS("id") %>&type=0&return_page=sent_video.asp"><img src="images/action_icons/stop.png" alt="<%=LNG_STOP_SCHEDULED_ALERT %>" title="<%=LNG_STOP_SCHEDULED_ALERT %>" width="16" height="16" border="0" hspace=5></a>
			<%
		else
			%> 
				<a href="change_schedule_type.asp?id=<%=RS("id") %>&type=1&return_page=sent_video.asp"><img src="images/action_icons/start.png" alt="<%=LNG_START_SCHEDULED_ALERT %>" title="<%=LNG_START_SCHEDULED_ALERT %>" width="16" height="16" border="0" hspace=5></a>
			<%
		end if
	end if
		Response.Write "<a href=""javascript: showAlertPreview('" & CStr(RS("id")) & "')""><img src=""images/action_icons/preview.png"" alt="""& LNG_PREVIEW &""" title="""& LNG_PREVIEW &""" width=""16"" height=""16"" border=""0"" hspace=""5""/></a>"
		%><a href="#" onclick="openDialog(<%=RS("id") %>)"><img src="images/action_icons/link.png" alt="<%=LNG_DIRECT_LINK%>" title="<%=LNG_DIRECT_LINK%>" width="16" height="16" border="0" hspace="6"/></a><%
		Response.Write "<a href='#' onclick='openDialogUI(""form"",""frame"",""statistics_da_alerts_view_details.asp?id=" & CStr(RS("id")) & """,600,550,"""&LNG_STATISTICS&""");'><img src=""images/action_icons/graph.png"" alt="""& LNG_STATISTICS &""" title="""& LNG_STATISTICS &""" width=""16"" height=""16"" border=""0"" hspace=""5""/></a>"
		if reschedule = 1 then
			%>
				<a href="#" onclick="openDialogUI('form','frame','reschedule.asp?id=<%=RS("id") %>',600,550,'<%=LNG_RESCHEDULE_ALERT %>');"><img src="images/action_icons/schedule.png" alt="<%=LNG_RESCHEDULE_ALERT %>" title="<%=LNG_RESCHEDULE_ALERT %>" border="0" hspace=5></a>
			<%
		end if
		%>
</td></tr>
		<%
		end if
	RS.MoveNext
	Loop
	RS.Close


%>

			</table>
<%
	response.write "<input type='hidden' name='back' value='video'></form><br/><A href='#' title='Hotkey: Delete' class='delete_button' onclick=""javascript: return LINKCLICK('"&linkclick_str&"');"">"&LNG_DELETE&"</a><br><br>"

	Response.Write make_pages(offset, cnt, limit, page, name, sortby) 

else

Response.Write "<center><b>"&LNG_THERE_ARE_NO_ALERTS&"</b><br><br></center>"

end if
%>

		 <img src="images/inv.gif" alt="" width="1" height="100%" border="0">
		</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>
<%
else
	Response.Redirect "index.asp"

end if
%>
<div id="linkDialog" title="<%= LNG_DIRECT_LINK %>" style="display:none">
	<input id="linkValue" type="text" readonly="readonly" style="width:500px" />
</div>
</body>
</html>
<!-- #include file="db_conn_close.asp" -->