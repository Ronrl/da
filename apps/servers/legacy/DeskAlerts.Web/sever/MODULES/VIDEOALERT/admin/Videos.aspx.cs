﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Castle.MicroKernel.Lifestyle;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Extensions.Enums;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlerts.Server.Utils.Interfaces.Services;
using DeskAlertsDotNet.Application;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Models.Permission;
using DeskAlertsDotNet.Parameters;
using Resources;

namespace DeskAlertsDotNet.Pages
{
    public partial class Videos : DeskAlertsBaseListPage
    {
        private readonly ICacheInvalidationService _cacheInvalidationService;
        private readonly IScheduledContentService _scheduledContentService;

        public Videos()
        {
            using (Global.Container.BeginScope())
            {
                _cacheInvalidationService = Global.Container.Resolve<ICacheInvalidationService>();
                _scheduledContentService = Global.Container.Resolve<IScheduledContentService>();
            }
        }

        protected override void OnLoad(EventArgs e)
        {

            base.OnLoad(e);
            searchButton.Text = searchButton.Text.Replace("{Search}", resources.LNG_SEARCH);
            headerTitle.Text = resources.LNG_VIDEO;
            scheduledCell.Text = resources.LNG_SCHEDULED;
            senderCell.Text = resources.LNG_SENT_BY;
            titleCell.Text = GetSortLink(resources.LNG_TITLE, "title", Request["sortBy"]);
            creationDateCell.Text = GetSortLink(resources.LNG_CREATION_DATE, "create_date", Request["sortBy"]);
            actionsCell.Text = resources.LNG_ACTIONS;
            Table = contentTable;
            bool shouldDisplayApprove = Config.Data.HasModule(Modules.APPROVE) && Settings.Content["ConfAllowApprove"].Equals("1");

            headerSelectAll.Visible = curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanDelete;
            selectAll.Visible = curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanDelete;
            bottomDelete.Visible = curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanDelete;
            upDelete.Visible = curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanDelete;
            addButton.Visible = curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanCreate;

            if (shouldDisplayApprove)
                approveCell.Text = resources.LNG_APPROVE_STATUS;
            else
                approveCell.Visible = false;

            bool isDraft = !String.IsNullOrEmpty(Request["draft"]) && Request["draft"].Equals("1");

            if (!IsPostBack)
            {
                searchTermBox.Value = Request["searchTermBox"];
            }
            string getUrlSearchTermBox = Request.QueryString["searchTermBox"] ?? string.Empty;

            bool isSearchTermBoxValueChanged = searchTermBox.Value != getUrlSearchTermBox;
            if (isSearchTermBoxValueChanged)
            {
                Offset = 0;
            }

            int cnt = this.Content.Count < (Offset + Limit) ? this.Content.Count : Offset + Limit;

            for (int i = Offset; i < cnt; i++)
            {
                DataRow row = this.Content.GetRow(i);
                int reccurence = row.GetInt("recurrence");
                var isRepeatableTemplate = row.GetBool("is_repeatable_template");
                DateTime toDate = row.GetDateTime("to_date");
                DateTime fromDate = row.GetDateTime("from_date");
                bool hasLifetime = row.GetInt("lifetime") == 1;

                bool isScheduled = toDate.Year != 1900 && toDate.Millisecond == 0 && fromDate.Millisecond == 0;

                TableRow tableRow = new TableRow();

                if (curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanDelete)
                {


                    CheckBox chb = GetCheckBoxForDelete(row);
                    TableCell checkBoxCell = new TableCell();
                    checkBoxCell.HorizontalAlign = HorizontalAlign.Center;
                    checkBoxCell.Controls.Add(chb);
                    tableRow.Cells.Add(checkBoxCell);
                }

                TableCell titleContentCell = new TableCell();
                titleContentCell.Text = string.Format("<a href=javascript:showAlertPreview('{0}')>{1}</a>", row.GetString("id"), row.GetString("title"));
                tableRow.Cells.Add(titleContentCell);

                var createDateContentCell = new TableCell
                {
                    Text = DateTimeConverter.ToUiDateTime(row.GetDateTime("create_date")),
                    HorizontalAlign = HorizontalAlign.Center
                };

                tableRow.Cells.Add(createDateContentCell);

                if (shouldDisplayApprove)
                { 
                    string approveLocKey;
                    if (row.IsNull("approve_status"))
                    {
                        approveLocKey = "LNG_APPROVED";
                    }   
                    else
                    {
                        var approveStatus = (ApproveStatus) row.GetInt("approve_status");
                        approveLocKey = approveStatus.GetStringValue();
                    }

                    var approveContentCell = new TableCell();
                    approveContentCell.HorizontalAlign = HorizontalAlign.Center;
                    approveContentCell.Text = resources.ResourceManager.GetString(approveLocKey);
                    tableRow.Cells.Add(approveContentCell);
                }

                var id = row.GetString("id");
                TableCell scheduleCell = new TableCell();
                if ((reccurence == 1 || isScheduled) && !hasLifetime)
                {
                    scheduleCell.Text =
                        string.Format(
                            "<a href=\"#\" onclick=\"openDialogUI('form', 'frame', 'rescheduler.aspx?id=" + id +
                            "',600,550, '');\">{0}</a>", resources.LNG_YES);
                }
                else
                {
                    scheduleCell.Text = resources.LNG_NO;
                }

                scheduleCell.HorizontalAlign = HorizontalAlign.Center;
                tableRow.Cells.Add(scheduleCell);

                var senderContentCell = new TableCell
                {
                    HorizontalAlign = HorizontalAlign.Center,
                    Text = string.IsNullOrEmpty(row.GetString("name")) ? resources.SENDER_WAS_DELETED : row.GetString("name")
                };

                tableRow.Cells.Add(senderContentCell);

                int padding = 10;


                LinkButton duplicateButton = GetActionButton(row.GetString("id"), "images/action_icons/duplicate.png", "LNG_RESEND_ALERT",
                    delegate
                    {
                        Response.Redirect("EditVideo.aspx?id=" + row.GetString("id"), true);
                    }, padding

                );


                duplicateButton.Style.Add(HtmlTextWriterStyle.MarginLeft, "8px");


                LinkButton sendButton = GetActionButton(row.GetString("id"), "images/action_icons/send.png",
                    "LNG_SEND_ALERT",

                    delegate
                    {
                        Response.Redirect("RecipientsSelection.aspx?return_page=Videos.aspx&id=" + row.GetString("id") + "&desktop=1", true);
                    }, padding);

                sendButton.Style.Add(HtmlTextWriterStyle.MarginLeft, "32px");

                LinkButton editButton = GetActionButton(row.GetString("id"), "images/action_icons/edit.png", "LNG_EDIT_ALERT",
                                                                delegate
                                                                {
                                                                    Response.Redirect("EditVideo.aspx?edit=1&id=" + row.GetString("id"), true);
                                                                }, padding

                                                            );

                int scheduleType = row.GetInt("schedule_type");
                LinkButton changeScheduleType = GetActionButton(row.GetString("id"), scheduleType == 1 ? "images/action_icons/stop.png" : "images/action_icons/start.png", scheduleType == 1 ? "LNG_STOP_SCHEDULED_ALERT" : "LNG_START_SCHEDULED_ALERT",
                    delegate
                    {
                        Response.Redirect("ChangeScheduleType.aspx?return_page=Videos.aspx&id=" + row.GetString("id") + "&type=" + Math.Abs(scheduleType - 1), true);
                    }, padding
                );

                LinkButton previewButton = GetActionButton(row.GetString("id"), "images/action_icons/preview.png",
                    "LNG_PREVIEW", "showAlertPreview(" + row.GetString("id") + ")", padding);

                LinkButton directLinkButton = GetActionButton(row.GetString("id"), "images/action_icons/link.png",
                    "LNG_DIRECT_LINK", "openDirDialog(" + row.GetString("id") + ")", padding);

                LinkButton detailsButton;
                if (isRepeatableTemplate)
                {
                    detailsButton = GetActionButton(
                        id,
                        "images/action_icons/graph.png",
                        "LNG_ALERT_DETAILS",
                        delegate
                        {
                            Response.Redirect($"InstancesOfTemplate.aspx?templateId={id}", true);
                        },
                        padding);
                }
                else
                {
                    detailsButton = GetActionButton(
                        id, 
                        "images/action_icons/graph.png",
                        "LNG_STATISTICS_REPORTS", 
                        $"openDialogUI(\"form\",\"frame\",\"AlertStatisticDetails.aspx?id={id}\",350,400,\"{resources.LNG_ALERT_DETAILS}\");", 
                        padding);
                }

                LinkButton rescheduleButton = GetActionButton(row.GetString("id"), "images/action_icons/schedule.png",
                    "LNG_RESCHEDULE_ALERT", "openDialogUI(\"form\",\"frame\",\"rescheduler.aspx?id=" + row.GetString("id") + "\",600,550,\"" +
                             resources.LNG_ALERT_DETAILS + "\");"
                    , 0);


                TableCell actionsContentCell = new TableCell();

                actionsCell.HorizontalAlign = HorizontalAlign.Center;


                if (!isDraft)
                {
                    if (curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanCreate)
                    {
                        actionsContentCell.Controls.Add(duplicateButton);
                    }

                    actionsContentCell.Controls.Add(changeScheduleType);
                    actionsContentCell.Controls.Add(previewButton);
                    actionsContentCell.Controls.Add(directLinkButton);
                    actionsContentCell.Controls.Add(detailsButton);

                    if (isScheduled)
                        actionsContentCell.Controls.Add(rescheduleButton);
                }
                else
                {
                    if (curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanSend)
                    {
                        actionsContentCell.Controls.Add(sendButton);
                    }

                    if (curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanEdit)
                    {
                        actionsContentCell.Controls.Add(editButton);
                    }

                    actionsContentCell.Controls.Add(previewButton);
                    actionsContentCell.Controls.Add(directLinkButton);
                }

                tableRow.Cells.Add(actionsContentCell);

                contentTable.Rows.Add(tableRow);
            }

        }

        protected override void OnDeleteRows(string[] ids)
        {
            foreach (var id in ids)
            {
                var contentId = Convert.ToInt64(id);

                _scheduledContentService.DeleteInstancesOfTemplate(contentId);
                _cacheInvalidationService.CacheInvalidation(AlertType.VideoAlert, contentId);
            }
        }

        #region DataProperties
        protected override string DataTable => "alerts";

        protected override string[] Collumns => new string[] { };

        protected override string DefaultOrderCollumn => "sent_date";

        protected override string CustomQuery
        {
            get
            {
                string viewListStr = "";

                if (!curUser.HasPrivilege && !curUser.Policy.CanViewAll)
                    viewListStr = string.Join(",", curUser.Policy.SendersViewList.ToArray());

                bool isDraft = !String.IsNullOrEmpty(Request["draft"]) && Request["draft"].Equals("1");
                string query = "";

                if (!curUser.HasPrivilege && !curUser.Policy.CanViewAll)
                {
                    query = "SELECT a.id, a.lifetime, a.approve_status, a.schedule, schedule_type, recurrence, alert_text, title, a.type, a.sent_date, a.create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp, u.name, a.is_repeatable_template FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id LEFT JOIN users u on u.id = a.sender_id WHERE " + (!isDraft ? "(a.type='S' OR a.type='L')" : "a.type='D'") + " AND a.video = 1 AND param_temp_id IS NULL AND (is_repeatable_template = 0 AND recurrence = 0 OR is_repeatable_template = 1) AND class = 32 AND a.sender_id IN (" + viewListStr + ") AND a.id NOT IN (SELECT alert_id FROM instant_messages) ";

                    if (!string.IsNullOrEmpty(Request["user_id"]))
                        query += " AND a.id IN (SELECT alert_id FROM alerts_sent WHERE user_id = " + Request["user_id"] + ")";
                }
                else
                {
                    query = "SELECT a.id, a.lifetime, a.approve_status, a.schedule, schedule_type, recurrence, alert_text, title, a.type, a.sent_date, a.create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp, u.name, a.is_repeatable_template FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id LEFT JOIN users u on u.id = a.sender_id WHERE " + (!isDraft ? "(a.type='S' OR a.type='L')" : "a.type='D'") + " AND a.video = 1 AND param_temp_id IS NULL AND (is_repeatable_template = 0 AND recurrence = 0 OR is_repeatable_template = 1) AND class = 32 AND a.id NOT IN (SELECT alert_id FROM instant_messages) ";

                    if (!string.IsNullOrEmpty(Request["user_id"]))
                        query += " AND a.id IN (SELECT alert_id FROM alerts_sent WHERE user_id = " + Request["user_id"] + ")";
                }

                if (IsPostBack && SearchTerm.Length > 0)
                    query += " AND title LIKE '%" + SearchTerm + "%'";

                if (!String.IsNullOrEmpty(Request["sortBy"]))
                    query += " ORDER BY " + Request["sortBy"];
                else
                {
                    query += " ORDER BY " + DefaultOrderCollumn + " DESC ";
                }

                return query;
            }
        }

        protected override string CustomCountQuery
        {
            get
            {
                string viewListStr = "";

                if (!curUser.HasPrivilege && !curUser.Policy.CanViewAll)
                    viewListStr = string.Join(",", curUser.Policy.SendersViewList.ToArray());

                bool isDraft = !String.IsNullOrEmpty(Request["draft"]) && Request["draft"].Equals("1");
                string query = "";

                if (!curUser.HasPrivilege && !curUser.Policy.CanViewAll)
                {
                    query = "SELECT count(1) FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id LEFT JOIN users u on u.id = a.sender_id WHERE " + (!isDraft ? "(a.type='S' OR a.type='L')" : "a.type='D'") + "  AND param_temp_id IS NULL AND (is_repeatable_template = 0 AND recurrence = 0 OR is_repeatable_template = 1) AND class = 32 AND a.video = 1  AND a.sender_id IN (" + viewListStr + ") AND a.id NOT IN (SELECT alert_id FROM instant_messages) ";

                    if (!String.IsNullOrEmpty(Request["user_id"]))
                        query += " AND a.id IN (SELECT alert_id FROM alerts_sent WHERE user_id = " + Request["user_id"] + ")";
                }
                else
                {
                    query = "SELECT count(1) FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id LEFT JOIN users u on u.id = a.sender_id WHERE " + (!isDraft ? "(a.type='S' OR a.type='L')" : "a.type='D'") + " AND param_temp_id IS NULL AND (is_repeatable_template = 0 AND recurrence = 0 OR is_repeatable_template = 1) AND class = 32 AND a.video = 1 AND a.id NOT IN (SELECT alert_id FROM instant_messages) ";

                    if (!String.IsNullOrEmpty(Request["user_id"]))
                        query += " AND a.id IN (SELECT alert_id FROM alerts_sent WHERE user_id = " + Request["user_id"] + ")";
                }

                if (IsPostBack && SearchTerm.Length > 0)
                    query += " AND title LIKE '%" + SearchTerm + "%'";

                return query;
            }
        }
        #endregion

        #region Interface properties

        protected override IButtonControl[] DeleteButtons
        {
            get
            {
                return new IButtonControl[] { upDelete, bottomDelete };
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl NoRowsDiv
        {
            get { return NoRows; }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TableDiv
        {
            get { return mainTableDiv; }
        }

        protected override string ContentName
        {
            get
            {
                return resources.LNG_VIDEO;
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl BottomPaging
        {
            get
            {
                return bottomPaging;
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TopPaging
        {
            get
            {
                return topPaging;
            }
        }

        #endregion
    }
}