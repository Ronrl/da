﻿namespace DeskAlerts.Server.sever.MODULES.VIDEOALERT
{
    public class VideoAlertSettingsDto
    {
        public string Link { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
        public bool Mute { get; set; }
        public bool Controls { get; set; }
        public bool Loop { get; set; }
        public bool Autoplay { get; set; }
        public string Description { get; set; }
    }
}