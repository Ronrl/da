<%
'---------------ADD TO CONFIG----------------


'E-Mail Config
Dim smtpServer, smtpPort, smtpSSL, smtpAuth, smtpUsername, smtpPassword
smtpServer="%smtpServer%"
smtpPort="%smtpPort%"
smtpSSL="%smtpSSL%"    ' 0 - Disable, 1 - Enable SSL authentication.
smtpAuth="%smtpAuth%"   ' 0 - Do not authenticate.
               ' 1 - Use basic (clear-text) authentication. The configuration sendusername/sendpassword or postusername/postpassword fields are used to specify credentials.
               ' 2 - Use NTLM authentication (Secure Password Authentication in Microsoft(R) Outlook(R) Express). The current process security context is used to authenticate with the service.
smtpUsername="%smtpUsername%"
smtpPassword="%smtpPassword%"
smtpFrom="%smtpFrom%"

smtpConnectTimeout=60

EM=1
'---------------ADD TO CONFIG----------------
%>