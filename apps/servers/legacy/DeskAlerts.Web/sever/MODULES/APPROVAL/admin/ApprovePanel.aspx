﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ApprovePanel.aspx.cs" Inherits="DeskAlertsDotNet.sever.MODULES.APPROVAL.admin.ApprovePanel" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="DeskAlerts.Server.Utils.Helpers" %>

<!DOCTYPE HTML>
<html>
<script language="javascript">
    window.parent.parent.document.getElementById('menu').src = window.parent.parent.document.getElementById('menu').src;
</script>
<head> 
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
        
<script type="text/JavaScript" language="JavaScript" src="jscripts/json2.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>
<script language="javascript" type="text/javascript" src="functions.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/preview.js"></script>
     
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/shortcut.js"></script>
<style rel="stylesheet" type="text/css">
    .style1 {
        height: 26px;
    }

    .style4 {
        height: 26px;
        width: 56px;
    }

    .date_time_param_input {
        width: 150px
    }

    .style5 {
        height: 26px;
        width: 208px;
    }

    .style6 {
        height: 26px;
        width: 300px;
    }

    .main_table {
        width: 99%;
    }
</style>

<script type="text/javascript">

    function closePreview() {
        $(document).unbind("click", closePreview);
        $(window).unbind("click", closePreview);
        $(window).unbind("scroll", closePreview);
        $(window).unbind("resize", closePreview);
        $("#preview_div").hide();
    }

    function htmlEscape(str) {
        return (str || '')
            .replace(/&/g, '&amp;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '&#39;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');
    }

    function showSurveyPreview(id) {

            $(function () {
                if ($("#preview_div").is(":visible") == true) {
                    closePreview();
                }

                $.get("../get_survey_data.asp?id=" + id, function (data) {

                    var surveyData = JSON.parse(data);

                    getAlertData(surveyData.alert_id, false, function (alertData) {
                        data = JSON.parse(alertData);
                    });

                    $("#preview_id").val(id);
                    // ToDo: Reimplement to GetSurveyFromData.aspx
                    $("#preview_title").val(htmlEscape(parseHtmlTitle(decodeURIComponent(data.alert_html), '<!-- html_title = ', '" -->')));
                    $("#preview_skin_id").val(surveyData.skin_id);
                    $("#create_date").val(surveyData.create_date);
                    $("#alert_width").val(surveyData.alert_width);
                    $("#alert_height").val(surveyData.alert_height);

                    if (surveyData.fullscreen == 1) {
                        $("#fullscreen").val(1);
                    }
                    else {
                        $("#fullscreen").val(0);
                    }

                    $("#position").val(surveyData.position);

                    $("#preview_form").submit();
                    $("#preview_div").draggable();
                    $("#preview_div").fadeIn(1000);

                    setTimeout(function () {
                        $(document).bind("click", closePreview);
                        $(window).bind("click", closePreview);
                        $(window).bind("scroll", closePreview);
                        $(window).bind("resize", closePreview);
                        $("#preview").unbind("click", closePreview);

                    }, 1000);


                    var position = surveyData.fullscreen;
                    $("#preview_div").css('left', '');
                    $("#preview_div").css('top', '');
                    $("#preview_div").css('bottom', '');
                    $("#preview_div").css('right', '');
                    switch (position) {

                        case 2:
                            {
                                $("#preview_div").css('left', '0px');
                                $("#preview_div").css('top', '0px');
                                break;
                            }
                        case 3:
                            {
                                $("#preview_div").css('right', '0px');
                                $("#preview_div").css('top', '0px');
                                break;
                            }
                        case 4:
                            {
                                $("#preview_div").center();

                                break;
                            }
                        case 5:
                            {
                                $("#preview_div").css('left', '0px');
                                $("#preview_div").css('bottom', '0px');
                                $("#preview_div").css('top', '500px');
                                break;
                            }
                        case 6:
                            {
                                $("#preview_div").css('right', '0px');
                                $("#preview_div").css('bottom', '0px');
                                $("#preview_div").css('top', '500px');
                                break;
                            }
                        default:
                            {
                                $("#preview_div").css('right', '0px');
                                $("#preview_div").css('bottom', '0px');
                                break;
                            }
                    }
                });
            });

        }

    function showWallpaperPreview(alertId) {

        $(function () {
            var data;
            getAlertData(alertId, false, function (alertData) {
                data = JSON.parse(alertData);
            });
            var imageSrc = data.alert_html.replace(/^admin\//, "");
            var position = data.fullscreen;
            $("#preview_image_src").val(imageSrc);
            $("#preview_position").val(position);

            var params = 'width=' + screen.width;
            params += ', height=' + screen.height;
            params += ', top=0, left=0'
            params += ', fullscreen=yes';


            window.open('', 'preview_win', params);

            $("#preview_wallpaper_post_form").submit();
            //  hideLoader();
        });
    }

    function showScreenSaverPreview(alertId) {

        $(function () {
            getAlertData(alertId, false, function (alertData) {
                data = JSON.parse(alertData);
            });

            var html = data.alert_html;
            $("#preview_html").val(html);

            var params = 'width=' + screen.width;
            params += ', height=' + screen.height;
            params += ', top=0, left=0'
            params += ', fullscreen=yes';

            window.open('', 'preview_win', params);

            $("#preview_screensaver_post_form").submit();

        });
    }

    $(document).ready(function () {
        $("#selectAll").click(function () {
            var checked = $(this).prop('checked');
            $(".content_object").prop('checked', checked);
        });

        function openDialogUI(_form, _frame, _href, _width, _height, _title) {
            $("#dialog-" + _form).dialog('option', 'height', _height);
            $("#dialog-" + _form).dialog('option', 'width', _width);
            $("#dialog-" + _form).dialog('option', 'title', _title);
            $("#dialog-" + _frame).attr("src", _href);
            $("#dialog-" + _frame).css("height", _height);
            $("#dialog-" + _frame).css("display", 'block');
            if ($("#dialog-" + _frame).attr("src") != undefined) {
                $("#dialog-" + _form).dialog('open');
                $("#dialog-" + _form).contents().find('#table_border').css('border', 'none');
            }
        }

        $("#confirm_sending").dialog({
            autoOpen: false,
            height: 100,
            width: 300,
            modal: false,
            position: {
                my: "center top",
                at: "center top",
                of: $('#parent_for_confirm')
            },
            hide: {
                effect: 'fade',
                duration: 1000
            },
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close", ui.dialog || ui).hide();
                setTimeout(function () {
                    $("#confirm_sending").dialog('close');
                }, 3000);
            },
            closeOnEscape: false
        });

        $("#dialog-form").dialog({
            autoOpen: false,
            height: 100,
            width: 100,
            modal: true
        });

        var is_sended = $("#is_sended").val();

        if (is_sended == 'true') {
            if ($('#reschedule').val() == '1') {
                $('#anchor_confirm').text('=LNG_CONFIRM_SCHEDULE ');
            } else {
                $('#anchor_confirm').text('=LNG_CONFIRM_SEND ');
            }
            $("#last_alert").effect('highlight', {
                color: '#98FB98'
            }, 2000);
            $("#confirm_sending").dialog('open');
        }

        var width = $(".data_table").width();
        var pwidth = $(".paginate").width();

        if (width != pwidth) {
            $(".paginate").width("100%");
        }

        $("#dialog-frame").load(function () {
            $("#dialog-frame").contents().find('.prelink_stats').on('click', function (e) {
                e.preventDefault();
                var link = $(this).attr('href');
                var title = $(this).attr('title');
                var last_id = $('.dialog_forms').size();
                var elem = '<div id="dialog-form-' + last_id + '" class="dialog_forms" style="overflow:hidden;display:none;"><iframe id="dialog-frame-' + last_id + '" style="display:none;width:100%;height:auto;overflow:hidden;border : none;"></iframe></div>';
                $('#secondary').append(elem);
                $("#dialog-form-" + last_id).dialog({
                    autoOpen: false,
                    height: 100,
                    width: 100,
                    modal: true
                });
                openDialogUI("form-" + last_id, "frame-" + last_id, link, 350, 350, title);
            });
        });

        function openDialog(mode) {
            var dialogId = "";
            var textBoxId = ""; //duplicateDialog

            switch (mode) {
                case 1: //duplicate
                    {
                        dialogId = "#duplicateDialog";
                        textBoxId = "#dupName";
                        break;
                    }
                case 2: //add
                    {
                        dialogId = "#linkDialog";
                        textBoxId = "#linkValue";
                        $("#schedule").prop("checked", false);
                        $("#start_date_and_time").prop('disabled', true);
                        $
                        break;
                    }
            }

            $(textBoxId).focus(function () {
                $(textBoxId).select().mouseup(function (e) {
                    e.preventDefault();
                    $(this).unbind("mouseup");
                });
            });

            $(dialogId).dialog('open');
        }

        $(function () {
            $(".delete_button").button({});

            $(".add_alert_button").button({
                icons: {
                    primary: "ui-icon-plusthick"
                }
            });

            $("#rejectDialog").dialog({
                autoOpen: false,
                height: 210,
                width: 400,
                modal: true,
                resizable: false
            });

            $("#linkDialog").dialog({
                autoOpen: false,
                height: 125,
                width: 550,
                modal: true,
                resizable: false
            });

            $("#rescheduleDialog").dialog({
                autoOpen: false,
                height: 130,
                width: 200,
                modal: true,
                resizable: false
            });
            $("#duplicateDialog").dialog({
                autoOpen: false,
                height: 110,
                width: 550,
                modal: true,
                resizable: false
            });

            $("#propertyDialog").dialog({
                autoOpen: false,
                height: 350,
                width: 200,
                modal: true,
                resizable: false
            });
        })

        function encode_utf8(s) {
            return encodeURIComponent(s);
        }

        function showAlertPreview(alertId) {
            var data = new Object();

            getAlertData(alertId, false, function (alertData) {
                data = JSON.parse(alertData);
            });

            var fullscreen = parseInt(data.fullscreen);
            var ticker = parseInt(data.ticker);
            var acknowledgement = data.acknowledgement;

            var alert_width = (fullscreen == 1) && !ticker ? $(".main_table_body").width() : (!ticker ? data.alert_width : $(".main_table_body").width());
            var alert_height = (fullscreen == 1) && !ticker ? $(window).height() - document.body.clientHeight + $(".main_table_body").height() : (!ticker ? data.alert_height : "");
            var alert_title = data.alert_title;
            var alert_html = data.alert_html;

            var top_template;
            var bottom_template;
            var templateId = data.template_id;

            if (templateId >= 0) {
                getTemplateHTML(templateId, "top", false, function (data) {
                    top_template = data;
                });

                getTemplateHTML(templateId, "bottom", false, function (data) {
                    bottom_template = data;
                });
            }

            data.top_template = top_template;
            data.bottom_template = bottom_template;
            data.alert_width = alert_width;
            data.alert_height = alert_height;
            data.ticker = ticker;
            data.fullscreen = fullscreen;
            data.acknowledgement = acknowledgement;
            data.alert_title = alert_title;
            data.alert_html = alert_html;
            data.caption_href = data.caption_href;

            initAlertPreview(data);
        }
    });
    //This functions don't work in document.ready wrapper for some reason
    //START
    function openUserList(id) {
        window.open('AlertRecipientsList.aspx?id=' + id,'', 'status=0, toolbar=0, width=400, height=400, scrollbars=1');
    }

    function confirmDelete() {
        if (confirm("Are you sure you want to delete selected items?") == true)
            return true;
        else
            return false;
    }

    shortcut.add("delete", function (e) {
        e.preventDefault();

        if ($("#upDelete").length == 0)
            return;


        if (confirmDelete())
            __doPostBack('upDelete', '');
    });

    function approveAlert(alertId) {
        $.ajax({
            type: "POST",
            url: "ApprovePanel.aspx/ApproveAlert",
            data: '{alertId: "' + alertId + '" }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                location.reload();
                window.parent.parent.document.getElementById('menu').src = window.parent.parent.document.getElementById('menu').src;
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }

    function showRejectDialogForAlertWithId(alertId) {
        $("#rejectDialog").dialog('open');
        $("#rejectButton").attr('alertId', alertId);
    }

    function rejectAlert() {
        var reason = $("#rejectReason").val()

        if (reason.length == 0) {
            alert('<% Response.Write(resources.LNG_APPROVE_REJECT_REASON_ERROR); %>');
                return;
            }

        var alertId = $("#rejectButton").attr('alertId');

        $.ajax({
            type: "POST",
            url: "ApprovePanel.aspx/RejectAlert",
            data: '{alertId: "' + alertId + '", reason: "' + reason + '" }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                location.reload();
                window.parent.parent.document.getElementById('menu').src = window.parent.parent.document.getElementById('menu').src;
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }

    function showPropertiesForAlertWithId(alertId) {
        $.ajax({
            type: "POST",
            url: "ApprovePanel.aspx/GetAlertDescription",
            data: '{alertId: "' + alertId + '" }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $("#propertyTable").html(response.d);
                $("#propertyDialog").dialog('open');
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
    //END
</script>
</head>
<body style="margin:0" class="body">
<div id="propertyDialog">
    <div id="propertyTable" style="text-align:center"></div>
</div>

<form id="form1" runat="server">
    <div id="dialog-form" style="overflow:hidden;display:none;">
        <iframe id='dialog-frame' style="display:none;width:100%;height:auto;overflow:hidden;border : none;"></iframe>
    </div>
    <div id="confirm_sending" style="overflow:hidden;text-align:center;">
        <a id="anchor_confirm"></a>
    </div>
    <input type="hidden" id="is_sended" value="=Request(" is_sended ") " />
    <div id="secondary" style="overflow:hidden;display:none;"></div>

    <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
        <tr>
            <td>
                <table height="100%" cellspacing="0" cellpadding="0" class="main_table">
                    <tr>
					    <td width="100%" height="31" class="main_table_title">
						    <img src="images/menu_icons/approve.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
						    <a href="ApprovePanel.aspx" class="header_title" style="position:absolute;top:15px">
                                <asp:Label ID="approveLabel" runat="server" Text="Label"></asp:Label>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td height="100%" valign="top" class="main_table_body">
                            <br />
                            <br />
                            <div style="margin-left:10px;">
                                <table style="padding-left: 10px;" width="21%" border="0">
                                    <tbody>
                                        <tr valign="middle">
                                            <td width="175">
                                                <% =resources.LNG_SEARCH_ALERTS_BY_TITLE %> 
                                                <input type="text" id="searchTermBox" runat="server" name="uname" value="" />
                                            </td>
                                            <td width="1%">
                                                <br />
                                                <asp:linkbutton runat="server" class="search_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" id="searchButton" style="margin-right:0px" role="button" aria-disabled="false" Text="<span class='ui-button-icon-primary ui-icon ui-icon-search'></span><span class='ui-button-text'>{Search}</span>"/>
	                                            <input type="hidden" name="search" value="1">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>	
                            </div>

                            <div style="margin-left:10px" runat="server" id="mainTableDiv">
			                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
				                    <tr>
                                        <td> 
    					                    <div style="margin-left:10px" id="topPaging" runat="server"></div>
				                        </td>
				                    </tr>
				                </table>
				                <br>
				                <asp:LinkButton style="margin-left:10px" runat="server" id="upDelete" class='delete_button'  OnClientClick="return confirmDelete(); "  title="Hotkey: Delete"/>
				                <br>
                                <br>
				                <asp:Table style="padding-left: 0px;margin-left: 10px;margin-right: 10px;" runat="server" id="contentTable" Width="98.5%" Height="100%" CssClass="data_table" CellSpacing="0" CellPadding="3">
				                    <asp:TableRow CssClass="data_table_title">
					                    <asp:TableCell runat="server" id="headerSelectAll" CssClass="table_title" Width="2%" >
				                            <asp:CheckBox id="selectAll" runat="server"/>
			                            </asp:TableCell>
					                    <asp:TableCell runat="server" width="2%" id="typeLabel" CssClass="table_title" ></asp:TableCell>
                                        <asp:TableCell runat="server" id="titleLabel" CssClass="table_title"></asp:TableCell>
					                    <asp:TableCell runat="server" id="senderLabel" CssClass="table_title"></asp:TableCell>
                                        <asp:TableCell runat="server" id="creationDateLabel" CssClass="table_title"></asp:TableCell>
                                        <asp:TableCell runat="server" Width="20%" id="actionsLabel" CssClass="table_title"></asp:TableCell>
				                    </asp:TableRow>
				                </asp:Table>
				                <br>
				                <asp:LinkButton style="margin-left:10px" runat="server" id="bottomDelete" class='delete_button'  OnClientClick="return confirmDelete();" title="Hotkey: Delete"/>
				                <br>
                                <br>
				                <table width="100%" border="0" cellspacing="0" cellpadding="0">
				                    <tr>
                                        <td> 
					                        <div style="margin-left:10px" id="bottomRanging" runat="server"></div>
				                        </td>
				                    </tr>
				                </table>
			                </div>

                            <div runat="server" id="NoRows">
                                <br />
                                <br />
				                <center><b><%=resources.LNG_THERE_ARE_NO_ALERTS %></b></center>
			                </div>
			                <br>
                            <br>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <div runat="server" id="rejectDialog">       
        <label><% Response.Write(resources.LNG_APPROVE_ENTER_REASON_REJECT); %></label>
        <br />
        <br />        
        <textarea id="rejectReason" rows="5" cols="50" style="resize:none" maxlength="1000" placeholder="<% Response.Write(resources.LNG_APPROVE_REASON_REJECT); %>"></textarea>
        <br />
        <br />        
         <a id="rejectButton"  href=''  class="delete_button" style="float:right" onclick="rejectAlert();"> <% Response.Write(resources.LNG_REJECT); %>  </a>      
    </div>
             
    <div id="userListDialog"></div>
</form>
    <form style="margin:0px; width:0px; height:0px; padding:0px" id="preview_wallpaper_post_form" action="WallpaperPreview.aspx" method="post" target="preview_win">
		<input type="hidden" id="preview_image_src" name="image_src"  value=""/>
		<input type="hidden" id="preview_position" name="position"  value=""/>
    </form>
     <form style="margin:0px; width:0px; height:0px; padding:0px" id="preview_screensaver_post_form" action="ScreensaverPreview.aspx" method="post" target="preview_win">
	    <input type="hidden" id="preview_html" name="html"  value=""/>
    </form>
<form action="../SurveyPreview.aspx" target="preview_frame" id="preview_form" method="post">
    <input type="hidden" name="skin_id" id="preview_skin_id" />
    <input type="hidden" name="id" id="preview_id" />
    <input type="hidden" name="create_date" id="create_date" />
    <input type="hidden" name="title" id="preview_title" />
    <input type="hidden" name="alert_width" id="alert_width" runat="server" />
    <input type="hidden" name="alert_height" id="alert_height" runat="server" />
    <input type="hidden" name="position" id="position" runat="server" />
    <input type="hidden" name="fullscreen" id="fullscreen" runat="server" />
</form>
<div id="preview_div" style="position: fixed; left: 40%; top: 20%; z-index: 1; display: none">
    <iframe id="preview_frame" name="preview_frame" width="520" height="420" frameborder="0" src="../SurveyPreview.aspx"></iframe>
</div>
</body>
</html>
