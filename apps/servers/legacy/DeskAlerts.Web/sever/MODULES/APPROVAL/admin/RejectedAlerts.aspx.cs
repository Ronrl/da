﻿namespace DeskAlertsDotNet.Pages
{
    using System;
    using System.Collections.Generic;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using DeskAlertsDotNet.Parameters;
    using DeskAlertsDotNet.DataBase;
    using DeskAlertsDotNet.Models;

    using DeskAlertsDotNet.Managers;
    using DeskAlertsDotNet.Utils.Pages;

    using Resources;
    using DeskAlerts.ApplicationCore.Enums;

    public partial class RejectedAlerts : DeskAlertsBaseListPage
    {
        public int offset;
        public int limit;
        public string sortBy;
        public int count;
        public string webalert;
        public string alertType;
        public int Offset { get { return offset; } }
        public int Limit { get { return limit; } }
        public string SortBy { get { return sortBy; } }
        public int Count { get { return count; } }

        public string Webalert { get { return webalert; } }
        public string AlertType { get { return alertType; } }
        public Dictionary<string, string> vars = new Dictionary<string, string>();
        public string order;

        public bool noRows;


        public string page;
        public string name;
        public string DefaultSortColumn => "create_date";

        public string DefaultSort => "DESC";

        string orderCollumn;
        public string Name { get { return name; } }
        public static User curUser;
        public static DBManager dbMgr;

        private Dictionary<string, string> screensaversPages = new Dictionary<string, string>
        {
            {"image", "EditScreensaversImage.aspx"},
            {"powerpoint", "EditScreensaverPowerPoint.aspx"},
            {"video","EditScreensaverVideo.aspx"}
        };

        #region DataProperties
        protected override string CustomQuery
        {
            get
            {
                var query = $@"
                    SELECT a.id as id, 
                        a.campaign_id as campaign_id, 
                        a.title as title, 
                        a.video as video, 
                        a.create_date as create_date, 
                        u.name as sender, 
                        a.reject_reason as reject_reason, 
                        a.class as type_id, 
                        a.type as type, 
                        a.urgent as urgent, 
                        a.fullscreen as fullscreen, 
                        a.ticker as ticker, 
                        s.id as is_rsvp 
                    FROM alerts as a 
                    INNER JOIN users as u ON u.id = a.sender_id 
                    LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id 
                    WHERE a.approve_status = {(int)ApproveStatus.Rejected} 
                        AND a.sender_id = {curUser.Id}";

                if (IsPostBack && searchTermBox.Value.Length > 0)
                {
                    query += " AND title LIKE '%" + searchTermBox.Value.Replace("'", "''") + "%'";
                }

                if (Request["sortBy"] != "")
                {
                    query += " ORDER BY a." + sortBy;
                }
                else
                {
                    query += " ORDER BY a.create_date DESC";
                }

                return query;
            }
        }

        protected override string CustomCountQuery
        {
            get
            {
                var query = $@"
                    SELECT count(1) 
                    FROM alerts as a 
                    INNER JOIN users as u ON u.id = a.sender_id 
                    LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id 
                    WHERE a.approve_status = {(int)ApproveStatus.Rejected} 
                        AND a.sender_id = {curUser.Id}";

                if (IsPostBack && searchTermBox.Value.Length > 0)
                {
                    query += " AND title LIKE '%" + searchTermBox.Value.Replace("'", "''") + "%'";
                }

                return query;
            }
        }

        protected override string DataTable => "alerts";

        protected override string[] Collumns => new string[] { };

        protected override string DefaultOrderCollumn
        {
            get
            {

                if (!string.IsNullOrEmpty(Request["draft"]) && Request["draft"].Equals("1"))
                {
                    return "create_date";
                }

                return "sent_date";

            }
        }

        #endregion

        #region Interface properties
        protected override HtmlGenericControl TableDiv => mainTableDiv;

        protected override HtmlGenericControl NoRowsDiv => noRowsDiv;

        protected override IButtonControl[] DeleteButtons => new IButtonControl[] { deleteLabel, deleteLabelBottom };

        protected override HtmlInputText SearchControl => searchTermBox;

        #endregion

        protected void Page_PreInit(object sender, EventArgs e)
        {

            order = !string.IsNullOrEmpty(Request.Params["order"]) ? Request.Params["order"] : DefaultSort;
            orderCollumn = !string.IsNullOrEmpty(Request.Params["collumn"]) ? Request.Params["collumn"] : DefaultSortColumn;
            offset = !string.IsNullOrEmpty(Request.Params["offset"]) ? Convert.ToInt32(Request.Params["offset"]) : 0;
            limit = !string.IsNullOrEmpty(Request.Params["limit"]) ? Convert.ToInt32(Request.Params["limit"]) : 25;
            sortBy = !string.IsNullOrEmpty(Request.Params["sortby"]) ? Request.Params["sortby"] : $"{DefaultSortColumn} {DefaultSort}";
            webalert = !string.IsNullOrEmpty(Request.Params["webalert"]) ? Request.Params["webalert"] : string.Empty;
            alertType = !string.IsNullOrEmpty(Request.Params["alertType"]) ? Request.Params["alertType"] : string.Empty;

            page = "RejectedAlerts.aspx?";

        }

        protected override void OnLoad(EventArgs e)
        {
            dbMgr = new DBManager();
            curUser = UserManager.Default.CurrentUser;

            base.OnLoad(e);
            Table = contentTable;
            typeLabel.Text = resources.LNG_TYPE;
            rejectDescription.Text = resources.LNG_LABEL_REJECTED_ALERTS_DESCRIPTION;
            rejectLabel.Text = resources.LNG_REJECTED_ALERTS;
            titleLabel.Text = resources.LNG_TITLE;
            rejectCommentLabel.Text = resources.LNG_REJECT_COMMENT;
            actionsLabel.Text = resources.LNG_ACTIONS;

            deleteLabelBottom.Text = deleteLabel.Text = resources.LNG_DELETE;
            ParseAndFillContentTable();
        }


        protected void Page_Unload(object sender, EventArgs e)
        {
            dbMgr.Dispose();
        }

        public DataSet GetNonApprovedAlerts()
        {
            searchButton.Text = searchButton.Text.Replace("{Search}", resources.LNG_SEARCH);

            var query = $@"
                SELECT a.id as id, 
                    a.campaign_id as campaign_id, 
                    a.title as title, 
                    a.video as video, 
                    a.create_date as create_date, 
                    u.name as sender, 
                    a.reject_reason as reject_reason, 
                    a.class as type_id, 
                    a.type as type, 
                    a.urgent as urgent, 
                    a.fullscreen as fullscreen, 
                    a.ticker as ticker, 
                    s.id as is_rsvp 
                FROM alerts as a 
                INNER JOIN users as u ON u.id = a.sender_id 
                LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id 
                WHERE a.approve_status = {(int)ApproveStatus.Rejected} 
                    AND a.sender_id = {curUser.Id}";

            if (IsPostBack && searchTermBox.Value.Length > 0)
            {
                query += " AND title LIKE '%" + searchTermBox.Value.Replace("'", "''") + "%'";
            }

            if (Request["sortBy"] != "")
            {
                query += " ORDER BY a." + sortBy;
            }
            else
            {
                query += " ORDER BY a.create_date DESC";
            }

            var alertsSet = dbMgr.GetDataByQuery(query);
            count = alertsSet.Count;

            return alertsSet;
        }

        public string MakePages()
        {
            var url = Request.Url.GetLeftPart(UriPartial.Path);
            var urlHelper = new UrlHelper(
                url,
                searchTermBox.Value,
                string.Empty,
                DefaultSort,
                DefaultSortColumn,
                string.Empty,
                resources.LNG_REJECTED_ALERTS,
                offset.ToString(),
                limit.ToString(),
                count,
                Webalert,
                alertType);
            return urlHelper.MakePages();
        }

        public void ParseAndFillContentTable()
        {
            DataSet alertsSet = GetNonApprovedAlerts();
            noRows = alertsSet.IsEmpty;
            foreach (DataRow row in alertsSet)
            {
                int typeId = row.GetInt("type_id");
                string link = "";
                string imgPath = "";
                string id = row.GetString("id");
                bool isUrgent = row.GetInt("urgent") == 1;
                string surveyId = "";
                bool isTicker = row.GetInt("ticker") == 1;
                bool isRsvp = !row.IsNull("is_rsvp");

                if (typeId == 64 || typeId == 128 || typeId == 256)
                {
                    surveyId = dbMgr.GetScalarByQuery("SELECT id FROM surveys_main WHERE sender_id = " + row.GetString("id")).ToString();

                }
                switch (typeId)
                {
                    case 2:
                        {
                            imgPath = "<img src=\"images/alert_types/fullscreen.png\" title=\"Fullscreen\">";
                            link = $"EditScreensavers.aspx?id={id}&shouldApprove=1";
                            break;
                        }

                    case 64:
                        {
                            imgPath = "<img src=\"images/alert_types/survey.png\" title=\"Survey\">";
                            link = $"EditSurvey.aspx?id={surveyId}&approveAlertId={id}";
                            break;
                        }
                    case 128:
                        {
                            imgPath = "<img src=\"images/alert_types/quiz.png\" title=\"Quiz\">";
                            link = $"EditSurvey.aspx?id={surveyId}&approveAlertId={id}";
                            break;
                        }
                    case 256:
                        {
                            imgPath = "<img src=\"images/alert_types/poll.png\" title=\"Poll\">";
                            link = $"EditSurvey.aspx?id={surveyId}&approveAlertId={id}";
                            break;
                        }
                    case 8:
                        {
                            imgPath = "<img src='images/alert_types/wallpaper.png' title='Wallpaper'>";
                            link = $"EditWallpaper.aspx?id={id}&shouldApprove=1";
                            break;
                        }
                    case 4:
                        {
                            imgPath = "<img src='images/alert_types/rss.gif' title='RSS'>";
                            link = $"EditRss.aspx?id={id}";
                            break;
                        }
                    case 16:
                        {
                            if (isUrgent)
                            {
                                imgPath = "<img src='images/alert_types/urgent_unobtrusive.gif' title='Urgent Unobtrusive'>";
                            }
                            else
                            {
                                imgPath = "<img src='images/alert_types/unobtrusive.gif' title='Unobtrusive'>";
                            }

                            link = $"CreateAlert.aspx?id={id}";
                            break;
                        }

                    default:
                        {

                            if (isTicker)
                            {
                                if (isUrgent)
                                {
                                    imgPath = "<img src='images/alert_types/urgent_ticker.png' title='Urgent Ticker'>";
                                }
                                else
                                {
                                    imgPath = "<img src='images/alert_types/ticker.png' title='Ticker'>";
                                }
                                link = $"CreateAlert.aspx?id={id}&ticker=1";
                            }
                            else if (isRsvp)
                            {
                                if (isUrgent)
                                {
                                    imgPath = "<img src='images/alert_types/urgent_rsvp.png' title='Urgent RSVP'>";
                                }
                                else
                                {
                                    imgPath = "<img src='images/alert_types/rsvp.png' title='RSVP'>";
                                }
                                link = $"CreateAlert.aspx?id={id}&rsvp=1";
                            }
                            else if (row.GetInt("fullscreen") == 1)
                            {
                                if (isUrgent)
                                {
                                    imgPath = "<img src='images/alert_types/urgent_fullscreen.png' title='Urgent Fullscreen'>";
                                }
                                else
                                {
                                    imgPath = "<img src='images/alert_types/fullscreen.png' title='Fullscreen'>";
                                }
                                link = $"CreateAlert.aspx?id={id}";
                            }
                            else if (isUrgent)
                            {
                                imgPath = "<img src='images/alert_types/urgent_alert.gif' title='Urgent Alert'>";
                                link = $"CreateAlert.aspx?id={id}";
                            }
                            else if (row.GetString("type") == "L")
                            {
                                imgPath = "<img src='images/alert_types/linkedin.png' title='LinkedIn Alert'>";
                                link = $"CreateAlert.aspx?id={id}";
                            }
                            else
                            {
                                imgPath = "<img src='images/alert_types/alert.png' title='Usual Alert'>";
                                link = $"CreateAlert.aspx?id={id}";
                            }

                            if (Request["campaignid"] != "-1" || Request["campaignid"] != "")
                            {
                                link += $"&alert_campaign={this.Request["campaignid"]}";
                            }

                            if (typeId == 2048)
                            {
                                link += "&webplugin=1";
                            }
                            break;
                        }
                }

                if (row.GetInt("video") == 1)
                {
                    imgPath = "<img src='images/alert_types/fullscreen.png' title='Video Alert'>";
                    link = $"EditVideo.aspx?id={id}";
                }

                // string id = row.GetString("id");
                string title = row.GetString("title");
                string reason = row.GetString("reject_reason");
                string sender = row.GetString("sender");


                TableRow tableRow = FormatTableRow(id, title, reason, imgPath, link);

                contentTable.Rows.Add(tableRow);

            }
        }
        public TableRow FormatTableRow(string id, string title, string reason, string type, string editPage)
        {
            TableRow row = new TableRow();
            row.Attributes.Add("id", "row" + id);

            var checkBoxCell = new TableCell();
            var checkBox = new CheckBox { ID = "chbDel_" + id };
            checkBox.InputAttributes.Add("class", "content_object");
            checkBox.InputAttributes.Add("value", id);
            checkBoxCell.Controls.Add(checkBox);
            row.Cells.Add(checkBoxCell);

            TableCell typeCell = new TableCell();
            typeCell.Text = type;
            row.Cells.Add(typeCell);

            TableCell titleCell = new TableCell();
            titleCell.Text = title;
            row.Cells.Add(titleCell);

            if (reason.Length > 70)
            {
                string fullReason = reason;
                reason = reason.Substring(0, 70) + "...";
                reason = "<a href='#' onclick='openReason(\"" + fullReason + "\");'>" + reason + "</a>";
            }
            TableCell reasonCell = new TableCell();
            reasonCell.Text = reason;
            row.Cells.Add(reasonCell);




            TableCell actionCell = new TableCell();
            string innerHtml =
                $"<a href=\"{editPage}&rejectedEdit=1&edit=1\"><img src=\'images/action_icons/edit.png\' ></a>&nbsp;";

            actionCell.Text = innerHtml;
            row.Cells.Add(actionCell);


            return row;
        }
    }
}
