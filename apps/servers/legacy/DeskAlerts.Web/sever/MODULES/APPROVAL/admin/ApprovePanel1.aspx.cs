﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Net;
using System.IO;
using DeskAlertsDotNet.Parameters;
using DeskALertsDotNet.DataBase;

namespace DeskAlertsDotNet.Pages
{

    public partial class ApprovePanel : System.Web.UI.Page
    {
       
        public int offset;
        public int limit;
        public string sortBy;
        public int count;
       public int Offset { get { return offset; } }
       public int Limit { get { return limit; } }
       public string SortBy { get { return sortBy; } }
       public int Count { get { return count; } }
       public Dictionary<string, string> vars = new Dictionary<string,string>();
          public string order;

          public bool noRows;


       public  string page ;
       public string name;
       string orderCollumn;
       public string Name { get { return name; } }

       
       protected void Page_PreInit(object sender, EventArgs e)
       {
          
           order = !string.IsNullOrEmpty(Request.Params["order"]) ? Request.Params["order"] : "DESC";
           orderCollumn = !string.IsNullOrEmpty(Request.Params["collumn"]) ? Request.Params["collumn"] : "create_date";
          offset = !string.IsNullOrEmpty(Request.Params["offset"]) ? Convert.ToInt32(Request.Params["offset"]) : 0;
          limit = !string.IsNullOrEmpty(Request.Params["limit"]) ? Convert.ToInt32(Request.Params["limit"]) : 25;
          sortBy = !string.IsNullOrEmpty(Request.Params["sortby"]) ? Request.Params["sortby"] : "date DESC";

          page = "approve_panel.aspx?uid=" + Request["uid"] + "%26";
           
        }

        protected void Page_Load(object sender, EventArgs e)
        {   
            typeLabel.Text = Localization.CurrentUserLocalization.Content["LNG_TYPE"];
            approveDescription.Text = Localization.CurrentUserLocalization.Content["LNG_LABEL_APPROVE_DESCRIPTION"];
            approveLabel.Text = Localization.CurrentUserLocalization.Content["LNG_APPROVAL"];
            titleLabel.Text = Localization.CurrentUserLocalization.Content["LNG_TITLE"];
            senderLabel.Text = Localization.CurrentUserLocalization.Content["LNG_SENDER"];
            creationDateLabel.Text = Localization.CurrentUserLocalization.Content["LNG_CREATION_DATE"];
            actionsLabel.Text = Localization.CurrentUserLocalization.Content["LNG_ACTIONS"];

           deleteLabelButtom.Text =  deleteLabel.Text =  Localization.CurrentUserLocalization.Content["LNG_DELETE"];
           ParseAndFillContentTable();


           
        }

        public Dictionary<string, string> GetAspVariables(params string[] keys)
        {
            string queryString = String.Join(":", keys);

            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("approve_panel"));
            string url_bridge = path + "/dotnetbridge.asp?var=" + queryString;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
           // Response.Write(reader.ReadToEnd());
            string[] variablesStrings = reader.ReadToEnd().Split('&');
            

            Dictionary<string, string> result = new Dictionary<string, string>();
            foreach (string keyVal in variablesStrings)
            {

                string[] keyValArray = keyVal.Split('|');

                if (keyValArray.Length != 2)
                    continue;

                string key = keyValArray[0];
                string value = keyValArray[1];

                result.Add(key, value);
            }

            return result;

            
        }
        public string GetAspVariable(string variableName)
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("approve_panel"));
            string url_bridge = path + "/dotnetbridge.asp?var="+variableName;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string variable = reader.ReadToEnd();
            return variable;
            
        }

        public string GetNonApprovedAlerts()
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("approve_panel"));
            string url_bridge = path + "/set_settings.asp?name=GetNonApprovedAlerts&value=" + order + "&collumn="+orderCollumn +"&userId=" + Request["uid"];
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());

            return reader.ReadToEnd();

        }

        public string MakePages()
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("approve_panel"));
            string url_bridge = path + "/dotnetbridge.asp?action=makepage&offset="+Offset+"&limit="+Limit+"&count="+Count+"&page="+page+"&name="+Name+"&sortby="+SortBy;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string variable = reader.ReadToEnd();
            return variable;

        }

        public void ParseAndFillContentTable()
        {
            string data = GetNonApprovedAlerts();
            
            string[] rows = data.Split('\n');

            noRows = data == string.Empty;
            foreach (string row in rows)
            {

                if (row == string.Empty)
                    return;

                    string[] fields = row.Split('\r');

                   if (fields.Length == 6)
                    {
                        HtmlTableRow htmlRow = FormatTableRow(fields[0], fields[1], fields[3], fields[4], fields[2], fields[5]);
                      
                        contentTable.Rows.Add(htmlRow);
                        count++;
                    }
 
            }
        }

        public HtmlTableRow FormatTableRow(string id, string title, string date, string type, string sender, string editPage)
        {

            HtmlTableRow row = new HtmlTableRow();
            row.Attributes.Add("id", "row" + id);
            HtmlTableCell checkCell = new HtmlTableCell();
            checkCell.InnerHtml = "<input type=\"checkbox\" name=\"objects\" value=\"" + id + "\">";
            row.Cells.Add(checkCell);

            HtmlTableCell typeCell = new HtmlTableCell();
            typeCell.Align = "center";
            typeCell.InnerHtml = type;
            row.Cells.Add(typeCell);

            HtmlTableCell titleCell = new HtmlTableCell();
            titleCell.Align = "center";
            titleCell.InnerHtml = title;
            row.Cells.Add(titleCell);

            HtmlTableCell dateCell = new HtmlTableCell();
             dateCell.Align = "center";
            dateCell.InnerHtml = date;
            row.Cells.Add(dateCell);           



            //HtmlTableCell nameCell = new HtmlTableCell();
            //nameCell.InnerHtml = string.Format("<a href=\"CampaignDetails.aspx?campaignid={0}&status={2}\">{1}</a>", id, name, status);
            //row.Cells.Add(nameCell);


            HtmlTableCell senderCell = new HtmlTableCell();
             senderCell.Align = "center";
            senderCell.InnerHtml = sender;
            row.Cells.Add(senderCell);

            //HtmlTableCell startDateCell = new HtmlTableCell();
            //startDateCell.InnerHtml = startDate;
            //row.Cells.Add(startDateCell);


            //HtmlTableCell statusCell = new HtmlTableCell();
            //statusCell.InnerHtml = status;
            //row.Cells.Add(statusCell);

            HtmlTableCell actionCell = new HtmlTableCell();
            actionCell.Align = "center";
            string innerHtml = string.Empty;

           if(!editPage.Contains("survey_add"))
                innerHtml += "<a href=\"javascript:showAlertPreview('" + id + "')\"><img src='images/action_icons/preview.gif' ></a>&nbsp;";

            innerHtml += "<a href=\"javascript:approveAlert(" + id + ")\"><img src='images/action_icons/approve2.png' ></a>&nbsp;";
            innerHtml += "<a href=\"javascript:showRejectDialogForAlertWithId(" + id + ")\"><img src='images/action_icons/reject2.png' ></a>&nbsp;";
            innerHtml += "<a href=\"javascript:showPropertiesForAlertWithId(" + id + ")\"><img src='images/action_icons/list.gif' ></a>&nbsp;";
            innerHtml += "<a href=\"javascript:openUserList(" + id + ");\"><img src='images/action_icons/recipients.png' ></a>&nbsp;";
            innerHtml += "<a href=\""+ editPage +"&shouldApprove=1&edit=1\"><img src='images/action_icons/edit.gif' ></a>&nbsp;";
            //javascript:duplicateCampaign({1}, {2})
            //innerHtml = string.Format("<a href='#' onclick='javascript:duplicateDialogFunc({1}, {2}, event)'><img src='images/action_icons/duplicate.gif' alt='{0}' title='{0}' width='16' height='16' border='0' hspace='5'></a>", Localization.CurrentUserLocalization.Content["LNG_DUPLICATE_CAMPAIGN"], id, "\"" + name + "\"");
            //if(!isStarted && startDate == string.Empty)
            //    innerHtml += string.Format("<a href=\"change_schedule_type.asp?campaign={0}&type=1\"><img id=\"{0}\" src=\"images/action_icons/start.gif\" alt=\"{1}\" title=\"{1}\" width=\"16\" height=\"16\" border=\"0\" hspace=5></a>", id, Localization.CurrentUserLocalization.Content["LNG_START_CAMPAIGN"]);
            //else
            //    innerHtml += string.Format("<a href=\"change_schedule_type.asp?campaign={0}&type=0\"><img id=\"{0}\" src=\"images/action_icons/stop.gif\" alt=\"={1}\" title=\"{1}\" width=\"16\" height=\"16\" border=\"0\" hspace=5></a> ", id, Localization.CurrentUserLocalization.Content["LNG_STOP_CAMPAIGN"]);



            //openDialogUI('form','frame','reschedule.asp?campaign_id={0}',600,550,'reschedule campaign');
          //  innerHtml += string.Format("<a href=\'javascript:openReDialog({2}, {1}, {0})\' onclick=\"\"><img src=\"images/action_icons/schedule.gif\" alt=\"reschedule alert\" title=\"reschedule alert\" border=\"0\" hspace=\"5\"></a>", id, "\"" + name + "\"", "\"" + startDate + "\"");
            actionCell.InnerHtml = innerHtml;
            row.Cells.Add(actionCell);
           

            return row;
        }




    }
}
