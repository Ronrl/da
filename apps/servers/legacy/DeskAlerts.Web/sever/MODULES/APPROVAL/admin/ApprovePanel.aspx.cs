﻿using Castle.MicroKernel.Lifestyle;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Enums;
using DeskAlerts.ApplicationCore.Interfaces;
using DeskAlerts.Scheduler.ContentJobScheduling;
using DeskAlerts.Server.Utils.Interfaces.Services;
using DeskAlertsDotNet.Application;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Managers;
using DeskAlertsDotNet.Pages;
using DeskAlertsDotNet.Utils.Consts;
using Resources;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace DeskAlertsDotNet.sever.MODULES.APPROVAL.admin
{
    public partial class ApprovePanel : DeskAlertsBaseListPage
    {
        private static readonly IAlertRepository AlertRepository;

        private static readonly IContentJobScheduler ContentJobScheduler;
        private static readonly ICacheInvalidationService _cacheInvalidationService;
        

        public int offset;
        public int limit;
        public string sortBy;
        public int count = 0;
        public Dictionary<string, string> Vars = new Dictionary<string, string>();
        public string Order;
        public string OrderCollumn;
        public string page;
        public string name;

        protected void Page_PreInit(object sender, EventArgs e)
        {
            Order = !string.IsNullOrEmpty(Request.Params["order"]) ? Request.Params["order"] : "DESC";
            OrderCollumn = !string.IsNullOrEmpty(Request.Params["collumn"]) ? Request.Params["collumn"] : "create_date";
            offset = !string.IsNullOrEmpty(Request.Params["offset"]) ? Convert.ToInt32(Request.Params["offset"]) : 0;
            limit = !string.IsNullOrEmpty(Request.Params["limit"]) ? Convert.ToInt32(Request.Params["limit"]) : 25;
            sortBy = !string.IsNullOrEmpty(Request.Params["sortby"]) ? Request.Params["sortby"] : "create_date DESC";
            searchButton.Text = searchButton.Text.Replace("{Search}", resources.LNG_SEARCH);
            page = "ApprovePanel.aspx?";
        }

        static ApprovePanel()
        {
            using (Global.Container.BeginScope())
            {
                AlertRepository = Global.Container.Resolve<IAlertRepository>();

                ContentJobScheduler = Global.Container.Resolve<IContentJobScheduler>();

                _cacheInvalidationService = Global.Container
                    .Resolve<ICacheInvalidationService>();
            }               
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            dbMgr = new DBManager();
            curUser = UserManager.Default.CurrentUser;
            approveLabel.Text = resources.LNG_APPROVAL;
            Table = contentTable;
            typeLabel.Text = resources.LNG_TYPE;
            titleLabel.Text = resources.LNG_TITLE;
            senderLabel.Text = resources.LNG_SENDER;
            creationDateLabel.Text = resources.LNG_CREATION_DATE;
            actionsLabel.Text = resources.LNG_ACTIONS;
            ParseAndFillContentTable();

            if (count > 0)
            {
                NoRows.Visible = false;
            }
        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            dbMgr.Dispose();
        }

        public void ParseAndFillContentTable()
        {
            var alertsSet = Content;

            count = alertsSet.Count;

            foreach (var row in alertsSet)
            {
                var typeId = row.GetInt("type_id");
                string link;
                string imgPath;
                var id = row.GetString("id");
                var isUrgent = row.GetInt("urgent") == 1;
                var surveyId = "";

                if (typeId == 64 || typeId == 128 || typeId == 256)
                {
                    surveyId = dbMgr.GetScalarByQuery("SELECT id FROM surveys_main WHERE sender_id = " + row.GetString("id")).ToString();
                }
                switch (typeId)
                {
                    case 2:
                        {
                            imgPath = "<img src=\"images/alert_types/fullscreen.png\" title=\"Fullscreen\">";
                            link = $"EditScreensavers.aspx?id={id}";
                            break;
                        }

                    case 64:
                        {
                            imgPath = "<img src=\"images/alert_types/survey.png\" title=\"Survey\">";
                            link = $"EditSurvey.aspx?id={surveyId}&approveAlertId={id}";
                            break;
                        }
                    case 128:
                        {
                            imgPath = "<img src=\"images/alert_types/quiz.png\" title=\"Quiz\">";
                            link = $"EditSurvey.aspx?id={surveyId}&approveAlertId={id}";
                            break;
                        }
                    case 256:
                        {
                            imgPath = "<img src=\"images/alert_types/poll.png\" title=\"Poll\">";
                            link = $"EditSurvey.aspx?id={surveyId}&approveAlertId={id}";
                            break;
                        }
                    case 8:
                        {
                            imgPath = "<img src='images/alert_types/wallpaper.png' title='Wallpaper'>";
                            link = $"EditWallpaper.aspx?id={id}";
                            break;
                        }
                    case 4:
                        {
                            imgPath = "<img src='images/alert_types/rss.png' title='RSS'>";
                            link = $"EditRss.aspx?id={id}";
                            break;
                        }
                    case 16:
                        {
                            imgPath = isUrgent ? "<img src='images/alert_types/urgent_unobtrusive.png' title='Urgent Unobtrusive'>" : "<img src='images/alert_types/unobtrusive.png' title='Unobtrusive'>";
                            link = $"CreateAlert.aspx?id={id}";
                            break;
                        }

                    default:
                        {
                            if (row.GetInt("ticker") == 1)
                            {
                                imgPath = isUrgent ? "<img src='images/alert_types/urgent_ticker.png' title='Urgent Ticker'>" : "<img src='images/alert_types/ticker.png' title='Ticker'>";
                                link = $"CreateAlert.aspx?ticker=1&id={id}";
                            }
                            else if (!row.IsNull("is_rsvp"))
                            {
                                imgPath = isUrgent ? "<img src='images/alert_types/urgent_rsvp.png' title='Urgent Ticker'>" : "<img src='images/alert_types/rsvp.png' title='RSVP'>";
                                link = $"CreateAlert.aspx?rsvp=1&id={id}";
                            }
                            else if (row.GetInt("fullscreen") == 1)
                            {
                                imgPath = isUrgent ? "<img src='images/alert_types/urgent_fullscreen.png' title='Urgent Fullscreen'>" : "<img src='images/alert_types/fullscreen.png' title='Fullscreen'>";
                                link = $"CreateAlert.aspx?id={id}";
                            }
                            else if (isUrgent)
                            {
                                imgPath = "<img src='images/alert_types/urgent_alert.png' title='Urgent Alert'>";
                                link = $"CreateAlert.aspx?id={id}";
                            }
                            else if (row.GetString("type") == "L")
                            {
                                imgPath = "<img src='images/alert_types/linkedin.png' title='LinkedIn Alert'>";
                                link = $"CreateAlert.aspx?id={id}";
                            }
                            else
                            {
                                imgPath = "<img src='images/alert_types/alert.png' title='Usual Alert'>";
                                link = $"CreateAlert.aspx?id={id}";
                            }

                            if (Request["campaignid"] != "-1" || Request["campaignid"] != "")
                            {
                                link += "&alert_campaign=" + Request["campaignid"];
                            }

                            if (typeId == 2048)
                            {
                                link += "&webplugin=1";
                            }
                            break;
                        }
                }

                if (row.GetInt("video") == 1)
                {
                    imgPath = "<img src='images/alert_types/fullscreen.png' title='Video Alert'>";
                    link = $"EditVideo.aspx?id={id}";
                }

                var title = row.GetString("title");
                var date = row.GetString("create_date");
                var sender = row.GetString("sender");
                var tableRow = FormatTableRow(row, id, title, date, imgPath, sender, link, typeId, surveyId);
                contentTable.Rows.Add(tableRow);
            }
        }

        public TableRow FormatTableRow(DataRow currentRowFromDb, string id, string title, string date, string type, string sender, string editPage, int typeId, string surveyId)
        {
            var row = new TableRow();

            var chb = GetCheckBoxForDelete(currentRowFromDb);
            var checkBoxCell = new TableCell();
            checkBoxCell.Controls.Add(chb);
            row.Cells.Add(checkBoxCell);

            var typeCell = new TableCell { Text = type };
            row.Cells.Add(typeCell);

            var titleCell = new TableCell { Text = title };
            row.Cells.Add(titleCell);

            var senderCell = new TableCell { Text = sender };
            row.Cells.Add(senderCell);

            var dateCell = new TableCell { Text = date };
            row.Cells.Add(dateCell);

            var actionCell = new TableCell();
            var innerHtml = string.Empty;

            switch (typeId)
            {
                case (int)AlertType.Wallpaper:
                    innerHtml += $"<a onClick=\'showWallpaperPreview({id});\' href=\'#\'><img src=\'images/action_icons/preview.png\'></a>&nbsp";
                    break;
                case (int)AlertType.ScreenSaver:
                    innerHtml += $"<a onClick=\'showScreenSaverPreview({id});\' href=\'#\'><img src=\'images/action_icons/preview.png\'></a>&nbsp";
                    break;
                case (int)AlertType.SimpleSurvey:
                    innerHtml += $"<a onClick=\'showSurveyPreview({surveyId});\' href=\'#\'><img src=\'images/action_icons/preview.png\'></a>&nbsp";
                    break;
                default:
                    if (!editPage.Contains("survey_add"))
                    {
                        innerHtml += $"<a href=\"javascript:showAlertPreview(\'{id}\')\"><img src=\'images/action_icons/preview.png\' ></a>&nbsp;";
                    }
                    innerHtml += $"<a href=\"javascript:showPropertiesForAlertWithId({id})\"><img src=\'images/action_icons/list.png\' ></a>&nbsp;";
                    break;
            }

            innerHtml += "<a href=\"javascript:approveAlert(" + id + ")\"><img src='images/action_icons/approve2.png' ></a>&nbsp;";
            innerHtml += "<a href=\"javascript:showRejectDialogForAlertWithId(" + id + ")\"><img src='images/action_icons/reject2.png' ></a>&nbsp;";
            innerHtml += "<a href=\"javascript:openUserList(" + id + ");\"><img src='images/action_icons/recipients.png' ></a>&nbsp;";
            innerHtml += "<a href=\"" + editPage + "&shouldApprove=1&edit=1\"><img src='images/action_icons/edit.png' ></a>&nbsp;";

            actionCell.Text = innerHtml;
            row.Cells.Add(actionCell);

            foreach (TableCell cell in row.Cells)
            {
                cell.HorizontalAlign = HorizontalAlign.Center;
            }

            return row;
        }

        [WebMethod]
        public static string ApproveAlert(int alertId)
        {
            using (var db = new DBManager())
            {
                var affectedRows = db.ExecuteQuery($"UPDATE alerts SET approve_status = {(int)ApproveStatus.ApprovedByModerator}, reject_reason = NULL WHERE id = {alertId}");
                var alertSet = db.GetDataByQuery($"SELECT * FROM alerts WHERE id = {alertId}");

                var campaignId = alertSet.GetInt(0, "campaign_id");
                var alertType = (AlertType) alertSet.GetInt(0, "class");

                _cacheInvalidationService.CacheInvalidation(alertType, alertId);

                if (campaignId == -1)
                {
                    Global.CacheManager.Add(alertId, DateTime.Now.ToString(DateTimeFormat.MsSqlFormat, CultureInfo.InvariantCulture));
                }
                //TODO:  change to AddPushNotificationToDb method


                return affectedRows > 0 ? "Ok" : "Fail";
            }
        }

        [WebMethod]
        public static string RejectAlert(int alertId, string reason)
        {
            int affectedRowsTotal;
            //TODO: change it to repository
            using (var db = new DBManager())
            {
                var query = $@"
                UPDATE alerts 
                SET approve_status = {(int)ApproveStatus.Rejected}, reject_reason = N'{reason}' 
                WHERE id = {alertId}";

                affectedRowsTotal = db.ExecuteQuery(query);
            }

            var alert = AlertRepository.GetById(alertId);
            var alertType = (AlertType) alert.Class;

            switch (alertType)
            {
                case AlertType.SimpleAlert:
                case AlertType.Ticker:
                case AlertType.Rsvp:
                case AlertType.Wallpaper:
                case AlertType.ScreenSaver:
                    var contentJobDto = new ContentJobDto(alertId, alertType);
                    ContentJobScheduler.DeleteJob(contentJobDto);
                    break;
            }

            return affectedRowsTotal > 0 
                ? "Ok" 
                : "Fail";
        }

        [WebMethod]
        public static string GetAlertDescription(int alertId)
        {
            DBManager db = new DBManager();
            DataSet alertProps = db.GetDataByQuery("SELECT urgent, aknown,  fullscreen, alert_width, alert_height, autoclose, self_deletable, from_date, to_date, sms, email, class, resizable FROM alerts WHERE id = " + alertId);
            db.Dispose();
            string urgent = alertProps.GetInt(0, "urgent") == 1 ? resources.LNG_YES : resources.LNG_NO;
            string aknow = alertProps.GetInt(0, "aknown") == 1 ? resources.LNG_YES : resources.LNG_NO;
            string fullscreen = alertProps.GetInt(0, "fullscreen") == 1 ? resources.LNG_YES : resources.LNG_NO;
            int width = alertProps.GetInt(0, "alert_width");
            int height = alertProps.GetInt(0, "alert_height");
            int autoclose = alertProps.GetInt(0, "autoclose");
            string selfDeletable = alertProps.GetInt(0, "self_deletable") == 1 ? resources.LNG_YES : resources.LNG_NO;
            DateTime fromDate = alertProps.GetDateTime(0, "from_date");
            DateTime toDate = alertProps.GetDateTime(0, "to_date");

            string sms = alertProps.GetInt(0, "sms") == 1 ? resources.LNG_YES : resources.LNG_NO;
            string email = alertProps.GetInt(0, "email") == 1 ? resources.LNG_YES : resources.LNG_NO;
            string resizable = alertProps.GetInt(0, "resizable") == 1 ? resources.LNG_YES : resources.LNG_NO;
            string tableHtml = "<table ><tr><th>" + resources.LNG_PROPERTY + "</th> <th>" + resources.LNG_VALUE + "</th></tr>";
            AlertType alertType = (AlertType)alertProps.GetInt(0, "class");

            if (alertType == AlertType.SimpleAlert || alertType == AlertType.Rsvp || alertType == AlertType.Ticker)
            {
                tableHtml += "<tr><td>" + resources.LNG_URGENT_ALERT
                                        + "</td><td style='text-align:center'>" + urgent + "</td></tr>";
                tableHtml += "<tr><td>" + resources.LNG_ACKNOWLEDGEMENT
                                        + "</td><td style='text-align:center'>" + aknow + "</td></tr>";
                tableHtml += "<tr><td>" + resources.LNG_SMS + "</td><td style='text-align:center'>" + sms + "</td></tr>";
                tableHtml += "<tr><td>" + resources.LNG_EMAIL + "</td><td style='text-align:center'>" + email + "</td></tr>";
                tableHtml += "<tr><td>" + resources.LNG_SELF_DELETABLE
                                        + "</td><td style='text-align:center'>" + selfDeletable + "</td></tr>";
                tableHtml += "<tr><td>" + resources.LNG_AUTOCLOSE_IN
                                        + "</td><td style='text-align:center'>" + autoclose + "</td></tr>";
            }

            if (alertType != AlertType.Wallpaper && alertType != AlertType.ScreenSaver)
            {
                tableHtml += "<tr><td>" + resources.LNG_FULLSCREEN
                                        + "</td><td style='text-align:center'>" + fullscreen + "</td></tr>";
                tableHtml += "<tr><td>" + resources.LNG_RESIZABLE + "</td><td style='text-align:center'>" + resizable + "</td></tr>";
                tableHtml += "<tr><td>" + resources.LNG_WIDTH + "</td><td style='text-align:center'>" + width + "</td></tr>";
                tableHtml += "<tr><td>" + resources.LNG_HEIGHT + "</td><td style='text-align:center'>" + height + "</td></tr>";
            }

            int lifetime = (int)(toDate - fromDate).TotalMinutes;
            if (lifetime > 0)
            {
                string factor = "minutes";
                if (lifetime % 1440 == 0)
                {
                    lifetime /= 1440;
                    factor = " days";
                }
                else if (lifetime % 60 == 0)
                {
                    lifetime /= 60;
                    factor = " hours";
                }

                if (lifetime == 1)
                    factor = factor.Remove(factor.Length - 1);

                tableHtml += "<tr><td>" + resources.LNG_LIFETIME + "</td><td style='text-align:center'>" + lifetime + factor + "</td></tr>";
            }

            return tableHtml;
        }

        #region DataProperties
        protected override string CustomQuery
        {
            get
            {
                var query = $@"
                    SELECT a.id as id, 
                        a.video as video, 
                        a.campaign_id as campaign_id, 
                        a.title as title, 
                        a.create_date as create_date, 
                        u.name as sender, 
                        a.class as type_id, 
                        a.type as type, 
                        a.urgent as urgent, 
                        a.fullscreen as fullscreen, 
                        a.ticker as ticker, 
                        s.id as is_rsvp 
                    FROM alerts as a 
                    INNER JOIN users as u ON u.id = a.sender_id 
                    LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id 
                    WHERE a.approve_status = {(int)ApproveStatus.NotModerated} 
                        AND a.type != 'D' 
                        AND a.sender_id != {curUser.Id}";

                if (SearchTerm.Length > 0)
                {
                    query += " AND title LIKE '%" + SearchTerm + "%'";
                }

                if (Request["sortBy"] != "")
                {
                    query += " ORDER BY a." + sortBy;
                }
                else
                {
                    query += " ORDER BY a.create_date DESC";
                }

                return query;
            }
        }
        protected override string CustomCountQuery
        {
            get
            {
                var query = $@"
                    SELECT count(1) 
                    FROM alerts as a 
                    INNER JOIN users as u ON u.id = a.sender_id 
                    LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id 
                    WHERE a.approve_status = {(int)ApproveStatus.NotModerated} 
                        AND a.type != 'D' 
                        AND a.sender_id != {curUser.Id}";

                if (IsPostBack && SearchTerm.Length > 0)
                {
                    query += " AND title LIKE '%" + SearchTerm + "%'";
                }

                return query;
            }
        }
        protected override string DataTable => "alerts";

        protected override string[] Collumns => new string[] { };

        protected override string DefaultOrderCollumn
        {
            get
            {

                if (!string.IsNullOrEmpty(Request["draft"]) && Request["draft"].Equals("1"))
                    return "create_date";

                return "sent_date";
            }
        }
        #endregion

        #region Interface properties
        protected override IButtonControl[] DeleteButtons => new IButtonControl[] { upDelete, bottomDelete };

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TableDiv => mainTableDiv;

        protected override System.Web.UI.HtmlControls.HtmlGenericControl NoRowsDiv => NoRows;
        protected override HtmlGenericControl BottomPaging
        {
            get { return bottomRanging; }
        }

        protected override HtmlGenericControl TopPaging
        {
            get { return topPaging; }
        }

        protected override HtmlInputText SearchControl => searchTermBox;

        #endregion
    }
}