﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ApprovePanel.aspx.cs" Inherits="DeskAlertsDotNet.Pages.ApprovePanel" %>
<%@ Import NameSpace="DeskAlertsDotNet.Utils" %>   
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>        
<%@ Import NameSpace="System.Globalization" %> 
            
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html>

    <script language="javascript">
        window.parent.parent.document.getElementById('menu').src = window.parent.parent.document.getElementById('menu').src;
    </script>
    <head> 
        <title>DeskAlerts</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
        <link href="css/style.css" rel="stylesheet" type="text/css">
       <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
        <!--[if IE 6]>
<script language="javascript" type="text/javascript" src="jscripts/DD_belatedPNG.js"></script>
<script language="javascript" type="text/javascript">DD_belatedPNG.fix('.ui-icon');</script>
<![endif]-->
        
        <script type="text/JavaScript" language="JavaScript" src="jscripts/json2.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/preview.js"></script>
        <script language="javascript" type="text/javascript" src="functions.asp"></script>
        <script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
     
	    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
	    <script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
        <style rel="stylesheet" type="text/css">
            /*.ui-dialog-titlebar-close {
  visibility: hidden;
}*/
            
            .style1 {
                height: 26px;
            }
            
            .style4
            {
                height: 26px;
                width: 56px;
            }
            .date_time_param_input{
	        width:150px
            }

            .style5
            {
                height: 26px;
                width: 208px;
            }

            .style6
            {
                height: 26px;
                width: 300px;
            }

            .main_table
            {
                width: 99%;
            }

        </style>
    </head>

    <script language="javascript" type="text/javascript">





        function openDialogUI(_form, _frame, _href, _width, _height, _title) {
            $("#dialog-" + _form).dialog('option', 'height', _height);
            $("#dialog-" + _form).dialog('option', 'width', _width);
            $("#dialog-" + _form).dialog('option', 'title', _title);
            $("#dialog-" + _frame).attr("src", _href);
            $("#dialog-" + _frame).css("height", _height);
            $("#dialog-" + _frame).css("display", 'block');
            if ($("#dialog-" + _frame).attr("src") != undefined) {
                $("#dialog-" + _form).dialog('open');
                $("#dialog-" + _form).contents().find('#table_border').css('border', 'none');
            }
        }




        $(document).ready(function() {
            $("#confirm_sending").dialog({
                autoOpen: false,
                height: 100,
                width: 300,
                modal: false,
                position: {
                    my: "center top",
                    at: "center top",
                    of: $('#parent_for_confirm')
                },
                hide: {
                    effect: 'fade',
                    duration: 1000
                },
                //show: { effect: "highlight", color: '#98FB98', duration: 2000 },
                open: function(event, ui) {
                    $(".ui-dialog-titlebar-close", ui.dialog || ui).hide();
                    setTimeout(function() {
                        $("#confirm_sending").dialog('close');
                    }, 3000);
                },
                closeOnEscape: false
            });

            $("#dialog-form").dialog({
                autoOpen: false,
                height: 100,
                width: 100,
                modal: true
            });


            var is_sended = $("#is_sended").val();
            if (is_sended == 'true') {
                if ($('#reschedule').val() == '1') {
                    $('#anchor_confirm').text('=LNG_CONFIRM_SCHEDULE ');
                } else {
                    $('#anchor_confirm').text('=LNG_CONFIRM_SEND ');
                }
                $("#last_alert").effect('highlight', {
                    color: '#98FB98'
                }, 2000);
                $("#confirm_sending").dialog('open');
            }

            var width = $(".data_table").width();

            var pwidth = $(".paginate").width();
            if (width != pwidth) {
                $(".paginate").width("100%");
            }




            $("#dialog-frame").load(function() {
                $("#dialog-frame").contents().find('.prelink_stats').on('click', function(e) {
                    e.preventDefault();
                    var link = $(this).attr('href');
                    var title = $(this).attr('title');
                    var last_id = $('.dialog_forms').size();
                    var elem = '<div id="dialog-form-' + last_id + '" class="dialog_forms" style="overflow:hidden;display:none;"><iframe id="dialog-frame-' + last_id + '" style="display:none;width:100%;height:auto;overflow:hidden;border : none;"></iframe></div>';
                    $('#secondary').append(elem);
                    $("#dialog-form-" + last_id).dialog({
                        autoOpen: false,
                        height: 100,
                        width: 100,
                        modal: true
                    });
                    openDialogUI("form-" + last_id, "frame-" + last_id, link, 350, 350, title);
                });
            });

        });



        var settingsDateFormat = "=GetDateFormat()";
        var settingsTimeFormat = "=GetTimeFormat()";

        var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
        var uiFormat = getUiDateTimeFormat(settingsDateFormat, settingsTimeFormat);

        function openDialog(mode) {
            //  var defaultValue = "Enter campaign`s name";
            // $('#linkValue').val(defaultValue);

            var dialogId = "";
            var textBoxId = ""; //duplicateDialog

            switch (mode) {
                case 1: //duplicate
                    {
                        dialogId = "#duplicateDialog";
                        textBoxId = "#dupName";
                        break;
                    }
                case 2: //add
                    {
                        dialogId = "#linkDialog";
                        textBoxId = "#linkValue";
                        $("#schedule").prop("checked", false);
                        $("#start_date_and_time").prop('disabled', true);
                        $
                        break;
                    }
            }
            $(textBoxId).focus(function() {
                $(textBoxId).select().mouseup(function(e) {
                    e.preventDefault();
                    $(this).unbind("mouseup");
                });
            });
            $(dialogId).dialog('open');

        }

        function deleteRow(id) {
            $.ajax({
                url: "set_settings.asp?name=DeleteRow&value=" + id
            }).done(function(data, textStatus, jqXHR) {
                $("#row" + id).remove();
            });


        }

        function showRejectDialogForAlertWithId(alertId) {

            $("#rejectDialog").dialog('open');
          //  $("#rejectDialog").alertId = alertId;
            $("#rejectButton").attr('alertId', alertId);
        }


        function rejectAlert() {
        
            var reason = $("#rejectReason").val()
            if(reason.length == 0)
            {
                alert('<% Response.Write(Localization.CurrentUserLocalization.Content"LNG_APPROVE_REJECT_REASON_ERROR"]); %>');
                return;
            }

            var alertId = $("#rejectButton").attr('alertId');

            $.get("set_settings.asp?name=RejectAlert&alertId=" + alertId + "&reason=" + reason, function(data) {
                location.reload();
                // parent.document.getElementById('menu').reload();

                window.parent.parent.document.getElementById('menu').src = window.parent.parent.document.getElementById('menu').src;


            });
        }
        
        

        $(function() {
            $(".delete_button").button({});

            $(".add_alert_button").button({
                icons: {
                    primary: "ui-icon-plusthick"
                }
            });

            $("#rejectDialog").dialog({
                autoOpen: false,
                height: 210,
                width: 400,
                modal: true,
                resizable: false
            });
            
            $("#linkDialog").dialog({
                autoOpen: false,
                height: 125,
                width: 550,
                modal: true,
                resizable: false
            });

            $("#rescheduleDialog").dialog({
                autoOpen: false,
                height: 130,
                width: 200,
                modal: true,
                resizable: false
            });
            $("#duplicateDialog").dialog({
                autoOpen: false,
                height: 110,
                width: 550,
                modal: true,
                resizable: false
            });

            $("#propertyDialog").dialog({
                autoOpen: false,
                height: 350,
                width: 200,
                modal: true,
                resizable: false
            });

            

        })

        function showPropertiesForAlertWithId(alertId) {

            $.get("set_settings.asp?name=getProperties&alertId=" + alertId, function(data) {

                $("#propertyTable").html(data);
                $("#propertyDialog").dialog('open');
            });
        }
        function encode_utf8(s) {
            return encodeURIComponent(s);
        }



        
        function showAlertPreview(alertId) {
            var data = new Object();

            getAlertData(alertId, false, function(alertData) {
                data = JSON.parse(alertData);
            });

            var fullscreen = parseInt(data.fullscreen);
            var ticker = parseInt(data.ticker);
            var acknowledgement = data.acknowledgement;

            var alert_width = (fullscreen == 1) && !ticker ? $(".main_table_body").width() : (!ticker ? data.alert_width : $(".main_table_body").width());
            var alert_height = (fullscreen == 1) && !ticker ? $(window).height() - document.body.clientHeight + $(".main_table_body").height() : (!ticker ? data.alert_height : "");
            var alert_title = data.alert_title;
            var alert_html = data.alert_html;

            var top_template;
            var bottom_template;
            var templateId = data.template_id;

            if (templateId >= 0) {
                getTemplateHTML(templateId, "top", false, function(data) {
                    top_template = data;
                });

                getTemplateHTML(templateId, "bottom", false, function(data) {
                    bottom_template = data;
                });
            }

            data.top_template = top_template;
            data.bottom_template = bottom_template;
            data.alert_width = alert_width;
            data.alert_height = alert_height;
            data.ticker = ticker;
            data.fullscreen = fullscreen;
            data.acknowledgement = acknowledgement;
            data.alert_title = alert_title;
            data.alert_html = alert_html;
            data.caption_href = data.caption_href;

            initAlertPreview(data);

        }       
        
    </script>

    <div id="propertyDialog">
        <div id="propertyTable" style="text-align:center">
            
        </div>
    </div>
    <body style="margin:0" class="body">
     <!--   <form id="form1" runat="server"> -->
            <div id="dialog-form" style="overflow:hidden;display:none;">
                <iframe id='dialog-frame' style="display:none;width:100%;height:auto;overflow:hidden;border : none;">

                </iframe>
            </div>
            <div id="confirm_sending" style="overflow:hidden;text-align:center;">
                <a id="anchor_confirm"></a>
            </div>
            <input type="hidden" id="is_sended" value="=Request(" is_sended ") " />

            <div id="secondary" style="overflow:hidden;display:none;">
            </div>
            <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
                <tr>
                    <td>
                        <table height="100%" cellspacing="0" cellpadding="0" class="main_table">
                            <tr>
								<td width="100%" height="31" class="main_table_title">
									<img src="images/menu_icons/approve.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
									<a href="ApprovePanel.aspx" class="header_title" style="position:absolute;top:15px">
                                        <asp:Label ID="approveLabel" runat="server" Text="Label"></asp:Label>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                            
                                <td height="100%" valign="top" class="main_table_body">
                                    <div style="margin:10px" id="parent_for_confirm">
                                        <span class="work_header"><asp:Label ID="approveDescription" runat="server" 
                                            Text="Label"></asp:Label> </span>

                                        <br />



                                      <!-- <table width="100%">
                                            <tr>
                                                <td align="left"> -->
                                                <%
                                                    if(!noRows)
                                                     Response.Write(MakePages());            
                                                    else
                                                        Response.Write("<br><br><br><center><b>" + Localization.CurrentUserLocalization.Content"LNG_NO_ALERT_TO_APPROVE"] + "</b><br><br><br></center>");

                                                %>
                                                
                                                <% if(!noRows) { %>
                                                    <br /><a href='#' class='delete_button' onclick="javascript: return LINKCLICK('Alerts');">
                                                        <asp:Label ID="deleteLabel" runat="server" Text="Label"></asp:Label>
                                                    </a>
                                               <% } %>
                                                    <br />
                                                    <br />
                                                    <br />
                                        <!--          </td>
                                            </tr>
                                        </table> -->
                                
                              <% if (!noRows)
                                 { %>          
                              <form action='delete_objects.asp' name='delete_objs' id='delete_objs' method='post'>
                                    <input type='hidden' name='objType' value='ApprovalAlerts' />
                                    <input type='hidden' name='userId' value='<% Response.Write(Request["uid"]); %>' />
                                        <table width="100%" height="100%" cellspacing="0" cellpadding="3" class="data_table" runat="server" id="contentTable">
                                            <tr class="data_table_title">
                                                <td width="10">
                                                    <input type='checkbox' name='select_all' id='select_all' onclick='javascript: select_all_objects();' />
                                                </td>
                                                <td class="style4" align="center">
                                                <% Response.Write("<a href='approve_panel.aspx?uid=" + Request["uid"] + "&order=" + (order.ToUpper() == "DESC" ? "ASC" : "DESC") + "&collumn=class'"); %>
                                                    <asp:Label ID="typeLabel" runat="server" Text="Type"></asp:Label>
                                                 <% Response.Write("</a>"); %>
                                                </td>
                                                <td class="style6" align="center">
                                                <% Response.Write("<a href='approve_panel.aspx?uid=" + Request["uid"] + "&order=" + (order.ToUpper() == "DESC" ? "ASC" : "DESC") + "&collumn=title'"); %>
                                                    <asp:Label ID="titleLabel" runat="server" Text="Title"></asp:Label>
                                                <% Response.Write("</a>"); %>
                                                </td>
                                                <td class="style5" align="center">
                                                  <% Response.Write("<a href='approve_panel.aspx?uid=" + Request["uid"] + "&order=" + (order.ToUpper() == "DESC" ? "ASC" : "DESC") + "&collumn=sender_id'"); %>
                                                    <asp:Label ID="senderLabel" runat="server" Text="Sender"></asp:Label>
                                                  <% Response.Write("</a>"); %>
                                                </td>
                                                 <td class="style5" align="center">
                                                  <% Response.Write("<a href='approve_panel.aspx?uid=" + Request["uid"] + "&order=" + (order.ToUpper() == "DESC" ? "ASC" : "DESC") + "&collumn=create_date'" ); %>
                                                    <asp:Label ID="creationDateLabel" runat="server" Text="CreationDate"></asp:Label>
                                                  <% Response.Write("</a>"); %>
                                                </td>
                                                <td class="style1" width="120" align="center">
                                                    <asp:Label ID="actionsLabel" runat="server" Text="Actions"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                           

                                        <img src="images/inv.gif" alt="" width="1" height="100%" border="0">
                                         <input type='hidden' name='back' value='ApprovalAlerts' />
                                   </form>
                                   <% } %>
                                   
                                   <% if (!noRows) { %>
                                    <br /><a href='#' class='delete_button' onclick="javascript: return LINKCLICK('Alerts');">
                                       <asp:Label ID="deleteLabelButtom" runat="server" Text="Label"></asp:Label> </a>
                                       <%
                                       Response.Write(MakePages());
                                      }
                                       %>
                                    </div>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>
            

   
             <script language=javascript>
     

                 function approveAlert(alertId) {

                     $.get("set_settings.asp?name=ApproveAlert&alertId=" + alertId, function(data) {
                         location.reload();

                         window.parent.parent.document.getElementById('menu').src = window.parent.parent.document.getElementById('menu').src;
                     
                      });
                 }

 

                 
                 function openUserList(id) {


                     $.get('view_recipients_list.asp?id=' + id, function(data) {

                         //$("#userListDialog").append("<div id='list'>" + data + "</div>");
                        $("#userListDialog").append("<iframe id='list' style='width: 100%; overflow: hidden; border: none; height: 350px; display: block;' src='view_recipients_list.asp?id=" + id + "'></iframe>");
                         $("#userListDialog").dialog({
                             width: 400,
                             height: 400,
                             modal: true,
                             autoOpen: false,
                             close: function() {
                                 $("#list").remove();
                             }
                         }).dialog('open');

                     });
                 }
                 
             </script>

             <div runat="server" id="rejectDialog">       
                <label><% Response.Write(Localization.CurrentUserLocalization.Content"LNG_APPROVE_ENTER_REASON_REJECT"]); %></label><br /> <br />
                
                <textarea id="rejectReason" rows="5" cols="50" style="resize:none" placeholder="<% Response.Write(Localization.CurrentUserLocalization.Content"LNG_APPROVE_REASON_REJECT"]); %>"></textarea>
                <br /> <br />
                
                <a id="rejectButton"  href=''  class="delete_button" style="float:right" onclick="rejectAlert();"> <% Response.Write(Localization.CurrentUserLocalization.Content"LNG_REJECT"]); %>  </a>      
             </div>
             
             <div id="userListDialog"></div>
            
      <!--  </form> -->
    </body>