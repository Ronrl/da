﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Net;
using System.IO;

namespace DeskAlertsDotNet
{

    public partial class RejectedAlerts : System.Web.UI.Page
    {
      
        public int offset;
        public int limit;
        public string sortBy;
        public int count;
       public int Offset { get { return offset; } }
       public int Limit { get { return limit; } }
       public string SortBy { get { return sortBy; } }
       public int Count { get { return count; } }
       public Dictionary<string, string> vars = new Dictionary<string,string>();
          public string order;

          public bool noRows;


       public  string page ;
       public string name;
       string orderCollumn;
       public string Name { get { return name; } }


       protected void Page_PreInit(object sender, EventArgs e)
       {
           
           order = !string.IsNullOrEmpty(Request.Params["order"]) ? Request.Params["order"] : "DESC";
           orderCollumn = !string.IsNullOrEmpty(Request.Params["collumn"]) ? Request.Params["collumn"] : "create_date";
          offset = !string.IsNullOrEmpty(Request.Params["offset"]) ? Convert.ToInt32(Request.Params["offset"]) : 0;
          limit = !string.IsNullOrEmpty(Request.Params["limit"]) ? Convert.ToInt32(Request.Params["limit"]) : 25;
          sortBy = !string.IsNullOrEmpty(Request.Params["sortby"]) ? Request.Params["sortby"] : "date DESC";

          page = "RejectedAlerts.aspx?uid=" + Request["uid"] + "%26";
           
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            vars = GetAspVariables("LNG_DELETE", "LNG_REJECTED_ALERTS", "LNG_LABEL_REJECTED_ALERTS_DESCRIPTION", "LNG_TITLE", "LNG_TYPE", "LNG_APPROVE_ENTER_REASON_REJECT", "LNG_APPROVE_REASON_REJECT", "LNG_REJECT_COMMENT", "LNG_FULL_REASON", "LNG_NO_REJECTED_ALERTS");

            typeLabel.Text = vars["LNG_TYPE"];
            rejectDescription.Text = vars["LNG_LABEL_REJECTED_ALERTS_DESCRIPTION"];
            rejectLabel.Text = vars["LNG_REJECTED_ALERTS"];
            titleLabel.Text = vars["LNG_TITLE"];
            rejectCommentLabel.Text = vars["LNG_REJECT_COMMENT"];
         //  actionsLabel.Text = vars["LNG_ACTIONS"];

           deleteLabelButtom.Text =  deleteLabel.Text =  vars["LNG_DELETE"];
           ParseAndFillContentTable();


           
        }

        public Dictionary<string, string> GetAspVariables(params string[] keys)
        {
            string queryString = String.Join(":", keys);

            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("RejectedAlerts"));
            string url_bridge = path + "/dotnetbridge.asp?var=" + queryString;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
           // Response.Write(reader.ReadToEnd());
            string[] variablesStrings = reader.ReadToEnd().Split('&');
            

            Dictionary<string, string> result = new Dictionary<string, string>();
            foreach (string keyVal in variablesStrings)
            {

                string[] keyValArray = keyVal.Split('|');

                if (keyValArray.Length != 2)
                    continue;

                string key = keyValArray[0];
                string value = keyValArray[1];

                result.Add(key, value);
            }

            return result;

            
        }
        public string GetAspVariable(string variableName)
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("RejectedAlerts"));
            string url_bridge = path + "/dotnetbridge.asp?var="+variableName;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string variable = reader.ReadToEnd();
            return variable;
        }

        public string GetRejectedAlerts()
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("RejectedAlerts"));
            string url_bridge = path + "/set_settings.asp?name=GetMyRejectedAlerts&user_id=" + Request["uid"] +  "&value=" + order + "&collumn=" + orderCollumn;
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());

            return reader.ReadToEnd();

        }

        public string MakePages()
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string path = url.Substring(0, url.IndexOf("RejectedAlerts"));
            string url_bridge = path + "/dotnetbridge.asp?action=makepage&offset="+Offset+"&limit="+Limit+"&count="+Count+"&page="+page+"&name="+Name+"&sortby="+SortBy;
          
            WebRequest request = WebRequest.Create(url_bridge);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string variable = reader.ReadToEnd();
            return variable;

        }
        public void ParseAndFillContentTable()
        {
            string data = GetRejectedAlerts();
            
            string[] rows = data.Split('\n');

            noRows = data == string.Empty;
            foreach (string row in rows)
            {

                if (row == string.Empty)
                    return;

                    string[] fields = row.Split('\r');

                   if (fields.Length == 5)
                    {
                        HtmlTableRow htmlRow = FormatTableRow(fields[0], fields[1], fields[2], fields[3], fields[4]);
                      
                        contentTable.Rows.Add(htmlRow);
                        count++;
                    }
 
            }
        }

        public HtmlTableRow FormatTableRow(string id, string title, string reason, string type, string editPage)
        {

            HtmlTableRow row = new HtmlTableRow();
            row.Attributes.Add("id", "row" + id);
            HtmlTableCell checkCell = new HtmlTableCell();
            checkCell.InnerHtml = "<input type=\"checkbox\" name=\"objects\" value=\"" + id + "\">";
            row.Cells.Add(checkCell);

            HtmlTableCell typeCell = new HtmlTableCell();
            typeCell.Align = "center";
            typeCell.InnerHtml = type;
            row.Cells.Add(typeCell);

            HtmlTableCell titleCell = new HtmlTableCell();
            titleCell.Align = "center";
            titleCell.InnerHtml = title;
            row.Cells.Add(titleCell);


            if (reason.Length > 70)
            {
                string fullReason = reason;
                reason = reason.Substring(0, 70) + "...";
                reason = "<a href='#' onclick='openReason(\"" + fullReason + "\");'>" + reason + "</a>";
            }
            HtmlTableCell reasonCell = new HtmlTableCell();
             reasonCell.Align = "left";
             reasonCell.InnerHtml = reason;
            row.Cells.Add(reasonCell);           




            HtmlTableCell actionCell = new HtmlTableCell();
            actionCell.Align = "center";
            string innerHtml = string.Empty;

            //innerHtml += "<a href=\"javascript:approveAlert(" + id + ")\"><img src='images/action_icons/approve.png' ></a>&nbsp;";
            //innerHtml += "<a href=\"javascript:showRejectDialogForAlertWithId(" + id + ")\"><img src='images/action_icons/reject.png' ></a>&nbsp;";
            //innerHtml += "<a href=\"javascript:showAlertPreview('"+ id + "')\"><img src='images/action_icons/preview.png' ></a>&nbsp;";
            //innerHtml += "<a href=\"javascript:showPropertiesForAlertWithId(" + id + ")\"><img src='images/action_icons/list.png' ></a>&nbsp;";
            innerHtml += "<a href=\"" + editPage + "&rejectedEdit=1&edit=1\"><img src='images/action_icons/edit.png' ></a>&nbsp;";
   
            actionCell.InnerHtml = innerHtml;
            row.Cells.Add(actionCell);
           

            return row;
        }




    }
}
