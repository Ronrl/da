﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RejectedAlerts.aspx.cs" Inherits="DeskAlertsDotNet.RejectedAlerts" %>
            
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html>

    <head>
        <title>DeskAlerts</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
        <link href="css/style9.css" rel="stylesheet" type="text/css">
       <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
        
        <script type="text/JavaScript" language="JavaScript" src="jscripts/json2.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>
        <script language="javascript" type="text/javascript" src="functions.js"></script>
        <script language="javascript" type="text/javascript" src="jscripts/date.js"></script>
     
	    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
	    <script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
        <style rel="stylesheet" type="text/css">
            /*.ui-dialog-titlebar-close {
  visibility: hidden;
}*/
            
            .style1 {
                height: 26px;
            }
            
            .style4
            {
                height: 26px;
                width: 56px;
            }
            .date_time_param_input{
	        width:150px
            }

            .style6
            {
                height: 26px;
                width: 151px;
            }

            .style7
            {
                height: 26px;
                width: 957px;
            }

        </style>
    </head>

    <script language="javascript" type="text/javascript">





        function openDialogUI(_form, _frame, _href, _width, _height, _title) {
            $("#dialog-" + _form).dialog('option', 'height', _height);
            $("#dialog-" + _form).dialog('option', 'width', _width);
            $("#dialog-" + _form).dialog('option', 'title', _title);
            $("#dialog-" + _frame).attr("src", _href);
            $("#dialog-" + _frame).css("height", _height);
            $("#dialog-" + _frame).css("display", 'block');
            if ($("#dialog-" + _frame).attr("src") != undefined) {
                $("#dialog-" + _form).dialog('open');
                $("#dialog-" + _form).contents().find('#table_border').css('border', 'none');
            }
        }




        $(document).ready(function() {
            $("#confirm_sending").dialog({
                autoOpen: false,
                height: 100,
                width: 300,
                modal: false,
                position: {
                    my: "center top",
                    at: "center top",
                    of: $('#parent_for_confirm')
                },
                hide: {
                    effect: 'fade',
                    duration: 1000
                },
                //show: { effect: "highlight", color: '#98FB98', duration: 2000 },
                open: function(event, ui) {
                    $(".ui-dialog-titlebar-close", ui.dialog || ui).hide();
                    setTimeout(function() {
                        $("#confirm_sending").dialog('close');
                    }, 3000);
                },
                closeOnEscape: false
            });

            $("#dialog-form").dialog({
                autoOpen: false,
                height: 100,
                width: 100,
                modal: true
            });


            var is_sended = $("#is_sended").val();
            if (is_sended == 'true') {
                if ($('#reschedule').val() == '1') {
                    $('#anchor_confirm').text('=LNG_CONFIRM_SCHEDULE ');
                } else {
                    $('#anchor_confirm').text('=LNG_CONFIRM_SEND ');
                }
                $("#last_alert").effect('highlight', {
                    color: '#98FB98'
                }, 2000);
                $("#confirm_sending").dialog('open');
            }

            var width = $(".data_table").width();

            var pwidth = $(".paginate").width();
            if (width != pwidth) {
                $(".paginate").width("100%");
            }




            $("#dialog-frame").load(function() {
                $("#dialog-frame").contents().find('.prelink_stats').on('click', function(e) {
                    e.preventDefault();
                    var link = $(this).attr('href');
                    var title = $(this).attr('title');
                    var last_id = $('.dialog_forms').size();
                    var elem = '<div id="dialog-form-' + last_id + '" class="dialog_forms" style="overflow:hidden;display:none;"><iframe id="dialog-frame-' + last_id + '" style="display:none;width:100%;height:auto;overflow:hidden;border : none;"></iframe></div>';
                    $('#secondary').append(elem);
                    $("#dialog-form-" + last_id).dialog({
                        autoOpen: false,
                        height: 100,
                        width: 100,
                        modal: true
                    });
                    openDialogUI("form-" + last_id, "frame-" + last_id, link, 350, 350, title);
                });
            });

        });



        var settingsDateFormat = "=GetDateFormat()";
        var settingsTimeFormat = "=GetTimeFormat()";

        var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
        var uiFormat = getUiDateTimeFormat(settingsDateFormat, settingsTimeFormat);

        function openDialog(mode) {
            //  var defaultValue = "Enter campaign`s name";
            // $('#linkValue').val(defaultValue);

            var dialogId = "";
            var textBoxId = ""; //duplicateDialog

            switch (mode) {
                case 1: //duplicate
                    {
                        dialogId = "#duplicateDialog";
                        textBoxId = "#dupName";
                        break;
                    }
                case 2: //add
                    {
                        dialogId = "#linkDialog";
                        textBoxId = "#linkValue";
                        $("#schedule").prop("checked", false);
                        $("#start_date_and_time").prop('disabled', true);
                        $
                        break;
                    }
            }
            $(textBoxId).focus(function() {
                $(textBoxId).select().mouseup(function(e) {
                    e.preventDefault();
                    $(this).unbind("mouseup");
                });
            });
            $(dialogId).dialog('open');

        }

        function deleteRow(id) {
            $.ajax({
                url: "set_settings.asp?name=DeleteRow&value=" + id
            }).done(function(data, textStatus, jqXHR) {
                $("#row" + id).remove();
            });


        }

        function showRejectDialogForAlertWithId(alertId) {

            $("#rejectDialog").dialog('open');
          //  $("#rejectDialog").alertId = alertId;
            $("#rejectButton").attr('alertId', alertId);
        }



        
        

        $(function() {
            $(".delete_button").button({});

            $(".add_alert_button").button({
                icons: {
                    primary: "ui-icon-plusthick"
                }
            });

            $("#rejectDialog").dialog({
                autoOpen: false,
                height: 210,
                width: 400,
                modal: true,
                resizable: false
            });
            
            $("#linkDialog").dialog({
                autoOpen: false,
                height: 125,
                width: 550,
                modal: true,
                resizable: false
            });

            $("#rescheduleDialog").dialog({
                autoOpen: false,
                height: 130,
                width: 200,
                modal: true,
                resizable: false
            });
            $("#duplicateDialog").dialog({
                autoOpen: false,
                height: 110,
                width: 550,
                modal: true,
                resizable: false
            });

            $("#propertyDialog").dialog({
                autoOpen: false,
                height: 350,
                width: 170,
                modal: true,
                resizable: false
            });

            

        })

        function showPropertiesForAlertWithId(alertId) {

            $.get("set_settings.asp?name=getProperties&alertId=" + alertId, function(data) {

                $("#propertyTable").html(data);
                $("#propertyDialog").dialog('open');
            });
        }
        function encode_utf8(s) {
            return encodeURIComponent(s);
        }

        function addCampaign() {
            var name = $("#linkValue").val();
            var date = "NULL";
            if ($("schedule").prop('checked')) {
                date = $("#start_date_and_time").val();
            }

            name = name.replace("#", "%23");
            name = name.replace("&", "%26");

            $.get("set_settings.asp?name=AddRow&value=" + encode_utf8(name) + "&date=" + date, function(data) {

                if (isNaN(parseInt(data, 10))) {
                    alert(data);
                }
                else {
                    location.replace("CampaignDetails.aspx?campaignid=" + data);
                }

            });
        }


        function duplicateDialogFunc(id, name, event) {

            $("#dupName").focus(function() {
                $("#dupName").select().mouseup(function(e) {
                    e.preventDefault();
                    $(this).unbind("mouseup");
                });
            });

            $("#dupName").val("Copy of " + name);
            $("#dupCampId").val(id);


            $("#duplicateDialog").dialog('open');

        }


        function showAlertPreview(alertId) {
            var data = new Object();

            getAlertData(alertId, false, function(alertData) {
                data = JSON.parse(alertData);
            });

            var fullscreen = parseInt(data.fullscreen);
            var ticker = parseInt(data.ticker);
            var acknowledgement = data.acknowledgement;

            var alert_width = (fullscreen == 1) && !ticker ? $(".main_table_body").width() : (!ticker ? data.alert_width : $(".main_table_body").width());
            var alert_height = (fullscreen == 1) && !ticker ? $(window).height() - document.body.clientHeight + $(".main_table_body").height() : (!ticker ? data.alert_height : "");
            var alert_title = data.alert_title;
            var alert_html = data.alert_html;

            var top_template;
            var bottom_template;
            var templateId = data.template_id;

            if (templateId >= 0) {
                getTemplateHTML(templateId, "top", false, function(data) {
                    top_template = data;
                });

                getTemplateHTML(templateId, "bottom", false, function(data) {
                    bottom_template = data;
                });
            }

            data.top_template = top_template;
            data.bottom_template = bottom_template;
            data.alert_width = alert_width;
            data.alert_height = alert_height;
            data.ticker = ticker;
            data.fullscreen = fullscreen;
            data.acknowledgement = acknowledgement;
            data.alert_title = alert_title;
            data.alert_html = alert_html;
            data.caption_href = data.caption_href;

            initAlertPreview(data);

        }       
        
    </script>

    <div id="propertyDialog">
        <div id="propertyTable">
            
        </div>
    </div>
    <body style="margin:0" class="body">
     <!--   <form id="form1" runat="server"> -->
            <div id="dialog-form" style="overflow:hidden;display:none;">
                <iframe id='dialog-frame' style="display:none;width:100%;height:auto;overflow:hidden;border : none;">

                </iframe>
            </div>
            <div id="confirm_sending" style="overflow:hidden;text-align:center;">
                <a id="anchor_confirm"></a>
            </div>
            <input type="hidden" id="is_sended" value="=Request(" is_sended ") " />

            <div id="secondary" style="overflow:hidden;display:none;">
            </div>
            <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
                <tr>
                    <td>
                        <table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
                            <tr>
								<td width="100%" height="31" class="main_table_title">
									<img src="images/menu_icons/reject.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
									<a href="campaign.aspx" class="header_title" style="position:absolute;top:15px">
                                        <asp:Label ID="rejectLabel" runat="server" Text="Label"></asp:Label>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                            
                                <td height="100%" valign="top" class="main_table_body">
                                    <div style="margin:10px" id="parent_for_confirm">
                                        <span class="work_header"><asp:Label ID="rejectDescription" runat="server" 
                                            Text="Label"></asp:Label> </span>

                                        <br />



                                      <!-- <table width="100%">
                                            <tr>
                                                <td align="left"> -->
                                                <%
                                                    if(!noRows)
                                                     Response.Write(MakePages());            
                                                    else
                                                        Response.Write("<br><br><br><center><b>" + vars["LNG_NO_REJECTED_ALERTS"] + "</b><br><br><br></center>");

                                                %>
                                                
                                                <% if(!noRows) { %>
                                                    <br /><a href='#' class='delete_button' onclick="javascript: return LINKCLICK('Alerts');">
                                                        <asp:Label ID="deleteLabel" runat="server" Text="Label"></asp:Label>
                                                    </a>
                                               <% } %>
                                                    <br />
                                                    <br />
                                                    <br />
                                        <!--          </td>
                                            </tr>
                                        </table> -->
                                
                              <% if (!noRows)
                                 { %>          
                              <form action='delete_objects.asp' name='delete_objs' id='delete_objs' method='post'>
                                    <input type='hidden' name='objType' value='RejectedAlerts' />
                                    <input type='hidden' name='userId' value='<% Response.Write(Request["uid"]); %>' />
                                        <table width="100%" height="100%" cellspacing="0" cellpadding="3" class="data_table" runat="server" id="contentTable">
                                            <tr class="data_table_title">
                                                <td width="10">
                                                    <input type='checkbox' name='select_all' id='select_all' onclick='javascript: select_all_objects();' />
                                                </td>
                                                <td class="style4" align="center">
                                                <% Response.Write("<a href='RejectedAlerts.aspx?&uid="+ Request["uid"] +"&order=" + (order.ToUpper() == "DESC" ? "ASC" : "DESC") + "&collumn=class'"); %>
                                                    <asp:Label ID="typeLabel" runat="server" Text="Type"></asp:Label>
                                                 <% Response.Write("</a>"); %>
                                                </td>
                                                <td class="style6" align="center">
                                                <% Response.Write("<a href='RejectedAlerts.aspx?&uid="+ Request["uid"] +"&order=" + (order.ToUpper() == "DESC" ? "ASC" : "DESC") + "&collumn=title'"); %>
                                                    <asp:Label ID="titleLabel" runat="server" Text="Title"></asp:Label>
                                                <% Response.Write("</a>"); %>
                                                </td>
                                                <td class="style7" align="center">
                                                  <% //Response.Write("<a href='RejectedAlerts.aspx?&uid=" + Request["uid"] + "&order=" + (order.ToUpper() == "DESC" ? "ASC" : "DESC") + "&collumn=reject_reason'"); %>
                                                    <asp:Label ID="rejectCommentLabel" runat="server" Text="Reject comment"></asp:Label>
                                                  <% //Response.Write("</a>"); %>
                                                </td>

                                                <td class="style1" width="30" align="center">
                                                    <asp:Label ID="actionsLabel" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                           

                                        <img src="images/inv.gif" alt="" width="1" height="100%" border="0">
                                         <input type='hidden' name='back' value='RejectedAlerts' />

                                   </form>
                                   <% } %>
                                   
                                   <% if (!noRows) { %>
                                    <br /><a href='#' class='delete_button' onclick="javascript: return LINKCLICK('Alerts');">
                                       <asp:Label ID="deleteLabelButtom" runat="server" Text="Label"></asp:Label> </a>
                                       <%
                                       Response.Write(MakePages());
                                      }
                                       %>
                                    </div>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>
            

            <div runat="server" id="linkDialog" style="display:none">
                <label id="campaignNameLabel" for="linkValue"></label>
                <input  runat="server" id="linkValue" type="text" style="width:500px" /><br />
               
              <!--  <input id="schedule" type="checkbox" name="schedule"><% Response.Write(GetAspVariable("LNG_SCHEDULE_CAMPAIGNS")); %></input ><br />
                 <% //Response.Write(GetAspVariable("LNG_CAMPAIGNS_START")); %>
                 <input name='start_date' id="start_date" type="hidden"/> 
                <input  disabled="true"  name="start_date_and_time" class="date_time_param_input" id="start_date_and_time" type="text" value="<% Response.Write(DateTime.Now.ToString("dd/MM/yyyy HH:mm")); %>" size="20"/>
                
                <br />-->
                <br />
                
                <a href='#' class="add_alert_button" style=" float:right;" onclick="javascript:addCampaign();">
                                  </a>
             </div>
             <script language=javascript>
                 $("#schedule").click(function() {
                     // alert('click');
                     $("#start_date_and_time").prop('disabled', !this.checked);
                 });

                 function initDateTimePicker(element, settingsDateFormat, settingsTimeFormat) {
                     //  alert(element.length);
                     (function($) {
                         if (element.length > 0) {
                             var dateFormat = parseDateFormat(settingsDateFormat);
                             //  alert(dateFormat);
                             $(element).datetimepicker({
                                 dateFormat: dateFormat,
                                 timeFormat: (settingsTimeFormat == "2") ? "hh:mm TT" : "HH:mm"
                             });
                         }
                     })(jQuery);
                 }

                 initDateTimePicker($("#reschedulePicker"), '<% Response.Write(GetAspVariable("GetDateFormat()")); %>', '<% Response.Write(GetAspVariable("GetTimeFormat()")); %>');



                 function rescheduleCampaign() {
                     var newdate = $("#reschedulePicker").val();
                     var id = $("#reCampId").val();
                     $.get("set_settings.asp?name=ReCampaign&id=" + id + "&date=" + newdate, function(data) { location.reload(); });
                 }

                 function approveAlert(alertId) {

                     $.get("set_settings.asp?name=ApproveAlert&alertId=" + alertId, function(data) { location.reload(); });
                 }

                 function openReDialog(date, name, id) {
                     if (date == "") {
                         date = '<% =DateTime.Now.ToString("dd/MM/yyyy HH:mm") %>';
                     }

                     $("#reCampId").val(id);
                     $("#reschedulePicker").val(date);

                    // name =  name;


                     $("#rescheduleDialog").attr('title', name);
                     $("#rescheduleDialog").dialog('open');
                     $("#reschedulePicker").blur();
                 }

                 function openReason(text) {
                     $("#reason_dialog").dialog({
                         width: 400,
                         height: 300,
                         modal: true,
                         autoOpen: false,
                         close: function() {
                             $("#reason_p").remove();
                         }
                     });
                     $("#reason_dialog").append("<p id='reason_p'>" + text + "</p>");
                     $("#reason_dialog").dialog('open');
                 }
             </script>
 
             <div runat="server" id="rejectDialog">       
                <label><% //Response.Write(vars["LNG_APPROVE_ENTER_REASON_REJECT"]); %></label><br /> <br />
                
                &nbsp;<br /> <br />
                
                <a id="rejectButton"  href=''  class="delete_button" style="float:right" onclick="rejectAlert();"> <% //'Response.Write(vars["LNG_REJECT"]); %>  </a>      
             </div>
            <div title="<% Response.Write(vars["LNG_FULL_REASON"]); %>" id="reason_dialog"></div>
                     
            
      <!--  </form> -->
    </body>