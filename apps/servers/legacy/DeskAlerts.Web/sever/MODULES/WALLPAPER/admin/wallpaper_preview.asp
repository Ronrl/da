<!-- #include file="config.inc" -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<% 
Response.Expires = 0 

%>
<html>
<head>
<style>
	html, body {
		padding:0px;
		margin:0px;
		width:100%;
		height:100%;
		overflow:hidden;
		border:0px;
	}

<%
image = Request("image_src")
position = Request("position")

imgHtml = ""
styleHtml = ""

if (position = "1") then
	imgHtml = "<div style='width:100%; height:100%;' id='bg_img'></div>"
	styleHtml = "background-repeat: no-repeat; background-position: 50% 50%; background-attachment: fixed;"
elseif (position = "2") then
	imgHtml = "<div style='width:100%; height:100%; ' id='bg_img'></div>"
	styleHtml = "background-repeat: repeat;"
elseif (position = "3") then
	imgHtml = "<img style='width:100%; height:100%; position:absolute; ' src='"+image+"'/>"
end if

%>
	#bg_img {
		background-image: url('<%=image%>');
		<%=styleHtml%>
	}
</style>
</head>
<body>
	<%=imgHtml%>
</body>
</html>