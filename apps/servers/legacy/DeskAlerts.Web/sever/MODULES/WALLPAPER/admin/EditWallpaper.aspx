﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditWallpaper.aspx.cs" Inherits="DeskAlertsDotNet.Pages.EditWallpaper" %>
<%@ Import NameSpace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="DeskAlerts.Server.Utils" %>
<%@ Import Namespace="DeskAlerts.Server.Utils.Helpers" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="css/style9.css" rel="stylesheet" type="text/css">
    <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet"
        type="text/css">

    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>

    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>

    <script language="javascript" type="text/javascript" src="jscripts/jquery.jqplot.min.js"></script>

    <script language="javascript" type="text/javascript" src="jscripts/jqplot.highlighter.min.js"></script>

    <script type="text/javascript" src="jscripts/jqplot.pieRenderer.min.js"></script>

    <script type="text/javascript" src="jscripts/jqplot.barRenderer.min.js"></script>

    <script type="text/javascript" src="jscripts/jqplot.categoryAxisRenderer.min.js"></script>

    <link rel="stylesheet" type="text/css" href="css/jquery.jqplot.css" />
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>

    <script language="javascript" type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>

    <script language="javascript" type="text/javascript" src="jscripts/json2.js"></script>

    <script language="javascript" type="text/javascript" src="jscripts/date.js"></script>

    <script language="javascript" type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>

    <script language="JavaScript" src="jscripts/FusionCharts.js"></script>

    <script language="javascript" type="text/javascript" src="functions.js"></script>
 
    
    <script language="JavaScript" type="text/javascript">
        function openDialogUI(_form, _frame, _href, _width, _height, _title) {
            $("#dialog-" + _form).dialog('option', 'height', _height);
            $("#dialog-" + _form).dialog('option', 'width', _width);
            $("#dialog-" + _form).dialog('option', 'title', _title);
            $("#dialog-" + _frame).attr("src", _href);
            $("#dialog-" + _frame).css("height", _height);
            $("#dialog-" + _frame).css("display", 'block');
            if ($("#dialog-" + _frame).attr("src") != undefined) {
                $("#dialog-" + _form).dialog('open');
                $("#dialog-" + _form).contents().find('#table_border').css('border', 'none');
            }
        }


        var serverDate;
        $(document).ready(function () {

            $("#dialog-form").dialog({
                autoOpen: false,
                height: 100,
                width: 100,
                modal: true
            });

             serverDate = new Date(getDateFromAspFormat("<% =DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")%>", responseDbFormat));

            $('#dialog-frame').on('load', function () {
                $("#dialog-frame").contents().find('.prelink_stats').on('click', function (e) {
                    e.preventDefault();
                    var link = $(this).attr('href');
                    var title = $(this).attr('title');
                    var last_id = $('.dialog_forms').size();
                    var elem = '<div id="dialog-form-' + last_id + '" class="dialog_forms" style="overflow:hidden;display:none;"><iframe id="dialog-frame-' + last_id + '" style="display:none;width:100%;height:auto;overflow:hidden;border : none;"></iframe></div>';
                    $('#secondary').append(elem);
                    $("#dialog-form-" + last_id).dialog({
                        autoOpen: false,
                        height: 100,
                        width: 100,
                        modal: true
                    });
                    openDialogUI("form-" + last_id, "frame-" + last_id, link, 350, 350, title);
                });
            });
        });




        function stat_submit(url) {
            if (url != "no addon") {
                document.getElementById("date_form").action = url;
                submitDateForm();
            }
            else {
                $("#dialog_text").html("Extended statistics functionality is available as an optional add-on. If you'd like to get that add-on, please contact sale department: <a href=\"mailto:sales@deskalerts.com\">sales@deskalerts.com</a>");
                $("#dialog").dialog("open");
            }
        }

        function my_disable(i) {
            if (i == 1) {
                enable_onlick(document.getElementById("tcalico_0"));
                enable_onlick(document.getElementById("tcalico_1"));
                document.getElementById("range").disabled = true;
                document.getElementById("from_date").disabled = false;
                document.getElementById("to_date").disabled = false;
            }
            else {
                disable_onlick(document.getElementById("tcalico_0"));
                disable_onlick(document.getElementById("tcalico_1"));
                document.getElementById("range").disabled = false;
                document.getElementById("from_date").disabled = true;
                document.getElementById("to_date").disabled = true;
            }
        }
        function check_date(form) {
            if (form.select_date_radio[1].checked) {
                try {

                    var fromDate = $.trim($("#from_date").val());
                    var toDate = $.trim($("#to_date").val());

                    if (!fromDate) {
                        alert("You enter wrong time period!");
                        return false;
                    }
                    if (!toDate) {
                        alert("You enter wrong time period!");
                        return false;
                    }

                    if (!isDate(fromDate, settingsDateFormat)) {
                        alert("You enter wrong time period!");
                        return false;
                    }
                    else {
                        if (fromDate != "") {
                            fromDate = new Date(getDateFromFormat(fromDate, settingsDateFormat));
                            $("#start_date").val(formatDate(fromDate, saveDbDateFormat));
                        }
                    }

                    if (!isDate(toDate, settingsDateFormat)) {
                        alert("You enter wrong time period!");
                        return false;
                    }
                    else {
                        if (toDate != "") {
                            toDate = new Date(getDateFromFormat(toDate, settingsDateFormat))
                            $("#end_date").val(formatDate(toDate, saveDbDateFormat));
                        }
                    }

                    if (fromDate > toDate) {

                        alert("You enter wrong time period!");
                        return false;
                    }
                }
                catch (e) { alert(e) }
            }
            return true;
        }
        function disable_onlick(el) {
            if (el && !el._onclick) {
                el._onclick = el.onclick;
                el.onclick = "return false";
            }
        }
        function enable_onlick(el) {
            if (el && el._onclick) {
                el.onclick = el._onclick;
                el._onclick = "";
            }
        }

        function showLoader() {

            $.grep($(window.top.document).find("frame"), function (n, i) {
                if ($(n).attr("name") == "main") {
                    var doc = $(n)[0].contentDocument || $(n)[0].contentWindow.document;
                    $("#loader", doc).fadeIn(500);
                    $("#shadow", doc).fadeIn(500);
                }
            });
        }
        function hideLoader() {
            $.grep($(window.top.document).find("frame"), function (n, i) {
                if ($(n).attr("name") == "main") {
                    var doc = $(n)[0].contentDocument || $(n)[0].contentWindow.document;
                    $("#loader", doc).fadeOut(500);
                    $("#shadow", doc).fadeOut(500);
                }
            });
        }

        function submitDateForm() {
            if (check_date($("#date_form")[0])) {
                if (window.parent && window.parent.showLoader) { window.parent.showLoader(); }
                $("#date_form").submit();
            }
        }

        $(function () {
            $("#dialog").dialog({
                title: 'Error',
                autoOpen: false,
                modal: true,
                open: function (event, ui) {
                    $('.ui-widget-overlay').bind('click', function () {
                        $("#dialog").dialog('close');
                    });
                },
                buttons: {
                    'OK': function () {
                        $(this).dialog("close");
                    }
                }
            });

            $(".date_time_param_input")
                .datetimepicker({ dateFormat: settingsDateFormat, timeFormat: settingsTimeFormat });
            $(".date_param_input").datepicker({ dateFormat: settingsDateFormat, showButtonPanel: true });
            $(".time_param_input").timepicker({ timeFormat: settingsTimeFormat });

            $(".delete_button").button();
            $(".close_button").button();

            $(".search_button").button({
                icons: {
                    primary: "ui-icon-search"
                }
            });

            $("#statistics_show_button").button();
        });

        function showPrompt(content) {
            $("#dialog-modal > p").empty();
            $("#dialog-modal > p").append(content)
            $("#dialog-modal").dialog({
                height: 150,
                modal: true,
                resizable: false,
                draggable: false
            });
        }

        function sendAlert(id, to_date) {
            var toDate = new Date(getDateFromFormat(to_date, "dd/MM/yyyy HH:mm:ss"));
            if (toDate < serverDate) {
                showPrompt(createExpiredMessage(id));
            }
            else {
                document.location = "{draftSendLink}" + "&id=" + id
            }
        }

        function createExpiredMessage(id) {
            var $table = $("<table style='margin:auto'>" +
								"<tr>" +
									"<td style='text-align:center'>" +
										"<h3>{ExpiredStr}<h3>" +
									"</td>" +
								"</tr>" +
								"<tr>" +
									"<td style='text-align:center'>" +
										"<a style='white-space: nowrap' id='edit_button'>{ExpiredEditStr}</a>" +
									"</td>" +
								"</tr>" +
							"</table>");

            $table.find("#edit_button").button().bind("click", function () {
                document.location = "{ExpireEditLink}" + "?id=" + id;
            });

            return $table;
        }

        var settingsDateFormat = "<% = DateTimeConverter.JsDateFormat %>";
        var settingsTimeFormat = "<% = DateTimeConverter.JsTimeFormat %>";

        var responseDbFormat = "dd/MM/yyyy HH:mm:ss";
        var saveDbFormat = "dd/MM/yyyy HH:mm";
        var saveDbDateFormat = "dd/MM/yyyy";

        var uiFormat = "<% = DateTimeConverter.JsDateTimeFormat %>";
    </script>

    <style>
    #wallpaperImg
    { 
	    max-width:400px;
	    max-height:400px;
	    width: expression(this.offsetWidth < 400 ? "auto" : 400); 
	    height: expression(this.offsetHeight < 400 ? "auto" : 400); 
    }

    .body {
	    border-spacing: 0px;
	    border-collapse:collapse;
    }

    html, body {
	    border: 0px;
	    overflow: auto;
	    padding:0px;
	    margin:0px;
    }

    #main_content_cell
    {
	    padding:10px;
    }


    </style>
    <!--[if IE 8 ]> 
	    <style>
	    #wallpaperImg
	    {
		    width:400px;
	    }
	    </style>
    <![endif]-->
    <script language="javascript" type="text/javascript" src="jscripts/jquery.hotkeys.js"></script>
    <script language="javascript" type="text/javascript">



        function isPositiveInteger(n) {
            return (/^\d+$/.test(n + ''));
        }

        function showPrompt(text) {
            $("#dialog-modal > p").text(text)
            $("#dialog-modal").dialog({
                height: 150,
                modal: true,
                resizable: false,
                buttons: {
                    "<% =resources.LNG_CLOSE %>": function () {
		            $(this).dialog("close");
		        }
		    }
	    });
    }

    $(window)
        .bind('keydown',
            'Ctrl+o',
            function () {

                openFileBrowser();

            }
        );

    $(window)
        .bind('keydown',
            'Alt+p',
            function () {

                showWallpaperPreview();

            }
        );

    $(window)
        .bind('keydown',
            'right',
            function () {

                saveWallpaper();

            }
        );

    function showPrompt(content) {
        $("#dialog-modal > p").empty();
        $("#dialog-modal > p").append(content)
        $("#dialog-modal").dialog({
            height: 150,
            modal: true,
            resizable: false,
            draggable: false
        });
    }

    function toggleDisable(checkbox, selector) {
        if (!$(checkbox).is(':checked')) {
            $(selector + ' :input').prop('disabled', true);
        } else {
            $(selector + ' :input').removeProp('disabled');
        }
    }

        function setDraftValue(val) {
            $("#isDraft").val(val);
        }

        function saveWallpaper() {
        var autoclose = $.trim($("#timeToDisplay").val());
        var fromDate = $.trim($("#startDateAndTime").val());
        var toDate = $.trim($("#endDateAndTime").val());
        var wallpaperSrc = $("#wallpaperImg").attr("src");
        var position = $("#wallpaperPositionSelect").val();

        if ($("#wallpaperName").val()) {
            $("#title").val($("#wallpaperName").val());
        }
        else {
            showPrompt("<% =resources.LNG_SPECIFY_NAME %>");
	        return;
	    }

        $("#campaign_id").val($("#campaign_select").val());


        //if ($("#time_to_display_checkbox").is(':checked'))
        //{
        if (!isPositiveInteger(autoclose)) {
            showPrompt("<% =resources.LNG_TIME_TO_DISPLAY_IS_NOT_VALID %>");
		        return;
		    }
		    else {
		        $("#autoclose").val(autoclose);
		    }
        //}
            if ($("#startAndEndDateAndTimeCheckbox").is(':checked')) {

                if (!isDate(fromDate, uiFormat)) {
                    showPrompt("<% =resources.LNG_FROM_DATE_AND_TIME_IS_NOT_VALID %>");
                    return;
                } else {
                    if (fromDate != "") {
                        fromDate = new Date(getDateFromFormat(fromDate, uiFormat));
                        $("#from_date").val(formatDate(fromDate, saveDbFormat));
                    }
                }

                if (!isDate(toDate, uiFormat)) {
                    showPrompt("<% =resources.LNG_TO_DATE_AND_TIME_IS_NOT_VALID %>");
                    return;
                } else {
                    if (toDate != "") {
                        toDate = new Date(getDateFromFormat(toDate, uiFormat))
                        $("#to_date").val(formatDate(toDate, saveDbFormat));
                    }
                }
                if (fromDate < serverDate && toDate < serverDate) {
                    showPrompt("<% =resources.LNG_DATE_IN_THE_PAST %>");
                    return;
                }
                if (fromDate >= toDate) {
                    showPrompt("<% =resources.LNG_REC_DATE_ERROR %>");
                    return;
                }

            }
        if (wallpaperSrc == "" || wallpaperSrc === undefined) {
            showPrompt("<%= resources.LNG_YOU_SHOULD_CHOOSE_WALLPAPER %>");
	        return;
	    }
        $
        $("#wallpaperSrc").val(wallpaperSrc);
        $("#wallpaper_position").val(position);
        $("#form1").submit();
    }

    function openFileBrowser() {
        var params = 'width=550';
        params += ', height=400';
        params += ', top=0, left=0'
        params += ', fullscreen=no';

        //openDialogUI('uploadform.asp', 350, 400);
        window.open('uploadform.aspx?suffix=_images&size=0', 'preview_win', params);
    }

    function fileBrowserSetSrc(id, name, src, size, width, height) {
        $("#wallpaperImg").attr("src", src);
        $("#wallapperImgTr").hide();
        $("#wallapperPositionTr").hide();
        $("#wallapperImgTr").fadeIn(500);
        $("#wallapperPositionTr").fadeIn(500);
        $("#fileNameTd").text(name);
        $("#dimensionsTd").text(width + " x " + height); 
        $("#fileSizeTd").text(size); 
    }

    function showWallpaperPreview() {
        var imageSrc = $("#wallpaperImg").attr("src");
        var position = $("#wallpaperPositionSelect").val();
        $("#preview_image_src").val(imageSrc);
        $("#preview_position").val(position);

        var params = 'width=' + screen.width;
        params += ', height=' + screen.height;
        params += ', top=0, left=0'
        params += ', fullscreen=no';

        //openDialogUI('', screen.width, screen.height);
        window.open('', 'preview_win', params);

        $("#preview_post_form").submit();
        hideLoader();
    }


    $(document).ready(function () {
        //   $(".main_table_title").prepend("<img src='images/menu_icons/wallpaper_20.png' alt='' width='20' height='20' border='0' style='padding-left:7px'>")
        var fromDate = $.trim($("#startDateAndTime").val());
        var toDate = $.trim($("#endDateAndTime").val());
        if (fromDate != "" && isDate(fromDate, responseDbFormat)) {
            $("#startDateAndTime").val(formatDate(new Date(getDateFromFormat(fromDate, responseDbFormat)), uiFormat));
        }

        if (toDate != "" && isDate(toDate, responseDbFormat)) {
            $("#endDateAndTime").val(formatDate(new Date(getDateFromFormat(toDate, responseDbFormat)), uiFormat));
        }
        
        $("#save_and_next_button").button().click(function () {
            saveWallpaper();
        });

        $("#save_button").button().click(function () {
            setDraftValue(true);
            saveWallpaper();
        });

        $("#preview_button").button().click(function () {
            showWallpaperPreview();
        });



        $("#choose_wallpaper_button").button().click(function () {
            openFileBrowser();
        });

        $(".date_time_param_input")
            .datetimepicker({ dateFormat: settingsDateFormat, timeFormat: settingsTimeFormat });

        $("#startAndEndDateAndTimeCheckbox").change(function () {
            toggleDisable(this, "#start_and_end_date_and_time_div");
        }).change();


        $("#wallpaper_tabs").tabs();

        var cur_src = $("#wallpaperSrc").val();
        if (cur_src) {
            fileBrowserSetSrc($("#alert_id").val(), cur_src, cur_src);
        }
    });
    </script>
</head>
<body>
    <form id="form1" runat="server" action="AddWallpaper.aspx" method="POST ">
    <div>
        <input name="wallpaper_src" id="wallpaperSrc" type="hidden" runat="server"/>
        <input runat="server" name="rejectedEdit" id="rejectedEdit" value="" type="hidden"/>
	    <input runat="server" name="shouldApprove" id="shouldApprove" value="" type="hidden"/>
        <input runat="server" name="alert_id" id="alertId" type="hidden"/>
        <input runat="server" name='edit' id="edit" value="" type="hidden"/>
        <input runat="server" name='isDraft' id="isDraft" value="" type="hidden"/>
        <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
         <tr>
             <td valign="top">
                <table width="100%" cellspacing="0" cellpadding="0" class="main_table">
                         <tr>
   			                <td width="100%" height="31" class="main_table_title"><img src="images/menu_icons/wallpaper_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
		                    <a href="EditWallpaper.aspx" class="header_title" style="position:absolute;top:15px"><%=resources.LNG_WALLPAPERS %></a></td>             
                         </tr>
                          <tr>
                              <td class="main_cell" id="main_content_cell" height="100%" >
 	                                 <table style="width:100%; height:100%;  border-collapse:collapse; border:0px; margin:0px; padding:0px; ">
		                                    <tr style="height:100px">
			                                    <td class="wallpaper_name_td" style="text-align:left; padding: 0px;">

				                                    <div style="padding:2px 0px">
                                                     <div id="campaignDiv" runat="server">
                                                         <b> <%=resources.LNG_CAMPAIGN %>  :</b>
                                                         <label><b runat="server" id="campaignNameLabel"></b></label>
                                                         <input runat="server" type='hidden' runat="server" id='campaignSelect' name='campaign_select' value=''/>
                                                     </div>
                                                    <div runat="server" id="nonCampaignDiv">
                                                        <input type='hidden' name='campaignSelect' value='-1'>
                                                    </div>
                                                    </div><br />
                       
                                                    <div style="padding:2px 0px">
                                                    <% =resources.LNG_STEP %> <strong>1</strong> <% =resources.LNG_OF %> &nbsp;2 : <strong><% =resources.LNG_ADD_WALLPAPER %>:</strong>
                                                    </div><br />

                                                    <div style="padding:2px 0px">
                                                    <label for="wallpaperName"><%=resources.LNG_NAME %>:</label>
				                                    <input runat="server" type="text" name="wallpaper_name" id="wallpaperName" value="" />
				                                    </div>

			                                    </td>
		                                    </tr>
		                                    <tr style="">
			                                    <td class="tabs_container" style="text-align:left; vertical-align:top; padding:0px">
			                                      <br />
				                                    <div id="wallpaper_tabs" style="">
					                                    <ul>
						                                    <li><a href="#content_tab"><%=resources.LNG_CONTENTS %></a></li>
						                                    <li><a href="#options_tab"><%=resources.LNG_OPTIONS %></a></li>
					                                    </ul>
					                                    <div id="content_tab" style="padding:0px; margin:10px;">
						                                    <table style="border-collapse:collapse; border:0px; margin:0px; padding:0px; ">
							                                    <tr id="wallapperImgTr" runat="server" style="display:none">
								                                    <td style="vertical-align:top; text-align:right; font-weight:bold">
									                                    <%=resources.LNG_IMAGE_FILE %>:
								                                    </td>
								                                    <td style="vertical-align:top; text-align:left; padding:0px 10px 0px 10px">
									                                    <img src="" id="wallpaperImg" runat="server"></img>
								                                    </td>
								                                    <td style="vertical-align:top; text-align:left; padding:0px 10px 0px 10px">
									                                    <table style=" border-collapse:collapse; border:0px; margin:0px;  ">
										                                    <tr>
											                                    <td style="text-align:right; padding-right:10px; font-weight:bold">
												                                    <%=resources.LNG_DIMENSIONS %>:
											                                    </td>
											                                    <td runat="server" id="dimensionsTd">
											                                    </td>
										                                    </tr>
										                                    <tr>
											                                    <td style="text-align:right; padding-right:10px; font-weight:bold">
												                                    <% =resources.LNG_FILE_SIZE %>:
											                                    </td>
											                                    <td runat="server" id="fileSizeTd">
											                                    </td>
										                                    </tr>
										                                    <tr>
											                                    <td style="text-align:right; padding-right:10px; font-weight:bold">
												                                    <% =resources.LNG_FILE_NAME %>:
											                                    </td>
											                                    <td runat="server" id="fileNameTd">
											                                    </td>
										                                    </tr>
									                                    </table>
								                                    </td>
							                                    </tr>
							                                    <tr id="wallapperPositionTr" runat="server" style="display:none; ">
								                                    <td style="vertical-align:top; text-align:right; padding:10px 0px 0px 0px; font-weight:bold">
									                                    <%=resources.LNG_POSITION %>:
								                                    </td>
								                                    <td style="vertical-align:top; text-align:left; padding:10px 10px 0px 10px">
									                                    <asp:DropDownList id="wallpaperPositionSelect" runat="server">

									                                    </asp:DropDownList>
								                                    </td>
								                                    <td></td>
							                                    </tr>
							                                    <tr>
								                                    <td></td>
								                                    <td  style="vertical-align:top; text-align:right;padding-right:8px">
									                                    <a style="white-space: nowrap" id="choose_wallpaper_button"><%=resources.LNG_CHOOSE_WALLPAPER %></a>
								                                    </td>
								                                    <td align=left style="padding: 2px; text-align:left">
									                                    <img alt="helpIcon" src="images/help.png"  title="<%=resources.LNG_WALLPAPER_CHOOSE_TEXT %>"/>
								                                    </td>
							                                    </tr>
						                                    </table>
						
					                                    </div>
					                                    <div id="options_tab" style="padding:0px; margin:10px;">
						                                    <div>
							                                    <input id="timeToDisplayCheckbox" type="hidden" ><%=resources.LNG_TIME_TO_DISPLAY %>:</input>&nbsp;
							                                    <input name="time_to_display" runat="server" id="timeToDisplay" type="text" style="width:30px" value="60"></input>&nbsp;
							                                    <label>							    
                                                                    <%=resources.LNG_SECONDS %>
							                                    </label>
							                                    <p style="color:#aaaaaa">
								                                    <%=resources.LNG_WALLPAPER_TIME_TO_DISPLAY_TITLE %>
							                                    </p>
						                                    </div>
						                                    <div style="padding-top:10px">
							                                    <input runat="server" id="startAndEndDateAndTimeCheckbox" name="start_and_end_date_and_time_checkbox" type="checkbox" ></input><label for="startAndEndDateAndTimeCheckbox"><%=resources.LNG_START_AND_END_DATE_AND_TIME %></label>
							                                    <div id="start_and_end_date_and_time_div" style="padding-top:5px">
								                                    <table style="width:300px height:100px; border-collapse:collapse; border:0px; margin:0px; padding:0px; ">
									                                    <tr>
										                                    <td style="text-align:right">
											                                    <label for="startDateAndTime"><% = resources.LNG_START_DATE_AND_TIME %>:</label>
										                                    </td>
										                                    <td>
											                                    <input runat="server" id="startDateAndTime" class="date_time_param_input" name="start_date_and_time" type="text" value=""></input>
										                                    </td>
									                                    </tr>
									                                    <tr>
										                                    <td style="text-align:right">
											                                    <label for="endDateAndTime"><% = resources.LNG_END_DATE_AND_TIME %>:</label>
										                                    </td>
										                                    <td>
											                                    <input runat="server" id="endDateAndTime" class="date_time_param_input" name="end_date_and_time" type="text" value=""></input>
										                                    </td>
									                                    </tr>
								                                    </table>
								
							                                    </div>
						                                    </div>
					                                    </div>
				                                    </div>
			                                    </td>
		                                    </tr>
		                                    <tr style="height:30px">
			                                    <td class="wallpaper_name_td" style="text-align:right; padding: 0px; padding-top: 10px;">
				                                    <a id="preview_button"><% =resources.LNG_PREVIEW %></a>
			                                        <a id="save_button"><% =resources.LNG_SAVE %></a>
			                                        <a id="save_and_next_button"><% =resources.LNG_SAVE_AND_NEXT %></a>
                                                    <br/><br/>
			                                    </td>
		                                    </tr>
	                               </table>  
               </td> 
          </tr>
        </table>
             </td>
          </tr>
          </table>
    </div>
    </form>
</body>
     <div id="dialog-modal" title="" style='display: none'>
        <p>
        </p>
    </div>
    <form style="margin:0px; width:0px; height:0px; padding:0px" id="preview_post_form" action="WallpaperPreview.aspx" method="post" target="preview_win">
		<input type="hidden" id="preview_image_src" name="image_src"  value=""/>
		<input type="hidden" id="preview_position" name="position"  value=""/>
	</form>
</html>
