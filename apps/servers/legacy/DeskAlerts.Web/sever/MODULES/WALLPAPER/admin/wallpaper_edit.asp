<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%
check_session()
%>
<!-- #include file="Common.asp" -->
<!-- #include file="header.asp" -->
<!-- #include file="footer.asp" -->
<!-- #include file="functions_inc.asp" -->

<%
'On Error Resume Next
' index CustomIncludes End
'-------------------------------

'===============================
' Save Page and File Name available into variables
'-------------------------------
Dim sFileName
Dim sTemplateFileName
Dim intSessionUserId

sFileName = "wallpaper_edit.asp"
sTemplateFileName = "wallpaper_edit.html"
'===============================

function updateAlert(id,alertText, alertTitle, fromDate, toDate, schedule, scheduleType, recurrence, urgent, email, desktop, rss, emailSender, ticker, fullscreen, alertWidth, alertHeight, aknown, autoclose, toolbarmode, deskalertsmode, template, uid, paramTemplateId)
	sqlRequest = "UPDATE alerts SET alert_text=N'"& Replace(alertText, "'", "''") &"', title=N'"& Replace(alertTitle, "'", "''") &"', type='D', from_date='"&fromDate&"', to_date='"&toDate&"', schedule='"&schedule&"', schedule_type='"&scheduleType&"', recurrence='"&recurrence&"', urgent='"&urgent&"', email='"&email&"', desktop='"&desktop&"', email_sender='"& Replace(emailSender, "'", "''") &"', ticker='"&ticker&"', fullscreen='"&fullscreen&"', alert_width="&alertWidth&",  alert_height="&alertHeight&", aknown='"&aknown&"',  autoclose="&autoclose&",  toolbarmode='"&toolbarmode&"', deskalertsmode='"&deskalertsmode&"', template_id="&template&", sender_id="&uid&", param_temp_id="&paramTemplateId&" WHERE id = " & Clng(id)
	Set RS_alert_update = Conn.Execute(sqlRequest)
        MemCacheUpdate "Update", id 
	updateAlert = id
end function

'alertClass 1 - usual alert, 2 - screensaver alert, 4 - rss alert, 8 - wallpaper alert

function addAlertToDataBase(alertText, alertTitle, fromDate, toDate, schedule, scheduleType, recurrence, urgent, email, desktop, rss, emailSender, ticker, fullscreen, alertWidth, alertHeight, aknown, autoclose, toolbarmode, deskalertsmode, template, uid, paramTemplateId, alertClass, campaign_id, approve_status)
	sqlRequest = "INSERT INTO alerts (alert_text, title, create_date, type, from_date, to_date, schedule, schedule_type, recurrence, urgent, email, desktop, email_sender, ticker, fullscreen, alert_width,  alert_height, aknown,  autoclose,  toolbarmode, deskalertsmode, template_id, sender_id, param_temp_id, class, campaign_id, approve_status) VALUES (N'"& Replace(alertText, "'", "''") &"', N'"& Replace(alertTitle, "'", "''") &"', GETDATE(), 'D', '"&fromDate&"','"&toDate&"','"&schedule&"', '"&scheduleType&"', '"&recurrence&"', '"&urgent&"', '"&email&"', '"&desktop&"','"& Replace(emailSender, "'", "''") &"',  '"&ticker&"', '"&fullscreen&"', "&alertWidth&",  "&alertHeight&", '"&aknown&"', "&autoclose&", '"&toolbarmode&"', '"&deskalertsmode&"',"&template&", "&uid&","&paramTemplateId&","&alertClass&","& campaign_id & "," & approve_status &"); select @@IDENTITY as 'newID'"
		response.write sqlRequest
	Set RS_alert = Conn.Execute(sqlRequest)
	Set RS_alert  = RS_alert.NextRecordset
	addAlertToDataBase=RS_alert("newID")
    MemCacheUpdate "Add", addAlertToDataBase 
	if(campaign_id = "-1") then
	    end if
	RS_alert.Close
end function
  
  
intSessionUserId = Session("uid")
uid = intSessionUserId
if (intSessionUserId <> "") then


' index Show begin

'===============================
' Display page
'-------------------------------
' Load HTML template for this page
'-------------------------------
LoadTemplate alerts_dir & "\" & templateFolder & "\" & sTemplateFileName, "main"

'-------------------------------
' Load HTML template of Header and Footer
'-------------------------------
sHeaderFileName=Replace(sHeaderFileName,"header.html","header-old.html")
LoadTemplate sHeaderFileName, "Header"
LoadTemplate sFooterFileName, "Footer"
'-------------------------------
SetVar "Menu", ""
SetVar "DateForm", ""

SetVar "ImageFileStr", LNG_IMAGE_FILE
SetVar "PositionStr", LNG_POSITION
SetVar "DimensionsStr", LNG_DIMENSIONS
SetVar "FileSizeStr", LNG_FILE_SIZE
SetVar "FileNameStr", LNG_FILE_NAME
SetVar "CenterStr", LNG_CENTER
SetVar "TileStr", LNG_TILE
SetVar "StretchStr", LNG_STRETCH
SetVar "ChooseWallpaperStr", LNG_CHOOSE_WALLPAPER
SetVar "LNG_ADD_WALLPAPER",LNG_ADD_WALLPAPER
SetVar "LNG_STEP", LNG_STEP
SetVar "LNG_OF", LNG_OF
SetVar "LNG_TITLE", LNG_TITLE
SetVar "LNG_DATE", LNG_DATE
SetVar "default_lng", lcase(default_lng)
SetVar "timezone_db", timezone_db
SetVar "start_and_end_date_and_time_checked", ""
SetVar "time_to_display_checked", ""
SetVar "time_to_display", "60"
SetVar "sFileName", sFileName
SetVar "strPageName", LNG_WALLPAPERS
SetVar "FromDateNotValidStr", LNG_FROM_DATE_AND_TIME_IS_NOT_VALID
SetVar "ToDateNotValidStr", LNG_TO_DATE_AND_TIME_IS_NOT_VALID
SetVar "ToDateEarlyError", LNG_REC_DATE_ERROR
SetVar "SpecifyWallpaperNameStr", LNG_SPECIFY_NAME
SetVar "TimeToDisplayNotValidStr", LNG_TIME_TO_DISPLAY_IS_NOT_VALID
SetVar "wallapper_img_tr_display", "none"
SetVar "wallapper_position_tr_display", "none"
SetVar "CenterPositionSelected", ""
SetVar "TilePositionSelected", ""
SetVar "StretchPositionSelected", ""
SetVar "WallpaperImgSrc", ""
SetVar "HelpImg", "images/help.png" 
SetVar "ChooseText", LNG_WALLPAPER_CHOOSE_TEXT
SetVar "PreviewStr", LNG_PREVIEW
SetVar "LNG_CLOSE", LNG_CLOSE
SetVar "edit", request("edit")

if CAMPAIGN = 1 then
SetVar "Campaign", "<label for='campaign_name'><b>" & LNG_CAMPAIGN & ":</b></label>"
else
SetVar "Campaign", ""   
end if

if CAMPAIGN = 1 then
                                                               
 if Request("alert_campaign") = "" then
  campaignSelector = "<input type='hidden' name='campaign_select' value='-1'>"
 else
 
    SQL = "SELECT name FROM campaigns WHERE id = " & Request("alert_campaign")
    Set RS2 = Conn.Execute(SQL) 
    if not RS2.EOF then
       campaignSelector =  "C <label><b>"&HtmlEncode(RS2("name"))&"</b></label>"
       campaignSelector = campaignSelector &  "<input type='hidden' id= 'campaign_select' name='campaign_select' value='"&Request("alert_campaign")&"'>"                                
    end if
  
 end if
   
else
 campaignSelector = "<input type='hidden' id= 'campaign_select'  name='campaign_select' value='-1'>"
end if

SetVar "CampaignSelector", campaignSelector & "<br>"
 
set rights = getRights(uid, true, false, true, "wallpapers_val")
gid = rights("gid")
gviewall = rights("gviewall")
gviewlist = rights("gviewlist")
wallpapers_arr = rights("wallpapers_val")


'--------------------


alertId = Request("id")
isEdit = Request("edit")
alertHTML = Request("wallpaper_src")
wallpaperPosition = Request("wallpaper_position")
fromDate = Request("from_date")
toDate = Request("to_date")
title = Request("title")
autoclose = Request("autoclose")
campaignid = Request("campaign_id")
if campaignid="" then
	campaignid="-1"
end if
schedule = 0

if alertId<>"" then
	dup_id = alertId
end if

if (autoclose = "") then
	autoclose = 0
end if

if (fromDate <> "") then
	schedule = 1
end if

set params = param("values")

if (alertHTML <> "") then

	if InStr(alertHTML, "://") = 0 then
		alertHTML = "admin/" & alertHTML
	end if
	alertHTML = Replace(alertHTML, "'", "''") 
	
	schedule_type=1
	
	if(campaignid <> "-1") then
	    schedule_type=0
	end if

	Set is_started = Conn.Execute("SELECT DISTINCT 1 FROM alerts WHERE campaign_id = " & campaignid & " AND schedule_type = '1'")
	if not is_started.EOF then
		schedule_type = 1
	end if 
	
	
	
	approve_status = 1
    
     if(APPROVE = 1) then
            Set enabled = Conn.execute("SELECT val FROM settings WHERE name = 'ConfAllowApprove'") 
         
            if(enabled("val") = "1" and  gid <> 0 and wallpapers_arr(6) <> "checked"  ) then
                approve_status = 0
            end if
    
      end if
      
    if(alertId <> "" and request("edit") <> "" ) then  
		 ' if(Request("rejectedEdit") = "1") then
		    '   Conn.Execute("UPDATE alerts SET approve_status = 0 WHERE id = " & alertId)
		 ' elseif(Request("shouldApprove") = "1") then
		       ' Conn.Execute("UPDATE alerts SET approve_status = 1 WHERE id = " & alertId)
		 ' end if 
		              ' id,alertText, alertTitle, fromDate, toDate, schedule, scheduleType, recurrence, urgent, email, desktop, rss, emailSender, ticker, fullscreen, alertWidth, alertHeight, aknown, autoclose, toolbarmode, deskalertsmode, template, uid, paramTemplateId)
        senderUserId = uid
		if  Request("rejectedEdit") = "1" or Request("shouldApprove") = "1" then
		 Set senderIdSet = Conn.Execute("SELECT sender_id FROM alerts WHERE id = " & alertId)
		    senderUserId = senderIdSet("sender_id")
		end if
		
      Call updateAlert(alertId,alertHTML,title, fromDate, toDate, schedule, schedule_type, 0, 0, 0, 1, 0, "", 0, 1, 500, 400, autoclose, 0, 1, 1, 1, senderUserId, "NULL")
	else	                    
	    alertId = addAlertToDataBase(alertHTML,title,fromDate,toDate,schedule,schedule_type,0,0,0,1,0,"",0,wallpaperPosition,500,400,0,autoclose,0,1,1,uid,"NULL",8, campaignid, approve_status)
	end if
	
	approve = "0"
			    
	if (gid = 0 or wallpapers_arr(6) = "checked") then
		 approve = "1"
	 end if

	 	 
if(campaignid = "-1") then			
	Response.Redirect "alert_users.asp?desktop=1&id="& CStr(alertId)&"&dup_id="&dup_id&"&return_page=wallpapers.aspx&header="& LNG_WALLPAPERS &"&title="& LNG_ADD_WALLPAPERS & "&campaign_id=" & campaignid & "&approve=" & approve & "&shouldApprove=" & request("shouldApprove") & "&rejectedEdit=" & request("rejectedEdit")
else
    Response.Redirect "alert_users.asp?desktop=1&id="& CStr(alertId)&"&dup_id="&dup_id&"&return_page=CampaignDetails.aspx?campaignid=" & campaignid & "&header="& LNG_WALLPAPERS &"&title="& LNG_ADD_WALLPAPERS & "&campaign_id=" & campaignid & "&approve=" & approve & "&shouldApprove=" & request("shouldApprove") & "&rejectedEdit=" & request("rejectedEdit")
end if
else
	SetVar "fromDate", ""
	SetVar "toDate", ""
	SetVar "title", ""
	SetVar "autoclose", ""
	SetVar "alertId", alertId

	if (alertId <> "") then
	
		Set RS = Conn.Execute("SELECT title, alert_text, from_date, to_date, autoclose, fullscreen FROM alerts WHERE id ="&alertId&" AND class = 8")
		 
		if (not RS.EOF) then
			alertHTML = RS("alert_text")
			if InStr(alertHTML, "admin/") = 1 then
				alertHTML = Mid(alertHTML, 6)
			end if
			
			SetVar "WallpaperImgSrc", alertHTML
			
			fromDate = RS("from_date")
			if year(fromDate) = 1900 OR varType(fromDate) <= 1 then
				fromDate = ""
			end if
			
			toDate = RS("to_date")
			if year(toDate) = 1900 OR varType(toDate) <= 1 then
				toDate = ""
			end if
			
			title = RS("title")
			autoclose = RS("autoclose")
			
			SetVar "wallapper_img_tr_display", ""
			SetVar "wallapper_position_tr_display", ""
			
			positionIndex = RS("fullscreen")
			
			if (positionIndex = "1") then
				SetVar "CenterPositionSelected", "selected"
			elseif (positionIndex = "2") then
				SetVar "TilePositionSelected", "selected"
			elseif (positionIndex = "3") then
				SetVar "StretchPositionSelected", "selected"
			end if
		end if
		RS.Close
			
		'if (autoclose <> "0" ) then
			'SetVar "time_to_display_checked", "checked"
			SetVar "time_to_display", autoclose
		'end if
		
		if (fromDate <> "") then
			SetVar "start_and_end_date_and_time_checked", "checked"
		end if
		
		SetVar "fromDate", FullDateTime(fromDate)
		SetVar "toDate", FullDateTime(toDate)
		
		if (isEdit = "1") then
			SetVar "alertId", alertId
			SetVar "rejectedEdit", Request("rejectedEdit")
			SetVar "shouldApprove", Request("shouldApprove")
		end if
				
	end if
	
	SetHtmlVar "title", title
	SetVar "ContentsStr", LNG_CONTENTS
	SetVar "WallpaperNameStr", LNG_NAME
	SetVar "OptionsStr", LNG_OPTIONS
	SetVar "TimeToDisplayStr", LNG_TIME_TO_DISPLAY
	SetVar "TimeToDisplayTitleStr", LNG_WALLPAPER_TIME_TO_DISPLAY_TITLE
	SetVar "SecondsStr", LNG_SECONDS
	SetVar "StartAndEndDateAndTimeStr", LNG_START_AND_END_DATE_AND_TIME
	SetVar "StartDateAndTimeStr", LNG_START_DATE_AND_TIME
	SetVar "EndDateAndTimeStr", LNG_END_DATE_AND_TIME
	SetVar "SaveAndNextStr", LNG_SAVE_AND_NEXT
	SetVar "ErrorWallpaperImageStr", LNG_YOU_SHOULD_CHOOSE_WALLPAPER

    if(gid = 0 or wallpapers_arr(3) = "checked") then
	    SetVar "strDisplaySaveNextButton", "" 
    else
        SetVar "strDisplaySaveNextButton", "none" 
    end if
	
end if



'-------------------------------
' Step through each form
'-------------------------------
Header_Show
Footer_Show
'-------------------------------
' Process page templates
'-------------------------------
Parse "Header", False
Parse "Footer", False
Parse "main", False
'-------------------------------
' Output the page to the browser
'-------------------------------

Response.write PrintVar("main")

' index Show End

'-------------------------------
' Destroy all object variables
'-------------------------------

' index Close Event begin
' index Close Event End

UnloadTemplate
'===============================

else

Login(Request("intErrorCode"))

end if

'===============================
' Display Grid Form
'-------------------------------
%><!-- #include file="db_conn_close.asp" -->