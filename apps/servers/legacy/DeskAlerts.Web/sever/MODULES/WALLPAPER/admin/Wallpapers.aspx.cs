using Castle.MicroKernel.Lifestyle;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Extensions.Enums;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Scheduler.ContentJobScheduling;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlerts.Server.Utils.Interfaces.Services;
using DeskAlertsDotNet.Application;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Models.Permission;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Utils.Consts;
using System;
using System.Globalization;
using System.Web.UI.WebControls;

namespace DeskAlertsDotNet.Pages
{
    using DeskAlerts.ApplicationCore.Enums;
    using Resources;

    public partial class Wallpapers : DeskAlertsBaseListPage
    {
        bool _shouldDisplayApprove;
        protected string pageTitle;

        private readonly IContentJobScheduler _contentJobScheduler;
        private readonly ICacheInvalidationService _cacheInvalidationService;

        public Wallpapers()
        {
            using (Global.Container.BeginScope())
            {
                _contentJobScheduler = Global.Container.Resolve<IContentJobScheduler>();
                _cacheInvalidationService = Global.Container.Resolve<ICacheInvalidationService>();
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            if (IsPostBack)
            {
                SearchTerm = searchTermBox.Value;
            }

            base.OnLoad(e);

            if (!IsPostBack)
            {
                searchTermBox.Value = SearchTerm;
            }

            searchButton.Text = searchButton.Text.Replace("{Search}", resources.LNG_SEARCH);
            headerTitle.Text = resources.LNG_WALLPAPERS;
            titleCell.Text = GetSortLink(resources.LNG_TITLE, "title", Request["sortBy"]);
            creationDateCell.Text = GetSortLink(resources.LNG_CREATION_DATE, "id", Request["sortBy"]);
            scheduledCell.Text = resources.LNG_SCHEDULED;
            Table = contentTable;
            pageTitle = !string.IsNullOrEmpty(Request["draft"]) && Request["draft"].Equals("1") ? resources.LNG_DRAFT : resources.LNG_CURRENT;

            _shouldDisplayApprove = Config.Data.HasModule(Modules.APPROVE) && Settings.Content["ConfAllowApprove"].Equals("1");
            addButton.Visible = string.IsNullOrEmpty(Request["draft"]) && string.IsNullOrEmpty(Request["sent"]);
            if (_shouldDisplayApprove)
                approveCell.Text = resources.LNG_APPROVE_STATUS;
            else
                approveCell.Visible = false;

            senderCell.Text = resources.LNG_SENDER;
            actionsCell.Text = resources.LNG_ACTIONS;

            headerSelectAll.Visible = curUser.HasPrivilege || curUser.Policy[Policies.WALLPAPERS].CanDelete;
            selectAll.Visible = curUser.HasPrivilege || curUser.Policy[Policies.WALLPAPERS].CanDelete;
            bottomDelete.Visible = curUser.HasPrivilege || curUser.Policy[Policies.WALLPAPERS].CanDelete;
            upDelete.Visible = curUser.HasPrivilege || curUser.Policy[Policies.WALLPAPERS].CanDelete;

            if (!curUser.HasPrivilege && !curUser.Policy[Policies.WALLPAPERS].CanCreate)
                addButton.Visible = false;

            if (!IsPostBack)
            {
                searchTermBox.Value = Request["searchTermBox"];
            }
            string getUrlSearchTermBox = Request.QueryString["searchTermBox"] ?? string.Empty;

            bool isSearchTermBoxValueChanged = searchTermBox.Value != getUrlSearchTermBox;
            if (isSearchTermBoxValueChanged)
            {
                Offset = 0;
            }

            int cnt = this.Content.Count < (Offset + Limit) ? this.Content.Count : Offset + Limit;

            for (int i = Offset; i < cnt; i++)
            {
                DataRow row = this.Content.GetRow(i);
                DateTime toDate = row.GetDateTime("to_date");
                DateTime fromDate = row.GetDateTime("from_date");

                bool isScheduled = row.GetInt("schedule") == 1;

                TableRow tableRow = new TableRow();

                if (curUser.HasPrivilege || curUser.Policy[Policies.WALLPAPERS].CanDelete)
                {
                    CheckBox chb = GetCheckBoxForDelete(row);
                    TableCell checkBoxCell = new TableCell();
                    checkBoxCell.HorizontalAlign = HorizontalAlign.Center;
                    checkBoxCell.Controls.Add(chb);
                    tableRow.Cells.Add(checkBoxCell);
                }


                TableCell titleContentCell = new TableCell();
                titleContentCell.Text = row.GetString("title");
                tableRow.Cells.Add(titleContentCell);

                var createDateContentCell = new TableCell
                {
                    Text = DateTimeConverter.ToUiDateTime(row.GetDateTime("create_date")),
                    HorizontalAlign = HorizontalAlign.Center
                };

                tableRow.Cells.Add(createDateContentCell);


                var scheduleCell = new TableCell
                {
                    Text = isScheduled
                    ? resources.LNG_YES
                    : resources.LNG_NO,

                    HorizontalAlign = HorizontalAlign.Center
                };
                tableRow.Cells.Add(scheduleCell);

                if (_shouldDisplayApprove)
                {
                    
                    string approveLocKey;
                    if (row.IsNull("approve_status"))
                    {
                        approveLocKey = "LNG_APPROVED";
                    }  
                    else
                    {
                        var approveStatus = (ApproveStatus) row.GetInt("approve_status");
                        approveLocKey = approveStatus.GetStringValue();
                    }

                    var approveContentCell = new TableCell();
                    approveContentCell.HorizontalAlign = HorizontalAlign.Center;
                    approveContentCell.Text = resources.ResourceManager.GetString(approveLocKey);
                    tableRow.Cells.Add(approveContentCell);
                }

                var senderContentCell = new TableCell
                {
                    HorizontalAlign = HorizontalAlign.Center,
                    Text = string.IsNullOrEmpty(row.GetString("name")) ? resources.SENDER_WAS_DELETED : row.GetString("name")
                };

                tableRow.Cells.Add(senderContentCell);

                LinkButton sendButton = GetActionButton(row.GetString("id"), "images/action_icons/send.png",
                                                        "LNG_SEND_ALERT",
                                                        delegate { Response.Redirect("RecipientsSelection.aspx?desktop=1&id=" + row.GetString("id")+ "&desktop=1&return_page=Wallpapers.aspx&dup_id="+ row.GetString("id"), true); });

                LinkButton duplicateButton = GetActionButton(row.GetString("id"), "images/action_icons/duplicate.png", "LNG_RESEND_WALLPAPER",
                    delegate
                    {
                        Response.Redirect("EditWallpaper.aspx?id=" + row.GetString("id"), true);
                    }

                );

                int scheduleType = row.GetInt("schedule_type");
                LinkButton changeScheduleType = GetActionButton(row.GetString("id"), scheduleType == 1 ? "images/action_icons/stop.png" : "images/action_icons/start.png", scheduleType == 1 ? "LNG_STOP_SCHEDULED_ALERT" : "LNG_START_SCHEDULED_ALERT",
                    delegate
                    {
                        Response.Redirect("ChangeScheduleType.aspx?id=" + row.GetString("id") + "&type=" + Math.Abs(scheduleType - 1) + "&return_page=Wallpapers.aspx", true);
                    }

                );

                LinkButton previewButton = GetActionButton(row.GetString("id"), "images/action_icons/preview.png", "LNG_PREVIEW", "showWallpaperPreview(" + row.GetString("id") + ")");

                TableCell actionsContentCell = new TableCell();

                actionsContentCell.HorizontalAlign = HorizontalAlign.Center;

                bool isDraft = !String.IsNullOrEmpty(Request["draft"]) && Request["draft"].Equals("1");
                if (isDraft)
                {
                    if (curUser.HasPrivilege || curUser.Policy[Policies.ALERTS].CanSend)
                        actionsContentCell.Controls.Add(sendButton);
                }
                if (curUser.HasPrivilege || curUser.Policy[Policies.WALLPAPERS].CanEdit)
                    actionsContentCell.Controls.Add(duplicateButton);

                if (curUser.HasPrivilege || curUser.Policy[Policies.WALLPAPERS].CanStop)
                    actionsContentCell.Controls.Add(changeScheduleType);
                actionsContentCell.Controls.Add(previewButton);

                tableRow.Cells.Add(actionsContentCell);

                contentTable.Rows.Add(tableRow);
            }
        }

        /// <summary>
        /// Define parameters to add to Uri.
        /// Override <see cref="DeskAlertsBaseListPage"/> PageParams. Add "user_id" param for sorting table.
        /// </summary>
        protected override string PageParams
        {
            get
            {
                try
                {
                    var uri = string.Empty;
                    uri += AddParameter(uri, "draft");
                    return uri;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                    Logger.Debug(ex);
                    throw;
                }
            }
        }

        #region DataProperties
        protected override string DataTable => "alerts";

        protected override string[] Collumns => new string[] { };

        protected override string DefaultOrderCollumn => "id";

        protected override string CustomQuery
        {
            get
            {
                string viewListStr = "";

                int classId = 8;
                string alertType = !string.IsNullOrEmpty(Request["draft"]) && Request["draft"].Equals("1") ? "D" : "S";

                if (!curUser.HasPrivilege && !curUser.Policy.CanViewAll)
                    viewListStr = string.Join(",", curUser.Policy.SendersViewList.ToArray());

                string query;
                if (!curUser.HasPrivilege && !curUser.Policy.CanViewAll)
                {
                    query = "SELECT a.id, a.lifetime, a.approve_status, a.schedule, schedule_type, recurrence, alert_text, title, a.type, sent_date, a.create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp, u.name FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id LEFT JOIN users u on u.id = a.sender_id WHERE video = 0 AND param_temp_id IS NULL AND class = " + classId + " AND a.type = '" + alertType + "' AND a.sender_id IN (" + viewListStr + ") ";

                    if (!string.IsNullOrEmpty(Request["parent_id"]))
                        query += " AND parent_id = " + Request["parent_id"];
                }
                else
                {
                    query = "SELECT a.id, a.lifetime, a.approve_status, a.schedule, schedule_type, recurrence, alert_text, title, a.type, sent_date, a.create_date, a.from_date, a.to_date, class, urgent, ticker, fullscreen, s.id as is_rsvp, u.name FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id LEFT JOIN users u on u.id = a.sender_id WHERE video = 0 AND param_temp_id IS NULL AND class = " + classId + " AND a.type = '" + alertType + "' ";

                    if (!string.IsNullOrEmpty(Request["parent_id"]))
                        query += " AND parent_id = " + Request["parent_id"];

                }

                if (SearchTerm.Length > 0)
                {
                    query += $" AND title LIKE '%{SearchTerm}%'";
                }

                if (!String.IsNullOrEmpty(Request["sortBy"]))
                    query += " ORDER BY " + Request["sortBy"];
                else
                {
                    query += " ORDER BY " + DefaultOrderCollumn + " DESC ";
                }

                return query;
            }
        }

        protected override string CustomCountQuery
        {
            get
            {
                string viewListStr = "";

                if (!curUser.HasPrivilege && !curUser.Policy.CanViewAll)
                    viewListStr = string.Join(",", curUser.Policy.SendersViewList.ToArray());

                int classId = 8;
                string alertType = !string.IsNullOrEmpty(Request["draft"]) && Request["draft"].Equals("1") ? "D" : "S";


                string query;
                if (!curUser.HasPrivilege && !curUser.Policy.CanViewAll)
                {
                    query = "SELECT count(1) FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id LEFT JOIN users u on u.id = a.sender_id WHERE video = 0 AND param_temp_id IS NULL AND class = " + classId + " AND a.type = '" + alertType + "' AND a.sender_id IN (" + viewListStr + ") ";

                    if (!string.IsNullOrEmpty(Request["parent_id"]))
                        query += " AND parent_id = " + Request["parent_id"];
                }
                else
                {
                    query = "SELECT count(1) FROM alerts a LEFT JOIN surveys_main s ON s.closed = 'A' and s.sender_id = a.id LEFT JOIN users u on u.id = a.sender_id WHERE video = 0 AND param_temp_id IS NULL AND class = " + classId + " AND a.type = '" + alertType + "' ";

                    if (!string.IsNullOrEmpty(Request["parent_id"]))
                        query += " AND parent_id = " + Request["parent_id"];
                }

                if (IsPostBack && SearchTerm.Length > 0)
                    query += " AND title LIKE '%" + SearchTerm + "%'";

                return query;
            }
        }
        /*
         * 
         * If you want to add condition for data selection override property ConditionSqlString 
         * Also you can implement your custom query by overriding properties CustomQuery and CustomCountQuery
         *  
         */
        #endregion

        #region Interface properties

        protected override IButtonControl[] DeleteButtons
        {
            get
            {
                return new IButtonControl[] { upDelete, bottomDelete };
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl NoRowsDiv
        {
            get { return NoRows; }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TableDiv
        {
            get { return mainTableDiv; }
        }

        protected override string ContentName
        {
            get
            {
                //return content name here
                return resources.LNG_WALLPAPERS;
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl BottomPaging
        {
            get
            {
                return bottomRanging;
            }
        }

        protected override System.Web.UI.HtmlControls.HtmlGenericControl TopPaging
        {
            get
            {
                return topPaging;
            }
        }

        #endregion

        protected override void OnDeleteRows(string[] ids)
        {
            foreach (var id in ids)
            {
                var wallpaperId = Convert.ToInt32(id);

                if (AppConfiguration.IsNewContentDeliveringScheme)
                {
                    _cacheInvalidationService.CacheInvalidation(AlertType.Wallpaper, Convert.ToInt64(id));
                }
                else
                {
                    Global.CacheManager.Delete(wallpaperId, DateTime.Now.ToString(DateTimeFormat.JavaScript, CultureInfo.InvariantCulture));
                }
                
                var contentJobDto = new ContentJobDto(wallpaperId, AlertType.Wallpaper);
                _contentJobScheduler.DeleteJob(contentJobDto);

                ReccurenceManager.DeleteReccurence(wallpaperId);
            }
        }
    }
}