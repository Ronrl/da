using Castle.MicroKernel.Lifestyle;
using DeskAlerts.ApplicationCore.Entities;
using DeskAlerts.ApplicationCore.Utilities;
using DeskAlerts.Scheduler.ContentJobScheduling;
using DeskAlerts.Server.Utils.Helpers;
using DeskAlertsDotNet.Application;
using DeskAlertsDotNet.Models.Permission;
using DeskAlertsDotNet.Pages;
using DeskAlertsDotNet.Parameters;
using DeskAlertsDotNet.Utils.Consts;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace DeskAlertsDotNet.sever.MODULES.WALLPAPER.admin
{
    public partial class AddWallpaper : DeskAlertsBasePage
    {
        //use curUser to access to current user of DA control panel      
        //use dbMgr to access to database
        private readonly IContentJobScheduler _contentJobScheduler;
        public static string FromDate = DateTimeUtils.MinSupportedDateTimeStr;

        public AddWallpaper()
        {
            using (Global.Container.BeginScope())
            {
                _contentJobScheduler = Global.Container.Resolve<IContentJobScheduler>();
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            var name = Request["wallpaperName"];
            var position = Request["wallpaperPositionSelect"];
            var timeToDisplay = Request["timeToDisplay"];

            var startDate = FromDate;
            var endDate = DateTime.Now.Add(TimeSpan.FromDays(365)).ToString(DateTimeFormat.MsSqlFormat, CultureInfo.InvariantCulture);

            var campaignId = Convert.ToInt32(Request["campaignSelect"]);
            var wallpaperSrc = Request["wallpaperSrc"];

            var alertDictionary = AlertDictionaryFilling(startDate, endDate, name, wallpaperSrc, campaignId, position, timeToDisplay);
            var isScheduled = Convert.ToInt32(alertDictionary["schedule"]) == 1;

            var alertId = 0;
            if (string.IsNullOrEmpty(Request["shouldApprove"]) || !Request["shouldApprove"].Equals("1"))
            {
                alertId = AlertManager.Default.AddAlert(dbMgr, alertDictionary);

                if (isScheduled)
                {
                    if (AppConfiguration.IsNewContentDeliveringScheme)
                    {
                        var dateTimeUtc = DateTimeConverter.ParseDateTimeFromUi(Request["startDateAndTime"]).ToUniversalTime();
                        var contentJobDto = new ContentJobDto(dateTimeUtc, alertId, AlertType.Wallpaper);
                        _contentJobScheduler.CreateJob(contentJobDto);
                    }                      

                    var reccurenceDto = new Dictionary<string, object>
                    {
                        {"alert_id", alertId},
                        {"pattern", "o"},
                        {"countdowns", new List<string>()}
                    };

                    ReccurenceManager.ProcessReccurence(dbMgr, reccurenceDto);
                }
            }
            else
            {
                alertId = Convert.ToInt32(Request["alertId"]);
                AlertManager.Default.UpdateAlert(dbMgr, alertDictionary, alertId);
            }

            string rejectedEdit = "";

            if (!string.IsNullOrEmpty(Request["rejectedEdit"]))
            {
                rejectedEdit = Request["rejectedEdit"];
            }

            string shouldApprove = "";
            if (!string.IsNullOrEmpty(Request["shouldApprove"]))
            {
                shouldApprove = Request["shouldApprove"];
            }

            string dupAlertId = "";

            if (!string.IsNullOrEmpty(Request["alertId"]))
                dupAlertId = Request["alertId"];

            Response.Redirect(GetRedirectUrl(alertId, campaignId, rejectedEdit, shouldApprove, dupAlertId));
        }

        private static string GetRedirectUrl(int alertId, int campaignId, string rejectedEdit, string shouldApprove, string dupAlertId)
        {
            string returnUrl;
            if (campaignId > 0)
            {
                returnUrl = $"RecipientsSelection.aspx?id={alertId}&desktop=1&return_page=CampaignDetails.aspx?campaignId={campaignId}";
            }
            else
            {
                returnUrl = $"RecipientsSelection.aspx?id={alertId}&desktop=1&return_page=Wallpapers.aspx&campaign_id={campaignId}&rejectedEdit={rejectedEdit}&shouldApprove={shouldApprove}&dup_id={dupAlertId}";
            }

            return returnUrl;
        }

        private Dictionary<string, object> AlertDictionaryFilling(string startDate, string endDate, string name, string wallpaperSrc, int campaignId, string position, string timeToDisplay)
        {
            Dictionary<string, object> alertDictionary = new Dictionary<string, object>();
            if (!string.IsNullOrEmpty(Request["startAndEndDateAndTimeCheckbox"]))
            {
                startDate = DateTimeConverter.ConvertFromUiToDb(Request["startDateAndTime"]);
                endDate = DateTimeConverter.ConvertFromUiToDb(Request["endDateAndTime"]);
                alertDictionary.Add("schedule", 1);
            }
            else
            {
                alertDictionary.Add("schedule", 0);
            }

            alertDictionary.Add("title", name);
            alertDictionary.Add("alert_text", wallpaperSrc);
            alertDictionary.Add("campaign_id", campaignId);
            alertDictionary.Add("fullscreen", position);
            alertDictionary.Add("from_date", startDate);
            alertDictionary.Add("to_date", endDate);
            alertDictionary.Add("autoclose", timeToDisplay);
            alertDictionary.Add("create_date", DateTime.Now.ToString(DateTimeFormat.JavaScript, CultureInfo.InvariantCulture));
            alertDictionary.Add("schedule_type", campaignId == -1 ? 1 : 0);

            alertDictionary.Add("type", "D");
            alertDictionary.Add("sender_id", curUser.Id);

            int approveStatus = 1;

            if (Config.Data.HasModule(Modules.APPROVE)
                && (!curUser.HasPrivilege && !curUser.Policy[Policies.WALLPAPERS].CanApprove)
                && Settings.Content["ConfAllowApprove"] == "1")
            {
                approveStatus = 0;
            }

            alertDictionary.Add("approve_status", approveStatus);
            alertDictionary.Add("class", 8);

            return alertDictionary;
        }
    }
}