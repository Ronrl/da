﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WallpaperPreview.aspx.cs" Inherits="DeskAlertsDotNet.Pages.WallpaperPreview" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
	html, body, form {
		padding:0px;
		margin:0px;
		width:100%;
		height:100%;
		overflow:hidden;
		border:0px;
	}
     </style>
</head>
<body>
    <form id="form1" runat="server">
   
        <div id="previewDiv" style="width:100%; height:100%; " runat="server"></div>
        <asp:Image runat="server" ID="previewImage"/>
  
    </form>
</body>
</html>
