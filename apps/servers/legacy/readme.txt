﻿Изменения в существующих файлах:
\installer\include\UpdateTables1.sql
\NewInstaller\additionalfiles\sqlfiles\UpdateTables1.sql
Добавлена локализация для заголовка страницы импорта телефонов 

\sever\MODULES\AD MODULE\admin\ad_sync_from.asp

По аналогии с уже существующими чекбоксами, добавлен чекбокс для проверки импорта телефонов. Также по аналогии в js-скриптах добавлена провека опции импорта с текущей синхронизации.

\sever\MODULES\AD MODULE\admin\AdSync.aspx.cs

В методе AddUserToDb добавлена проверка с текущей синхронизации, в зависимости от данных поля notImpPhones выбирается соответствующий запрос.

\sever\STANDARD\Models\Synchronization.cs

Добавление поле типа int для проверки импорта.         
public int notImpPhones;


Новые файлы

\sever\STANDARD\admin\
ImportPhones.aspx
ImportPhones.aspx.cs
ImportPhones.aspx.designer.cs

Верстка и функционал импорта и парсинга из csv

Описание парсинга:

Парсится по двум делимитерам - ',' и ';'.

По умолчанию первым полем считается юзер, второе поле - телефоном. 
Парсится построчно. После каждой строки проверяет в базе по юзеру, если находит - перезаписывает.