﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DeskAlertsDotNet.Managers;
using DeskAlertsDotNet.Models;
using DeskAlertsDotNet.Models.Permission;
using DeskAlertsDotNet.Utils.Factories;

namespace DeskAlerts.IntegrationTestsProject
{
    public class UnitTestUserManager
    {
        private const string SelectUserByNameQuery =
            @"SELECT u.id, u.start_page, u.mobile_phone, u.email, u.next_request, p.type, pe.policy_id 
                FROM users as u
                LEFT JOIN policy_editor as pe ON pe.editor_id = u.id
                LEFT JOIN policy as p ON p.id = pe.policy_id WHERE [role] != 'U' AND u.name = @Name
                ORDER BY u.id ASC";

        private Dictionary<string, User> usersTable;
        public List<Token> Tokens { get; set; }

        private static UnitTestUserManager _instance;

        public static UnitTestUserManager Default
        {
            get
            {
                if (_instance == null)
                    return _instance = new UnitTestUserManager();

                return _instance;
            }
        }

        private UnitTestUserManager()
        {
            usersTable = new Dictionary<string, User>();
            Tokens = new List<Token>();
        }

        public IntegrationTestsUser GetUserByName(string name, UnitTestDbManager dbManager)
        {
            var parameter = SqlParameterFactory.Create(DbType.String, name, "Name");

            var userSet = dbManager.GetDataByQuery(SelectUserByNameQuery, parameter);
            if (userSet == null || userSet.IsEmpty)
            {
                return null;
            }

            var id = userSet.GetInt(0, "id");
            var type = userSet.GetString(0, "type");
            var isAdmin = id == 1 || (type != null && type.Equals("A"));
            var isModer = !isAdmin && (type != null && type.Equals("M"));
            var isPublisher = type != null && type.Equals("E");
            var isGroupPolicyUser = type != null && type.Equals("U");
            var email = userSet.GetString(0, "email");
            var phone = userSet.GetString(0, "mobile_phone");
            var pullPeriod = userSet.GetInt(0, "next_request");
            var startPage = userSet.GetString(0, "start_page");

            var langId = 1;
            var langSet = dbManager.GetDataByQuery($"SELECT lang_id FROM users_langs WHERE user_id = {id}");

            if (!langSet.IsEmpty)
            {
                langId = langSet.First().GetInt("lang_id", 1);
            }

            var policy = new Policy();

            var mockWigets = new List<Widget>();

            return new IntegrationTestsUser(id, name, isAdmin, isModer, isPublisher, policy, mockWigets, email, phone, pullPeriod,
                startPage, langId);
        }
    }
}