﻿using NUnit.Framework;

namespace DeskAlerts.IntegrationTestsProject.Transport
{
    [TestFixture]
    public class AlertDelivery
    {
        public readonly UnitTestDbManager DbManager = new UnitTestDbManager();

        [SetUp]
        public void Init()
        {
            DbManager.OpenConnection();
        }

        [TearDown]
        public void Dispose()
        {
            DbManager.CloseConnection();
        }

        [Test]
        public void TestAlertDelivery()
        {
            //Create alert
            DbManager.ExecuteQuery("INSERT INTO alerts (alert_text) VALUES ('Test Alert Title');");

            var createdAlertIdDataSet = DbManager.GetDataByQuery("SELECT IDENT_CURRENT('dbo.alerts') AS Current_Identity");
            var createdAlertId = createdAlertIdDataSet.GetInt(0, "Current_Identity");

            var userManager = UnitTestUserManager.Default;
            var buildUser = userManager.GetUserByName("BUILD", DbManager);

            Assert.AreNotEqual(buildUser, null, "BUILD user is null!");

            //Add user for alert
            DbManager.ExecuteQuery("INSERT INTO alerts_sent (alert_id, user_id) VALUES (" + createdAlertId + ", " + buildUser.Id + ");");

            //Convert to milliseconds
            System.Threading.Thread.Sleep(buildUser.PullPeriod * 1000);

            var recievedAlertDataSet = DbManager.GetDataByQuery("SELECT * FROM alerts_recieved WHERE alert_id = " + createdAlertId);

            Assert.AreNotEqual(0, recievedAlertDataSet.Count, "Alert do not recieved!");

            //Delete DB entries
            DbManager.ExecuteQuery("DELETE FROM alerts WHERE id = " + createdAlertId);
            DbManager.ExecuteQuery("DELETE FROM alerts_sent WHERE alert_id = " + createdAlertId);
            DbManager.ExecuteQuery("DELETE FROM alerts_recieved WHERE alert_id = " + createdAlertId);
        }
    }
}
