﻿using System.Globalization;
using System.Threading;
using DeskAlertsDotNet.sever.MODULES.RSS;
using NUnit.Framework;

namespace DeskAlerts.IntegrationTestsProject.RSS
{
    [TestFixture]
    internal class RssSenderTest
    {
        public RssSender Target { get; private set; }

        private UnitTestDbManager UnitTestDbManager { get; set; }

        [SetUp]
        public void SetUp()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
            Target = new RssSender();
            UnitTestDbManager = new UnitTestDbManager();
            UnitTestDbManager.OpenConnection();
        }

        [TearDown]
        public void TearDown()
        {
            UnitTestDbManager.ExecuteQuery("DELETE FROM alerts WHERE alerts.alert_text LIKE '%TestRSS| | | %'");
            UnitTestDbManager.CloseConnection();
        }

        //[Test]
        //public void NotScheduledRssShouldPassCheck()
        //{
        //    UnitTestDbManager.ExecuteQuery("INSERT INTO alerts (alert_text, class, schedule_type, schedule) VALUES ('TestRSS| | | ', 4, 1, 0)");

        //    var rssAlerts = UnitTestDbManager.GetDataByQuery(@"SELECT * FROM alerts WHERE class = 4 AND schedule_type = '1'");

        //    Assert.Throws<Exception>(
        //        delegate { Target.TrySendRss(rssAlerts); });
        //}

        //[Test]
        //public void ScheduledRssWithRightDateRangeShouldPass()
        //{
        //    var startDate = DateTime.Now.Subtract(new TimeSpan(1, 0, 0)).ToString("yyyy-MM-dd HH:mm:ss.fff");
        //    var endDate = DateTime.Now.AddHours(1).ToString("yyyy-MM-dd HH:mm:ss.fff");

        //    UnitTestDbManager.ExecuteQuery("" +
        //        "INSERT INTO alerts (alert_text, class, schedule_type, schedule, from_date, to_date) " +
        //                    $"VALUES ('TestRSS| | | ', 4, 1, 1, '{startDate}', '{endDate}')");

        //    var rssAlerts = UnitTestDbManager.GetDataByQuery(@"SELECT * FROM alerts WHERE class = 4 AND schedule_type = '1'");

        //    Assert.Throws<Exception>(
        //        delegate { Target.TrySendRss(rssAlerts); });
        //}

        //[Test]
        //public void ScheduledRssWithDateRangeBeforeRightShouldNotPass()
        //{
        //    var startDate = DateTime.Now.Subtract(new TimeSpan(2, 0, 0)).ToString("yyyy-MM-dd HH:mm:ss.fff");
        //    var endDate = DateTime.Now.Subtract(new TimeSpan(1, 0, 0)).ToString("yyyy-MM-dd HH:mm:ss.fff");

        //    UnitTestDbManager.ExecuteQuery("" +
        //                                   "INSERT INTO alerts (alert_text, class, schedule_type, schedule, from_date, to_date) " +
        //                                   $"VALUES ('TestRSS| | | ', 4, 1, 1, '{startDate}', '{endDate}')");

        //    var rssAlerts = UnitTestDbManager.GetDataByQuery(@"SELECT * FROM alerts WHERE class = 4 AND schedule_type = '1'");

        //    Assert.DoesNotThrow(
        //        delegate { Target.TrySendRss(rssAlerts); });
        //}

        //[Test]
        //public void ScheduledRssWithDateRangeAfterRightShouldNotPass()
        //{
        //    var startDate = DateTime.Now.AddHours(1).ToString("yyyy-MM-dd HH:mm:ss.fff");
        //    var endDate = DateTime.Now.AddHours(2).ToString("yyyy-MM-dd HH:mm:ss.fff");

        //    UnitTestDbManager.ExecuteQuery("" +
        //                                   "INSERT INTO alerts (alert_text, class, schedule_type, schedule, from_date, to_date) " +
        //                                   $"VALUES ('TestRSS| | | ', 4, 1, 1, '{startDate}', '{endDate}')");

        //    var rssAlerts = UnitTestDbManager.GetDataByQuery(@"SELECT * FROM alerts WHERE class = 4 AND schedule_type = '1'");

        //    Assert.DoesNotThrow(
        //        delegate { Target.TrySendRss(rssAlerts); });
        //}
    }
}
