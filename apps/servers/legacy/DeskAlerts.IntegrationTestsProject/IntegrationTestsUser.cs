﻿using System.Collections.Generic;
using DeskAlertsDotNet.Models;
using DeskAlertsDotNet.Models.Permission;

namespace DeskAlerts.IntegrationTestsProject
{
    public class IntegrationTestsUser
    {
        public IntegrationTestsUser(int id, string name, bool isAdmin, bool isModer, bool isPublisher, Policy policy, List<Widget> widgets, string email = "", string phone = "", int pullPeriod = 5, string startPage = "dashboard.aspx", int langId = 1)
        {
            Id = id;
            Name = name;
            IsAdmin = isAdmin;
            IsModer = isModer;
            IsPublisher = isPublisher;
            Policy = policy;
            Widgets = widgets;
            Email = email;
            Phone = phone;
            PullPeriod = pullPeriod;
            StartPage = startPage;
            LangId = langId;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsSalesManager { get; set; }
        public bool IsModer { get; set; }
        public bool IsPublisher { get; set; }

        public bool IsSsoAuthorized { get; set; }
        public Policy Policy { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int PullPeriod { get; set; }
        public List<Widget> Widgets { get; set; }
        public string StartPage { get; set; }
        public int LangId { get; set; }

        public bool HasPrivilege => IsModer || IsAdmin;
    }
}
