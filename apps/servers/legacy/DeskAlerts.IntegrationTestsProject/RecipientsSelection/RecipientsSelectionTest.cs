﻿using System;
using System.Data;
using DeskAlerts.UIAutomationTests.Tests;
using DeskAlertsDotNet.Utils.Factories;
using NUnit.Framework;


namespace DeskAlerts.IntegrationTestsProject.RecipientsSelection
{
    [TestFixture]
    public class RecipientsSelectionTest : BaseAdminAuthorizationTest
    {
        public readonly UnitTestDbManager DbManager = new UnitTestDbManager();

        [SetUp]
        public void Init()
        {
            DbManager.OpenConnection();
        }

        [TearDown]
        public void Dispose()
        {
            DbManager.CloseConnection();
        }

        [Test]
        public void Should_SortUsersByOnline_When_UserIsOnline()
        {
            // Arrange
            var now = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            DbManager.ExecuteQuery($@"INSERT INTO [dbo].[users] ([name], [display_name], [role], [last_request]) VALUES ('{OnlineUserName}', '{OnlineUserName}', 'U', @Date); ", SqlParameterFactory.Create(DbType.DateTime, now, "Date"));
            DbManager.ExecuteQuery($@"INSERT INTO [dbo].[users] ([name], [display_name], [role]) VALUES ('{OfflineUserName}', '{OfflineUserName}', 'U'); ");

            // Act
            MenuPage.OpenCreateAlert();
            AlertTitleFramePage.Focus();
            AlertTitleFramePage.SetTitle(SelectedRecipientsTitle);
            AlertTitleFramePage.FocusOnBody();

            AlertBodyFramePage.Focus();
            AlertBodyFramePage.SetBody(SelectedRecipientsBody);
            AlertBodyFramePage.FocusOnBody();

            CreateAlertPage.ClickSaveAndNext();

            RecipientsSelectionPage.SelectRecipients();
            RecipientsFramePage.Focus();
            RecipientsFramePage.ClickSortByOnline();
            RecipientsFramePage.ClickSortByOnline();


            // Assert
            Assert.AreEqual($"{OnlineUserName}", RecipientsFramePage.GetFirstCellOfFirstRowValue());

            // Clean Up
            DbManager.ExecuteQuery($"DELETE FROM users WHERE name = '{OnlineUserName}'");
            DbManager.ExecuteQuery($"DELETE FROM users WHERE name = '{OfflineUserName}'");
        }

        [Test]
        public void Should_SortUsersByOnline_When_UserIsOffline()
        {
            // Arrange
            var now = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            DbManager.ExecuteQuery($@"INSERT INTO [dbo].[users] ([name], [display_name], [role], [last_request]) VALUES ('{OnlineUserName}', '{OnlineUserName}', 'U', @Date); ", SqlParameterFactory.Create(DbType.DateTime, now, "Date"));
            DbManager.ExecuteQuery($@"INSERT INTO [dbo].[users] ([name], [display_name], [role]) VALUES ('{OfflineUserName}', '{OfflineUserName}', 'U'); ");

            // Act
            MenuPage.OpenCreateAlert();
            AlertTitleFramePage.Focus();
            AlertTitleFramePage.SetTitle(SelectedRecipientsTitle);
            AlertTitleFramePage.FocusOnBody();

            AlertBodyFramePage.Focus();
            AlertBodyFramePage.SetBody(SelectedRecipientsBody);
            AlertBodyFramePage.FocusOnBody();

            CreateAlertPage.ClickSaveAndNext();

            RecipientsSelectionPage.SelectRecipients();
            RecipientsFramePage.Focus();
            RecipientsFramePage.ClickSortByOnline();

            // Assert
            Assert.AreEqual($"{OfflineUserName}", RecipientsFramePage.GetFirstCellOfFirstRowValue());

            // Clean Up
            DbManager.ExecuteQuery($"DELETE FROM users WHERE name = '{OnlineUserName}'");
            DbManager.ExecuteQuery($"DELETE FROM users WHERE name = '{OfflineUserName}'");
        }
    }
}
