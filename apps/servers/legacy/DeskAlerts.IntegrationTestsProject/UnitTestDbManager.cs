﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using DeskAlertsDotNet.DataBase;
using DeskAlertsDotNet.Exceptions;

namespace DeskAlerts.IntegrationTestsProject
{
    public class UnitTestDbManager
    {
        private readonly string _connectionString;

        private SqlConnection _sqlConnection;

        public UnitTestDbManager()
        {
            var settings = ConfigurationManager.ConnectionStrings;
            var dbConnectionString = settings["DBConnectionString"];

            _connectionString = dbConnectionString.ConnectionString;
        }

        public void OpenConnection()
        {
            _sqlConnection = new SqlConnection(_connectionString);

            _sqlConnection.Open();
        }

        public void CloseConnection()
        {
            _sqlConnection.Close();
        }

        public int ExecuteQuery(string query, params SqlParameter[] parameters)
        {
            if (string.IsNullOrEmpty(query))
            {
                return -1;
            }

            try
            {
                using (var command = new SqlCommand(query, _sqlConnection))
                {
                    foreach (var parameter in parameters)
                    {
                        command.Parameters.Add(parameter);
                    }

                    var result = command.ExecuteNonQuery();

                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new DeskAlertsDBException(query, ex.Message);
            }
        }

        public DataSet GetDataByQuery(string sqlQuery, params SqlParameter[] parameters)
        {
            var arrayList = new List<DataRow>();

            try
            {
                using (var cmd = new SqlCommand(sqlQuery, _sqlConnection))
                {
                    foreach (var parameter in parameters)
                    {
                        cmd.Parameters.Add(parameter);
                    }

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var row = new Dictionary<string, object>();

                            for (var i = 0; i < reader.FieldCount; i++)
                            {
                                if (!row.ContainsKey(reader.GetName(i)))
                                {
                                    row.Add(reader.GetName(i), reader.GetValue(i));
                                }
                            }

                            var dataRow = new DataRow(row);

                            arrayList.Add(dataRow);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new DeskAlertsDBException(sqlQuery, ex.Message);
            }

            return new DataSet(arrayList);
        }
    }
}
