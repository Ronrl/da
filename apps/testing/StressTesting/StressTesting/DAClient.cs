﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;

namespace StressTesting
{
    public class CreateAlertRequest
    {
        public string version { get; set; }
        public string alert_text { get; set; }
        public string title { get; set; }
        public bool IsTicker { get; set; }
        public IList<string> users { get; set; }
        public IList<string> computers { get; set; }
        public IList<string> groups { get; set; }
        public IList<string> OUs { get; set; }
    }


    public class Alert
    {
        public string Href { get; set; }
    }
    public class AddAlert
    {
        public string alertContent => "<p><span style=\"font-family: Arial;\">Alert body #</span></p>";
        public string popup = "1";
        public string ticker = "0";
        public string htmlTitle = "<span style=\"font-weight: bold;\">Alert #</span>";
        public string rsvp = "0";
        public string edit = "";
        public string rejectedEdit = "0";
        public string urgentCheckBox = null;
        public string alertCampaign = null;
        public string campaignSelect = "-1";
        public string aknown = null;
        public string unobtrusiveCheckbox = null;
        public string webAlert = "";
        public string isVideoAlert = null;
        public string langs = "";
        public string defaultLang = "";
        public string alertContent_ = null;
        public string selfDeletable = null;
        public string autoCloseCheckBox = null;
        public string autoClosebox = null;
        public string manualClose = null;
        public string tickerSpeedValue = "4";
        public string linkedIn = "0";
        public string recurrenceCheckbox = "1";
        public string lifetime = "1";
        public string lifetimeFactor = "1440";

        // request name prefix: "schedulePanel$"
        // For example:
        // schedulePanel$start_date_and_time
        // schedulePanel$pattern
        // schedulePanel$end_time
        //public string start_date_and_time = "29/06/2020 12:21:59";
        //public string end_date_and_time = "29/06/2020 13:21:59";
        //public string pattern = "d";
        public string start_date_rec = "29/06/2020";
        public string start_time = "00:00:00";
        public string end_date_rec = "30/06/2030";
        //public string end_time = "23:59:59";

        public string resizable = "1";
        public string fullscreen = "0";
        public string alertWidth = "505";
        public string alertHeight = "404";
        public string position = "4";
        public string ticker_position = "9";
        public string dev = "3";
        public string sms = null;
        public string emailCheck = null;
        public string IsDraft = "0";
        public string IsInstant = "";
        public string email_sender = null;
        public string desktopCheck = "1";
        public string skin_id = "87ebf16f-2561-4d39-8e12-00aaab3c4aa9";
        public string shouldApprove = "";
        public string colorСode = "";
        public string printAlertCheckBox = null;
        public string socialMedia = null;
        public string blogPost = null;
        public string textToCall = null;
        public string videoLink = null;
        public string link_embed = null;
        public string alertId = "";
        public string countdowns = null;
        public string question = "Will you accept the invitation?";
        public string question_option1 = "Yes";
        public string question_option2 = "No";
        public string need_second_question = null;
        public string second_question = "What is your reason?";
        public string isModerated = "";
    }

    public enum ClientDeviceType
    {
        WindowsClient = 1,
        MacClient = 2,
        AndroidClient = 3,
        IosClient = 4
    }

    public class AuthorizeUserRequestDto
    {
        public string DeskbarId { get; set; }
        public string UserName { get; set; }
        public string ComputerName { get; set; }
        public string ComputerDomainName { get; set; }
        public string Ip { get; set; }
        public ClientDeviceType DeviceType { get; set; }
        public string ClientVersion { get; set; }
        public string Md5Password { get; set; }
        public string PollPeriod { get; set; } = "5";
    }

    public class DAClient
    {
        private string url = "http://localhost:4410/";
        private string token = "";
        private bool isLogged = false;

        private string password = "3FC0A7ACF087F549AC2B266BAF94B8B1";

        private string apiKey = "q6zqQDiT64OZhf0h";

        private string deskbarId = Guid.NewGuid().ToString();
        public DAClient()
        {
        }

        public void CreateAlert(string username)
        {
            var alertRequest = new CreateAlertRequest()
            {
                alert_text = "test",
                title = "test",
                version = "v1",
                IsTicker = false,
                users = new List<string>(),
                computers = new List<string>(),
                OUs = new List<string>(),
                groups = new List<string>()
            };

            alertRequest.users.Add(username);
            var json = JsonConvert.SerializeObject(alertRequest);
            var data = new StringContent(json, Encoding.UTF8, "application/json");


            string path = this.url + "/api/alerts";


            using (var client = new HttpClient())
            {

                client.DefaultRequestHeaders.Add("apiKey", apiKey);

                var response = client.PostAsync(path, data).Result;

                if (response.StatusCode == HttpStatusCode.OK)
                {
                }
            }
        }


        public string Login(string username)
        {
            string path = this.url + "/api/v1/security/token/user";
            deskbarId = username.Substring(7, 4) + new Guid().ToString().Substring(4);


            var param = new AuthorizeUserRequestDto()
            {
                UserName = username,
                ClientVersion = "0.0.0.0",
                ComputerDomainName = "local",
                ComputerName = "TestComputer",
                DeskbarId = this.deskbarId,
                Ip = "",
                DeviceType = ClientDeviceType.WindowsClient,
                Md5Password = this.password,
            };

            var json = JsonConvert.SerializeObject(param);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            string content;

            using (var client = new HttpClient())
            {

                var response = client.PostAsync(path, data).Result;

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    content = response.Content.ReadAsStringAsync().Result;

                    JObject o = JObject.Parse(content);

                    this.token = o["data"]["authorization"].ToString().Split()[1];
                }
            }

            return "";
        }

        public string Register(string username)
        {
            string path = $"{this.url}/register_com.asp?desk_id={this.deskbarId}&client_id=3&uname={username}&upass=qwerty123";

            using (HttpClient client = new HttpClient())
            {
                var response = client.GetAsync(path).Result;
                return response.ToString();
            }
        }

        public List<string> GetContents(string username)
        {
            Login(username);

            var result = new List<string>();

            var timeZoneSeconds = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow).TotalSeconds;

            var path = $"{url}/api/v1/user/contents?timezone={timeZoneSeconds}";

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                var response = client.GetAsync(path).Result;
                response.EnsureSuccessStatusCode();
                var responseBody = response.Content.ReadAsStringAsync().Result;


                JObject o = JObject.Parse(responseBody);
                var alerts = o["data"]["alerts"];

                for (var i = 0; i < alerts.Count(); i++)
                {
                    string input = alerts[i]["body"].ToString();
                    string regex = "href=\"(.*)\"";
                    Match match = Regex.Match(input, regex);
                    if (match.Success)
                    {
                        string link = match.Groups[1].Value;
                        result.Add(link);
                    }
                }
            }

            return result;
        }

        public string GetAlert(long alertId, long userId)
        {
            var localtime = DateTime.UtcNow.ToString("dd.MM.yyyy|HH:mm:ss");
            var url = $"{this.url}/GetAlert.aspx?id={alertId}&user_id={userId}&deskbar_id={this.deskbarId}&localtime={localtime}&isMobile=0";

            using (var client = new HttpClient())
            {
                var response = client.GetAsync(url).Result;
                response.EnsureSuccessStatusCode();
                var responseBody = response.Content.ReadAsStringAsync().Result;
                //Console.WriteLine($"{thread.Name} iteration {i}: \t\t{responseBody.Substring(0, 30)}");
            }

            return "";

        }


    }
}
