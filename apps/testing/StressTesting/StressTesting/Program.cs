using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;

namespace StressTesting
{
    class Program
    {
        static void CreateUsers()
        {
            var client = new DAClient();

            for (int i = 1; i < 1000; i++)
            {
                client.Register($"tl_user{i:D4}");
            }
        }


        public static void CreateCSV(string filename, int count, int uf, int ut)
        {
            var random = new Random();

            var lines = new List<string>();

            using (var file = new System.IO.StreamWriter(filename))
            {
                for (int i = 1; i <= count; i++)
                {
                    var user = $"tl_user{random.Next(uf, ut):D4}";
                    var line =
                        $"{user}, 22.10.2019 00:00:00, {i}, test alert {i} Download and install the latest version of the UniFi Controller software at downloads.ubnt.com/unifi. Launch the";
                    file.WriteLine(line);
                }
            }
        }

        public static void ReadUserAlerts(string username)
        {
            DAClient dclient = new DAClient();
            List<string> alerts = new List<string>();
            try
            {
                alerts = dclient.GetContents(username);

                if (alerts == null) return;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //throw;
            }


            foreach (var url in alerts)
            {
                using (var client = new HttpClient())
                {
                    try
                    {
                        var response = client.GetAsync(url).Result;
                        response.EnsureSuccessStatusCode();
                        Thread.Sleep(500);
                        var responseBody = response.Content.ReadAsStringAsync().Result;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
            }
        }


        public static void ReadAlertsThreadFunc()
        {
            try
            {
                var random = new Random();

                for (int i = 0; i < 1000; i++)
                {
                    try
                    {
                        int user = random.Next(1, 999);
                        ReadUserAlerts($"tl_user{user:D4}");
                        Thread.Sleep(1000);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        throw;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public static void CreateAlertsThreadFunc()
        {
            var random = new Random();

            var client = new DAClient();

            for (var i = 0; i < 10000; i++)
            {
                client.CreateAlert($"tl_user{random.Next(1, 999):D4}");
                Thread.Sleep(1000);
            }
        }

        static void Main(string[] args)
        {
            //CreateUsers();

            Thread createThread = new Thread(new ThreadStart(CreateAlertsThreadFunc));
            createThread.Start();

            for (var i = 0; i < 20; i++)
            {
                Thread thread = new Thread(new ThreadStart(ReadAlertsThreadFunc));
                thread.Start();
            }
        }
    }
}
