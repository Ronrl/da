﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegisterForm.aspx.cs" Inherits="DeskAlerts.Server.RegisterForm" %>

<%@ Import Namespace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DeskAlerts - Desktop Alert Software</title>
    <link href="admin/css/style.css" rel="stylesheet" type="text/css">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script language="javascript" type="text/javascript" src="admin/jscripts/jquery/jquery.min.js"></script>
    <script language="javascript" type="text/javascript" src="admin/jscripts/jquery/jquery-ui.min.js"></script>
    <style>
        #registerFormBlock select, #registerFormBlock input {
            vertical-align: middle;
            cursor: default;
            padding: 2px 4px;
            text-decoration: none;
            background-color: #ffffff;
            color: black;
            font-size: 0.9rem;
            font-weight: normal;
            border-radius: 0px;
            width: 150px;
        }
        #registerFormBlock input[type="image"] {
            padding: 0;
            border: none;
            width: 78px;
        }
    </style>
</head>
<body class="body" scroll="no" style="background-color: #2DB14F;">
    <form id="form1" runat="server" method="POST" action="RegisterForm.aspx">
        <div id="registerFormBlock">
            <table height="100%" border="0" align="center" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center">
                        <script type="text/javascript">
                            function DeskAlertsInit(tool) {
                                if (tool) {
                                    window.external.Close();
                                }
                            }

                            function Close() {
                                try {
                                    window.external.Close();
                                } catch (e) { }
                                return false;
                            }
                        </script>
                        <div id="messageDiv" runat="server" visible="False" style="margin: 20px 10px 10px 10px;">
                            <br />
                            <br />
                            <p>
                                <strong>
                                    <asp:Label runat="server" ID="SuccessMessage" Visible="true"></asp:Label>
                                </strong>
                            </p>
                            <input id="closeButton" runat="server" type="image" src="" alt="" title="" border="0" onclick="return Close();" />
                        </div>
                        <div runat="server" id="mainForm" style="margin: 20px 10px 10px 10px;">
                            <input type="hidden" name="disableUnameChange" id="disableUnameChange" runat="server" />
                            <input type="hidden" name="deskbar_id" id="deskbar_id" runat="server"/>
                            <table cellpadding="0" cellspacing="4" border="0">
                                <tr>
                                    <td class="body" style="background-color: #2DB14F;"><%=resources.LNG_USERNAME %>:</td>
                                    <td>
                                        <input name="uname" class="reg_input" type="text" value="" /></td>
                                </tr>
                                <tr>
                                    <td class="body" style="background-color: #2DB14F;"><%=resources.LNG_PASSWORD %>:</td>
                                    <td>
                                        <input name="upass" class="reg_input" type="password" /></td>
                                </tr>
                                <tr runat="server" id="domainRow">
                                    <td class="body" style="background-color: #2DB14F;"><%=resources.LNG_DOMAIN %>:</td>
                                    <td>
                                        <select runat="server" id="domain" name="domain" class="reg_input"></select>
                                    </td>
                                </tr>
                                <tr runat="server" id="mobilePhoneRow">
                                    <td class="body" style="background-color: #2DB14F;"><%=resources.LNG_MOBILE_PHONE %>:</td>
                                    <td>
                                        <input name="uphone" class="reg_input"  type="text" value="" /></td>
                                </tr>
                                <tr runat="server" id="emailRow">
                                    <td class="body" style="background-color: #2DB14F;"><%=resources.LNG_EMAIL %>:</td>
                                    <td>
                                        <input name="uemail" class="reg_input" type="text" value="" /></td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="right">
                                        <input id="submitButton" type="image" src="<% =Config.Data.GetString("alertsDir") %>/admin/images/langs/EN/save.gif" alt="<%=resources.LNG_SAVE %>" title="<%=resources.LNG_SAVE %>" border="0">
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
