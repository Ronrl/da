﻿<%@ Page EnableEventValidation="false" Language="C#" AutoEventWireup="true" CodeBehind="CreateAlert.aspx.cs" Inherits="DeskAlertsDotNet.Pages.CreateAlert" %>

<%@ Import Namespace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="DeskAlertsDotNet.DataBase" %>

<%@ Register Src="ReccurencePanel.ascx" TagName="ReccurencePanel"
    TagPrefix="da" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DeskAlerts</title>
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="css/style9.css" rel="stylesheet" type="text/css" />
    <link href="css/form_style.css" rel="stylesheet" type="text/css" />

    <style>
        .checkbox_div {
            padding: 1px;
        }
    </style>

    <script language="javascript" type="text/javascript" src="functions.js"></script>

    <!-- TinyMCE -->

    <script language="javascript" type="text/javascript" src="jscripts/tiny_mce/tinymce.min.js"></script>

    <!-- /TinyMCE -->
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/shortcut.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>
    <script language="javascript" type="text/javascript" src="jscripts/json2.js"></script>
    <script language="javascript" type="text/javascript" src="jscripts/CreateAlert.js"></script>
    <script type="text/javascript" src="https://s0.assets-yammer.com/assets/platform_social_buttons.min.js"></script>

    <script language="javascript" type="text/javascript">
        function submitForm(isDraft) {
            $(function () {
                if (isDraft) {
                    $("#IsDraft").val("1");
                }

                $("#form1").submit();
            });
        }

        function showPrompt(text) {
            $(function () {
                $("#dialog-modal > p").text(text);
                $("#dialog-modal")
                    .dialog({
                        height: 180,
                        modal: true,
                        resizable: false,
                        buttons: {
                            "<%=Resources.resources.LNG_CLOSE %>": function () {
                                $(this).dialog("close");
                            }
                        }
                    });
            });
        }

        function showColorCodeDialog() {
            $("#color_code_dialog").dialog({
                position: { my: "left top", at: "left top", of: "#dialog_dock" },
                modal: true,
                resizable: false,
                draggable: false,
                width: 'auto',
                resize: 'auto',
                margin: 'auto'

            });
        }

        if (!$("input[name='id']").val()) {
            $("#yammerShare").removeClass("yammerShare");
            $("#yammerShare").click(function (e) { e.preventDefault(); return false; });
            $("#yammerNotification").show();
        }
        else {
            var url = "<%=Config.Data.GetString("alertsDir")%>preview.asp?id=" + $("input[name='id']").val();
            var msg = $("#title").val();
        }
        var options = {
            customButton: true, //false by default. Pass true if you are providing your own button to trigger the share popup
            classSelector: 'yammerShare', //if customButton is true, you must pass the css class name of your button (so we can bind the click event for you)
            defaultMessage: msg, //optionally pass a message to prepopulate your post
            pageUrl: url //current browser url is used by default. You can pass your own url if you want to generate the OG object from a different URL.
        };
            <% if (Config.Data.HasModule(Modules.YAMMER))
        { %>

        yam.platform.yammerShare(options);
        <% } %>
</script>
    <script language="javascript" type="text/javascript">

        function check_size() {

            var shouldCheckSize = <% =shouldCheckSize%>;

            if (shouldCheckSize == 0)
                return;

            var elem = document.getElementById('tickerTypeTab');
            if (elem && !elem.checked) {
                var disabled = elem.checked;
                var el = document.getElementById('size_div');
                toggleDisabled(el, disabled);
                el = document.getElementById('size_message');
                toggleDisabled(el, disabled);
            }
            check_message();
        }

        function check_message() {

            var elem = document.getElementById('size2');
            var msg = document.getElementById('size_message');
            var al_width = document.getElementById('alertWidth');
            var al_height = document.getElementById('alertHeight');
            var resizable = document.getElementById('resizable');
            var position_fieldset = document.getElementById('position_fieldset');
            var tickerTab = document.getElementById('tickerTypeTab');
            var isTicker = tickerTab && tickerTab.checked ? true : false;
            var webplugin = '<%=digitalSignage%>';
            if (elem && al_width && al_height && !isTicker) {
                if (msg) msg.style.display = elem.checked || webplugin == "1" ? "none" : "";

                if (al_width)
                    al_width.disabled = elem.checked;

                if (al_height)
                    al_height.disabled = elem.checked;

                if (position_fieldset) {
                    position_fieldset.disabled = elem.checked;
                    $("#ticker_position_fieldset").disabled = elem.checked;
                }

                if (resizable) resizable.disabled = elem.checked;
            }
            if (isTicker) {
                //      first_elem.disabled = true;
                elem.disabled = true;
                if (resizable) resizable.disabled = true;
            }
        }


    </script>

    <% if (Config.Data.HasModule(Modules.LINKEDIN))
        { %>
    <script type="text/javascript" src="//platform.linkedin.com/in.js">
         api_key: <%=liApiKey %>;
         onLoad: onLinkedInLoad;
         authorize: true;
    </script>

    <script type="text/javascript" language="javascript">

        function onLinkedInLoad() {
            IN.Event.on(IN, "auth", function () { onLinkedInLogin(); });
            IN.Event.on(IN, "logout", function () { onLinkedInLogout(); });
            $("#linkedin_settings_hint").hide();
        }

        function onLinkedInLogout() {
            $("#shareAPI").hide();
        }

        function onLinkedInLogin() {
            IN.Event.on("shareAPI", "click", doSharingTest);
            $("#shareAPI").show();
        }
    </script>

    <% }
        else
        {%>
    <script type="text/javascript">
        $("#linkedInDiv").hide();
        $("#linkedin_settings_hint").show();
    </script>
    <% } %>

    <script type="text/javascript">
        var skinId;
        var confShowTypesDialog = <%= Settings.Content["ConfShowTypesDialog"]  %>;

        var isInstant = '<%=IsInstantVal %>';
        var skins = <% =skinsJson%>;
        var Langs = '<% =ConfEnableAlertLangs%>';
        var languagesData = '<% =languages%>';



        var old_aknown;
        function check_alert_type() {
            var webplugin = parseInt('<%=digitalSignage %>');

            if (webplugin == "1") {
                $("#alertDeviceHeader").hide();
                $("#alertDeviceTable").hide();

                if ($("#tickerTypeTab").prop("checked")) {
                    $("#alertAppearanceHeader").hide();
                }
                else {
                    $("#alertAppearanceHeader").show();
                }
            }
            else {
                $("#alertDeviceHeader").show();
                $("#alertDeviceTable").show();
                $("#alertAppearanceHeader").show();

            }
            //$("#templatesList").show();

            if ($("#rsvpTypeTab").prop('checked')) {
                $("#tickerInfo").hide();
                $("#non_rsvp").hide();
                $("#position_div").parent().show();
                $("#ticker_position_div").parent().hide();
                $("#tickerSpeedSliderWrapper").hide();
                var imgSrc = $("#skinPreviewImg").attr("src");
                if (imgSrc) {
                    imgSrc = imgSrc.replace("thumbnail_tick.png", "thumbnail.png");
                    $("#skinPreviewImg").attr("src", imgSrc);
                }
                $("#once").prop('checked', true);
                //   patternChange('o');
                $("#dayly,label[for='dayly'],#weekly,label[for='weekly'],#monthly,label[for='monthly'],#yearly,label[for='yearly']").fadeOut(500);
                $("#rsvp_div").fadeIn(500);
                $("#linkedin_div").hide();
                $("#elements_to_display").show();
                $("#buttons_to_display").show();
                $(".save_button").show();
                $("#main_buttons_next").show();
                $("#right_items").height("430px");
                old_aknown = $("#aknown").is(':checked');
                $("#aknown").prop('checked', false).parent().hide();
                check_aknown();
                $("#print_div").hide();
                $('.tile').show();
                $('#schedule_webplugin').hide();
                $('#schedule_webplugin_table').hide();
                $('#webalert').prop("disabled", true);
                need_second_question_click();
            }
            else if ($("#lnkdnTypeTab").prop('checked')) {
                $("#linkedin_div").fadeIn(500);
                $("#tickerInfo").hide();
                $("#non_rsvp").show();
                $("#tickerSpeedSliderWrapper").hide();
                var imgSrc = $("#skinPreviewImg").attr("src");
                if (imgSrc) {
                    imgSrc = imgSrc.replace("thumbnail_tick.png", "thumbnail.png");
                    $("#skinPreviewImg").attr("src", imgSrc);
                }
                $("#elements_to_display").hide();
                $("#buttons_to_display").hide();
                $(".save_button").hide();
                $("#main_buttons_next").hide();
                $("#right_items").height("50px");
                $("#rsvp_div").hide();
                $("#shareAPI").button();
                old_aknown = $("#aknown").is(':checked');
                $("#aknown").prop('checked', false).parent().hide();
                check_aknown();
                $("#print_div").hide();
                $('.tile').show();
                $('#schedule_webplugin').hide();
                $('#schedule_webplugin_table').hide();
                $('#webalert').prop("disabled", true);
            }
            else if ($("#alert_type_webplugin").prop('checked')) {
                $("#linkedin_div").hide();
                $("#tickerInfo").hide();
                $("#non_rsvp").show();
                $("#tickerSpeedSliderWrapper").hide();
                var imgSrc = $("#skinPreviewImg").attr("src");
                if (imgSrc) {
                    imgSrc = imgSrc.replace("thumbnail_tick.png", "thumbnail.png");
                    $("#skinPreviewImg").attr("src", imgSrc);
                }
                $("#elements_to_display").hide();
                $("#buttons_to_display").hide();
                $(".save_button").hide();
                $("#main_buttons_next").hide();
                $("#right_items").height("50px");
                $("#rsvp_div").hide();
                $("#shareAPI").button();
                old_aknown = $("#aknown").is(':checked');
                $("#aknown").prop('checked', false).parent().hide();
                check_aknown();
                $("#print_div").hide();
                $('.tile').hide();
                $('#schedule_webplugin').show();
                $('#schedule_webplugin_table').show();
                $('#webalert').prop("disabled", false);
            }
            else {


                $('#webalert').prop("disabled", true);
                $('#schedule_webplugin').hide();
                $('#schedule_webplugin_table').hide();
                $('.tile').show();
                $("#right_items").height(430);
                $("#non_rsvp").show();
                $(".save_button").show();
                $("#main_buttons_next").show();
                $("#elements_to_display").show();
                $("#buttons_to_display").show();
                $("#dayly,label[for='dayly'],#weekly,label[for='weekly'],#monthly,label[for='monthly'],#yearly,label[for='yearly']").fadeIn(500);
                if (old_aknown !== null) {
                    $("#aknown").prop('checked', old_aknown);
                    check_aknown();
                }
                $("#aknown").parent().show();
                $("#print_div").show();
                if ($("#tickerTypeTab").prop("checked")) {
                    var imgSrc = $("#skinPreviewImg").attr("src");
                    if (imgSrc) {
                        imgSrc = imgSrc.replace("thumbnail.png", "thumbnail_tick.png");
                        $("#skinPreviewImg").attr("src", imgSrc);
                    }
                    $("#tableAlertAppear").show();
                    $("#desktopCheckLabel").show();
                    $("#urgent_div").show();
                    $("#aknown_div").show();
                    $("#unobtrusive_div").show();
                    $("#self_deletable_div").show();
                    $("#aut_close_div").show();
                    $("#tickerInfo").fadeIn(500);
                    $("#buttons_to_display").hide();
                    $("#tickerSpeedSliderWrapper").fadeIn(500);
                    $("#digsign_info").hide();
                    $("#rsvp_div").hide();
                    $("#non_rsvp").show();
                    $("#position_div").parent().hide();
                    $("#linkedin_div").hide();
                    $("#ticker_position_div").parent().show();
                    $("#print_div").hide();
                    $("#alertDeviceHeader").hide();
                    $("#alertDeviceTable").hide();
                    //$("#templatesList").hide();
                }
                else {
                    if ($('#mobileDevice').is(':checked')) {
                        hideAppearance();
                    };

                    $("#tickerInfo").fadeOut(500);
                    $("#tickerSpeedSliderWrapper").fadeOut(500);
                    $("#digsign_info").fadeIn(500);
                    var imgSrc = $("#skinPreviewImg").attr("src");
                    if (imgSrc) {
                        imgSrc = imgSrc.replace("thumbnail_tick.png", "thumbnail.png");
                    }
                    $("#skinPreviewImg").attr("src", imgSrc);
                    if (webplugin != 1)
                        $("#position_div").parent().show();
                    $("#ticker_position_div").parent().hide();
                    $("#non_rsvp").show();
                    $("#linkedin_div").fadeOut(500);
                    $("#rsvp_div").fadeOut(500);
                }
            }


        }

        function showAlertPreview(type) {
            window.parent.parent.scrollTo(0);
            $('html, body').animate({ scrollTop: 0 }, 10, 'swing', showPreview);
            function showPreview() {
                if (!type) type = "desktop";
                //alert();
                var data = new Object();

                data.create_date = "<%=DateTime.Now.ToString(("dd/MM/yyyy HH:mm:ss"))%>";

                //var fullscreen = ($("#size2").prop("checked") == "checked") ? "1" : $("input[name='position']:checked").attr("value");
                var ticker = ($("#tickerTypeTab").prop("checked")) ? 1 : 0;
                var fullscreen = "9";
                if (ticker == 0) {
                    if ($("#size2").prop("checked")) {
                        fullscreen = "1"
                    } else {
                        fullscreen = $("input[name='position']:checked").attr("value");
                    }
                } else {
                    fullscreen = $("input[name='ticker_position']:checked").attr("value");
                }

                var acknowledgement = ($("#aknown").prop("checked")) ? 1 : 0;
                var self_deletable = ($("#selfDeletable").prop("checked")) ? 1 : 0;
                var alert_width = $("#alertWidth").val();
                var alert_height = $("#alertHeight").val();
                var print_alert = ($("#printAlertCheckBox").prop("checked")) ? 1 : 0;
                var alert_html = tinyMCE.get('alertContent').getContent();

                if (ticker == 1) {
                    <% if (Config.Data.HasModule(Modules.TICKER_PRO))
        {%>
                    if (alert_html.indexOf("<-- ticker_pro -->") == -1) {
                        alert_html += "<!-- ticker_pro -->";
                    }

                    alert_html += "<!-- tickerSpeed=" + $("#tickerSpeedValue").val() + " -->";

                    const tickerHeight = 30;
                    //Replace all img tag's attribure "height" to ticker height
                    alert_html = alert_html.replace(/(.*<img.*height=").*(".*\/>)/, "$1" + tickerHeight + "$2");
                    <%}%>
                }
            <% if (Settings.Content["ConfEnableHtmlAlertTitle"].Equals("1"))
        { %>
                var html_title_ed = tinyMCE.get("htmlTitle");
                var html_title = html_title_ed.getContent();
                if (html_title)
                    alert_html += "<!-- html_title = '" + html_title.replace(/&/g, '&amp;').replace(/"/g, '&quot;').replace(/'/g, '&#39;').replace(/</g, '&lt;').replace(/>/g, '&gt;') + "' -->";

                //var alert_title = $('<div/>').html(html_title).text();
                var alert_title = html_title;
            <% }
        else
        {%>
                var alert_title = $("#simpleTitle").val();
            <% }%>

                var top_template;
                var bottom_template;
                var templateId = $("#template_select").val();

                if (print_alert == 1) {
                    if (alert_html.indexOf("<!-- printable_alert -->") == -1)
                        alert_html += "<!-- printable_alert -->";
                }
                else {
                    if (alert_html.match(/<!-- printable_alert -->/img))
                        alert_html = alert_html.replace(/<!-- printable_alert -->/img, "");
                }

                data.top_template = top_template;
                data.bottom_template = bottom_template;
                data.alert_width = alert_width;
                data.alert_height = alert_height;
                data.ticker = ticker;
                data.fullscreen = fullscreen;
                data.acknowledgement = acknowledgement;
                data.self_deletable = self_deletable;
                data.caption_href = "";
                var skinId = $("#skin_id").val();
                skinId = (skinId !== "default" && skinId !== "") ? "{" + skinId + "}" : "default";
                data.skin_id = skinId;
                data.type = type;
                data.need_question = type !== 'sms' && $("#rsvpTypeTab").prop('checked');
                data.question = $("#question").html();
                data.question_option1 = $("#question_option1").html();
                data.question_option2 = $("#question_option2").html();
                data.question_options_count = getAnswerInputCount();
                data.need_second_question = $("#need_second_question").prop('checked');
                data.second_question = $("#second_question").html();
                data.alert_title = alert_title;
                if (type === 'sms') {
                    getTextFromHtml(alert_html, showAlert);
                } else {
                    var response = {
                        d: alert_html
                    };
                    showAlert(response);
                }

                function getAnswerInputCount() {
                    var result = 0;

                    if (data.question_option1.length > 0)
                        result++;

                    if (data.question_option2.length > 0)
                        result++;

                    return result;
                }

                function showAlert(alertText) {
                    data.alert_html = alertText.d;
                    initAlertPreview(data);
                    return false;
                }
            }
        }

        function getTextFromHtml(htmlText, cbFunc) {
            var data = JSON.stringify({
                htmlText: htmlText
            });
            $.ajax({
                type: "POST",
                url: "CreateAlert.aspx/HtmlToText",
                data: data,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: cbFunc
            });
        }

        function check_value() {
            if ($("#alertWidth").length > 0) {
                var al_width = document.getElementById('alertWidth').value;
                var al_height = document.getElementById('alertHeight').value;
                var size_message = document.getElementById('size_message');

                if (al_width > 1024 && al_height > 600) {
                    size_message.innerHTML =
                        '<font color="red"><%=Resources.resources.LNG_RESOLUTION_DESC1 %></font>';
                    return;
                }

                if (al_width < 300 || al_height < 200) {
                    size_message.innerHTML = "<font color='red'><%=Resources.resources.LNG_RESOLUTION_DESC2 %>. <A href='#' onclick='set_default_size();'><%=Resources.resources.LNG_CLICK_HERE %></a> <%=Resources.resources.LNG_RESOLUTION_DESC3 %>.</font>";
                    return;
                }
                size_message.innerHTML = "";
            }
        }
        function checkTitle() {
            var body = tinymce.get("htmlTitle").getBody();
            var content = tinymce.trim(body.innerText || body.textContent);

            if (content.length < 3 || content.length >= 255) {
                alert("<% =Resources.resources.LNG_TITLE_SHOULD_BE_LESS_THEN %>");
                return false;
            } else {
                return true;
            }

        }
        function check_tab(type) {

            $(function () {
                document.getElementById('alertTypeTab').checked = false;
                //  if (type != "ticker") {
                //    }
                if (document.getElementById('tickerTypeTab'))
                    document.getElementById('tickerTypeTab').checked = false;

                if (document.getElementById('rsvpTypeTab'))
                    document.getElementById('rsvpTypeTab').checked = false;

                if (document.getElementById('lnkdnTypeTab'))
                    document.getElementById('lnkdnTypeTab').checked = false;
                if (type == 'alert') {
                    // $("#webplugin").val(0);
                    document.getElementById('alertTypeTab').checked = true;
                    $("#ticker").val("0");
                    $("#linkedin").val("0");
                    $("#rsvp").val("0");
                    $("#popup").val("1");
                    $("#chanelsTab").show();
                } else if (type == 'ticker') {
                    //	 $("#webplugin").val(0);
                    document.getElementById('tickerTypeTab').checked = true;
                    $("#blogPost").parent().hide();
                    $("#ticker").val("1");
                    $("#linkedin").val("0");
                    $("#rsvp").val("0");
                    $("#popup").val("0");
                    $("#chanelsTab").show();
                } else if (type == 'rsvp') {
                    // $("#webplugin").val(0);
                    document.getElementById('rsvpTypeTab').checked = true;
                    $("#rsvp").val("1");
                    $("#popup").val("0");
                    $("#ticker").val("0");
                    $("#linkedin").val("0");
                    $("#chanelsTab").hide();

                } else if (type == 'linkedin') {
                    //	 $("#webplugin").val(0);
                    document.getElementById('lnkdnTypeTab').checked = true;
                    $("#linkedin").val("1");
                    $("#ticker").val("0");
                    $("#rsvp").val("0");
                    $("#chanelsTab").hide();
                } else if (type == 'webplugin') {
                    //	 $("#webplugin").val(1);
                    document.getElementById('alert_type_webplugin').checked = true;
                    $("#chanelsTab").hide();
                }
                check_alert_type();
                if ('<%=IsInstantVal %>' == '1') {
                    $("#rsvpTypeTab").hide();
                    im_interface();
                }

                check_size();
                $("#alertWidth").attr('disabled', type == "ticker");
                $("#alertHeight").attr('disabled', type == "ticker");

            });

        }



        function showPromptRedir(text) {
            $("#dialog-modal > p").text(text);
            $("#dialog-modal").dialog({
                height: 180,
                modal: true,
                resizable: false,
                buttons: {
                    "<%=Resources.resources.LNG_CLOSE %>": function () {
                        $(this).dialog("close");
                        location.href = "index.asp";
                    }
                }
            });
        }

        $(document).ready(function () {
            /*     if(Langs == "True"){
                     $("#alertLangs").show();
                     $("#mainView").hide();}
                 else{
                     $("#alertLangs").hide(); } 
                     
             */
            //Initialize ticker speed slider


            $(".translate_button").click(function (e) {
                e.preventDefault();
                var code = $(this).attr("lang");
                var text = tinyMCE.get('alertContent').getContent();
                var title = tinyMCE.get('htmlTitle').getContent();

                $(".loader_" + code).show();

                $.ajax({

                    type: "POST",
                    url: "CreateAlert.aspx/TranslateText",
                    data: '{code: "' + code + '", text: "' + text.replace(/"/g, '\\"') + '", title: \"' + title.replace(/"/g, '\\"') + ' \"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {

                        tinyMCE.get('htmlTitle' + code).setContent(response.d.Title.replace(/\\"/g, '"'));
                        $('#htmlTitle' + code + 'Hidden').val(response.d.Title.replace(/\\"/g, '"'));
                        tinyMCE.get('alertContent_' + code).setContent(response.d.Content.replace(/\\"/g, '"'));
                        $(".loader_" + code).hide();

                    },
                    error: function (e) {
                        $(".loader_" + code).hide();
                        alert("Failed to connect to Google Translate please try again");

                    }
                });

            });


            $("#tickerSpeedSlider").slider({
                value: <%=tickerSpeed%>,
                min: 1,
                max: 7,
                step: 1,
                slide: function (event, ui) {
                    $("#tickerSpeedValue").val(ui.value);
                }
            });


            if (confShowTypesDialog === 1 && isInstant !== '1' &&
                (window.location.href.indexOf("?id=") < 0 || window.location.href.indexOf("&id=") < 0)
                && <%=tickerValue%> == 0) {
                $("#alert_type_dialog")
                    .dialog({
                        position: { my: "left top", at: "left top", of: "#dialog_dock" },
                        modal: true,
                        resizable: false,
                        draggable: false,
                        width: 'auto',
                        resize: 'auto',
                        margin: 'auto',
                        autoOpen: true,
                        close: function () { closeTypesDialog() }

                    });
            }



            //content editor initialization
            tinyMCE.baseURL = window.location.href.substring(0, window.location.href.lastIndexOf("/")) + "/jscripts/tiny_mce";
            tinymce.init({
                entity_encoding: 'raw',
                setup: function (ed) {
                    ed.on('init', function (ed) {
                        ed.target.editorCommands.execCommand("fontName", false, "Dubai W23 Regular");
                    })
                },
                selector: '.alertContent',
                plugins: "moxiemanager textcolor link code emoticons insertdatetime fullscreen imagetools directionality table image autolink lists advlist",
                toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | fontselect fontsizeselect | forecolor backcolor | fullscreen ",
                toolbar2: "cut copy paste | bullist numlist | outdent indent | undo redo | link unlink | image insertfile code | rotateleft rotateright flipv fliph | ltr rtl",
                moxiemanager_image_settings: {
                    view: 'thumbs',
                    extensions: 'jpg,png,gif'
                },
                moxiemanager_media_settings: {
                    view: 'thumbs',
                    extensions: '<%if (digitalSignage.Equals("1")) { Response.Write("mp4,ogg,webm,wmv,mp3,wav,pcm,aiff,aac,wma"); } else { Response.Write("mp3,wav,pcm,aiff,aac,wma"); }%>'
                },
                default_link_target: "_blank",
                link_assume_external_targets: true,
                relative_urls: false,
                remove_script_host: false,
                fontsize_formats: '8pt 10pt 11pt 12pt 14pt 16pt 18pt 24pt 36pt',
                height: 300,
                document_base_url: "<%=Config.Data.GetString("alertsDir")%>/admin/images/upload",
                content_style: "body{color:#000 !important; font-family:Arial, Times New Roman, Helvetica, sans-serif !important; font-size:14px !important}",
                content_css : '/admin/jscripts/tiny_mce/themes/advanced/fonts/tinymce-fonts.css',
                font_formats: 'Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Calibri=calibri;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Dubai W23 Regular=Dubai W23 Regular;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Tahoma=tahoma,arial,helvetica,sans-serif;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats',
                directionality: "ltr",
                branding: false,
                advlist_bullet_styles: 'circle,disc,square',
                advlist_number_styles: 'lower-alpha,lower-roman,upper-alpha,upper-roman'
            });
            //html title editor initialization
            tinymce.init({
                entity_encoding: 'raw',
                selector: '.htmlTitle',
                menubar: false,
                statusbar: false,
                resize: false,
                setup: function (ed) {
                    ed.on('change', function (ed) {
                        $("#" + ed.id + "Hidden").val(ed.getContent());
                    });
                    ed.on('init', function(ed) {
                        ed.target.editorCommands.execCommand("fontName", false, "Dubai W23 Regular");
                    });
                },
                plugins: "autoresize textcolor autolink",
                toolbar: "italic underline strikethrough | fontselect fontsizeselect | forecolor",
                autoresize_bottom_margin: 0,
                autoresize_min_height: 20,
                autoresize_max_height: 80,
                height: 300,
                fontsize_formats: '8pt 10pt 11pt 12pt 14pt 16pt 18pt',
                forced_root_block: false,
                valid_elements: "span[*],font[*],i[*],b[*],u[*],strong[*],s[*],em[*]",
                content_style: "body{font:bold 21px Arial !important;color:#000000 !important; font-weight:bold !important;}",
                content_css : '/admin/jscripts/tiny_mce/themes/advanced/fonts/tinymce-fonts.css',
                font_formats: 'Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Calibri=calibri;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Dubai W23 Regular=Dubai W23 Regular;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Tahoma=tahoma,arial,helvetica,sans-serif;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats',
                branding: false
            });

            initializeInlineTinyMce('#question');
            initializeInlineTinyMce('#question_option2');
            initializeInlineTinyMce('#question_option1');
            initializeInlineTinyMce('#second_question');

            $(document).tooltip({
                items: "img[title],a[title]",
                position: {
                    my: "center bottom-20",
                    at: "center top",
                    using: function (position, feedback) {
                        $(this).css(position);
                        $("<div>")
                            .addClass("arrow")
                            .addClass(feedback.vertical)
                            .addClass(feedback.horizontal)
                            .appendTo(this);
                    }
                }
            });

            shortcut.add("Alt+s", function (e) {
                e.preventDefault();
                __doPostBack('saveButton', '');
            });

            //shortcut.add("right",function(e) {
            //    e.preventDefault();
            //    __doPostBack('saveNextButton', '');
            //});

            shortcut.add("Alt+P", function (e) {
                e.preventDefault();
                showAlertPreview();
            });

            shortcut.add("Alt+1", function (e) {
                e.preventDefault();
                $('#alert_type_tabs').tabs("option", "active", 0);
                check_tab('alert');
                check_alert_type();
            });

            shortcut.add("Alt+2", function (e) {
                e.preventDefault();
                $('#alert_type_tabs').tabs("option", "active", 1);
                check_tab('ticker');
                check_alert_type();
            });

            shortcut.add("Alt+3", function (e) {
                e.preventDefault();
                $('#alert_type_tabs').tabs("option", "active", 2);
                check_tab('rsvp');
                check_alert_type();
            });

            shortcut.add("Alt+4", function (e) {
                e.preventDefault();
                $('#alert_type_tabs').tabs("option", "active", 3);
                check_tab('linkedin');
                check_alert_type();
            });

            if (skins.length > 0) {
                $(".tile").hover(function () {
                    $(this).switchClass("", "tile_hovered", 200);
                }, function () {
                    $(this).switchClass("tile_hovered", "", 200);
                }).click(function () {

                    showSkinsTilesDialog();
                });
            }

            if ('<% =Request["id"]%>'.length == 0) {
                if (<%= Settings.Content["ConfShowSkinsDialog"]  %> === 1) {
                    showSkinsTilesDialog();
                }
            }
        });


        function select_all_objects() {

            var status = document.getElementById('select_all').checked;
            var field = document.getElementsByClassName("alert_lang");
            console.log(field);
            for (i = 0; i < field.length; i++) {
                field[i].checked = status;
            }

        }


        $(document).ready(function () {
            $(function () {
                //  $(".translate_btn").button();
                $(".cancel_button").button();
                $(".save_button").button();
                $(".preview_button").button();
                $("#yammerShare").button();
                $(".save_and_next_button").button({
                    icons: {
                        secondary: "ui-icon-carat-1-e"
                    }
                });

                $(".preview_sms_button").button().click(function () {
                    showAlertPreview("sms");
                });

                isDigSign = parseInt('<% =Request["webplugin"] %>');

                if (isDigSign != 1) {
                    $("#accordion").accordion({
                        active: 0,
                        heightStyle: "content",
                        collapsible: true
                    });
                }

                var ticker = <% =this.tickerValue %>;
                var rsvp = '<% =this.rsvpValue %>';
                $('#alert_type_tabs').tabs({ active: $("#alert_type_webplugin").is(':checked') ? 4 : $("#lnkdnTypeTab").is(':checked') ? 3 : $("#rsvpTypeTab").is(':checked') ? 2 : $("#alertTypeTab").is(':checked') && ticker != 1 ? 1 : 0 }).css("border", "none");



                if ('<% =Request["id"] %>'.length == 0) {
                    var currentType = '<% =Settings.Content["ConfDesktopAlertType"]%>';

                    switch (currentType) {

                        case 'R':
                            {
                                $('#alert_type_tabs').tabs("option", "active", 2);
                                check_tab('rsvp');
                                check_alert_type();
                                break;
                            }

                        case 'P':
                            {
                                $('#alert_type_tabs').tabs("option", "active", 0);
                                check_tab('alert');
                                check_alert_type();
                                break;
                            }

                        case 'T':
                            {
                                $('#alert_type_tabs').tabs("option", "active", 1);
                                check_tab('ticker');
                                check_alert_type();
                                break;
                            }
                    }
                }

                if (ticker == 1) {
                    $('#alert_type_tabs').tabs({ active: 1 });

                    check_tab('ticker');
                }

                if (rsvp == '1') {
                    $('#alert_type_tabs').tabs({ active: 2 });

                    check_tab('rsvp');
                }
                window.parent.parent.onChangeBodyHeight($(window.parent.parent.document).height(), false);
            });

            $("#tickerTypeTab").click(check_alert_type);
            $("#alertTypeTab").click(check_alert_type);
            $("#rsvpTypeTab").click(check_alert_type);
            $("#lnkdnTypeTab").click(check_alert_type);
            $("#alert_type_webplugin").click(check_alert_type);


            /*
             * Template dropdownlist handler   
             */
            $("#<%=templatesList.ClientID%>").change(function () {
                var selectedValue = $("#<%=templatesList.ClientID%>").val();


                if (selectedValue == 0) //template does not select
                {
                    tinyMCE.get('alertContent').setContent("");
                    tinyMCE.get('htmlTitle').setContent("");
                    $("#title").val("");
                    $("#titleHidden").val("");
                    return;
                }

                //ajax request to get template body
                $.ajax({
                    type: "POST",
                    url: "CreateAlert.aspx/GetTemplate",
                    data: '{templateId: ' + selectedValue + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {

                        var body = response.d;
                        var title = $( "#<%=templatesList.ClientID%> option:selected").text();

                        tinyMCE.get('alertContent').setContent(body); //add body to tinyMCE
                        tinyMCE.get('htmlTitle').setContent(title); //add html title to tinyMCE
                        $("#title").val(title); //add sinple title to tinyMCE
                        $("#titleHidden").val(title);

                    },
                    failure: function (response) {
                        tinyMCE.get('alertContent').setContent("");
                        tinyMCE.get('htmlTitle').setContent("");
                        $("#title").val("");
                        $("#titleHidden").val(title);
                    }
                });

            }
            );

            check_lifetime();
            //   check_settings();
            check_alert_type();

            //  var isChecked = $("#recurrenceCheckbox").prop('checked');

            //  $(".reccurenceControl").prop('disabled', !isChecked);
            //  $(".reccurenceControl input").prop('disabled', !isChecked);  //function(i, v) { return !v; }


        });



        function check_recurrence() {

            var isChecked = $("#recurrenceCheckbox").prop('checked');
            $("#recurrenceCheckboxValue").val(isChecked);

            if (isChecked) {
                $("#lifeTimeCheckBox").prop('checked', false);
            }

            $(".reccurenceControl input").prop('disabled', !isChecked);  //function(i, v) { return !v; }
            $(".reccurenceControl").prop('disabled', !isChecked);
            var displayValue = $(".count_down_delete_img").css('display') == "none" ? "block" : "none";


            $(".count_down_delete_img").prop('display', displayValue);
        }

        function createSkinsTilesHtml() {
            var thumbnail;
            if ($("#tickerTypeTab").prop("checked")) {
                thumbnail = "thumbnail_tick.png";
            }
            else {
                thumbnail = "thumbnail.png";
            }
            var list = $('<ul style="list-style:none; margin:0px; padding:0px"></ul>');

            for (let i = 0; i < skins.length; i++) {

                let skinsId = "";
                if (skins[i].id === "default") {
                    skinsId = skins[i].id;
                } else {
                    skinsId = "{" + skins[i].id + "}";
                }

                let li = '<li id="' + skins[i].id + '" class="tile" style="float:left; margin:10px; padding:10px">' +
                    '<div style="padding-bottom:5px"><span style="margin:0px" class="header_title">' + skins[i].name + '</span></div>' +
                    '<div>' +
                    '<img src="skins/' + skinsId + '/' + thumbnail + '"/>' +
                    '</div>' +
                    '</li>';
                $(list).append(li);
            }

            $(list).find(".tile").hover(function () {
                $(this).switchClass("", "tile_hovered", 200);
            }, function () {
                $(this).switchClass("tile_hovered", "", 200);
            }).click(function () {
                $("#skins_tiles_dialog").dialog("close");
                skinId = $(this).attr("id");
                $("#skin_id").val(skinId);

                if (skinId !== "default") {
                    skinId = "{" + skinId + "}";
                }

                var imgSrc = "skins/" + skinId + "/" + thumbnail;
                $("#skinPreviewImg").attr("src", imgSrc);
            });
            return list;
        }
    </script>
</head>
<body>

    <form id="form1" runat="server">

        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <div id="alertLangs" runat="server" visible="false">
            <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">

                <table id="dialog_dock" width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
                    <tr>
                        <td width="100%" height="31" class="main_table_title">

                            <img id="Img1" runat="server" alt="" border="0" height="20" src="images/menu_icons/alert_20.png" style="padding-left: 7px" width="20" />

                            <a runat="server" id="A1" href="#" class="header_title" style="position: absolute; top: 22px">
                                <div>
                                    <%=Resources.resources.LNG_STEP %><strong><% =(Config.Data.IsOptionEnabled("ConfEnableAlertLangs")  ? "2" : "1" ) %></strong>
                                    <%=Resources.resources.LNG_OF %>
                                    <% =(Config.Data.IsOptionEnabled("ConfEnableAlertLangs")  ? "4" : "3" ) %> : <strong>
                                        <%=Resources.resources.LNG_CHOOSE_LANGUAGES %>:</strong><br />
                                </div>
                            </a>
                        </td>
                    </tr>



                </table>


                <br />
                <br />
                <table style="padding-left: 0px; margin-left: 10px; margin-right: 10px; border: 1px solid black;" width="10%" height="100%" cssclass="data_table" cellspacing="0" cellpadding="3" id="languagesTable">

                    <tr class="table_title">
                        <td id="headerSelectAll" cssclass="table_title">
                            <input type="checkbox" name="select_all" id="select_all" onclick="select_all_objects();"></td>
                        <td style="border-left: 1px solid black;" cssclass="table_title" width="2%" id="languagesTableHeader"><%=Resources.resources.LNG_LANGUAGE %></td>
                    </tr>
                    <form>
                        <% foreach (string key in languages.Keys)
                            {%>

                        <tr style="text-align: center;">
                            <td style="border-top: 1px solid black;" cssclass="table_title" id="languagesSelectionTable"><%if (languages[key])
                                {%>
                                <input disabled='disabled' checked='checked' type="checkbox" value="<%Response.Write(languagesLocale[key]);%>">
                                <%}
                                    else
                                    {%>
                                <input id="alert_lang" class="alert_lang" type="checkbox" value="<%Response.Write(languagesLocale[key]);%>">
                                <%} %>

                            </td>
                            <td style="border-left: 1px solid black; border-top: 1px solid black;" width="2%" id="languagesNameTable"><%Response.Write(key);%></td>

                        </tr>
                        <%}%>
                    </form>
                </table>
                <br />
                <tr>
                    <td>
                        <a class="save_and_next_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-secondary" id="main_buttons_next_bottom" role="button" aria-disabled="false"><span class="ui-button-text"><%= Resources.resources.LNG_NEXT %></span><span class="ui-button-icon-secondary ui-icon ui-icon-carat-1-e"></span></a>
                    </td>
                </tr>
            </table>
        </div>

        <div runat="server" id="mainView">
            <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
                <tr>
                    <td>
                        <table id="dialog_dock" width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
                            <tr>
                                <td width="100%" height="31" class="main_table_title">

                                    <img id="contentImage" runat="server" alt="" border="0" height="20" src="images/menu_icons/alert_20.png" style="padding-left: 7px" width="20" />

                                    <a runat="server" id="headerTitle" href="#" class="header_title" style="position: absolute; top: 22px"></a>
                                </td>
                            </tr>
                            <tr>
                                <td height="100%" vclass="main_table_body">
                                    <div id="alert_type_tabs" style="padding: 0px;">
                                        <ul>
                                            <li runat="server" id="alertTypeTab"><a href="#alertTypeTab" onclick="check_tab('alert');">
                                                <%=Resources.resources.LNG_ADD_POPUP_ALERT %></a>

                                            </li>

                                            <li runat="server" id="tickerTypeTab"><a href="#alertTypeTab" onclick="check_tab('ticker');">
                                                <%=Resources.resources.LNG_ADD_TICKER %></a>

                                            </li>

                                            <li runat="server" id="rsvpTypeTab"><a href="#alertTypeTab" onclick="check_tab('rsvp');document.getElementById('printAlertCheckBox').checked=false;">
                                                <%=Resources.resources.LNG_ADD_RSVP %></a>

                                            </li>


                                            <li runat="server" id="lnkdnTypeTab"><a href="#alertTypeTab" onclick="check_tab('linkedin');">
                                                <%=Resources.resources.LNG_ADD_LINKEDIN %></a>

                                            </li>
                                        </ul>
                                    </div>
                                    <div id="alertTypeTab" style="padding: 0.5em 0.5em;">
                                        <span class="work_header"></span>
                                        <input type="hidden" runat="server" id="IsInstant" name="instant" value="" />
                                        <input type="hidden" runat="server" id="webAlert" name="webalert" value="" />
                                        <input type="hidden" runat="server" name="alert_campaign" value="" id="alertСampaign" />
                                        <input type="hidden" runat="server" id="colorСode" name="color_code" value="" />
                                        <input type='hidden' runat="server" id="edit" name='edit' value='' />
                                        <input type='hidden' runat="server" id="alertId" name='id' value='' />
                                        <input type='hidden' runat="server" name='rejectedEdit' id="rejectedEdit" value='0' />
                                        <input type='hidden' runat="server" name='shouldApprove' id="shouldApprove" value='' />

                                        <div style="width: 955px; margin: auto;">
                                            <div runat="server" id="campaignLabel">
                                                <b><%=Resources.resources.LNG_CAMPAIGN %>: </b>
                                                <label><b runat="server" id="campaignName"></b></label>
                                                <br>
                                                <br />
                                            </div>
                                            <input runat="server" id='campaignSelect' type='hidden' name='campaign_select' value='' />
                                            <br />
                                            <div>
                                                <%=Resources.resources.LNG_STEP %>
                                                <span id="currentAlertCreationStepNumberSpan" runat="server"></span>
                                                <%=Resources.resources.LNG_OF %>
                                                <span id="totalAlertCreationStepNumberSpan" runat="server"></span>: 
                                                <strong>
                                                    <% =Resources.resources.LNG_ENTER_TEXT_OF_YOUR_ALERT %>
                                                    <img src="images/help.png" title="<%=Resources.resources.LNG_FINAL_VIEW_TAP_PREVIEW %>">:
                                                </strong>
                                            </div>
                                            <br />
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <asp:Literal ID="defaultLanguage" runat="server"></asp:Literal></td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: bottom; width: 1px; overflow: visible" rowspan="2">
                                                        <%=Resources.resources.LNG_TITLE %>:&nbsp;&nbsp;
                                                    </td>
                                                    <td rowspan="2" style="vertical-align: bottom">
                                                        <div>

                                                            <div runat="server" id="htmlTitleDiv">
                                                                <div class="html_title_div">
                                                                    <textarea runat="server" id="htmlTitle" name="htmlTitle" rows="1" class="htmlTitle"></textarea>
                                                                    <input type="hidden" runat="server" id="titleHidden" name="alert_title" value=""
                                                                        maxlength="255" style="width: 304px" />
                                                                </div>
                                                            </div>

                                                            <div runat="server" id="simpleTitleDiv">
                                                                <input type="text" runat="server" id="simpleTitle" name="alert_title" value=""
                                                                    maxlength="255" style="width: 300px" />
                                                                <input type="hidden" runat="server" id="htmlTitleHidden" name="html_title" value="" />
                                                            </div>

                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div align="right" style="margin-bottom: 5px;">
                                                            <script language="javascript">
                                                                check_aknown();
                                                                check_email();
                                                                check_desktop();
                                                                check_size();
                                                            </script>
                                                            <div>
                                                                <a runat="server" id="previewButton" class="preview_button" onclick="showAlertPreview()" title="Hotkey: Alt+P">
                                                                    <%=Resources.resources.LNG_PREVIEW %></a>
                                                                <asp:LinkButton runat="server" CssClass="save_button" OnClientClick="return checkTitle(); " ID="saveButton" title="Hotkey: Alt+S" />
                                                                <asp:LinkButton runat="server" CssClass="save_and_next_button" OnClientClick="return checkTitle(); " ID="saveNextButton" />
                                                                <br />
                                                                <div align="right">
                                                                    <br />
                                                                    <asp:DropDownList runat="server" ID="templatesList" Width="200px">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />
                                            <table style="width: 100%">
                                                <tr>
                                                    <td style="width: 100%">
                                                        <textarea runat="server" id="alertContent" name="alertContent" rows="15" cols="80" style="width: 100%" class="alertContent"></textarea>
                                                    </td>
                                                    <td>
                                                        <div runat="server" id="changeColorCode" onclick="showColorCodeDialog()" style="background: white; color: black; border: 1px solid #999; border-radius: 4px; cursor: pointer; cursor: hand; width: 200px; margin: 0px 10px 10px 10px; padding: 8px 10px; text-align: center; vertical-align: middle">
                                                            <%=Resources.resources.LNG_CLICK_HERE_TO_CHANGE_COLOR_CODE %>
                                                        </div>
                                                        <div runat="server" class="tile" id="default" style="margin: 10px; padding: 10px">
                                                            <div class="hints" style="font-size: 12px; text-align: center; padding-bottom: 2px">
                                                                <%= Resources.resources.LNG_CLICK_TO_CHANGE_SKIN %>
                                                            </div>
                                                            <div style="font-size: 1px">
                                                                <img runat="server" id="skinPreviewImg" src="skins/default/thumbnail.png" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div runat="server" id="multilangsControls">
                                                        </div>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                            <script type="text/javascript">


                                                var shouldBeReadOnly = parseInt("<% =shouldBeReadOnly %>", 10) == 1;
                                                var readOnly = { readonly: shouldBeReadOnly };

                                             /*initTinyMCE("EN", "exact", "alertContent", function(ed){
                                                 
					
                                             }, readOnly, newTinySettings);   */

                                             <% if (Settings.Content["ConfEnableHtmlAlertTitle"].Equals("1"))
                                                {%>

                                                var hiddenTitle = tinyMCE.get("htmlTitle").getContent();
                                                $("#titleHidden").val(hiddenTitle);
                                             <%} %>


</script>
                                            <div id="bottom_template_div">
                                            </div>
                                            <div runat="server" id="digsignInfo" class="ticker_info" style="">
                                                <br />
                                                <%= Resources.resources.LNG_DIGSIGN_WARNING %>
                                            </div>
                                            <div id="tickerInfo" class="ticker_info" style="display: none; margin-top: 10px; margin-bottom: 10px;">
                                                <%=Resources.resources.LNG_TICKER_INFO %>
                                            </div>
                                            <div runat="server" id="digSignNoLinksWarning" class="ticker_info" style="display: none">
                                                <br />
                                                <%=Resources.resources.LNG_NO_LINKS_ADM %>
                                            </div>

                                            <div id="rsvp_div" style="display: none">
                                                <div style="padding: 10px"><%=Resources.resources.LNG_INVITATION_INSTRUCTIONS %></div>
                                                <table border="0">
                                                    <tr>
                                                        <td colspan="2" style="padding-left: 24px">
                                                            <b>
                                                                <%=Resources.resources.LNG_QUESTION_WORD %>
                                                                    #1</b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <%=Resources.resources.LNG_QUESTION %>:
                                                        </td>
                                                        <td>
                                                            <div id="question" contenteditable="true" runat="server" name="question" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <%=Resources.resources.LNG_OPTION %>:
                                                        </td>
                                                        <td>
                                                            <div id="question_option1" contenteditable="true" runat="server" name="question_option1" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <%=Resources.resources.LNG_OPTION %>:
                                                        </td>
                                                        <td>
                                                            <div id="question_option2" contenteditable="true" runat="server" name="question_option2" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <input runat="server" runat="server" id="need_second_question" name="need_second_question" type="checkbox" value="1"
                                                                onclick="need_second_question_click()" />
                                                            <label for="need_second_question">
                                                                <b>
                                                                    <%=Resources.resources.LNG_QUESTION_WORD %>
                                                                        #2</b></label>
                                                        </td>
                                                    </tr>
                                                    <tr class="second_question">
                                                        <td>
                                                            <%=Resources.resources.LNG_QUESTION %>:
                                                        </td>
                                                        <td>
                                                            <div id="second_question" contenteditable="true" runat="server" name="second_question" />
                                                        </td>
                                                    </tr>
                                                    <tr class="second_question">
                                                        <td>
                                                            <%=Resources.resources.LNG_QUESTION_TYPE %>:
                                                        </td>
                                                        <td>
                                                            <%=Resources.resources.LNG_INPUT_ANSWER %>
                                                            <i>(<%=Resources.resources.LNG_INPUT_ANSWER_DESC %>)</i>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <br />
                                            </div>

                                            <div id="linkedin_div" style="display: none">
                                                <div id="linkedin_settings_hint" style="padding-bottom: 5px; color: #ff0000;">
                                                    Manage LinkedIn settings to enable the feature
                                                </div>
                                                <div id="linkedInDiv" style="padding-bottom: 5px">

                                                    <script type="IN/Login">Currently logged as <strong><?js= firstName ?> <?js= lastName ?></strong></script>

                                                    <br />
                                                    <br />
                                                    <a id="shareAPI" style="display: none" onclick="my_sub(4);" href="#">
                                                        <%= Resources.resources.LNG_SHARE_ON_LINKEDIN %></a>
                                                </div>
                                                <br />
                                            </div>
                                            <div id="elements_to_display" style="display: none">
                                                <table border="0" style="width: 100%">
                                                    <tr>
                                                        <td>
                                                            <div id="accordion" style="width: 100%">
                                                                <h3 runat="server" id="alertSettingsHeader"><%= Resources.resources.LNG_ALERT_SETTING_ACC %></h3>
                                                                <table runat="server" id="alertSettingsTable" border="0" style="width: 100%">
                                                                    <tr>
                                                                        <td>
                                                                            <div class="checkbox_div" id="urgent_div" runat="server">
                                                                                <input type="checkbox" runat="server" id="urgentCheckBox" name="urgent" value="1" onclick="check_aknown();" />
                                                                                <label for="urgentCheckBox">
                                                                                    <%=Resources.resources.LNG_URGENT_ALERT %></label>
                                                                                <img src="images/help.png" title="<%=Resources.resources.LNG_URGENT_ALERT_DESC %>" />
                                                                                <br />
                                                                            </div>
                                                                            <div class="checkbox_div" id="aknown_div" runat="server">
                                                                                <input type="checkbox" runat="server" id="aknown" name="aknown" onclick="check_aknown();" value="1" />
                                                                                <label for="aknown">
                                                                                    <%=Resources.resources.LNG_ACKNOWLEDGEMENT %></label>

                                                                                <img src="images/help.png" title="<%=Resources.resources.LNG_ACKNOWLEDGEMENT_DESC %>." />
                                                                                <br />
                                                                            </div>
                                                                            <div class="checkbox_div" id="unobtrusive_div" runat="server" style="display: none;">
                                                                                <input type="checkbox" runat="server" id="unobtrusiveCheckbox" name="unobtrusive" onclick="check_aknown();" value="1" />

                                                                                <label for="unobtrusiveCheckbox">
                                                                                    <%=Resources.resources.LNG_UNOBTRUSIVE %></label>
                                                                                <img src="images/help.png" title="<%=Resources.resources.LNG_UNOBTRUSIVE_HINT %>." />
                                                                                <br />
                                                                            </div>
                                                                            <div class="checkbox_div" id="self_deletable_div" runat="server">
                                                                                <input type="checkbox" runat="server" id="selfDeletable" name="self_deletable" value="1" />

                                                                                <label for="selfDeletable">
                                                                                    <%=Resources.resources.LNG_SELF_DELETABLE %></label>
                                                                                <img src="images/help.png" title="<%=Resources.resources.LNG_SELF_DELETABLE_HINT %>." />
                                                                                <br />
                                                                            </div>
                                                                            <div class="checkbox_div" id="aut_close_div" runat="server">
                                                                                <input type="checkbox" runat="server" id="autoCloseCheckBox" name="aut_close" onclick="check_aknown();"
                                                                                    value="1" />
                                                                                <label for="autoCloseCheckBox">
                                                                                    <%=Resources.resources.LNG_AUTOCLOSE_IN %></label>
                                                                                <input type="number" size="3" runat="server" name="autoclose" id="autoClosebox" onkeypress="return check_size_input(this, event);"
                                                                                       value="0" disabled="True" />
                                                                                <%=Resources.resources.LNG_MINUTES %>
                                                                                <img src="images/help.png" title="<%=Resources.resources.LNG_AUTOCLOSE_TITLE %>." />
                                                                                <span id="man_close_div">
                                                                                    <input type="checkbox" runat="server" id="manualClose" name="man_close" value="1" />
                                                                                    <label for="manualClose">
                                                                                        <%=Resources.resources.LNG_ALLOW_MANUAL_CLOSE %></label>
                                                                                    <img src="images/help.png" title="<%=Resources.resources.LNG_ALLOW_MANUAL_CLOSE_DESC %>." />
                                                                                </span>
                                                                                <br />
                                                                            </div>
                                                                            <div class="checkbox_div" id="lifeTimeAlert">
                                                                                <input type="checkbox" id="lifeTimeCheckBox" runat="server" name="lifetimemode" value="1" onclick="check_lifetime();" checked="True" />
                                                                                <label for="lifeTimeCheckBox"><%=Resources.resources.LNG_LIFETIME_IN %></label>
                                                                                <input type="text" id="lifetime" runat="server" name="lifetimeval" style="width: 50px" value="1" onkeypress="return check_size_input(this, event);" />
                                                                                <select runat="server" id="lifetimeFactor" name="lifetime_factor">
                                                                                </select>
                                                                                &nbsp;
                                                                                <label id ="LifetimeAdditionalDescription" runat="server"></label>
                                                                                <img src="images/help.png" id="LifeTimeHelpImage" runat="server" />
                                                                                <br />
                                                                            </div>
                                                                            <div class="checkbox_div" id="print_div" runat="server">
                                                                                <input type="checkbox" id="printAlertCheckBox" runat="server" name="print_alert" value="1" />
                                                                                <label for="printAlertCheckBox">
                                                                                    <%=Resources.resources.LNG_PRINT_ALERT %></label>
                                                                                <img src="images/help.png" title="<%=Resources.resources.LNG_PRINT_HINT %>." /><br />
                                                                            </div>
                                                                            <div id="tickerSpeedSliderWrapper" style="margin-left: 5px; display: none;">
                                                                                <div>
                                                                                    <h3><%=Resources.resources.LNG_TICKER_SPEED_DESCRIPTION %></h3>
                                                                                </div>
                                                                                <div style="margin-bottom: 5px;">
                                                                                    <span style="margin-right: 100px;"><%=Resources.resources.LNG_TICKER_SPEED_SLOW %></span>
                                                                                    <span style="margin-right: 105px;"><%=Resources.resources.LNG_TICKER_SPEED_DEFAULT %></span>
                                                                                    <span><%=Resources.resources.LNG_TICKER_SPEED_FAST %></span>
                                                                                </div>
                                                                                <div id="tickerSpeedSlider" style="max-width: 300px; margin-bottom: 20px;"></div>
                                                                                <input type="hidden" id="tickerSpeedValue" name="tickerSpeedValue" value="4" runat="server" />
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <h3 runat="server" id="alertScheduleHeader"><%= Resources.resources.LNG_SCHEDULE_ALERT %></h3>
                                                                <table runat="server" border="0" style="width: 100%" id="schedulingMenu">
                                                                    <tr>
                                                                        <td>
                                                                            <div runat="server" class="checkbox_div" id="scheduleAlert">
                                                                                <input type="checkbox" runat="server" id="recurrenceCheckbox" name="recurrence" value="1" onclick="check_recurrence();" />
                                                                                <asp:HiddenField runat="server" ID="recurrenceCheckboxValue" />

                                                                                <label for="recurrenceCheckbox"><%= Resources.resources.LNG_SCHEDULE_ALERT %> </label>

                                                                                <img src="images/help.png" title="<%=Resources.resources.LNG_SCHEDULE_HINT %>." /><br />
                                                                            </div>

                                                                            <div runat="server" id="recurrenceDiv">
                                                                                <da:reccurencepanel id="schedulePanel" runat="server"></da:reccurencepanel>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <h3 id="alertAppearanceHeader"><%= Resources.resources.LNG_ALERT_APPER_ACC %></h3>
                                                                <table border="0" style="width: 100%" runat="server" id="appearanceMenu">

                                                                    <tr>
                                                                        <td id="buttons_to_display" style="width: 200px; display: none">
                                                                            <div>
                                                                                <div id="desktopCheckLabel">
                                                                                    <input type="checkbox" id="desktopCheck" runat="server" name="desktop" value="1" onclick="check_desktop();" checked="checked" />
                                                                                    <label for="desktopCheck"><%= Resources.resources.LNG_DESKTOP_ALERT %></label>
                                                                                </div>
                                                                                <div id="ticker_div">
                                                                                    <div id="size_message">
                                                                                    </div>
                                                                                    <table border="0" cellspacing="0" cellpadding="0" style="width: auto;" id="tableAlertAppear">
                                                                                        <tr>
                                                                                            <td valign="top" runat="server" id="sizeDiv">
                                                                                                <fieldset class="reccurence_pattern" style="height: 110px">
                                                                                                    <legend><%=Resources.resources.LNG_SIZE %></legend>
                                                                                                    <div id="size_div" style="padding-left: 10px;">
                                                                                                        <table border="0">
                                                                                                            <tr>
                                                                                                                <td valign="middle">
                                                                                                                    <input type="radio" runat="server" id="size1" name="fullscreen" value="0" onclick="check_message()" checked />
                                                                                                                </td>

                                                                                                                <td valign="middle">
                                                                                                                    <%=Resources.resources.LNG_WIDTH %>:
                                                                                                                </td>
                                                                                                                <td valign="middle">
                                                                                                                    <input type="text" runat="server" name="alert_width" id="alertWidth" size="4"
                                                                                                                        value="500" onkeypress="return check_size_input(this, event);"
                                                                                                                        onkeyup="check_value(this)" />
                                                                                                                </td>
                                                                                                                <td valign="middle">
                                                                                                                    <%=Resources.resources.LNG_PX %>
                                                                                                                </td>

                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>&nbsp;
                                                                                                                </td>
                                                                                                                <td valign="middle">
                                                                                                                    <%=Resources.resources.LNG_HEIGHT %>:
                                                                                                                </td>
                                                                                                                <td valign="middle">
                                                                                                                    <input type="text" runat="server" name="alert_height" id="alertHeight" size="4"
                                                                                                                        value="400" onkeypress="return check_size_input(this, event);"
                                                                                                                        onkeyup="check_value(this)" />
                                                                                                                </td>
                                                                                                                <td valign="middle">
                                                                                                                    <%=Resources.resources.LNG_PX %>
                                                                                                                </td>

                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td>&nbsp;
                                                                                                                </td>
                                                                                                                <td valign="middle" colspan="3">
                                                                                                                    <input type="checkbox" runat="server" id="resizable" name="resizable" value="1" />
                                                                                                                    <label runat="server" id="resizableLabel" for="resizable">
                                                                                                                        <%=Resources.resources.LNG_RESIZABLE %></label>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr id="FullScreenOption" runat="server">
                                                                                                                <td valign="middle">
                                                                                                                    <input type="radio" runat="server" id="size2" name="fullscreen" value="1" onclick="check_message()" />
                                                                                                                </td>
                                                                                                                <td valign="middle" colspan="3">
                                                                                                                    <label for="size2">
                                                                                                                        <%=Resources.resources.LNG_FULLSCREEN %></label><br />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </div>

                                                                                                </fieldset>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                        <td runat="server" id="positionCell" valign="middle" style="height: 130px; width: 200px;">
                                                                            <br />
                                                                            <fieldset id="position_fieldset" class="reccurence_pattern" style="height: 105px;">
                                                                                <legend>
                                                                                    <%=Resources.resources.LNG_POSITION %></legend>
                                                                                <div id="position_div">
                                                                                    <table border="0" cellspacing="0" cellpadding="5" width="100%">
                                                                                        <tr>
                                                                                            <td valign="middle" style="text-align: center">
                                                                                                <input type="radio" runat="server" id="positionTopLeft" name="position" value="2" />
                                                                                            </td>
                                                                                            <td></td>
                                                                                            <td valign="middle" style="text-align: center">
                                                                                                <input type="radio" runat="server" id="positionTopRight" name="position" value="3" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td></td>
                                                                                            <td valign="middle" style="text-align: center">
                                                                                                <input runat="server" id="positionCenter" type="radio" name="position" value="4" />
                                                                                            </td>
                                                                                            <td></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td valign="middle" style="text-align: center">
                                                                                                <input type="radio" runat="server" id="positionBottomLeft" name="position" value="5" />
                                                                                            </td>
                                                                                            <td></td>
                                                                                            <td valign="middle" style="text-align: center">
                                                                                                <input runat="server" id="positionBottomRight"
                                                                                                    type="radio" name="position" value="6" checked />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>

                                                                                </div>

                                                                            </fieldset>
                                                                            <fieldset id="ticker_position_fieldset" class="reccurence_pattern" style="height: 135px; display: none">
                                                                                <legend>
                                                                                    <%=Resources.resources.LNG_POSITION %></legend>
                                                                                <div id="ticker_position_div">
                                                                                    <table border="0" cellspacing="0" cellpadding="5" width="100%">
                                                                                        <tr>
                                                                                            <td valign="middle" style="text-align: center">
                                                                                                <input type="radio" runat="server" id="positionTop" name="ticker_position" value="7" />
                                                                                            </td>
                                                                                            <td>
                                                                                                <span><%=Resources.resources.LNG_TOP %></span>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td valign="middle" style="text-align: center">
                                                                                                <input type="radio" runat="server" id="positionMiddle" name="ticker_position" value="8" />
                                                                                            </td>
                                                                                            <td>
                                                                                                <span><%=Resources.resources.LNG_MIDDLE %></span>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td valign="middle" style="text-align: center">
                                                                                                <input type="radio" runat="server" id="positionBottom" name="ticker_position" value="9" />
                                                                                            </td>
                                                                                            <td>
                                                                                                <span><%=Resources.resources.LNG_BOTTOM %></span>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr style="display: none">
                                                                                            <td valign="middle" style="text-align: center">
                                                                                                <input type="radio" runat="server" id="positionDocked" name="ticker_position" value="D" />
                                                                                            </td>
                                                                                            <td>
                                                                                                <span><%=Resources.resources.LNG_DOCKED_AT_BOTTOM %></span><img style="margin: 0px 5px" src="images/help.png" title="<%=Resources.resources.LNG_DOCKED_TICKER_HINT %>." />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                            </fieldset>
                                                                        </td>
                                                                        <td></td>
                                                                    </tr>

                                                                </table>
                                                                <h3 runat="server" id="chanelsTab"><%=Resources.resources.LNG_MES_CHAN_ACC %></h3>
                                                                <table runat="server" id="chanelsTable" border="0" style="width: 100%">
                                                                    <tr>
                                                                        <td valign="top" style="width: 25%">
                                                                            <div id="non_rsvp">

                                                                                <div runat="server" id="twitterDiv" style="padding: 0px 0px 8px 0px">
                                                                                    <input runat="server" type="checkbox" id="socialMedia" name="socialMedia" value='1' onclick="" /><label for="socialMedia">
                                                                                        <%=Resources.resources.LNG_TWEET %></label>
                                                                                    <img src="images/help.png" title="<%=Resources.resources.LNG_TWEET_HINT %>." />
                                                                                    <div runat="server" id="socialMediaSettingsHint" value='1' style="display: none; font-size: 10px; color: #ff0000;">
                                                                                        Manage Twitter settings to enable the feature
                                                                                    </div>
                                                                                </div>


                                                                                <div style="padding: 0px 0px 5px 0px">
                                                                                    <div runat="server" id="demoBlogDiv">
                                                                                        <input type="checkbox" id="blogPost" name="blogPost" value='1' disabled title="Demo version is not configured to send Blog post." /><label for="blogPost" title="Demo version is not configured to send Blog post.">
                                                                                            <%=Resources.resources.LNG_BLOG_POST %></label>
                                                                                    </div>
                                                                                    <div runat="server" id="blogDiv">
                                                                                        <input type="checkbox" id="blogPost" runat="server" name="blogPost" value='1' onclick="check_blogpost()" /><label for="blogPost">
                                                                                            <%=Resources.resources.LNG_BLOG_POST %></label>
                                                                                        <img src="images/help.png" title="<%=Resources.resources.LNG_BLOGPOST_HINT %>." />
                                                                                        <div id="blogPostSettingsHint" runat="server" value='1' style="display: none; font-size: 10px; color: #ff0000;">
                                                                                            Manage Blog settings to enable the feature
                                                                                        </div>
                                                                                    </div>

                                                                                </div>

                                                                                <div style="padding: 0px 0px 5px 0px">
                                                                                    <div runat="server" id="demoTextToCallDiv">
                                                                                        <input type="checkbox" id="textToCall" name="textToCall" value='1' disabled title="Demo version is not configured to send text-to-call messages." /><label for="textToCall" title="Demo version is not configured to send text-to-call messages.">
                                                                                            <%=Resources.resources.LNG_TEXT_TO_CALL_ALERT %></label>
                                                                                    </div>
                                                                                    <div runat="server" id="textToCallDiv">
                                                                                        <input type="checkbox" id="textToCall" runat="server" name="textToCall" value="1" onclick="checkTextToCall(true)" />
                                                                                        <label for="textToCall">
                                                                                            <%=Resources.resources.LNG_TEXT_TO_CALL_ALERT %></label>
                                                                                        <img src="images/help.png" title="<%=Resources.resources.LNG_TEXT_TO_CALL_HINT %>" />
                                                                                        <div id="textToCallErrorDiv">
                                                                                        </div>
                                                                                    </div>

                                                                                </div>

                                                                            </div>
                                                                        </td>
                                                                        <td valign="top" runat="server" id="mailSettingsDiv" style="width: 25%">
                                                                            <input type='checkbox' runat="server" name='email' id="emailCheck" value='1' onclick="check_email(true);">
                                                                            <label for="emailCheck">
                                                                                <%=Resources.resources.LNG_EMAIL_ALERT %>
                                                                            </label>
                                                                            &nbsp;
																			<img src="images/help.png" title="<%=Resources.resources.LNG_EMAIL_HINT %>">
                                                                            <br>
                                                                            <div id="mailErrorDiv">
                                                                            </div>
                                                                        </td>
                                                                        <td valign="top" runat="server" id="smsSettingsDiv" style="width: 25%">
                                                                            <div style="padding: 0px 0px 3px 0px">

                                                                                <div runat="server" id="smsDiv">
                                                                                    <input runat="server" type='checkbox' id='sms' name='sms' value='1' onclick="check_sms(true);" /><label for="sms">
                                                                                        <%=Resources.resources.LNG_SMS_ALERT %></label>&nbsp;<img src="images/help.png" title="<%=Resources.resources.LNG_SMS_HINT %>" /><br />
                                                                                    <div id="smsErrorDiv">
                                                                                    </div>
                                                                                    <a class="preview_sms_button">
                                                                                        <%=Resources.resources.LNG_PREVIEW_SMS %></a><br />
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                        <td valign="top" runat="server" id="yammerSettingsDiv" style="width: 25%">
                                                                            <div style="padding: 0px 0px 3px 0px">
                                                                                <button id="yammerShare" class="yammerShare"><%=Resources.resources.LNG_YAMMER_SHARE %></button>
                                                                                <span style="display: none" id="yammerNotification"><%= Resources.resources.LNG_YAMMER_WARNING %></span>
                                                                            </div>
                                                                        </td>


                                                                    </tr>
                                                                </table>
                                                                <h3 runat="server" id="alertDeviceHeader"><%=Resources.resources.LNG_DEVICE_TYPE %></h3>
                                                                <table border="0" style="width: 100%" runat="server" id="alertDeviceTable">
                                                                    <tr>
                                                                        <td>
                                                                            <div>
                                                                                <input type="radio" runat="server" name="dev" value="3" id="allDevices" onclick="showAppearance()" checked />
                                                                                <%=Resources.resources.LNG_ALL_DEVICES %>
                                                                                <input type="radio" runat="server" name="dev" value="1" id="desktopDevice" onclick="showAppearance()" />
                                                                                <%=Resources.resources.LNG_DEVICE_TYPE_DESKTOP %>
                                                                                <input type="radio" runat="server" name="dev" value="2" id="mobileDevice" onclick="hideAppearance()" />
                                                                                <%=Resources.resources.LNG_DEVICE_TYPE_MOBILE %>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>


                                                            </div>
                                                </table>
                                                <div align="right" style="margin-top: 10px;">

                                                    <script language="javascript">
                                                        check_aknown();
                                                        check_email();
                                                        check_desktop();
                                                        check_size();
                                                    </script>

                                                    <div>

                                                        <a runat="server" id="bottomPreview" class="preview_button" onclick="showAlertPreview()" title="Hotkey: Alt+P">
                                                            <%=Resources.resources.LNG_PREVIEW %></a>
                                                        <asp:LinkButton runat="server" CssClass="save_button" OnClientClick="return checkTitle(); " ID="bottomSaveButton" title="Hotkey: Alt+S" />
                                                        <asp:LinkButton runat="server" CssClass="save_and_next_button" OnClientClick="return checkTitle(); " ID="bottomSaveNextButton" />

                                                        <asp:UpdatePanel runat="server" ID="updatePanel">
                                                            <ContentTemplate>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </td>
                            </tr>


                        </table>
                    </td>
                </tr>
            </table>
        </div>

        <input type="hidden" id="IsDraft" name="IsDraft" value="0" />
        <input type="hidden" runat="server" id="skin_id" name="skin_id" value="" />
        <input type="hidden" id="linkedIn" name="linkedIn" value="0" />
        <input type="hidden" id="rsvp" name="rsvp" value="0" />
        <input type="hidden" id="popup" name="popup" value="0" />
        <input type="hidden" runat="server" id="ticker" name="ticker" value="0" />
        <input type="hidden" id="color_code" name="color_code" value="" />
        <input type="hidden" id="langs" name="langs" runat="server" value="" />
        <input type="hidden" id="defaultLang" name="defaultLang" runat="server" value="" />


    </form>
    <div id="skins_tiles_dialog" title="<%=Resources.resources.LNG_SELECT_SKIN %>" style='display: none'>
        <p>
        </p>


        <div id="dialog-modal" style="display: none">
            <p style="vertical-align: middle">
            </p>
        </div>

        <div id="color_code_dialog" title="<%=Resources.resources.LNG_PICK_COLOR_CODE %>" style='display: none'>
            <p>
                <%=Resources.resources.LNG_PICK_COLOR_CODE_HINT %>
            </p>
            <%
                DataSet colorCodeSet = dbMgr.GetDataByQuery("SELECT * FROM color_codes");

                //  if (colorCodeSet.IsEmpty)
                //    changeColorCode.Visible = false;

                foreach (var colorCodeRow in colorCodeSet)
                {
                    string ccId = colorCodeRow.GetString("id");
                    ccId = ccId.Replace("{", "");
                    ccId = ccId.Replace("}", "");
                    string ccColor = colorCodeRow.GetString("color");
                    string ccTitle = colorCodeRow.GetString("name");
            %>
            <div id="<%= ccId %>" onclick="pickColorCode('<%= ccColor %>', '<% = ccId%>')" name="<%= ccTitle %>" style="width: 180px; float: left; height: 180px; margin: 10px; padding: 10px; display: table; background-color: #<%= ccColor %>">
                <div style="cursor: pointer; display: table-cell; vertical-align: middle">
                    <div style="font-family: Tahoma; font-size: 24px; text-align: center;">
                        <%= ccTitle %>
                    </div>
                </div>
            </div>

            <script>
                $("#<%= ccId %> > div > div").css("color", darkOrLight("<%= ccColor %>"));
            </script>

            <%
                }
            %>
        </div>

    </div>

    <div id="alert_type_dialog" title="<%=Resources.resources.LNG_SELECT_ALERT_TYPE %>" style='display: none'>
        <div style="display: block; height: 200px; width: auto;">
            <div style='float: left; cursor: pointer; cursor: hand; margin-right: 10px; margin-bottom: 10px; position: relative'
                onclick="selectAlertTypeAndClose('alert')">
                <img src='images/alert_types/popup_big.png' />
                <div style='position: absolute; text-align: left; background-color: rgba(0,0,0,0.5); z-index: 999; bottom: 0px; width: 200px; height: 32px; color: white; text-align: center'>
                    <div style="padding-top: 10px">
                        <%= Resources.resources.LNG_DESKTOP_ALERT %>
                    </div>
                </div>
            </div>
            <% if (Config.Data.HasModule(Modules.TICKER) || Config.Data.HasModule(Modules.TICKER_PRO))
                { %>
            <div style='float: left; cursor: pointer; cursor: hand; margin-right: 10px; margin-bottom: 10px; position: relative'
                onclick="selectAlertTypeAndClose('ticker')">
                <img src='images/alert_types/ticker_big.png' />
                <div style='position: absolute; text-align: left; background-color: rgba(0,0,0,0.5); z-index: 999; bottom: 0px; width: 200px; height: 32px; color: white; text-align: center'>
                    <div style="padding-top: 10px">
                        <%= Resources.resources.LNG_SCROLL_IN_TICKER %>
                    </div>
                </div>
            </div>
            <% }%>

            <div runat="server" id="rsvpDialogTile" style='float: left; cursor: pointer; cursor: hand; margin-right: 10px; margin-bottom: 10px; position: relative'
                onclick="selectAlertTypeAndClose('rsvp')">
                <img src='images/alert_types/rsvp_big.png' />
                <div style='position: absolute; text-align: left; background-color: rgba(0,0,0,0.5); z-index: 999; bottom: 0px; width: 200px; height: 32px; color: white; text-align: center'>
                    <div style="padding-top: 10px">
                        RSVP
                    </div>
                </div>
            </div>

        </div>

        <input type="hidden" value="" id="" />
    </div>
    <div id="loader" style="display: none; top: 0; height: 100%; width: 100%; background-color: rgba(0,0,0,.2); position: fixed; transition: .5;"></div>
    <input type="hidden" id="smtpConnectTimeoutTimer" value="0">
</body>
</html>
