﻿<!-- #include file="config.inc" -->
<!-- #include file="db_conn.asp" -->
<!-- #include file="lang.asp" -->
<%

check_session()

uid = Session("uid")

if (uid <> "")  then

'save alert and exit

	if( Request ("sub1") = 1) then
		alert=Request("text")
		text=Replace(Request ("elm1"), "'", "''")
			
		name=Replace(Request ("tt_name"), "'", "''")
		id = Request("id")
		
		'check for existing name
		fl_check = 0
		Set rs_check = Conn.Execute("select id from text_templates WHERE name='" & Replace(name, "'", "''") & "'")
		if(rs_check.eof) then
			fl_check = 1
		else
			if(clng(rs_check("id"))=clng(id)) then
				fl_check = 1
			end if
		end if

		if(fl_check = 1) then
			if(Request ("edit")=1) then
				SQL = "UPDATE text_templates SET template_text = N'" & text & "', name=N'"& name &"' WHERE id=" & Request("id")
				Conn.Execute(SQL)
			else
				SQL = "INSERT INTO text_templates (template_text, name) VALUES (N'" & text & "', N'"&name&"')"
				Conn.Execute(SQL)
			end if
			redir="TextTemplates.aspx"
			Response.Redirect redir
		else
			error_descr = "'"&name&"' "& LNG_TEMPLATE_NAME_ERROR_DESC &".<br/><br/>"		
		end if

	end if
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>DeskAlerts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style9.css" rel="stylesheet" type="text/css">
<link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>

<!-- TinyMCE -->
<script language="javascript" type="text/javascript" src="jscripts/tiny_mce/tinymce.min.js"></script>
<!--<script language="javascript" type="text/javascript" src="jscripts/da_tinymce.min.js"></script>-->
<script language="javascript" type="text/javascript">
	/*var newTinySettings = {<%=newTinySettings%>};
	initTinyMCE("<%=lcase(default_lng)%>", "textareas", "", null, null, newTinySettings);*/
	
</script>
<!-- /TinyMCE -->
<script language="javascript">
	$(document).ready(function() {
		$(".cancel_button").button();
		$(".save_button").button();
		$(".preview_button").button();
		tinyMCE.baseURL = window.location.href.substring(0, window.location.href.lastIndexOf("/")) + "/jscripts/tiny_mce/";
		tinymce.init({
				selector: 'textarea#elm1',
				plugins: "moxiemanager textcolor link code emoticons insertdatetime fullscreen imagetools directionality table media image autolink",
				toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | fontselect fontsizeselect | forecolor backcolor | fullscreen | ltr rtl",
				toolbar2: "cut copy paste | bullist numlist | outdent indent | undo redo | link unlink | image insertfile code | rotateleft rotateright flipv fliph",
				moxiemanager_image_settings : { 	
					view : 'thumbs', 
					extensions : 'jpg,png,gif'
				},
				moxiemanager_video_settings : { 	
					view : 'thumbs', 
					extensions : 'mp3,wav,ogg'
				},
				relative_urls: false, branding: false,
                remove_script_host: false,
                fontsize_formats: '8pt 10pt 11pt 12pt 14pt 16pt 18pt 24pt 36pt',
				document_base_url: "<%=alerts_folder%>/admin/images/upload",
                content_style: "body{color:#000 !important; font-family:Arial, Times New Roman, Helvetica, sans-serif !important; font-size:14px !important}",
                content_css : '/admin/jscripts/tiny_mce/themes/advanced/fonts/tinymce-fonts.css',
                font_formats: 'Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Calibri=calibri;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Dubai W23 Regular=Dubai W23 Regular;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Tahoma=tahoma,arial,helvetica,sans-serif;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats',
                branding: false
			});
	});
	
function LINKCLICK() {
  var yesno = confirm ("Are you sure that you wish to cancel this page?","");
  if (yesno == false) return false;
  return true;
}

function my_redirect()
{
	if(LINKCLICK()==true)
	{
		location.href="text_templates.asp";
	}
}

function my_sub(i)
{

if(document.getElementsByName('tt_name')[0].value=="") 
{
	alert("<%=LNG_PLEASE_ENTER_TEMPLATE_TITLE %>");
	return false;
}
else
{
	if(document.getElementsByName('tt_name')[0].value.length>255) {
		alert("<%=LNG_TITLE_SHOULD_BE_LESS_THEN %>.");
		return false;
	}	
document.my_form.preview.value=0;
document.my_form.sub1.value=i;

return true;
}

}



function mypreview1()
{
document.my_form.preview.value=1;
document.my_form.sub1.value=2;
}

function mypreview()
{
	var obj = document.getElementById("preview2");
	document.body.removeChild(obj);

	obj = document.getElementById("preview1");
	document.body.removeChild(obj);

//	document.getElementsByName('preview2')[0].style.display = (document.getElementsByName('preview2')[0].style.display=='none')?'':'none'; 
//	document.getElementsByName('preview1')[0].style.display = (document.getElementsByName('preview1')[0].style.display=='none')?'':'none'; 
	return false;
}


	function showAlertPreview()
	{
		var data = new Object();
		
		data.create_date = "<%=al_create_date%>";
	
		var fullscreen = false;
		var ticker =  0;
		var acknowledgement =  0; 
		
		var alert_width = 500;
		var alert_height = 400;
		var alert_title = "Sample title";
		
		var text_template;
	
		text_template = tinyMCE.get('elm1').getContent();
		
		

		data.alert_width = alert_width;
		data.alert_height = alert_height;
		data.ticker = ticker;
		data.fullscreen = fullscreen;
		data.acknowledgement = acknowledgement;
		data.alert_title = alert_title;
		data.alert_html = text_template;
				
		initAlertPreview(data);
		
		return false;
	}

</script>
</head>
<body style="margin:0px" class="body">

<%
	id=Request("id")

'save alert and continue editing

	if( Request ("sub1") = 2) then
		alert=Request("text")
		text=Request ("elm1")
		name=Request ("tt_name")
'		if(Request ("edit")=1) then
'			SQL = "UPDATE text_templates SET template_text = '" & text & "', name='"& name &"' WHERE id=" & Request("id")
'			Conn.Execute(SQL)
'		else
'			SQL = "INSERT INTO text_templates (template_text, name) VALUES ('" & text & "', '"&name&"')"
'			Conn.Execute(SQL)
'			Set rs1 = Conn.Execute("select @@IDENTITY newID from text_templates")
'                        id=rs1("newID")
'			rs1.Close
'    		end if

		SQL = "INSERT INTO templates_preview (text_template) VALUES ('" & text & "')"
		Conn.Execute(SQL)
		Set rs1 = Conn.Execute("select @@IDENTITY newID from templates_preview")
                preview_id=rs1("newID")
		rs1.Close

		al_text = text
		tt_name = name
'		edit=0
	else
		if(id <> "") then
		    	SQL = "SELECT template_text, name FROM text_templates WHERE id=" & CStr(id)
				Set rs = Conn.Execute(SQL)
		        if ( Not rs.EOF ) then 
				al_text = rs("template_text")
				tt_name = rs("name")
'				edit=1
			end if
		end if
	end if

%>

<%if (request("preview")=1) then %>

<div id="preview2" name="preview2" STYLE="display:block; position:absolute; bottom:10px; right:10px; background-image: url(images/alert_preview.gif);" class="div_preview">

<% else %>

<div id="preview2" name="preview2" STYLE="display:none; position:absolute; bottom:10px; right:10px;">

<% end if%>

<iframe  STYLE="position:absolute;" src="<% Response.Write alerts_folder & "preview.asp?id=" & preview_id &"&text_template=1"%>" frameborder="0" class="iframe_preview"></iframe>
</div>
<%if (request("preview")=1) then %>

<div id="preview1" name="preview1" STYLE="display:block; position:absolute; cursor: pointer; background-image: url(images/close_preview.gif);" onClick="mypreview();" class="preview_close">

<% else %>

<div id="preview1" name="preview1" STYLE="display:none; position:absolute; bottom:295px; right:30px; background-color: #FFFFFF;">

<% end if%>

<!--<A href="#" onClick="mypreview();"><img src="images/action_icons/delete.gif" border="0"/></a>--></div>

<table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
	<tr>
	<td>
	<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
		<tr>
		<td width=100% height=31 class="main_table_title"><img src="images/menu_icons/templates_20.png" alt="" width="20" height="20" border="0" style="padding-left:7px">
		<a href="" class="header_title" style="position:absolute;top:15px"><%=LNG_TEXT_TEMPLATES %></a></td>
		</tr>
		<tr>
		<td class="main_table_body" height="100%">
		<div style="margin:10px;">
		<span class="work_header"><%=LNG_ADD_TEMPLATE %></span>
		<br><br>

<!-- TinyMCE -->
<font color="red"><%=error_descr %></font>
<form name="my_form" method="post" action="text_template_edit.asp">
	<input type="hidden" name="sub1" value="">
	<input type="hidden" name="preview" value="">
<%=LNG_NAME %>:	<input type="text" id="tt_name" name="tt_name" value="<% response.write tt_name %>"><br><br>

<%=LNG_TEMPLATE_TEXT %>:<br><br>
	<textarea id="elm1" name="elm1" rows="15" cols="80" style="width: 100%">
	<% 
		Response.Write al_text 
	%>
	</textarea>
<%


if(id <> "") then
	Response.Write "<input type='hidden' name='edit' value='1'/>"
	Response.Write "<input type='hidden' name='id' value='" 
	Response.Write id 
	Response.Write "'/>"
end if
%>

<br>

<div align="right">
<a class="preview_button" href='#' onclick="javascript: showAlertPreview();"><%=LNG_PREVIEW%></a> 
<a class="save_button" onclick="javascript:if (my_sub(1)) {document.my_form.submit()};"><%=LNG_SAVE%></a>
<a class="cancel_button" onclick="javascript:my_redirect(); return false;"><%=LNG_CANCEL %></a>
</form></div>
		 <img src="images/inv.gif" alt="" width="1" height="100%" border="0">
		</div>
		</td>
		</tr>
	</table>
	</td>
	</tr>
</table>

<%

else
  Response.Redirect "index.asp"

end if

%>
</body>
</html>
<!-- #include file="db_conn_close.asp" -->