USE [%dbname_hook%];


IF NOT EXISTS (
    SELECT id FROM alert_captions WHERE id = '87ebf16f-2561-4d39-8e12-00aaab3c4aa9'
)
    INSERT INTO alert_captions (id, name) VALUES('87ebf16f-2561-4d39-8e12-00aaab3c4aa9', N'Blue')
ELSE
    UPDATE alert_captions SET name = N'Blue' WHERE id = '87ebf16f-2561-4d39-8e12-00aaab3c4aa9'
	
IF NOT EXISTS (
    SELECT id FROM alert_captions WHERE id = 'be097872-6326-423f-bc8b-2e2daccd851b'
)
    INSERT INTO alert_captions (id, name) VALUES('be097872-6326-423f-bc8b-2e2daccd851b', N'White')
ELSE
    UPDATE alert_captions SET name = N'White' WHERE id = 'be097872-6326-423f-bc8b-2e2daccd851b'
	
IF NOT EXISTS (
    SELECT id FROM alert_captions WHERE id = 'c139afeb-3546-49d2-bd23-f4f46a3176af'
)
    INSERT INTO alert_captions (id, name) VALUES('c139afeb-3546-49d2-bd23-f4f46a3176af', N'Blank')
ELSE
    UPDATE alert_captions SET name = N'Blank' WHERE id = 'c139afeb-3546-49d2-bd23-f4f46a3176af'