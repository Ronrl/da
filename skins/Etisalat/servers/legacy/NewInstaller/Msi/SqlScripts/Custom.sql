USE [%dbname_hook%];

/*==========================*/
IF NOT EXISTS (
SELECT id FROM alert_captions WHERE id = '1a8b0ec7-6406-45ca-85ee-9f896d952a92'
)
INSERT INTO alert_captions (id, name) VALUES('1a8b0ec7-6406-45ca-85ee-9f896d952a92', N'Incident Management')
ELSE
UPDATE alert_captions SET name = N'Incident Management' WHERE id = '1a8b0ec7-6406-45ca-85ee-9f896d952a92'
/*==========================*/
IF NOT EXISTS (
SELECT id FROM alert_captions WHERE id = '8e66ec4c-09b8-444c-a358-45a8506b2a6e'
)
	INSERT INTO alert_captions (id, name) VALUES('8e66ec4c-09b8-444c-a358-45a8506b2a6e', N'Process Tweaks & System Enhancements')
ELSE
	UPDATE alert_captions SET name = N'Process Tweaks & System Enhancements' WHERE id = '8e66ec4c-09b8-444c-a358-45a8506b2a6e'
/*==========================*/
IF NOT EXISTS (
SELECT id FROM alert_captions WHERE id = '87ebf16f-2561-4d39-8e12-00aaab3c4aa9'
)
	INSERT INTO alert_captions (id, name) VALUES('87ebf16f-2561-4d39-8e12-00aaab3c4aa9', N'Incident Management')
ELSE
	UPDATE alert_captions SET name = N'Incident Management' WHERE id = '87ebf16f-2561-4d39-8e12-00aaab3c4aa9'
/*==========================*/
IF NOT EXISTS (
SELECT id FROM alert_captions WHERE id = '410184a0-b1ed-4ffa-ab71-49e708d1ce2c'
)
	INSERT INTO alert_captions (id, name) VALUES('410184a0-b1ed-4ffa-ab71-49e708d1ce2c', N'Attention!!')
ELSE
	UPDATE alert_captions SET name = N'Attention!!' WHERE id = '410184a0-b1ed-4ffa-ab71-49e708d1ce2c'
/*==========================*/
IF NOT EXISTS (
SELECT id FROM alert_captions WHERE id = 'be097872-6326-423f-bc8b-2e2daccd851b'
)
	INSERT INTO alert_captions (id, name) VALUES('be097872-6326-423f-bc8b-2e2daccd851b', N'Products/ Services Update')
ELSE
	UPDATE alert_captions SET name = N'Products/ Services Update' WHERE id = 'be097872-6326-423f-bc8b-2e2daccd851b'
/*==========================*/
IF NOT EXISTS (
SELECT id FROM alert_captions WHERE id = 'c139afeb-3546-49d2-bd23-f4f46a3176af'
)
	INSERT INTO alert_captions (id, name) VALUES('c139afeb-3546-49d2-bd23-f4f46a3176af', N'Incident Management')
ELSE
	UPDATE alert_captions SET name = N'Incident Management' WHERE id = 'c139afeb-3546-49d2-bd23-f4f46a3176af'
/*==========================*/
IF NOT EXISTS (
	SELECT name FROM settings WHERE name = N'DefaultSkinDisplayName'
)
	INSERT INTO settings (name, val) VALUES(N'DefaultSkinDisplayName', N'New Product Launch')
ELSE
	UPDATE settings SET val = N'New Product Launch' WHERE name = N'DefaultSkinDisplayName'
/*==========================*/
UPDATE [settings] SET [val]='0' WHERE name= 'ConfErrorLogLevelEnabled'