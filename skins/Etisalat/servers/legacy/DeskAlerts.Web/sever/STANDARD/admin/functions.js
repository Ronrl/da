function LINKCLICK(strObject) {
  if(strObject=="not"){
	alert("You have no permissions to delete.");
	return false;
  }
  
  var yesno = confirm ("Are you sure you want to delete selected "+strObject+"?","");
  if (yesno == false) return false;
  document.getElementById('delete_objs').submit();
  return true;
}

function getWidgetWidth(title) {
	if (title == "Calendar" || title == "Last Alerts") return "100%"
	if (title == "Users Statistics" || title == "User Details") return 340;
	if (title == "Alert Details" || title == "Survey Answers" || title == "Survey Details") return 440;
	if (title == "Create Alert") return 200;
	if (title == "Instant Send") return 730;
	if (title == "Emergency Alerts") return 730;
	return 500;
}


function getUiTimeFormat(settingsTimeFormat) {
    var uiFormat = "";

    if (settingsTimeFormat == "1") {
        uiFormat += "HH:mm";
    }
    else if (settingsTimeFormat == "2") {
        uiFormat += "hh:mm a";
    }

    return uiFormat;
}



function getUiDateTimeFormat(settingsDateFormat, settingsTimeFormat) {
    var uiFormat = settingsDateFormat;

    uiFormat += " " + getUiTimeFormat(settingsTimeFormat);

    return uiFormat;
}


function getDateFromAspFormat(date, format) {
    if (date.indexOf(' ') == -1) {
        var dt = new Date(getDateFromFormat(date, format.split(' ')[0]));
        dt.setHours(0);
        dt.setMinutes(0);
        dt.setSeconds(0);
        dt.setMilliseconds(0);
        return dt;
    }
    else
        return new Date(getDateFromFormat(date, format));
}


function getUiDateTime(date) {
    var dateText = "";
    if (date)
        dateText = formatDate(getDateFromAspFormat(date, responseDbFormat), uiFormat);
    return dateText;
}

function getUiDate(date) {
    var dateText = "";
    if (date)
        dateText = formatDate(getDateFromAspFormat(date, responseDbFormat), settingsDateFormat);
    return dateText;
}


function initDateTimePicker(element, settingsDateFormat, settingsTimeFormat) {
    (function ($) {
        if (element.length > 0) {
            var dateFormat = parseDateFormat(settingsDateFormat);
            $(element).datetimepicker({
                dateFormat: dateFormat,
                timeFormat: (settingsTimeFormat == "2") ? "hh:mm TT" : "HH:mm"
            });
        }
    })(jQuery);
}

function initDatePicker(element, settingsDateFormat) {
    (function ($) {
        if (element.length > 0) {
            var dateFormat = parseDateFormat(settingsDateFormat);

            $(element).datepicker({
                dateFormat: dateFormat,
                showButtonPanel: true
            });
        }
    })(jQuery);
}



function initTimePicker(element, settingsDateFormat, settingsTimeFormat) {
    (function ($) {
        if (element.length > 0) {
            var dateFormat = parseDateFormat(settingsDateFormat);
            $(element).timepicker({
                timeFormat: (settingsTimeFormat == "2") ? "hh:mm TT" : "HH:mm"
            });
        }
    })(jQuery);
}

function pageTheme(href, search) {
	var about;
	switch (href.toLowerCase()) {
	    case 'optinhelp.aspx':
	        about = "OptIns";
	        break;
		case 'edit_videos.asp':
		case 'editvideo.aspx':
			about = "CreateVideo";
			break;
		case 'sent_video.asp':
        case 'videos.aspx':
            if(search.indexOf("draft=1") != -1)
			    about = "VideoAlertsDraft";
            else
                about = "VideoAlertsSent";
			break;
		case 'digsignlinks.aspx':
			about = "DigitalSignageLinks";
			break;
		case 'channels.asp':
			about = "Channels";
			break;
        case 'Campaign.aspx':
			about = "CampaignsList";
			break;
		case 'campaigndetails.aspx':
			about = "CampaignContent";
			break;
		case 'settings_alert_langs.asp':
			about = "LanguagesSettings";
			break;
		case 'feedback_show.asp':
			about = "Feedback";
			break;
		case 'colorcodes.aspx':
		case 'color_codes.asp':
		case 'color_codes_add.asp':
			about = "ColorCodes";
			break;
		case 'addons_trial_info.asp':
			about = "TrialAddons";
			break;
        case 'settings_webplugin_settings.asp':
            about = "WebPlugin";
            break; 
		case 'request_quote.asp':
			about = "RequestQuote";
			break;
		case 'request_demo.asp':
			about = "RequestDemo";
			break;
		case 'buy_addons.asp':
			about = "BuyAddons";
			break;
	    case 'dashboard.aspx':
		case 'dashboard.asp':
		    about = "Dashboard";
		    break;
		case 'instant_messages.asp':
		case 'instantmessages.aspx':
		   	about = "InstantMessages";
		   	break;
        case 'dashboard_list.asp':
            about = "DashboardList";
            break;
        case 'editsurvey.aspx':
        case 'addsurvey.aspx':
            about = 'NewSurveyCreation';
            break;
        case 'editquestions.aspx':
        case 'questions.aspx':
            about = 'CreateQuestion';
            break;
		case 'survey_type_choose.asp':
		case 'survey_add':
			about = "SurveysCreate";
			break;
		case 'settings_system_configuration.asp':
        case 'systemconfigurationsettings.aspx':
			about = "SystemConfiguration";
			break;
		case 'settings_default_settings.asp':
        case 'defaultsettings.aspx':
			about = "DefaultSettings";
			break;
		case 'settings_blog_settings.asp':
			about = "BlogSettings";
			break;
		case 'settings_social_media.asp':
			about = "SocialMediaSettings";
			break;
		case 'settings_social_linkedin.asp':
			about = "SocialLinkedInSettings";
			break;
		case 'settings_data_archiving.asp':
			about = "DataArchiving";
			break;
		case 'settings_edit_profile.asp':
        case 'profilesettings.aspx':
			about = "ProfileSettings";
			break;
        case 'statistics.aspx':
		case 'statistics_da_alerts.asp':
			if (search.indexOf('class=1') != -1 || search.indexOf('class=16') != -1) {
				about = "AlertsStatistics";
			}
			else if (search.indexOf('class=5') != -1) {
				about = "RSSStatistics";
			}
			else if (search.indexOf('class=2') != -1) {
				about = "ScreensaversStatistics";
			}
			else if (search.indexOf('class=8') != -1) {
				about = "WallpapersStatistics";
			}
			break;
		case 'statistics_da_users.asp':
			about = "UsersStatistics";
			break;
		case 'statistics_da_surveys.asp':
			about = "SurveysStatistics";
			break;
		case 'texttemplates.aspx':
		case 'text_templates.asp':
			about = "MessageTemplates";
			break;
		case 'policy_list.asp':
		case 'policylist.aspx':
			about = "Policies";
			break;
		case 'admin_admin.asp':
		case 'admin_edit.asp':
		case 'publisherlist.aspx':
			about = "EditorsPublishers";
			break;
		case 'ou_new_interface.asp':
		case 'index.aspx':
			about = "OrganizationPage";
			break;
	    case 'synchronizations.asp':
	    case 'synchronizations.aspx':
			about = "Synchronizations";
			break;
		case 'admin_domains.asp':
		case 'domains.aspx':
			about = "Domains";
			break;
	    case 'admin_ip_groups.asp':
	    case 'ipgroups.aspx':
			about = "IPGroups";
			break;
	    case 'wallpapers.asp':
	    case 'wallpapers.aspx':
			if (search.indexOf('draft=1') != -1) {
				about = 'WallpapersDraft';
			}
			else {
				about = 'WallpapersCurrent';
			}
			break;
        case 'wallpaper_edit.asp':
	    case 'editwallpaper.aspx':
			about = "WallpapersCreate";
			break;
        case 'createalert.aspx':
		case 'edit_alert_db.asp':
		case 'template_tiles.asp':
			about = "AlertsCreating";
			break;
        case 'alert_users.asp':
        case 'recipientsselection.aspx': 
			about = "AlertUsers";
			break;
		case 'edit_user.asp':
			about = "AddUser";
			break;
		case 'edit_group.asp':
			about = "AddGroup";
			break;
		case 'group_users.asp':
			about = "GroupUsers";
			break;
        case 'ad_sync_form.asp':
			about = "ADSync";
			break;
		case 'edir_sync_form.asp':
			about = "EdirSync";
			break;
		case 'text_template_edit.asp':
			about = "TemplateEdit";
			break;
		case 'edit_ip_group.asp':
			about = "IPGroupEdit";
			break;
		case 'policy_edit.asp':
			about = "PolicyEdit";
			break;
		case 'languages_edit.asp':
			about = "LanguageEdit";
			break;
	    case 'screensavers.asp':
	    case 'screensavers.aspx':
			if (search.indexOf('draft=1') != -1) {
				about = 'ScreensaversDraft';
			}
			else {
				about = 'ScreensaversCurrent';
			}
			break;
		case 'screensaver_type_choose.asp':
		case 'editscreensavers.aspx':
			about = "ScreensaversCreate";
			break;
	    case 'rss_list.asp':
        case 'rsslist.aspx':
			if (search.indexOf('draft=1') != -1) {
				about = 'RSSDraft';
			}
			else {
				about = 'RSSCurrent';
			}
			break;
        case 'rss_edit.asp':
        case 'editrss.aspx':
			about = "RSSCreate";
			break;
		case 'surveys_archived.asp':
			about = "SurveysArchived";
			break;
	    case 'surveys.asp':
	    case 'surveys.aspx':
			about = "SurveysActive";
			break;
		case 'draft.asp':
			about = "AlertsDraft";
			break;
	    case 'sent.asp':
	    case 'popupsent.aspx':
	        if (search.indexOf('draft=1') != -1) {
	            about = 'AlertsDraft';
	        }
	        else {
	            about = 'AlertsSent';
	        }

			break;
		case 'create_button_ui.asp':
			about = "Shortcut";
			break;
		default:
			about = "";
			break;
	}
	return about;
}


function select_all_objects_ou()
{
	var status = document.getElementById('select_all').checked;
	var field = document.getElementsByName('objects');
	for (i = 0; i < field.length; i++){
		field[i].checked = status;
		if(status==true){
			var objName = document.getElementById("object_name" + field[i].value).value;
			addObject(field[i].value, objName);
		}
		else{
			removeObject(field[i].value);	
		}
	}
		
}

function select_all_objects()
{
	var status = document.getElementById('select_all').checked;
	var field = document.getElementsByName('objects');
	for (i = 0; i < field.length; i++)
		field[i].checked = status;
}

function filterCheckboxClicked(e)
{
	var event = e || window.event;
	if( ! event.target ) {
		event.target = event.srcElement
    }	
	if(event.target.checked==true){
		switch(event.target.id){
		 case "filter_users":
			window.parent.iframeChangeFilter("f_users", 1);
			break;
		 case "filter_groups":	
			window.parent.iframeChangeFilter("f_groups", 1);
			break;			
		 case "filter_computers":	
		 	window.parent.iframeChangeFilter("f_computers", 1);
			break;
		}
		return true;
		//addObject(event.target.value);
	}
	else{
		switch(event.target.id){
		 case "filter_users":
			if(!window.parent.iframeChangeFilter("f_users", 0)){
				event.target.checked = true;
				return false;
			}
			break;
		 case "filter_groups":	
		 	if(!window.parent.iframeChangeFilter("f_groups", 0)){
				event.target.checked = true;
				return false;
			}
			
			break;			
		 case "filter_computers":	
		 	if(!window.parent.iframeChangeFilter("f_computers", 0)){
				event.target.checked = true;
				return false;
			}
			break;
		}
		return true;
		//removeObject(event.target.value);	
	}
}


function checkboxClicked(e)
{
	var event = e || window.event;
	if( ! event.target ) {
		event.target = event.srcElement
    }	
	if(event.target.checked==true){
		var objName = document.getElementById("object_name" + event.target.value).value;
		addObject(event.target.value, objName);
	}
	else{
		removeObject(event.target.value);	
	}
}

function addObject(id, objName){
	window.parent.iframeAddObject(id, objName);
}

function removeObject(id){
	window.parent.iframeRemoveObject(id);
}

function parseDateFormat(settingsDateFormat) {
    var dateFormat;
    switch (settingsDateFormat) {
    case "dd/MM/yyyy":
        dateFormat = "dd/mm/yy";
        break;
    case "yyyy/MM/dd":
        dateFormat = "yy/mm/dd";
        break;
    case "MM/dd/yyyy":
        dateFormat = "mm/dd/yy";
        break;
    case "yyyy/dd/MM":
        dateFormat = "yy/dd/mm";
        break;
    case "MM/yyyy/dd":
        dateFormat = "mm/yy/dd";
        break;
    case "dd/yyyy/MM":
        dateFormat = "dd/yy/mm";
        break;
    case "dd-MM-yyyy":
        dateFormat = "dd-mm-yy";
        break;
    case "yyyy-MM-dd":;
        dateFormat = "yy-mm-dd";
        break;
    case "MM-dd-yyyy":
        dateFormat = "mm-dd-yy";
        break;
    case "yyyy-dd-MM":
        dateFormat = "yy-dd-mm";
        break;
    case "MM-yyyy-dd":
        dateFormat = "mm-yy-dd";
        break;
    case "dd-yyyy-MM":
        dateFormat = "dd-yy-mm";
        break;
    }
    return dateFormat;
}

function Return() {
    window.history.back();
}

function initializeInlineTinyMce(selectorTag) {
    tinyMCE.baseURL = window.location.href.substring(0, window.location.href.lastIndexOf("/")) + "/jscripts/tiny_mce";
    tinymce.init({
        inline: true,
        selector: selectorTag,
        menubar: false,
        force_br_newlines: true,
        force_p_newlines: false,
        convert_newlines_to_brs: false,
        remove_linebreaks: true,
        apply_source_formatting: false,
        forced_root_block: '',
        paste_as_text: false,
        setup: function (ed) {
            ed.on('init',
                function(ed) {
                    ed.target.editorCommands.execCommand("fontName", false, "Arial");
                });
        },
        toolbar: 'fontselect fontsizeselect | bold italic underline',
        fontsize_formats: '8pt 10pt 11pt 12pt 14pt 16pt 18pt',
        content_css: '/admin/jscripts/tiny_mce/themes/advanced/fonts/tinymce-fonts.css',
        font_formats: 'Neo Tech Alt=Neo Tech Alt,Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Calibri=calibri;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Tahoma=tahoma,arial,helvetica,sans-serif;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats'
    });
};

function validateFileName(fileName) {
    if ((/^[\w-._~]{0,50}\.[\w]{3,4}$/i.test(fileName))) {
        return true;
    }
    return false;
}

function showErrorFileUpload(error) {
    alert(error);
    event.preventDefault();
}
