$(document).ready(function () {

    CreateSkin("default");
    CreateSkin("{87ebf16f-2561-4d39-8e12-00aaab3c4aa9}");
    CreateSkin("{be097872-6326-423f-bc8b-2e2daccd851b}");
    CreateSkin("{c139afeb-3546-49d2-bd23-f4f46a3176af}");
	CreateSkin("{1a8b0ec7-6406-45ca-85ee-9f896d952a92}");
	CreateSkin("{8e66ec4c-09b8-444c-a358-45a8506b2a6e}");
	CreateSkin("{410184a0-b1ed-4ffa-ab71-49e708d1ce2c}");

})

function ReadFileByPath(file) {
    var result = null;

    $.ajax({
        url: file,
        async: false,
        cache: false,
        dataType: "text",
        success: function (data, textStatus, jqXHR) {
            result = data;
        }
    });

    return result;
}

function GetXmlDocumentFromFile(path) {
    var file = ReadFileByPath(path);

    var result = null;
    result = $.parseXML(file);

    return result;
}

function FindViewsTagElement(xmlDoc) {
    var windowOptions = null;
    windowOptions = $(xmlDoc).find('VIEWS');

    return windowOptions;
}

function GetWindowTagObjectProperies(name, object, tag) {
    object.name = name;
    object.width = tag.find("[name=" + name + "]").attr("width");
    object.height = tag.find("[name=" + name + "]").attr("height");
    object.leftmargin = tag.find("[name=" + name + "]").attr("leftmargin");
    object.topmargin = tag.find("[name=" + name + "]").attr("topmargin");
    object.rightmargin = tag.find("[name=" + name + "]").attr("rightmargin");
    object.bottommargin = tag.find("[name=" + name + "]").attr("bottommargin");

    CheckPositionValue(object);

    object.leftmargin = (parseInt(object.leftmargin) + 4).toString();
    object.rightmargin = (parseInt(object.rightmargin) + 4).toString();
}

function CheckPositionValue(object) {
    object.width = ReplaceUndefined(object.width);
    object.height = ReplaceUndefined(object.height);
    object.leftmargin = ReplaceUndefined(object.leftmargin);
    object.topmargin = ReplaceUndefined(object.topmargin);
    object.rightmargin = ReplaceUndefined(object.rightmargin);
    object.bottommargin = ReplaceUndefined(object.bottommargin);
}

function ReplaceUndefined(value) {
    if (value === undefined) {
        return "0";
    } else {
        return value;
    }
}

function SetContentPosition(objectName, windowObject) {
    $("#" + objectName).css("top", windowObject.topmargin + "px");
    $("#" + objectName).css("right", windowObject.rightmargin + "px");
    $("#" + objectName).css("bottom", windowObject.bottommargin + "px");
    $("#" + objectName).css("left", windowObject.leftmargin + "px");
}

function CreateSkin(skinName) {
    var filePath = './' + skinName + '/blank/conf.xml'
    var xmlDoc = GetXmlDocumentFromFile(filePath);
    var xmlTag = FindViewsTagElement(xmlDoc);

    var skinNameWithoutBraces = skinName.substring(1, skinName.length - 1);

    CreateAlertWindows(skinName, skinNameWithoutBraces, xmlTag);
    CreateHistoryWindows(skinName, skinNameWithoutBraces, xmlTag);
    CreateOptionWindows(skinName, skinNameWithoutBraces, xmlTag);
    CreateBrowserWindows(skinName, skinNameWithoutBraces, xmlTag);
    CreateTickerWindows(skinName, skinNameWithoutBraces, xmlTag);
}

function CreateAlertWindows(skinName, skinNameWithoutBraces, xmlTag) {
    var elementId = "AlertId" + skinNameWithoutBraces;
    var alertId = "AlertContentId" + skinNameWithoutBraces;
    var alertWindow = {};
    GetWindowTagObjectProperies("alertwindow1", alertWindow, xmlTag);

    $("#SkinWindows").append('<div class="alert" id="' + elementId + '"></div>');
    $("#" + elementId).append('<iframe frameborder="0" src="' + skinName + '/version/alertcaption.html" title="default" width="' + alertWindow.width + '" height="' + alertWindow.height + '"></iframe>');
    $("#" + elementId).append('<div class="alert__title"><div class="alert__title_text">It Department is aware of the email outage. Some of our users have not been able to access their email since 10 am EST. The issues has been harder to fix than we originally expected.</div><div class="alert__title_date">Created:<br />2000-12-31 19:55:15</div></div>');
    $("#" + elementId).append('<div class="alert__content" id="' + alertId + '">It Department is aware of the email outage. Some of our users have not been able to access their email since 10 am EST. The issues has been harder to fix than we originally expected.<br />We have dozens of people working around the clock to bring it to a resolution. We believe our current efforts will restore our users access to their inboxes by 3 pm today. We will keep you posted. It Department is aware of the email outage. Some of our users have not been able to access their email since 10 am EST. The issues has been harder to fix than we originally expected.<br />We have dozens of people working around the clock to bring it to a resolution. We believe our current efforts will restore our users access to their inboxes by 3 pm today. We will keep you posted.</div>');

    SetContentPosition(alertId, alertWindow);

    var elementPreviewId = "AlertPreviewId" + skinNameWithoutBraces;
    $("#Previews__alerts").append('<div class="alertPreview" id="' + elementPreviewId + '"></div>');
    $("#" + elementPreviewId).append('<iframe frameborder="0" src="' + skinName + '/version/alertcaption.html" title="default" width="' + alertWindow.width + '" height="' + alertWindow.height + '"></iframe>');
}

function CreateHistoryWindows(skinName, skinNameWithoutBraces, xmlTag) {
    var elementId = "HistoryId" + skinNameWithoutBraces;
    var alertId = "HistoryContentId" + skinNameWithoutBraces;
    var alertWindow = {};
    GetWindowTagObjectProperies("historywindow1", alertWindow, xmlTag);

    alertWindow.height = parseInt(alertWindow.height) + 50;
    alertWindow.topmargin = parseInt(alertWindow.topmargin) + 10;
    alertWindow.bottommargin = parseInt(alertWindow.bottommargin) + 50;

    $("#SkinWindows").append('<div class="alert" id="' + elementId + '"></div>');
    $("#" + elementId).append('<iframe id="' + skinNameWithoutBraces + '" frameborder="0" src="' + skinName + '/version/historycaption.html" title="default" width="' + alertWindow.width + '" height="' + alertWindow.height + '"></iframe>');
    $("#" + elementId).append('<div class="alert__content history" id="' + alertId + '"><table><tr><td><div class="history__icon history__icon-alert"></div></td><td>It Department is aware of the email outage. Some of our users have not been able to access their email since 10 am EST. The issues has been harder to fix than we originally expected.</td><td>2018-08-07 17:28:52</td><td><div class="history__icon history__icon-delete"></div></td></tr><tr><td><div class="history__icon history__icon-alert"></div></td><td>It Department is aware of the email outage. Some of our users have not been able to access their email since 10 am EST. The issues has been harder to fix than we originally expected.</td><td>2018-08-07 17:28:52</td><td><div class="history__icon history__icon-delete"></div></td></tr><tr><td><div class="history__icon history__icon-alert"></div></td><td>It Department is aware of the email outage. Some of our users have not been able to access their email since 10 am EST. The issues has been harder to fix than we originally expected.</td><td>2018-08-07 17:28:52</td><td><div class="history__icon history__icon-delete"></div></td></tr><tr><td><div class="history__icon history__icon-alert"></div></td><td>It Department is aware of the email outage. Some of our users have not been able to access their email since 10 am EST. The issues has been harder to fix than we originally expected.</td><td>2018-08-07 17:28:52</td><td><div class="history__icon history__icon-delete"></div></td></tr><tr><td><div class="history__icon history__icon-alert"></div></td><td>It Department is aware of the email outage. Some of our users have not been able to access their email since 10 am EST. The issues has been harder to fix than we originally expected.</td><td>2018-08-07 17:28:52</td><td><div class="history__icon history__icon-delete"></div></td></tr><tr><td><div class="history__icon history__icon-alert"></div></td><td>It Department is aware of the email outage. Some of our users have not been able to access their email since 10 am EST. The issues has been harder to fix than we originally expected.</td><td>2018-08-07 17:28:52</td><td><div class="history__icon history__icon-delete"></div></td></tr></table><div class="history__buttons"><input type="button" value="Previous"/><input type="text" value="1"/>of 2<input type="button" value="Next"/></div></div>');

    SetContentPosition(alertId, alertWindow);
}

function CreateOptionWindows(skinName, skinNameWithoutBraces, xmlTag) {
    var elementId = "OptionId" + skinNameWithoutBraces;
    var alertId = "OptionContentId" + skinNameWithoutBraces;
    var alertWindow = {};
    GetWindowTagObjectProperies("optionwindow1", alertWindow, xmlTag);

    alertWindow.bottommargin = parseInt(alertWindow.bottommargin) + 49;

    $("#SkinWindows").append('<div class="alert" id="' + elementId + '"></div>');
    $("#" + elementId).append('<iframe frameborder="0" src="' + skinName + '/version/optionscaption.html" title="default" width="' + alertWindow.width + '" height="' + alertWindow.height + '"></iframe>');
    $("#" + elementId).append('<div class="alert__content" id="' + alertId + '"><div class="options"><table><tr><td>Build version</td><td>9.2.7.30 / 9.2.7.30</td></tr><tr><td>Server Url</td><td>http://demo.deskalerts.com</td></tr><tr><td>User name</td><td>Skin user</td></tr><tr><td>Client ID</td><td>{69F6BF96-E09F-4C93-A2DC-0F2F5383A81C}</td></tr><tr><td>Play alert sound</td><td><input type="checkbox" checked="checked"/> </td></tr><tr><td colspan="2" class="options__buttons"><input type="button" value="Cancel" /><input type="button" value="Save" /></td></tr></table></div></div>');

    SetContentPosition(alertId, alertWindow);
}

function CreateBrowserWindows(skinName, skinNameWithoutBraces, xmlTag) {
    var elementId = "BrowserId" + skinNameWithoutBraces;
    var alertId = "BrowserContentId" + skinNameWithoutBraces;
    var alertWindow = {};
    GetWindowTagObjectProperies("minibrowserwindow", alertWindow, xmlTag);

    alertWindow.height = parseInt(alertWindow.height) + 42;
    alertWindow.bottommargin = parseInt(alertWindow.bottommargin) + 46;

    $("#SkinWindows").append('<div class="alert" id="' + elementId + '"></div>');
    $("#" + elementId).append('<iframe frameborder="0" src="' + skinName + '/version/minibrowsercaption.html" title="default" width="' + alertWindow.width + '" height="' + alertWindow.height + '"></iframe>');
    $("#" + elementId).append('<div class="alert__content" id="' + alertId + '"><div id="mainForm" style="margin: 20px 10px 10px 10px;"><input name="disableUnameChange" type="hidden" id="disableUnameChange" /><table cellpadding=0 cellspacing=4 border=0><tr><td class="body">Username:</td><td><input name="uname" style="width:150px;" class="reg_input" type="text" value="" /></td></tr><tr><td class="body">Password:</td><td><input name="upass" style="width:150px;" class="reg_input" type="password" /></td></tr><tr id="mobilePhoneRow"><td class="body">Mobile phone:</td><td><input name="uphone" class="reg_input" style="width:150px;" type="text" value="" /></td></tr><tr id="emailRow"><td class="body">Email:</td><td><input name="uemail" class="reg_input" style="width:150px;" type="text" value="" /></td></tr><tr><td colspan=2 align=right style="padding-right: 13px;"><input id="submitButton" type="image" src="http://192.168.0.83/admin/images/langs/EN/save.gif" alt="Save" title="Save" border="0"></td></tr></table></div></div>');

    SetContentPosition(alertId, alertWindow);
}

function CreateTickerWindows(skinName, skinNameWithoutBraces, xmlTag) {
    var elementId = "TickerId" + skinNameWithoutBraces;
    var alertId = "TickerContentId" + skinNameWithoutBraces;
    var alertWindow = {};
    GetWindowTagObjectProperies("tickerwindow1", alertWindow, xmlTag);

    $("#SkinWindows").append('<div class="ticker" id="' + elementId + '" style="height: ' + alertWindow.height + 'px;"></div>');
    $("#" + elementId).append('<iframe frameborder="0" src="' + skinName + '/version/tickercaption.html" title="default" width="100%" height="' + alertWindow.height + '"></iframe>');
    $("#" + elementId).append('<div class="ticker__content" style="top:' + alertWindow.topmargin + 'px; right:' + alertWindow.rightmargin + 'px; bottom: ' + alertWindow.bottommargin + 'px; left: ' + alertWindow.leftmargin + 'px"><marquee>It Department is aware of the email outage. Some of our users have not been able to access their email since 10 am EST. The issues has been harder to fix than we originally expected. We have dozens of people working around the clock to bring it to a resolution. We believe our current efforts will restore our users access to their inboxes by 3 pm today. We will keep you posted. It Department is aware of the email outage. Some of our users have not been able to access their email since 10 am EST. The issues has been harder to fix than we originally expected. We have dozens of people working around the clock to bring it to a resolution. We believe our current efforts will restore our users access to their inboxes by 3 pm today. We will keep you posted.</marquee></div>');

    SetContentPosition(alertId, alertWindow);

    alertWindow.height = parseInt(alertWindow.height) - 1;
    tickerContentHeight = parseInt(alertWindow.height) - 4;
    tickerContentTopMargin = parseInt(alertWindow.topmargin) + 1;
    var elementPreviewId = "TickerPreviewId" + skinNameWithoutBraces;
    $("#Previews__tickers").append('<div class="tickerPreview" id="' + elementPreviewId + '" style="height: ' + alertWindow.height + 'px;"></div>');
    $("#" + elementPreviewId).append('<iframe frameborder="0" src="' + skinName + '/version/tickercaption.html" title="default" width="100%" height="' + alertWindow.height + '"></iframe>');
    $("#" + elementPreviewId).append('<div class="ticker__content" style="height:' + tickerContentHeight + 'px; top:' + tickerContentTopMargin + 'px; right:' + alertWindow.rightmargin + 'px; bottom: ' + alertWindow.bottommargin + 'px; left: ' + alertWindow.leftmargin + 'px"><marquee></marquee></div>');
}