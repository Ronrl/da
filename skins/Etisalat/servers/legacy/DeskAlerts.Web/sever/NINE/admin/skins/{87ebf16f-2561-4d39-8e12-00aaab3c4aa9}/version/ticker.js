function createDomDocument(source)
{
	var xmlDoc = null;

	if (window.ActiveXObject){
		xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async = "false";
		xmlDoc.loadXML(source);
	}
	else
	{
		var parser = new DOMParser();
		xmlDoc = parser.parseFromString(source,"text/xml");
	}
	return xmlDoc;
}

function unescapeXML(str)
{
	str = str.replace(/&amp;/g, "&");
	str = str.replace(/&quot;/g, "\"");
	str = str.replace(/&apos;/g, "'");
	str = str.replace(/&lt;/g, "<");
	str = str.replace(/&gt;/g, ">");
	str = str.replace(/&#38;/g, "&");
	str = str.replace(/&#34;/g, "\"");
	str = str.replace(/&#39;/g, "'");
	str = str.replace(/&#60;/g, "<");
	str = str.replace(/&#62;/g, ">");
	str = str.replace(/&#x9;/g, "\t");
	str = str.replace(/&#xA;/g, "\n");
	str = str.replace(/&#xD;/g, "\r");
	
	return str;
}

function checkLang(current, langs)
{
	var currents = current.toLowerCase().replace(" ", "").split(",");
	var languages = langs.toLowerCase().replace(" ", "").split(",");
	for(var i=0; i<currents.length; i++)
	{
		for(var j=0; j<languages.length; j++)
		{
			if(currents[i].indexOf(languages[j])==0 ||
				languages[j].indexOf(currents[i])==0)
			{
				return true;
			}
		}
	}
	return false;
}
function replaceWithInput(elem, name)
{
	var inp = document.createElement("input");
	inp.type = "submit";
	if(elem.tagName.toLowerCase() == "img")
	{
		var func = elem.onclick||'';
		if(typeof(func) == 'function')
			inp.onclick = function() {
				func();
				return false;
			};
		else
			inp.onclick = func+";return false";
	}
	else
	{
		inp.onclick = elem.onclick;
	}
	inp.name = elem.name;
	inp.value = name;
	elem.parentNode.replaceChild(inp, elem);
}
function onContextMenuMain(event)
{
	if (!event) event = window.event;
	event.returnValue = false;  
	event.cancelBubble = true;
	return false;
}
function getDocumentSize(doc)
{
	var size ={
		height: 0,
		width: 0
	};
	var body = doc.body;
	if (!doc.compatMode || doc.compatMode=="CSS1Compat")
	{
		var topMargin = parseInt(body.currentStyle.marginTop, 10) || 0;
		var bottomMargin = parseInt(body.currentStyle.marginBottom, 10) || 0;
		var leftMargin = parseInt(body.currentStyle.marginLeft, 10) || 0;
		var rightMargin = parseInt(body.currentStyle.marginRight, 10) || 0;

		size.width=Math.max(body.offsetWidth + leftMargin + rightMargin, doc.documentElement.clientWidth, doc.documentElement.scrollWidth);
		size.height=Math.max(body.offsetHeight + topMargin + bottomMargin, doc.documentElement.clientHeight, doc.documentElement.scrollHeight);
	}
	else
	{
		size.width = body.scrollWidth;
		size.height = body.scrollHeight;
	}
	return size;
}
function resizeByBody(docSize)
{
	try
	{
		var isResizible = window.external.isResizible();
		if (isResizible)
		{
			var docHeight = docSize.height;
			var curBodyHeight = window.external.getBodyHeight();
			if (curBodyHeight>docHeight) docHeight=curBodyHeight;
			window.external.setCaptionHeightByBodyHeight(docHeight);
		}
	} catch (e) {}
}
function replace_tags(html)
{
	return html.replace(/<(\/?)(\w+)(.*?)(\sstyle\s*=\s*(('|")(.*?)\6|(\w+)))?(.*?)>/gi, function(all, c, tag, a1, s, style, q, s1, s2, a2) {
		var before = '';
		var after = '';
		switch(tag.toLowerCase())
		{
			case 'hr':
			case 'br':
			{
				return '';
			}
			case 'html':
			case 'body':
			case 'head':
			case 'table':
			case 'thead':
			case 'tbody':
			case 'tr':
			case 'th':
			case 'td':
			case 'pre':
			case 'ul':
			case 'ol':
			case 'li':
			case 'dl':
			case 'dt':
			case 'dd':
			case 'blockquote':
			case 'q':
			case 'p':
			{
				tag = 'span';
				after = '';
				break;
			}
			case 'h1':
			case 'h2':
			case 'h3':
			case 'h4':
			case 'h5':
			case 'h6':
			{
				tag = 'b';
				after = '';
				break;
			}
		}
		return before+'<'+c+tag+a1+(s?' style='+q+(s1||s2||'').replace(/!\s*important/,'')+q:'')+a2+'>'+after;
	});
}

var body_width;
var alert_body;

var alert_body_left = 0;
var alert_body_width;

var tickerstop=false;
var initialized = false;

var remainingMessages = [];
var interval;
var skinId;
var textDir;
var alertsContent;
var gIndexes;

function writeReadItButton(readIt_button, match) {
    var result = "";
    if (readIt_button.indexOf("/") >= 0) {
        result += "<img";
    }
    else
        result += "<input type=\"button\"";

    result +=
        " id='readitButton' class='readitButton' style='position: relative !important; display: inline-block !important; margin-left: 1rem !important;'" +
        " src=\"" + readIt_button + "\" value=\"" + readIt_button + "\" alt=\"OK\" title=\"OK\" border=\"0\"";
    result += " onclick=\"window.external.readIt('" + match[1] + "','" + match[2] + "','" + match[3] + "');return false\"/>";	
    return result;
}

var closedTickers = new Array()
var tickers = new Array()

function Initial(indexes)
{
	setTimeout("Initial_Timeout('"+indexes+"')", 1);
}

function Initial_Timeout(indexes)
{
	if(initialized) return;
    else initialized = true;    
	skinId = null;
	textDir = null;
	var server = window.external.getProperty("server");
	try
	{
		document.body.style.overflow = 'hidden';
		document.body.scroll = 'no';
		var unread = indexes.split(',');
		if (!gIndexes) { gIndexes = unread.slice(0);}
		remainingMessages = unread.slice(0);
		var alerts = alertsContent || (alertsContent = replace_tags(document.body.innerHTML).split('<!-- separator -->'));        
		html = '';
		var main_title = '';
		try {
			main_title = window.external.getProperty("title");
		} catch(e) {}
		
		var alertsToTerminate = window.external.getProperty("terminated_alerts_ids");
		
        for (var i = unread.length - 1; i >= 0; i--)
        {
            var cur = alerts[alerts.length - unread[i] - 1];
            var currentTickerSpeed = GetTickerSpeed(cur);
            var ticker = {
                internalId: unread[i],
                alertId: window.external.getProperty('alertid'),
                aknowledgment: cur.indexOf('<!-- cannot close -->') > 0,
                autoclose: 0,
                autocloseDate: 0,
                autocloseButton: "",
                isExpired: false,
                isAutoclose: false
            };
			
            var match = cur.match(/<!-- autoclose = '-?([\d]+)', '(.*?)', autoclose_date = '-?([\d]+)' -->/i);
            if (match && match[1] > 0) {
                try {
                    if (window.external) {
                        
                        ticker.autoclose = Math.abs(match[1]);
                        ticker.autocloseButton = match[2];
                        ticker.autocloseDate = match[3];
                        ticker.isAutoclose = true;
                        if (Math.abs(ticker.autoclose) > 0) {
                            var now = Math.round(new Date().getTime() / 1000);
                            var diff = ticker.autocloseDate - now;
                            if (diff < 0) {
                                ticker.isExpired = true;
                            }
                        }

                        addTicker(ticker)
                        updateTickerExpiration(ticker)
                        setAutoclose(ticker)
                    }
                } catch (e) { }
            }
			
			var realAlertId = ticker.alertId;
			match = cur.match(/<!-- alert_id = '(.*?)' -->/i);
			if (match) {
				realAlertId = match[1];
			}
			
			var isTerminated = false;
			var alertsToTerminateArray = alertsToTerminate.split(',');
			if(alertsToTerminateArray.indexOf(realAlertId) != -1){
				isTerminated = true;
				addToClosed(ticker);
			}

            match = cur.match(/<!-- cdata = '(.*?)' -->/i)
            if (match) {
                var doc = createDomDocument("<ALERT>" + unescapeXML(match[1]) + "</ALERT>");
                var alertWindows = doc.getElementsByTagName("WINDOW");
                if (alertWindows.length > 0) {
                    newSkinId = alertWindows[0].getAttribute("skin_id");
                }
            }

			var root_path, close_button, save_button, submit_button, readIt_button;
			var newSkinId = "";
			var newTextDir = "";

            var cdataDiv = document.getElementById('cdata');
			if(cdataDiv)
			{
				var doc = createDomDocument("<ALERT>"+unescapeXML(cdataDiv.getAttribute("data"))+"</ALERT>");
				var alertWindows = doc.getElementsByTagName("WINDOW");
				if (alertWindows.length > 0)
				{
					newSkinId = alertWindows[0].getAttribute("skin_id");
				}
			}
			
            newTextDir = (cur.indexOf("dir=ltr") > 0 || cur.indexOf('dir="ltr"') > 0) ? "ltr" : "rtl";
			var ncur; 
			var count = 0;
			match = cur.match(/<object+(\s|\S)+?<\/object>/im);
			
			while(match)
			{
			var str;
			var name;
				match = cur.match(/<object+(\s|\S)+?\/object>/im)[0]; 
				ncur = cur.match(/<object+(\s|\S)+?\/object>/im)[0]; 
				ncur = ncur.replace(/\'/mg,"\"");
				str = ncur.match(/<EMBED src=\"+(\s|\S)+?(?=\" type="application\/x-mplayer2")/img); 
				if(str!== null)
					{
					str = ncur.match(/<EMBED src=\"+(\s|\S)+?(?=\" type="application\/x-mplayer2")/img)[0];	
					str = str.replace(/<embed src=\"/img,""); 	
					name = str.match(/upload_video+\S*/img)[0];
					name = name.replace("upload_video\/",""); 
					}
				else
					{
					str = ncur.match(/flvToPlay=http+(\s|\S)+?(?=&amp;autoStart=false">)/img);
						if(str!==null)
						{
						str = ncur.match(/flvToPlay=http+(\s|\S)+?(?=&amp;autoStart=false">)/img)[0];
						str = str.replace(/flvToPlay=/im,""); 
						name = ncur.match(/upload_video+(\s|\S)+?(?=&amp;autoStart=false">)/img); 
							if (name !== null){
								name = ncur.match(/upload_video+(\s|\S)+?(?=&amp;autoStart=false">)/img)[0]; 
								name = name.replace(/upload_video\//im,""); 
							}
						}
						else{
								str = ncur.match(/<param name=\"movie\" value=+(\s|\S)+?(?=\">)/img)[0]; 
								str = str.replace(/<param name=\"movie\" value=\"/img,""); 
								name = str.match(/upload_flash+\S*/img); 
								if (name !== null){
									name = str.match(/upload_flash+\S*/img)[0]; 
									name = name.replace(/upload_flash\//img,""); 
								}						
						}
					}
					
				if(!count) 
					{
					    cur = cur.replace(/<object+(\s|\S)+?<\/object>/im, 'Click on the name to watch video: <a href="' + str + '" target="_blank">' + name + " " + '</a>' + " ");
					}
				else
					{
					    cur = cur.replace(/<object+(\s|\S)+?<\/object>/im, '<a href="' + str + '" target="_blank">' + " " + name + " " + '</a>' + " ");
					}
				ncur = cur.match(/<object+(\s|\S)+?\/object>/im); 
				match = cur.match(/<object+(\s|\S)+?\/object>/im);
				count++;
			}
			
			if (!skinId)
			{
				skinId = "default";
				if (newSkinId)
				{
					skinId = newSkinId;
				}
				window.external.callInCaption("refreshSkin", skinId);
			}
			
			if (!textDir)
			{
				textDir = newTextDir;
			}
			
			if (textDir != newTextDir) { break; }

			if ((skinId != newSkinId) && (skinId!="default" || newSkinId))
			{
				break;
			}

			remainingMessages.splice(remainingMessages.length - 1,1);

			if (skinId && skinId != "default" && window.external.getProperty('AlertMode') != 'offline')
            {                
                close_button = server + "/admin/skins/" + skinId + "/version/close_button.gif";
				save_button = server + "/admin/skins/" + skinId + "/version/save.gif";
				submit_button = server + "/admin/skins/" + skinId + "/version/submit_button.gif";
				root_path = server + "/admin/skins/" + skinId + "/version/";
				readIt_button = server + "/admin/skins/" + skinId + "/version/read_it.gif";
			}
			else
			{
				try {
					root_path = window.external.getProperty("root_path").replace(/\\/g, "/");
                    close_button = window.external.getProperty("closeButton").replace(/\\/g, "/");
					save_button = window.external.getProperty("saveButton").replace(/\\/g, "/");
					submit_button = window.external.getProperty("submitButton").replace(/\\/g, "/");
					readIt_button = "file:///" + root_path + "/read_it.gif";
					
				} catch(e) {
					root_path = null;
					close_button = null;
					save_button = null;
					submit_button = null;
				}
			}
			if(close_button || save_button || submit_button)
            {
				var images = document.getElementsByTagName("img");
				for(var j = 0; j < 2; j++)
				{
                    if (j == 1) images = document.getElementsByTagName("input");
                    for (var imgIndex = 0; imgIndex < images.length; imgIndex++)
                    {

						if(images[imgIndex].tagName.toLowerCase() == "img" || images[imgIndex].getAttribute("type").toLowerCase() == "image")
						{
                            var src = images[imgIndex].getAttribute("src");
							if(close_button && /.*admin\/images\/(langs\/\w+\/?|)close_button\.gif/i.test(src))
                            {
								if(/^[\w\s]+$/.test(close_button))
									replaceWithInput(images[imgIndex], close_button);
								else
                                {
                                    images[imgIndex].src = (/https?:\/\//i.test(close_button) ? "" : "file:///") + close_button;                                    
									images[imgIndex].alt = "Close";
									images[imgIndex].title = "Close";
								}

							}
							else if(save_button && /.*admin\/images\/(langs\/\w+\/?|)save\.gif/i.test(src))
                            {
								if(/^[\w\s]+$/.test(save_button))
									replaceWithInput(images[imgIndex], save_button);
								else
								{
									images[imgIndex].src = (/https?:\/\//i.test(save_button) ? "" : "file:///") + save_button;
									images[imgIndex].alt = "Save";
									images[imgIndex].title = "Save";
								}
							}
							else if(submit_button && /.*admin\/images\/(langs\/\w+\/?|)submit_button\.gif/i.test(src))
                            {
								if(/^[\w\s]+$/.test(submit_button))
									replaceWithInput(images[imgIndex], submit_button);
								else
								{
									images[imgIndex].src = (/https?:\/\//i.test(submit_button) ? "" : "file:///") + submit_button;
									images[imgIndex].alt = "Submit";
									images[imgIndex].title = "Submit";
								}
							}
						}
					}
				}
			}

			if (cur.indexOf('<!-- ticker_pro -->') < 0) {
				textDir = "rtl";
			}
			
			match = cur.match(/<!-- self-deletable -->/i);
			if (match) {
			    window.external.setProperty('self_deletable', '1');
			}
			else window.external.setProperty('self_deletable', '0');
			
			match = cur.match(/<!-- readit = '(.*?)', '([\d]+)', '([\d]+)' -->/i);			
			
			
			if(match && match[2] > 0 && match[3] > 0)
            {
                
                if (textDir == "rtl") {
                    cur += writeReadItButton(readIt_button, match);
                }
                else {
                    cur = writeReadItButton(readIt_button, match) + cur;
                }
                ticker.aknowledgment = true;
            }

            addTicker(ticker)

			var to_date_match = cur.match(/<!-- todate = '(.*?)', '([\d]+)', '([\d]+)', '(.*?)' -->/i);
			
			
			if(to_date_match && to_date_match[2] > 0 && to_date_match[3] > 0 && to_date_match[4])
			{
				    var now = Math.round(new Date().getTime()/1000.0);
                    var lifetimedate = to_date_match[1];
                    if(now > parseInt(lifetimedate))
                    {
						window.external.readIt( to_date_match[4].replace(/\\\\/g, '\\') ,  to_date_match[2] ,to_date_match[3]);
                    }

			}
			else
			{
				
             var toDateDiv = document.getElementById('todate');
             	if(toDateDiv)
                    {
                    	    var now = Math.round(new Date().getTime()/1000.0);
                            var lifetimedate =  toDateDiv.getAttribute('date');//to_date_match[1];
                            if(now > parseInt(lifetimedate))
                            {
                                window.external.readIt( toDateDiv.getAttribute('file').replace(/\\\\/g, '\\') ,  toDateDiv.getAttribute('alert_id') ,toDateDiv.getAttribute('user_id'));
                            }                
                    }



			}
			var title = main_title;
			match = cur.match(/<!-- title *= *(['"])(.*?)\1 -->/i);
			if (match)
				title = match[2];
			match = cur.match(/<!-- html_title *= *(['"])(.*?)\1 -->/i);
			if (match) {
				title = unescapeXML(match[2]);
			}
			match = cur.match(/<!-- *begin_lang[^>]*-->([\w\W]*?)<!-- *end_lang *-->/ig);
			if(match)
			{
				var locale;
				try
				{
					locale = window.external.getProperty("customlocale", "");
					if(!locale) locale = window.external.getProperty("locale", "");
				}
				catch(err)
				{
					locale = "EN";
				}
				var default_lang, use_dafault = true;
				for(var j=0; j<match.length; j++)
				{
					var lang = match[j].match(/<!-- *begin_lang[^>]*lang *= *(['"])([^"']*)\1[^>]*-->/i);
					if(lang && checkLang(locale, lang[2]))
					{
						cur = cur.replace(/<!-- *begin_lang[^>]*-->([\w\W]*)<!-- *end_lang *-->/i, match[j]);
						use_dafault = false;
						var rtitle = match[j].match(/<!-- *begin_lang[^>]*title *= *(['"])([^"']*)\1[^>]*-->/i);
						if(rtitle && rtitle[2])
							title = rtitle[2];
						break;
					}
					var is_default = match[j].match(/<!-- *begin_lang[^>]*is_default *= *(['"])([^"']*)\1[^>]*-->/i);
					if(is_default && (is_default[2]!=0 || is_default[2].toLowerCase()=="true"))
						default_lang = match[j];
				}
				if(use_dafault && default_lang)
					cur = cur.replace(/<!-- *begin_lang[^>]*-->([\w\W]*)<!-- *end_lang *-->/i, default_lang);			
            }

            var filteredClosedTickers = closedTickers.filter(function (x) {
                return x.internalId == ticker.internalId;
            })

            var isClosed = filteredClosedTickers.length == 1
            var now = Math.round(new Date().getTime() / 1000);
            //alert('Title:' + cur.match(/<!-- title *= *(['"])(.*?)\1 -->/i)[2] + '\n tickers.length:' + tickers.length + '\nfilteredClosedTickers.length:' + filteredClosedTickers.length + '\nticker.isExpired:' + ticker.isExpired + '\nticker.internalId' + ticker.internalId + '\nticker.aknowledgment:' + ticker.aknowledgment + '\nticker.autocloseDate:' + ticker.autocloseDate + '\nisClosed:' + isClosed + '\nticker.isAutoclose:' + ticker.isAutoclose + '\nDiff:' + (ticker.autocloseDate - now))
            if (ticker.isExpired || isClosed || isTerminated) {
                continue
            }

            if (html) html += "<td id='ticker_" + ticker.internalId + "' style='vertical-align: middle' class='text'><span class='ticker_separator'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td style='vertical-align: middle'></td><td style='vertical-align: middle' class='text'><span class='ticker_dot'>&#9679;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
            else html += "<td id='ticker_" + ticker.internalId + "' style='vertical-align: middle' class='text'>";

			var topTemplate = "", bottomTemplate = "";
			try
			{
				topTemplate = replace_tags(window.external.getProperty("top_template",""));
				bottomTemplate = replace_tags(window.external.getProperty("bottom_template",""));
			}
			catch(e)
			{
			}
			
			if (title)
            {
                html += "</td>";

				if (textDir == "rtl") {
                    html += "<td style='vertical-align: middle' class='ticker_title_container'>";
                    html += "   <div class='ticker_title' name='tickerTitle' style='vertical-align: middle'>" + title + "</div>";
                    html += "</td>";
                    html += "<td class='ticker_after_title_td'>&nbsp;</td>";
                    html += "<td style='vertical-align: middle;' class='text'>";
                    html += "   <div id='ticker_" + ticker.internalId + "' name='tickerContent'>" + topTemplate + cur + bottomTemplate + "</div>";
                    html += "</td>";
                } else {
				    cur = ReversTickersSpan(cur);
                    
                    html += "<td class='ticker_after_title_td'>&nbsp;</td>";
                    html += "<td style='vertical-align: middle;' class='text'>";
                    html += "   <div id='ticker_" + ticker.internalId + "' name='tickerContent'>" + topTemplate + cur + bottomTemplate + "</div>";
                    html += "</td>";
                    html += "<td style='vertical-align: middle' class='ticker_title_container'>";
                    html += "   <div class='ticker_title' id='ticker_Title' style='vertical-align: middle; display:inline-block'>" + title + "</div>";
                    html += "</td>";  
				}
			}
			else{
				html += topTemplate + cur + bottomTemplate + "</div></td>";
			}
        }

		window.external.setProperty("terminated_alerts_ids", "");
		
		html = "<table id='contentTable' style='padding:0, border-spacing:0'><tr>" + html + "</tr></table>";

        setShowCloseButton()        
        var now = Math.round(new Date().getTime() / 1000)
        var filteredExpiredTickers = getExpiredTickers()
        var isAllClosed = (filteredExpiredTickers.length + closedTickers.length) == tickers.length
        if (isAllClosed) {
            window.external.callInCaption('CloseWindow')
        }

		document.oncontextmenu = onContextMenuMain;

		//insert base tag	
		var headTag = document.getElementsByTagName('head')[0];
		var base = document.createElement('base');
		//base.setAttribute("href", "file:///" + root_path);
		base.setAttribute("target", "_blank");
		headTag.appendChild(base);

		//insert style
		var mainCss = document.getElementById("ticker_main_css");
		if(!mainCss)
		{
			var css = "\
			* { margin:0px!important; \
				padding:0px!important; \
				orphans:0!important; \
				widows:0!important; \
				vertical-align:middle; \
				white-space:nowrap!important; \
				overflow:show!important; \
			} \
			.alert_body td * { border:0px none!important; \
				outline:0px none!important; \
				position:static!important; \
				z-index:auto!important; \
				clip:auto!important; \
				width:auto!important; \
				clear:none!important; \
				float:none!important; \
				line-height:normal!important; \
				page-break-after:auto!important; \
				page-break-before:auto!important; \
				page-break-inside:auto!important; \
				text-indent:0px!important; \
				text-shadow:none!important; \
				visibility:visible!important; \
			} \
			.alert_body td.text * { display:inline!important; \
				top:auto!important; \
				right:auto!important; \
				bottom:auto!important; \
				left:auto!important; \
			} \
			.alert_body td img { display:block!important; } \
			.alert_body { display:block!important; height:100%!important; width:1px; overflow:show!important; } \
			table, tr, tbody, td { height:100%!important; } \
			.hidden {display:none!important} \
			body {-moz-user-select:none;-khtml-user-select:none;user-select:none;}";
			var imprt = document.createElement('style');
			imprt.setAttribute("type", "text/css");
			imprt.setAttribute("id", "ticker_main_css");
			if (imprt.styleSheet) imprt.styleSheet.cssText = css; //IE
			else imprt.appendChild(document.createTextNode(css)); // Other browsers
			headTag.appendChild(imprt);
		}

		var cssEl = document.getElementById("ticker_css");
		if (cssEl) cssEl.parentNode.removeChild(cssEl);
		var baseEl = document.getElementById("ticker_base");
		if (baseEl) baseEl.parentNode.removeChild(baseEl);
		
		document.body.innerHTML = "<div id='alert_body' class='alert_body' onmouseover='tickerstop=true' onmouseout='tickerstop=false' style='position:relative'>"+html+"</div>";
		document.body.className = "tickercontent";

		//resize images
        var imgs = document.getElementsByTagName('img');
        var height = window.innerHeight - 4;
		for(var i=0; i < imgs.length; i++)
		{
			//imgs[i].align = 'absmiddle';
			if(imgs[i].clientHeight > height && imgs[i].className != "readitButton")
			{
				var h = imgs[i].clientHeight;
				imgs[i].height = height;
				imgs[i].width = h > 0 ? imgs[i].clientWidth * height/h : height;
			}
		}
		alert_body = document.getElementById('alert_body');
		alert_body_width = alert_body.offsetWidth;


		if (textDir == "ltr")
		{
 			alert_body_left = -document.getElementById("contentTable").clientWidth;
		}
		else
		{
			alert_body_left = window.innerWidth;
		}
		
		body_width = document.body.clientWidth + 5;
		alert_body.style.left = alert_body_left+"px";

		if (remainingMessages.length==0)
		{
			remainingMessages = gIndexes.slice(0);
        }

		//Setup ticker speed
		var tickerSpeedFrequency = 15;
		var tickerSpeedDistance = 1;
		
        switch (currentTickerSpeed) {
			case "1":
				tickerSpeedFrequency = 60;
				break;
			case "2":
				tickerSpeedFrequency = 45;
				break;
			case "3":
				tickerSpeedFrequency = 30;
				break;
			case "5":
				tickerSpeedDistance = 2;
				break;
			case "6":
				tickerSpeedDistance = 3;
				break;
			case "7":
				tickerSpeedDistance = 4;
				break;
		}
		
		if(textDir == "rtl")
		{
			interval = setInterval('scrollticker(' + tickerSpeedDistance + ', false)', tickerSpeedFrequency);
		}
		else
		{
			interval = setInterval('scrollticker(' + -tickerSpeedDistance + ', false)', tickerSpeedFrequency);
		}
	}
	catch(e) {
	    console.log(e);
	}
}
function scrollticker(delta, force)
{
    try {
		var loadNext = false;
	
		if((tickerstop && !force) || !alert_body) return
		
		
		var tickerTitles =  document.getElementsByName("tickerTitle");
		var tickerContents = document.getElementsByName("tickerContent");
		
		
		
		var contentWidth = 0; // document.getElementById("tickerTitle").offsetWidth + document.getElementById("tickerContent").offsetWidth;
		
		
		for(var i = 0; i < tickerTitles.length; i++)
		{
			contentWidth += ( tickerTitles[i].offsetWidth + tickerContents[i].offsetWidth );
        }

        var endPoint = 0

		if (alert_body_left + contentWidth- delta  <= endPoint && textDir == "rtl") //has been scrolled to left out of screen
		{
			loadNext = true;
			alert_body_left = body_width;
		}
		else if (alert_body_left-delta > body_width) //has been scrolled to right out of screen
		{
			loadNext = true;
			alert_body_left = -1;
		}
		else
		{
			alert_body_left -= delta;
		}
		alert_body.style.left = alert_body_left+"px";
		if(loadNext)
		{
			initialized = false;
			if (interval) clearInterval(interval);
			Initial(remainingMessages.join());
		}
	} catch(e) {}
}

function wheel(event)
{
	try {
		var delta = 0;
		if (!event) event = window.event;
		if (event.wheelDelta) {
			delta = event.wheelDelta/120;
			if (window.opera)
				delta = -delta;
		} else if (event.detail) {
			delta = -event.detail/3;
		}
		if (delta)
				scrollticker(Math.round(delta)*20, true);
		if (event.preventDefault)
				event.preventDefault();
		event.returnValue = false;
	} catch(e) {}
}

if (window.addEventListener) window.addEventListener('DOMMouseScroll', wheel, false);
window.addEventListener("wheel", wheel);
window.onmousewheel = document.onmousewheel = wheel;

function addToClosed(ticker) {
    var filteredClosedTickers = closedTickers.filter(function (x) {
        return x.internalId == ticker.internalId
    })

    if (filteredClosedTickers.length == 0) {
        closedTickers.push(ticker)
    }
}

Array.prototype.findIndex = function (predicate, thisValue) {
    var arr = Object(this);
    if (typeof predicate !== 'function') {
        throw new TypeError();
    }
    for (var i = 0; i < arr.length; i++) {
        if (i in arr) {
            var elem = arr[i];
            if (predicate.call(thisValue, elem, i, arr)) {
                return i;
            }
        }
    }
    return undefined;
}

function updateTickerExpiration(ticker) {
    var tickersFiltered = tickers.filter(function (x) {
        return x.internalId == ticker.internalId;
    });

    if (tickersFiltered.length == 1) {
        var t = tickersFiltered[0]
        t.isExpired = ticker.isExpired
    }
}

function addTicker(ticker) {
    var tickersFiltered = tickers.filter(function (x) {
        return x.internalId == ticker.internalId;
    });

    if (tickersFiltered.length == 0) {
        tickers.push(ticker);
    }
}

function setAutoclose(ticker) {    
    setShowCloseButton()
    var now = Math.round(new Date().getTime() / 1000)
    var diff = (ticker.autocloseDate * 1000) - now;
    if (diff > 0) {
        setTimeout(function () { autoClose(ticker); }, diff);
    }
    else {
        autoClose(ticker);
    }
}

function setShowCloseButton() { 
    var isAknowledgment = true
    tickers.forEach(function (ticker) {
        if (!ticker.aknowledgment) {
            isAknowledgment = false
        }
    })

    if (tickers.length == 0) {
        isAknowledgment = false
    }

    window.external.callInCaption('showCloseButton', !isAknowledgment)
}

function Close() {
    tickers.forEach(function (ticker) {
        if (!ticker.aknowledgment) {
            addToClosed(ticker)
        }
    })
    var filteredExpiredTickers = getExpiredTickers()
    if (tickers.length == (closedTickers.length + filteredExpiredTickers.length)) {
        window.external.callInCaption('CloseWindow')
    }
}

function spliceTicker(tickerId) {
    var index = tickers.findIndex(function (x) {
        return x.internalId == tickerId;
    });

    if (index != undefined) {
        tickers.splice(index, 1);
    }
}

function autoClose(ticker) {
    var filteredExpiredTickers = getExpiredTickers()
    if ((filteredExpiredTickers.length + closedTickers) == tickers.length) {
        window.external.callInCaption('CloseWindow')
    }
}

function getExpiredTickers() {
    var now = Math.round(new Date().getTime() / 1000);
    return tickers.filter(function (x) {
        var diff = x.autocloseDate - now
        return diff < 0 && x.isAutoclose
    })
}

function GetTickerSpeed(content) {
    if (content !== null) {
        var match = content.match(/<!-- tickerSpeed=(.*?) -->/i);

        return match[1];
    }
}

function ReversTickersSpan(content) {
    if (content === null) {
        return '';
    }

    var regExpString = /<span[^>]*(\.*dir=(\"|')ltr(\"|'))?>\t?\r?\n?(.*)\t?\r?\n?<\/span>/g;

    content = content
        .replace("<span><span", "<span")
        .replace(/<!-- html_title (.*?) -->/g, '');

    var allSpans = content.match(regExpString);
    var allSpansString = allSpans.reverse().join(" ");

    content = content.replace(regExpString, '');

    return allSpansString + content;
}