﻿<%@ Page Language="C#" EnableEventValidation="true" AutoEventWireup="true" CodeBehind="EditSurvey.aspx.cs" Inherits="DeskAlertsDotNet.Pages.EditSurvey" %>

<%@ Import Namespace="DeskAlertsDotNet.Parameters" %>
<%@ Import Namespace="Resources" %>
<%@ Import Namespace="DeskAlerts.Server.Utils.Helpers" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DeskAlerts</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="css/style9.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="jscripts/jquery/jquery.min.js"></script>
    <link href="css/form_style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="jscripts/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript" src="jscripts/jquery.hotkeys.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/JavaScript" src="jscripts/json2.js"></script>
    <script type="text/javascript" src="jscripts/preview.js?v=9.2.1.31"></script>
    <script type="text/javascript" src="functions.js"></script>
    <script type="text/javascript" src="jscripts/date.js"></script>
    <script type="text/javascript" src="jscripts/jquery.hotkeys.js"></script>
    <script type="text/javascript" src="jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="jscripts/jquery/datepicker_fix.js"></script>
    <script type="text/javascript" src="jscripts/moment.js"></script>
    <script type="text/javascript" src="jscripts/tiny_mce/tinymce.min.js"></script>

    <style type="text/css">
        #default {
            margin-top: 0px;
        }

        .save_and_next_button {
            float: right;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {

            var settingsDateFormat = "<% = DateTimeConverter.JsDateFormat %>";
            var settingsTimeFormat = "<% = DateTimeConverter.JsTimeFormat %>";

            tinyMCE.baseURL = window.location.href.substring(0, window.location.href.lastIndexOf("/")) + "/jscripts/tiny_mce/";
            tinymce.init({
                selector: 'textarea#title_survey',
                menubar: false,
                statusbar: false,
                resize: false,
                setup: function (ed) {
                    ed.on('change', function (e) {
                        $("#titleHidden").val(ed.getContent());
                    });
                },
                plugins: "autoresize textcolor autolink",
                toolbar: "italic underline strikethrough | fontselect fontsizeselect | forecolor",
                autoresize_bottom_margin: 0,
                autoresize_min_height: 20,
                autoresize_max_height: 80,
                fontsize_formats: '8pt 10pt 11pt 12pt 14pt 16pt 18pt',
                forced_root_block: false,
                valid_elements: "span[*],font[*],i[*],b[*],u[*],strong[*],s[*],em[*]",
                content_style: "body{font:bold 21px Arial !important;color:#000000 !important}",
                content_css : '/admin/jscripts/tiny_mce/themes/advanced/fonts/tinymce-fonts.css',
                font_formats: 'Neo Tech Alt=Neo Tech Alt,Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Calibri=calibri;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Tahoma=tahoma,arial,helvetica,sans-serif;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats',
                branding: false
            });

            $(document).tooltip({
                items: "img[title],a[title]",
                position: {
                    my: "center bottom-20",
                    at: "center top",
                    using: function (position, feedback) {
                        $(this).css(position);
                        $("<div>")
                            .addClass("arrow")
                            .addClass(feedback.vertical)
                            .addClass(feedback.horizontal)
                            .appendTo(this);
                    }
                }
            });

            $("#accordion").accordion(
                {
                    collapsible: true
                }
            );

            function isBadSkin(skinId) {

                skinId = skinId.toLowerCase();
                return skinId == '{c28f6977-3254-418c-b96e-bda43ce94ff4}' || skinId == '{138f9281-0c11-4c48-983e-d4ca8076748b}' || skinId == '{04f84380-b31d-4167-8b83-c6642e3d25a8}';
            }
            var skinsDialog = $("#skins_tiles_dialog").dialog({
                position: { my: "left top", at: "left top", of: "#dialog_dock" },
                modal: true,
                resizable: false,
                draggable: false,
                width: 'auto',
                resize: 'auto',
                margin: 'auto',
                bgiframe: true,
                autoOpen: false,
                close: function () { closeSkinsDialog() }
            });

            $('#default').click(function () {
                skinsDialog.dialog('open');
            });


            var dupId = parseInt('<% =Request["id"] %>', 10);



            function closeSkinsDialog() {
                skinsDialog.dialog('close');
            }
            function check_size_input(myfield, e, dec) {
                var key;
                var keychar;

                if (window.event)
                    key = window.event.keyCode;
                else if (e)
                    key = e.which;
                else
                    return true;

                keychar = String.fromCharCode(key);

                // control keys
                if ((key == null) || (key == 0) || (key == 8) ||
                    (key == 9) || (key == 13) || (key == 27)) {
                    return true;
                }
                // numbers
                else if (myfield.value.length > 6) {
                    return false;
                }
                else if ((("0123456789").indexOf(keychar) > -1)) {
                    return true;
                }
                // decimal point jump
                else if (dec && (keychar == ".")) {
                    myfield.form.elements[dec].blur();
                    return false;
                }
                else
                    return false;
            }

            function check_message() {

                var elem = document.getElementById('size2');
                var msg = document.getElementById('size_message');
                var alertWidth = document.getElementById('alertWidth');
                var alertHeight = document.getElementById('alertHeight');
                var resizable = document.getElementById('surveyResizable');
                var position_fieldset = document.getElementById('position_fieldset');

                if (elem && alertWidth && alertHeight) {
                    if (msg) msg.style.display = elem.checked ? "none" : "";
                    alertWidth.disabled = elem.checked;
                    alertHeight.disabled = elem.checked;
                    position_fieldset.disabled = elem.checked;
                    $("#ticker_position_fieldset").disabled = elem.checked;
                    if (resizable) resizable.disabled = elem.checked;
                }
            }

            $('.tile').hover(function () {
                $(this).switchClass("", "tile_hovered", 200);
            }, function () {
                $(this).switchClass("tile_hovered", "", 200);
            }).click(function () {
                var src = $(this).find('img').eq(0).attr('src');
                var tmp_arr = src.split('/');
                var id = tmp_arr[1];
                $("#skin_id").val(id);
                var alertWidth = document.getElementById('alertWidth');
                var alertHeight = document.getElementById('alertHeight');
                if (isBadSkin(id)) {
                    $("#size1").prop('checked', true);
                    $("#size2").prop('checked', false);
                    $("#size1").prop('disabled', true);
                    $("#size2").prop('disabled', true);
                    $("#surveyResizable").prop('disabled', true);

                    alertWidth.disabled = true;
                    alertHeight.disabled = true;
                    $("input[name=position]").prop('disabled', true);
                }
                else {
                    $("#size1").prop('disabled', false);
                    $("#size2").prop('disabled', false);
                    alertWidth.disabled = false;
                    $("#resizable").prop('disabled', false);
                    alertHeight.disabled = false;
                    $("input[name=position]").prop('disabled', false);
                    check_message();
                }
                var src_tumb = $("#skin_preview_img").attr('src');
                var src_tumb_elems = src_tumb.split('/');
                var new_src_tumb = src_tumb_elems[0] + "/" + id + "/" + src_tumb_elems[2];
                $("#skin_preview_img").attr('src', new_src_tumb);
                closeSkinsDialog();
            });

            $(document)
                .bind('keydown',
                'right',
                function () {

                    $('.save_and_next_button').click();
                    $(".preview_button").button();
                });
            $('.save_and_next_button').click(function () {
                var ready = checkForm();
                if (!ready) return;

                var quiz = $('#right_wrong').is(":checked");
                var pol = $('#view_res').is(":checked");
                var anonym = $('#anonymous_survey').is(":checked");

                if (quiz) $('#stype').val(2);
                if (pol) $('#stype').val(3);
                if (anonym) $('#stype').val(4);

                $("#title").val(encodeURI(tinyMCE.get("title_survey").getContent()));
                var plain_text = $('<div/>').html(tinyMCE.get("title_survey").getContent()).text();
                $('#plain_title').val(plain_text);

                if ($("#schedule").is(":checked")) {
                    var start = moment($("#dateStart").val(), "<% = DateTimeConverter.MomentJsDateTimeFormat %>");
                    var end = moment($("#dateEnd").val(), "<% = DateTimeConverter.MomentJsDateTimeFormat %>");
                    $.ajax({
                        type: "POST",
                        url: "EditSurvey.aspx/ValidDateTime",
                        data: JSON.stringify({ 'fromDate': start.toDate(), 'toDate': end.toDate() }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            if (response.d) {
                                alert(response.d);
                            }
                            else {
                                $("#survey_settings").submit();
                            }
                        },
                        failure: function (response) {
                            console.log(response.d);
                        }
                    });
                }
                else {
                    $("#survey_settings").submit();
                }
            });

            $(".delete_button").button({});

            var newTinySettings = {
                "theme_advanced_buttons1": "bold,italic,underline,|,formatselect,fontselect,fontsizeselect,|,forecolor,backcolor",
                "theme_advanced_buttons2": "",
                "theme_advanced_buttons3": ""
            };

            function getLifeTimeControl(lifetime, name) {

                var minSel = "";
                var hourSel = "";
                var daySel = "";

                if (lifetime != 0) {
                    if (lifetime % 1440 == 0) {
                        lifetime /= 1400;
                        daySel = " selected";
                    }
                    else if (lifetime % 60 == 0) {
                        lifetime /= 60;
                        hourSel = " selected";
                    }
                    else {
                        minSel = " selected";
                    }
                }
                var textBox = "<input type='text' id='" + name + "' name='" + name + "' style='width:50px' value='" + lifetime + "'onkeypress='return check_size_input(this, event);'>";
                var comboBox = "<select id='" + name + "_factor' name='" + name + "_factor'>" +
                    "<option value='1'" + minSel + ">" + '<% =resources.LNG_MINUTES%>' + "</option>" +
                    "<option value='60'" + hourSel + ">" + '<%=resources.LNG_HOURS%>' + "</option>" +
                    "<option value='1440'" + daySel + ">" + '<% =resources.LNG_DAYS%>' + "</option>" +
                    "</select>";
                return (textBox + comboBox);
            }

            $('#lifetimemode').click(function () {
                if ($(this).prop("checked")) {
                    $('#schedule').prop("checked", false);
                    $('#lifetime').prop('disabled', false);
                    $('#lifetime_factor').prop('disabled', false);
                }
                else {
                    $('#lifetime').prop('disabled', true);
                    $('#lifetime_factor').prop('disabled', 'disabled');
                }
                checkScheduleFields();
            });

            $('#schedule').click(function () {
                if ($(this).prop("checked")) {
                    $('#lifetimemode').prop("checked", false);
                    $('#lifetime').prop('disabled', true);
                    $('#lifetime_factor').prop('disabled', 'disabled');
                }
                else {
                }
                checkScheduleFields();
            });

            function checkScheduleFields() {

                if ($("#schedule").prop("checked")) {
                    $("#dateStart").prop('disabled', false);
                    $("#dateEnd").prop('disabled', false);
                }
                else {
                    $("#dateStart").prop('disabled', true);
                    $("#dateEnd").prop('disabled', true);
                }
            }

            $(".date_time_param_input")
                .datetimepicker({ dateFormat: settingsDateFormat, timeFormat: settingsTimeFormat });

            setTimeout(function () {

                if (isNaN(dupId))
                    return;
                $.ajax({
                    type: "POST",
                    url: "EditSurvey.aspx/GetSurveyData",
                    data: '{id: ' + dupId + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {

                        var surveyData = JSON.parse(response.d);

                        $("#stype").val(surveyData.type);

                        tinyMCE.get("title_survey").setContent(surveyData.name);

                        if (surveyData.skin == null)
                            surveyData.skin = "default";

                        var skinId = surveyData.skin;

                        $("#skin_preview_img").attr("src", "skins/" + skinId + "/thumbnail.png");
                        $("#skin_id").val(skinId);
                        if (surveyData.type == 2) {
                            $("#right_wrong").prop('checked', true);
                        }
                        else if (surveyData.type == 3) {
                            $("#view_res").prop('checked', true);
                        }
                        else if (surveyData.type == 4) {
                            $("#anonymous_survey").prop('checked', true);
                        }

                        if (surveyData.schedule == 1) {
                            $("#lifetimemode").prop('checked', false);
                            $("#lifetime").prop('disabled', true);
                            $("#lifetime_factor").prop('disabled', true);
                            $("#schedule").prop('checked', true);
                            $("#dateStart").prop('disabled', false);
                            $("#dateEnd").prop('disabled', false);
                        }
                        else if (surveyData.lifetime == 1) {
                            $("#lifetimemode").prop('checked', true);

                            var lifetime = parseInt(surveyData.lifetime);
                            switch (surveyData.lifeTimeType) {
                                case 'dayly':
                                    $('select>option:eq(2)').attr('selected', true);
                                    break;
                                case 'hourly':
                                    $('select>option:eq(1)').attr('selected', true);
                                    break;
                                case 'minutes':
                                    $('select>option:eq(0)').attr('selected', true);
                                    break;
                                default:
                                    break;
                            }

                            $('#lifetime').val(parseInt(surveyData.lifeTimeValue));
                            $("#schedule").prop('checked', false);
                            
                            $("#dateStart").prop('disabled', true);
                            $("#dateEnd").prop('disabled', true);
                        }
                        else {
                            $("#lifetimemode").prop('checked', false);
                            $("#lifetime").prop('disabled', true);
                            $("#lifetime_factor").prop('disabled', true);
                            $("#schedule").prop('checked', false);
                            
                            $("#dateStart").prop('disabled', true);
                            $("#dateEnd").prop('disabled', true);
                        }

                        $("#alertHeight").val(surveyData.alert_height);
                        $("#alertWidth").val(surveyData.alert_width);

                        if (surveyData.fullscreen == 1) {
                            $("#size2").prop('checked', true);
                            check_message();
                        }
                        else {
                            $("#size1").prop('checked', true);
                            check_message();

                            var value = surveyData.fullscreen;
                            if (value == 0) value = 6;
                            $("input[name=position][value=" + value + "]").prop('checked', true);
                        }

                        if (surveyData.resizable != "False") {

                            $("#surveyResizable").prop('checked', true);
                            $("#surveyResizable").val(1);
                        }

                        $("#cant_close").prop('checked', surveyData.autoclose == 2147482647);
                        $("#survey_data").val(encodeURI(JSON.stringify(surveyData.questions)));
                    },
                    failure: function (response) {

                    }
                });
            }, 500);


        });

        function showAlertPreview(type) {
            if (!type) type = "desktop";

            var data = new Object();

            data.create_date = "<%=DateTime.Now.ToString(("dd/MM/yyyy HH:mm:ss"))%>";

            var fullscreen = ($("#size2").prop("checked") == "checked") ? "1" : $("input[name='position']:checked").attr("value");
            if ($("#size2").prop("checked")) { fullscreen = "1" }

            var alert_width = $("#alertWidth").val();
            var alert_height = $("#alertHeight").val();

                <% if (Settings.Content["ConfEnableHtmlAlertTitle"].Equals("1"))
        { %>
            var html_title_ed = tinyMCE.get("title_survey");
            var html_title = html_title_ed.getContent();

            var alert_title = html_title;
                <% } %>

            data.alert_width = alert_width;
            data.alert_height = alert_height;
            data.fullscreen = fullscreen;
            data.alert_title = alert_title;
            var skinId = $("#skin_id").val();
            data.skin_id = skinId;
            data.type = type;
            initAlertPreview(data);
            return false;
        }

        function checkForm() {
            var result = true;
            var al_value = tinyMCE.get("title_survey").getContent();
            var plain_text = $('<div/>').html(tinyMCE.get("title_survey").getContent()).text();
            if (plain_text.length < 3 || plain_text.length >= 255) {
                result = false;
                alert("<% =resources.LNG_TITLE_SHOULD_BE_LESS_THEN %>");
                return result;
            }
            return result;

        }

        function check_value() {
            if ($("#alertWidth").length > 0) {
                var alertWidth = document.getElementById('alertWidth').value;
                var alertHeight = document.getElementById('alertHeight').value;
                var size_message = document.getElementById('size_message');

                if (alertWidth > 1024 && alertHeight > 600) {
                    size_message.innerHTML = '<font color="red"><% =resources.LNG_RESOLUTION_DESC1 %></font>';
                    return;
                }

                if (alertWidth < 300 || alertHeight < 200) {
                    size_message.innerHTML = '<font color="red"><%=resources.LNG_RESOLUTION_DESC2 %>. <A href="#" onclick="set_default_size();"><% =resources.LNG_CLICK_HERE %></a> <% =resources.LNG_RESOLUTION_DESC3 %>.</font>';
                    return;
                }
                size_message.innerHTML = "";
            }
        }

        function check_message() {
            var elem = document.getElementById('size2');
            var msg = document.getElementById('size_message');
            var alertWidth = document.getElementById('alertWidth');
            var alertHeight = document.getElementById('alertHeight');
            var resizable = document.getElementById('surveyResizable');
            var position_fieldset = document.getElementById('position_fieldset');
            if (elem && alertWidth && alertHeight) {
                if (msg) msg.style.display = elem.checked ? "none" : "";
                alertWidth.disabled = elem.checked;
                alertHeight.disabled = elem.checked;
                position_fieldset.disabled = elem.checked;
                $("#ticker_position_fieldset").disabled = elem.checked;
                if (resizable) resizable.disabled = elem.checked;
            }
        }

        function check_scheduled() {
            var disabled = !document.getElementById('schedule').checked;
            $("#dateStart").prop('disabled', disabled);
            $("#dateEnd").prop('disabled', disabled);
        }

        function set_default_size() {
            document.getElementById('alertWidth').value = 300;
            document.getElementById('alertHeight').value = 200;
            document.getElementById('size_message').innerHTML = "";
        }

    </script>
</head>
<body>
    <form runat="server" id="survey_settings" style="width: inherit;" method="POST" action="EditQuestions.aspx">
        <div>
            <div id="skins_tiles_dialog" title="<%=resources.LNG_SELECT_SKIN %>" style='display: none'>
                <asp:Literal ID="skinContent" runat="server" Text="Label"></asp:Literal>
            </div>
        </div>
        <table width="100%" border="0" cellspacing="0" cellpadding="6" height="100%">
            <tr>
                <td>
                    <table width="100%" height="100%" cellspacing="0" cellpadding="0" class="main_table">
                        <tr>
                            <td width="100%" height="31" class="main_table_title">
                                <img src="images/menu_icons/survey_20.png" alt="" width="20" height="20" border="0" style="padding-left: 7px">
                                <a href="addSurvey.aspx" class="header_title" style="position: absolute; top: 22px">
                                    <asp:Literal ID="surveyQuizzesPolls" runat="server" Text="Label"></asp:Literal>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td height="100%" valign="top" class="main_table_body">
                                <div style="margin: auto; width: 1000px; padding: 10px 15px" id="parent_for_confirm">
                                    <span class="work_header"></span>
                                    <div style="display: table-cell; padding-top: 20px; width: inherit">
                                        <br />
                                        <div align="right">
                                            <a class="delete_button" style="margin-bottom: 20px" onclick="showAlertPreview()" id="button_preview" role="button" aria-disabled="false">
                                                <span class=""><%=resources.LNG_PREVIEW%></span>
                                                <span class=""></span>
                                            </a>
                                            <a class="save_and_next_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-secondary" id="main_buttons_next_bottom" role="button" aria-disabled="false"><span class="ui-button-text"><% =resources.LNG_NEXT %></span><span class="ui-button-icon-secondary ui-icon ui-icon-carat-1-e"></span></a>
                                        </div>
                                        <br />
                                        <br />
                                        <p class="work_header" style="font-size: 17px; margin-top: 0px; margin-bottom: 0px; margin-left: 0px"><% =resources.LNG_TITLE %>:</p>
                                        <div style="display: inline-block; vertical-align: top; margin-top: 25px;">
                                            <textarea id="title_survey" cols="90"></textarea>
                                            <input type="hidden" id="title" name="title" val="" />
                                            <input type="hidden" id="plain_title" name="plain_title" val="" />
                                            <input type="hidden" id="survey_data" name="survey_data" />
                                        </div>
                                        <div class="tile" id="default" style="display: inline-block; vertical-align: top; float: right; margin-right: 0px;">
                                            <div class="hints" style="font-size: 12px; text-align: center; padding-bottom: 2px">
                                                <asp:Literal ID="changeSkinLabel" runat="server" Text="Label"></asp:Literal>
                                            </div>
                                            <div style="font-size: 1px">
                                                <img id="skin_preview_img" src="skins/default/thumbnail.png" />
                                            </div>
                                            <input type="hidden" value="default" runat="server" name="skin_id" id="skin_id" />
                                            <input type="hidden" value="-1" runat="server" name="campaignId" id="campaignId" />
                                            <input type="hidden" runat="server" name="approveAlertId" id="approveAlertId" />
                                        </div>
                                    </div>
                                    <div id="accordion" style="width: 1000px;">
                                        <h3><%  =resources.LNG_SURVEY_OPTIONS %></h3>
                                        <div>
                                            <p>
                                                <input type="radio" name="option" id="classic_survey" checked/><label style="display: inline" for="classic_survey"><% =resources.LNG_SURVEY_CS %></label>
                                                <img border="0" style="display: inline; cursor: pointer; cursor: hand" src="images/help.png" title="Use this if you don't want to add any extra options">
                                            </p>
                                            <p>
                                                <input type="radio" name="option" id="right_wrong" /><label style="display: inline" for="right_wrong"><% =resources.LNG_SURVEY_RW %></label>
                                                <img border="0" style="display: inline; cursor: pointer; cursor: hand" src="images/help.png" title="Use this if you want to run series of questions and match them against predefined correct answers. Results are private (available only to the publishers)">
                                            </p>
                                            <p>
                                                <input type="radio" name="option" id="view_res" /><label style="display: inline" for="view_res"><% =resources.LNG_SURVEY_VR %></label>
                                                <img border="0" style="display: inline; cursor: pointer; cursor: hand" src="images/help.png" title="Use this if you want to collect answers on several questions publicly">
                                            </p>
                                            <p>
                                                <input type="checkbox" value="1" name="cant_close" id="cant_close" checked /><label style="display: inline" for="cant_close"><% =resources.LNG_SURVEY_CC %></label>
                                                <img border="0" style="display: inline; cursor: pointer; cursor: hand" src="images/help.png" title="Recipient will not be able to close survey manually">
                                            </p>
                                            <p>
                                                <input type="checkbox" value="1" name="anonymous_survey" id="anonymous_survey" /><label style="display: inline" for="anonymous_survey"><% =resources.LNG_ANONYMOUS_SURVEY %></label>
                                                <img border="0" style="display: inline; cursor: pointer; cursor: hand" src="images/help.png" title="Survey report will only display a number of users who have answered the question in a certain way, without revealing exact user accounts list.">
                                            </p>
                                            <input type="hidden" value="1" name="stype" id="stype" />
                                        </div>
                                        <h3><% =resources.LNG_SCHEDULE_SURVEY %></h3>
                                        <div>
                                            <p>
                                                <input type="checkbox" id="lifetimemode" runat="server" name="lifetimemode" value="1" checked="checked" onclick="check_lifetime();" />
                                                <label for="lifetimemode">
                                                    <% =resources.LNG_LIFETIME_IN %></label>

                                                <input type="hidden" runat="server" id="alertId" name="alertId" />
                                                <input type='text' id='lifetime' runat="server" name='lifetime' style='width: 50px' value='1' onkeypress='return check_size_input(this, event);' />&nbsp;
		                                        <select id='lifetime_factor' runat="server" name='lifetime_factor'>
                                                </select>
                                                &nbsp;
                                                <label id ="LifetimeAdditionalDescription" runat="server"></label>
                                            </p>
                                            <p>
                                                <input type="checkbox" id="schedule" name="schedule" value="1" onclick="check_scheduled();" />

                                                <label for="schedule">
                                                    <asp:Literal ID="LNG_SCHEDULE_SURVEY" runat="server"></asp:Literal><% =resources.LNG_SCHEDULE_SURVEY%></label>
                                                <br />
                                                <label style="padding-left: 24px;" for="start_date"><% =resources.LNG_START_DATE%>:</label>
                                                <input class="date_time_param_input" name="start_date" id="dateStart" runat="server" type="text" value="" size="20" disabled />
                                                <input name="start_date_time" id="start_date_time" type="hidden" />
                                                <br />
                                                <br />
                                                <label style="padding-left: 24px;" for="end_date"><% =resources.LNG_END_DATE%>:&nbsp</label>
                                                <input class="date_time_param_input" name="end_date" id="dateEnd" runat="server" type="text" value="" size="20" disabled />
                                                <input name="end_date_time" id="end_date_time" type="hidden" />
                                            </p>
                                        </div>
                                        <h3><% =resources.LNG_SURVEY_APPER_ACC %></h3>
                                        <div>
                                            <div id="size_message">
                                            </div>
                                            <table border="0" cellspacing="0" cellpadding="0" style="width: auto;">
                                                <tr>
                                                    <td valign="top">
                                                        <fieldset class="reccurence_pattern" style="height: 120px">
                                                            <legend><%=resources.LNG_SIZE %></legend>

                                                            <div id="Div1" style="padding-left: 10px;">
                                                                <table border="0">
                                                                    <tr>
                                                                        <td valign="middle">
                                                                            <input type="radio" runat="server" id="size1" name="fullscreen" value="0" onclick="check_message()" />
                                                                        </td>

                                                                        <td valign="middle"><%=resources.LNG_WIDTH %>:
                                                                        </td>
                                                                        <td valign="middle">
                                                                            <input type="text" runat="server" name="alert_width" id="alertWidth" size="4" value="500" onkeypress="return check_size_input(this, event);" onkeyup="check_value(this)" />
                                                                        </td>
                                                                        <td valign="middle">
                                                                            <%=resources.LNG_PX %>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>&nbsp;
                                                                        </td>
                                                                        <td valign="middle"><%=resources.LNG_HEIGHT %>:
                                                                        </td>
                                                                        <td valign="middle">
                                                                            <input type="text" size="4" runat="server" name="alert_height" onkeypress="return check_size_input(this, event);" onkeyup="check_value(this)" id="alertHeight" value="400" />
                                                                        </td>
                                                                        <td valign="middle">
                                                                            <%=resources.LNG_PX %>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>&nbsp;
                                                                        </td>
                                                                        <td valign="middle" colspan="3">
                                                                            <input type="checkbox" runat="server" id="surveyResizable" name="survey_resizable" value="1" />
                                                                            <label runat="server" id="surveyResizableLabel" for="surveyResizable">
                                                                                <%=resources.LNG_RESIZABLE %></label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="middle">
                                                                            <input type="radio" runat="server" id="size2" name="fullscreen" value="1" onclick="check_message()" />
                                                                        </td>
                                                                        <td valign="middle" colspan="3">
                                                                            <label for="size2">
                                                                                <%=resources.LNG_FULLSCREEN %></label><br />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </fieldset>
                                                    </td>
                                                    <td>
                                                        <fieldset id="position_fieldset" class="reccurence_pattern" style="height: 105px; margin-left: 20px;">
                                                            <legend><%=resources.LNG_POSITION %></legend>
                                                            <div id="position_div">
                                                                <table border="0" cellspacing="0" cellpadding="5" width="100%">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td valign="middle" style="text-align: center">
                                                                                <input type="radio" runat="server" id="positionTopLeft" name="position" value="2" />
                                                                            </td>
                                                                            <td></td>
                                                                            <td valign="middle" style="text-align: center">
                                                                                <input type="radio" runat="server" id="positionTopRight" name="position" value="3" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td></td>
                                                                            <td valign="middle" style="text-align: center">
                                                                                <input type="radio" runat="server" id="positionCenter" name="position" value="4" />
                                                                            </td>
                                                                            <td></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign="middle" style="text-align: center">
                                                                                <input type="radio" runat="server" id="positionBottomLeft" name="position" value="5" />
                                                                            </td>
                                                                            <td></td>
                                                                            <td valign="middle" style="text-align: center">
                                                                                <input type="radio" runat="server" id="positionBottomRight" name="position" value="6" checked="" />
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </fieldset>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <br />
                                    <div align="right">
                                        <a class="delete_button" style="margin-bottom: 20px" onclick="showAlertPreview()" id="preview_button" role="button" aria-disabled="false">
                                            <span class=""><%=resources.LNG_PREVIEW%></span>
                                            <span class=""></span>
                                        </a>
                                        <a class="save_and_next_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-secondary" style="margin-bottom: 20px;" id="A1" role="button" aria-disabled="false"><span class="ui-button-text"><%=resources.LNG_NEXT %></span><span class="ui-button-icon-secondary ui-icon ui-icon-carat-1-e"></span></a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
