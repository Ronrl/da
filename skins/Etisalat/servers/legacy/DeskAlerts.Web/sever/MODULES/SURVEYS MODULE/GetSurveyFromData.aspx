﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GetSurveyFromData.aspx.cs" Inherits="DeskAlertsDotNet.Pages.GetSurveyFromData" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<title>DeskAlerts</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link href="admin/css/jquery/jquery-ui.css" rel="stylesheet" type="text/css" />
		<link href="admin/css/style.css" rel="stylesheet" type="text/css">
		<link href="admin/css/jquery/jquery_timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css">
		<script language="javascript" type="text/javascript" src="admin/jscripts/jquery/jquery.min.js"></script>
		<script language="javascript" type="text/javascript" src="admin/jscripts/jquery/jquery-ui.min.js"></script>
		<script language="javascript" type="text/javascript" src="admin/jscripts/jquery/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/JavaScript" language="JavaScript" src="admin/jscripts/json2.js"></script>
		<script language="javascript" type="text/javascript" src="admin/jscripts/preview.js?v=9.2.1.31"></script>
		<script language="javascript" type="text/javascript" src="admin/functions.asp"></script>
		<script language="javascript" type="text/javascript" src="admin/jscripts/date.js"></script>
	 
		<script language="javascript" type="text/javascript" src="admin/jscripts/jquery/jquery_timepicker/jquery-ui-timepicker-addon.js"></script>
		<script language="javascript" type="text/javascript" src="admin/jscripts/jquery/datepicker_fix.js"></script>
		
		
	    <script language="javascript" type="text/javascript" src="admin/jscripts/tiny_mce/tinymce.min.js"></script>

    
    
		<style rel="stylesheet" type="text/css">  
		.tile
		{
			margin: 10px; 
			padding: 10px;
			width: 200px;
		}
		
		#skins_tiles_dialog .tile
		{
			display: inline-block;
		}
		#default
		{
			margin-top:0px;
		}
		 .save_and_next_button
		 {
			float:right;
		 }        
		</style>
	
		
		<script language="javascript" type="text/javascript">
		$(document).ready(function() {
			$( "#accordion" ).accordion();
		
			var skinsDialog = $("#skins_tiles_dialog").dialog({
				position: {my: "left top", at: "left top", of: "#dialog_dock"},
				modal: true,
				resizable: false,
				draggable: false,
				width: 'auto',
				resize: 'auto',
				margin:'auto',
				bgiframe: true,
				autoOpen: false,
				close: function(){ closeSkinsDialog() }
			});
	
			$('#default').click(function(){
				skinsDialog.dialog('open');
			});
			
			function closeSkinsDialog()
			{
				skinsDialog.dialog('close');
			}
			
			
			$('.tile').hover(function(){
				$(this).switchClass("","tile_hovered",200);
			},function()
			{
				$(this).switchClass("tile_hovered","",200);
			}).click(function(){
				var src = $(this).find('img').eq(0).attr('src');
				var tmp_arr = src.split('/');
				var id = tmp_arr[1];
				$("#skin_id").val(id);
				var src_tumb = $("#skin_preview_img").attr('src');
				var src_tumb_elems = src_tumb.split('/');
				var new_src_tumb = src_tumb_elems[0]+"/"+id+"/"+src_tumb_elems[2];
				$("#skin_preview_img").attr('src',new_src_tumb);
				closeSkinsDialog();
			});
			
			$('.save_and_next_button').click(function()
			{
				var ready = checkForm();
				if (!ready) return;
				
				var quiz = $('#right_wrong').is( ":checked" );
				var pol = $('#view_res').is( ":checked" );
		
				if(quiz) $('#stype').val(2);
				if(pol) $('#stype').val(3);
				
				$("#title").val(encodeURI(tinyMCE.get("title_survey").getContent()));
				
				var plain_text = $('<div/>').html(tinyMCE.get("title_survey").getContent()).text();
				
				$('#plain_title').val(plain_text);
				
				
				$("#survey_settings").submit();				
			
			});
			
			$('.end_questions').click(function(){
				
			});
			
			var newTinySettings = {
				"theme_advanced_buttons1":"bold,italic,underline,|,formatselect,fontselect,fontsizeselect,|,forecolor,backcolor",
				"theme_advanced_buttons2":"",
				"theme_advanced_buttons3":""
			};
			
			//initTinyMCE("en", "exact", "title_survey",null,null,newTinySettings);
			tinyMCE.baseURL = window.location.href.substring(0, window.location.href.lastIndexOf("/")) + "/jscripts/tiny_mce/";
			tinymce.init({
				selector: "textarea#title_survey",
				plugins: "autoresize moxiemanager textcolor link code emoticons insertdatetime fullscreen imagetools directionality table autolink",
				toolbar: "bold italic underline strikethrough | fontselect fontsizeselect | forecolor backcolor | link unlink | insertfile code | ltr rtl",
				menubar: false,
				statusbar: false,
				autoresize_bottom_margin : 0,
				autoresize_min_height : 20,
				autoresize_max_height : 80,
				fontsize_formats: '8pt 10pt 11pt 12pt 14pt 16pt 18pt',
				forced_root_block : false,
                valid_elements: "span[*],font[*],i[*],b[*],u[*],strong[*],s[*],em[*]",
                content_css : '/admin/jscripts/tiny_mce/themes/advanced/fonts/tinymce-fonts.css',
				font_formats: 'Neo Tech Alt=Neo Tech Alt,Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Calibri=calibri;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Tahoma=tahoma,arial,helvetica,sans-serif;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats',
                branding: false
			});
			
			$('#right_wrong').click(function(){
				if ($(this).prop("checked")) $('#view_res').prop("checked",false);
			});
			
			$('#view_res').click(function(){
				if ($(this).prop("checked")) $('#right_wrong').prop("checked",false);
			});
			
		
		});     
		function checkForm()
		{
			var result = true;
			var al_value = tinyMCE.get("title_survey").getContent();
			var plain_text = $('<div/>').html(tinyMCE.get("title_survey").getContent()).text();
			if(plain_text.length < 3) 
			{
				result = false;
				alert("Title must contain more than 3 symbols");
				return result; 
			}
			return result; 	
		}
		function check_scheduled() 
		{
			var disabled = !document.getElementById('schedule').checked;
			if (disabled) {
				document.getElementById("schedule_div").style.display = "none";
			}
			else {
				document.getElementById("schedule_div").style.display = "";
			}
		}
		
	</script>
	</head>

	

	<body style="margin:0" class="body">
	       <div id="skin" runat="server">
	       </div>
	</body>
</html>