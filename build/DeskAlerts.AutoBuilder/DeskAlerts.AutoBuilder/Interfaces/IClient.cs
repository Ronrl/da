﻿using System;

namespace DeskAlerts.AutoBuilder.Interfaces
{
    public interface IClient
    {
        string CompanyName { get; set; }
        DateTime ContractDate { get; set; }
        DateTime LicenseDate { get; set; }
        string Version { get; set; }
        int LicensesCount { get; set; }
        int PublishersCount { get; set; }
        bool SingleInstance { get; set; }
        int Trial { get; set; }
        bool Demo { get; set; }
        IModules Modules { get; set; }
    }
}
