﻿namespace DeskAlerts.AutoBuilder.Interfaces
{
    public interface IModules
    {
        bool Encryption { get; set; }
        bool ActiveDirectory { get; set; }
        bool SingleSignOn { get; set; }
        bool VideoAlert { get; set; }
        bool EMail { get; set; }
        bool Twitter { get; set; }
        bool LinkedIn { get; set; }
        bool Yammer { get; set; }
        bool TextToCall { get; set; }
        bool Ticker { get; set; }
        bool ScreenSaver { get; set; }
        bool Wallpaper { get; set; }
        bool Lockscreen { get; set; }
        bool Rss { get; set; }
        bool InstantMessenger { get; set; }
        bool Emergency { get; set; }
        bool Fullscreen { get; set; }
        bool MobileSendingModule { get; set; }
        bool DigitalSignage { get; set; }
        bool ExtendedStatistics { get; set; }
        bool Survey { get; set; }
        bool SmppSms { get; set; }
        bool Sms { get; set; }
        bool Campaigns { get; set; }
        bool MultipleAlerts { get; set; }
        bool MultiLanguageAlerts { get; set; }
        bool RestApi { get; set; }
        bool Approval { get; set; }
        bool BulkUploadOfRecipients { get; set; }
    }
}
