﻿using System;

namespace DeskAlerts.AutoBuilder.Interfaces
{
    public interface ILogger
    {
        void Fatal(Exception exception, string message = "", bool showToConsole = false);

        void Fatal(string message = "", bool showToConsole = false);

        void Error(Exception exception, string message = "", bool showToConsole = false);

        void Error(string message = "", bool showToConsole = false);

        void Warn(Exception exception, string message = "", bool showToConsole = false);

        void Warn(string message = "", bool showToConsole = false);

        void Info(Exception exception, string message = "", bool showToConsole = false);

        void Info(string message = "", bool showToConsole = false);

        void Debug(Exception exception, string message = "", bool showToConsole = false);

        void Debug(string message = "", bool showToConsole = false);

        void Trace(Exception exception, string message = "", bool showToConsole = false);

        void Trace(string message = "", bool showToConsole = false);
    }
}
