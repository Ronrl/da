﻿using System;
using DeskAlerts.AutoBuilder.Interfaces;

namespace DeskAlerts.AutoBuilder.Models
{
    public class Client : IClient
    {
        public Client()
        {
            Modules = new Modules();
        }

        public string CompanyName { get; set; }
        public DateTime ContractDate { get; set; }
        public DateTime LicenseDate { get; set; }
        public string Version { get; set; }
        public int LicensesCount { get; set; }
        public int PublishersCount { get; set; }
        public bool SingleInstance { get; set; }
        public int Trial { get; set; }
        public bool Demo { get; set; }
        public IModules Modules { get; set; }
    }
}
