﻿using DeskAlerts.AutoBuilder.Interfaces;

namespace DeskAlerts.AutoBuilder.Models
{
    public class Modules : IModules
    {
        public bool Encryption { get; set; }
        public bool ActiveDirectory { get; set; }
        public bool SingleSignOn { get; set; }
        public bool VideoAlert { get; set; }
        public bool EMail { get; set; }
        public bool Twitter { get; set; }
        public bool LinkedIn { get; set; }
        public bool Yammer { get; set; }
        public bool TextToCall { get; set; }
        public bool Ticker { get; set; }
        public bool ScreenSaver { get; set; }
        public bool Wallpaper { get; set; }
        public bool Lockscreen { get; set; }
        public bool Rss { get; set; }
        public bool InstantMessenger { get; set; }
        public bool Emergency { get; set; }
        public bool Fullscreen { get; set; }
        public bool MobileSendingModule { get; set; }
        public bool DigitalSignage { get; set; }
        public bool ExtendedStatistics { get; set; }
        public bool Survey { get; set; }
        public bool SmppSms { get; set; }
        public bool Sms { get; set; }
        public bool Campaigns { get; set; }
        public bool MultipleAlerts { get; set; }
        public bool MultiLanguageAlerts { get; set; }
        public bool RestApi { get; set; }
        public bool Approval { get; set; }
        public bool BulkUploadOfRecipients { get; set; }
    }
}
