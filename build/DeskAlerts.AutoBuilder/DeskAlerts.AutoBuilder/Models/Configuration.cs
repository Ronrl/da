﻿using System.Configuration;

namespace DeskAlerts.AutoBuilder.Models
{
    public class Configuration
    {
        public static string OutputDirectoryPath { get; set; } = ConfigurationManager.AppSettings["OutputDirectoryPath"];
        public static string ServerDirectoryPath { get; set; } = ConfigurationManager.AppSettings["ServerDirectoryPath"];
        public static string CustomersDirectoryPath { get; set; } = ConfigurationManager.AppSettings["CustomersDirectoryPath"];
        public static string ProductVersionPrefix { get; set; } = ConfigurationManager.AppSettings["ProductVersionPrefix"];
        public static string DeveloperEnvironment { get; set; } = ConfigurationManager.AppSettings["DeveloperEnvironment"];
    }
}
