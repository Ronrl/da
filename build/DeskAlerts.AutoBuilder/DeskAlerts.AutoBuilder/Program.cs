﻿using DeskAlerts.AutoBuilder.Controllers;
using System;

namespace DeskAlerts.AutoBuilder
{
    public class Program
    {
        private static readonly Logger Logger = new Logger();

        static void Main(string[] args)
        {
            InitConsoleSettings();

            if (!Utils.CheckApplicationConfig())
            {
                ChangeConsoleColorError();
                Console.WriteLine("Please set application configuration in \"DeskAlerts.AutoBuilder.exe.config\" file.");
                ChangeConsoleColorReset();
                Environment.Exit(0);
            }

            try
            {
                var companyName = CheckArguments(args);

                if (companyName.Equals("template"))
                {
                    Utils.CreateTemplate();
                    ChangeConsoleColorOk();
                    Console.WriteLine("\nTemplate was created in the output directory.\n");
                    ChangeConsoleColorReset();
                    Environment.Exit(0);
                }

                if (!string.IsNullOrEmpty(companyName))
                {
                    PackageBuilder.CreatePackage(Utils.GetCustomersPath(companyName));
                }
                else
                {
                    var companyList = Utils.GetCompanyList();

                    foreach (var company in companyList)
                    {
                        PackageBuilder.CreatePackage(Utils.GetCustomersPath(company));
                    }
                }

                ChangeConsoleColorOk();
                Logger.Info("Builder completed work.", true);
                ChangeConsoleColorReset();
            }
            catch (Exception ex)
            {
                ChangeConsoleColorError();
                Logger.Error($"{ex.Message}:\r\n\t{ex}", true);
                ChangeConsoleColorReset();
                throw;
            }
            finally
            {
                Git.ClearSolutions();
            }
        }

        public static string CheckArguments(string[] args)
        {
            if (args == null || args.Length < 1 || string.IsNullOrEmpty(args[0]))
            {
                return string.Empty;
            }

            return args[0];
        }

        public static void ChangeConsoleColorOk()
        {
            Console.ForegroundColor = ConsoleColor.Green;
        }

        public static void ChangeConsoleColorError()
        {
            Console.ForegroundColor = ConsoleColor.Red;
        }

        public static void ChangeConsoleColorReset()
        {
            Console.ResetColor();
        }

        private static void InitConsoleSettings()
        {
            Console.Title = "DeskAlerts.AutoBuilder";
        }
    }
}
