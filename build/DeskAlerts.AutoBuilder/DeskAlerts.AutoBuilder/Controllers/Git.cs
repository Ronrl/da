﻿using DeskAlerts.AutoBuilder.Models;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace DeskAlerts.AutoBuilder.Controllers
{
    public static class Git
    {
        private static readonly Logger Logger = new Logger();

        public static string FindGitApplication()
        {
            var systemEnvironmentVariables = Environment.GetEnvironmentVariable("Path")
                ?.Split(';');

            var gitEnvironmentPath = systemEnvironmentVariables
                ?.Where(x => x.Contains(@"\Git\"))
                .Select(x => x)
                .FirstOrDefault();

            if (string.IsNullOrEmpty(gitEnvironmentPath))
            {
                const string message = "Git not found is system.";
                Logger.Fatal(message, true);
                throw new Exception(message);
            }

            var gitPath = Path.Combine(gitEnvironmentPath, "git.exe");
            var result = File.Exists(gitPath)
                ? gitPath
                : string.Empty;

            return result;
        }

        public static void ClearSolutions()
        {
            ClearSolution(Path.GetFullPath(Configuration.ServerDirectoryPath));
            ClearSolution(Path.Combine(Path.GetFullPath(Configuration.ServerDirectoryPath), "NewInstaller"));
        }

        public static void ClearSolution(string workingDirectory)
        {
            if (string.IsNullOrEmpty(workingDirectory))
            {
                throw new ArgumentNullException(nameof(workingDirectory));
            }

            var gitApplication = FindGitApplication();

            var process = new Process
            {
                StartInfo =
                {
                    FileName = "cmd.exe",
                    CreateNoWindow = true,
                    RedirectStandardInput = true,
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    WorkingDirectory = workingDirectory
                }
            };
            process.Start();
            process.StandardInput.WriteLine($"\"{gitApplication}\" reset --hard");
            process.StandardInput.WriteLine($"\"{gitApplication}\" clean -d -f -q");
            process.StandardInput.Flush();
            process.StandardInput.Close();
            Console.WriteLine(process.StandardOutput.ReadToEnd());
        }

        public static string GetGitCommitHash()
        {
            const string command = "rev-parse HEAD";

            var result = Utils.ExecuteCommand(FindGitApplication(), command)
                .Trim()
                .TrimEnd(new char[] { '\n', '\r' });

            return result;
        }

        public static void ApplyGitPatch(string companyName)
        {
            if (string.IsNullOrEmpty(companyName))
            {
                throw new ArgumentNullException(nameof(companyName));
            }

            const string customerPatchesPattern = "CustomerPatchFile*.patch";

            var patchesFiles = new DirectoryInfo(companyName)
                .GetFiles(customerPatchesPattern)
                .OrderBy(x=>x.CreationTime)
                .ThenBy(x=>x.Name)
                .ToList();
            if (!patchesFiles.Any())
            {
                return;
            }

            foreach (var patchesFile in patchesFiles)
            {
                ExecuteGitPatch(patchesFile.FullName);
            }
        }

        public static void ExecuteGitPatch(string patchName)
        {
            var gitApplication = FindGitApplication();

            var process = new Process
            {
                StartInfo =
                {
                    FileName = "cmd.exe",
                    CreateNoWindow = true,
                    RedirectStandardInput = true,
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    WorkingDirectory = Configuration.ServerDirectoryPath
                }
            };
            process.Start();
            process.StandardInput.WriteLine($"\"{gitApplication}\" am < {patchName}");
            process.StandardInput.Flush();
            process.StandardInput.Close();
            Console.WriteLine(process.StandardOutput.ReadToEnd());
        }
    }
}
