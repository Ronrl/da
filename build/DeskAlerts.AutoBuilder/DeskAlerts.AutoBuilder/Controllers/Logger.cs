﻿using System;
using NLog;

namespace DeskAlerts.AutoBuilder.Controllers
{
    public class Logger : Interfaces.ILogger
    {
        private static readonly NLog.Logger NLog = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Writes the diagnostic message and exception at the <c>Fatal</c> level.
        /// </summary>
        /// <param name="exception">An exception to be logged.</param>
        /// <param name="message">A <see langword="string" /> to be written.</param>
        /// <param name="showToConsole">Checked if you want show message to console window.</param>
        public void Fatal(Exception exception, string message = "", bool showToConsole = false)
        {
            if (exception == null || string.IsNullOrEmpty(message))
            {
                return;
            }

            if (showToConsole)
            {
                Console.WriteLine(message);
            }

            NLog.Fatal(exception, message);
        }

        /// <summary>
        /// Writes the diagnostic message and exception at the <c>Fatal</c> level.
        /// </summary>
        /// <param name="message">A <see langword="string" /> to be written.</param>
        /// <param name="showToConsole">Checked if you want show message to console window.</param>
        public void Fatal(string message = "", bool showToConsole = false)
        {
            if (string.IsNullOrEmpty(message))
            {
                return;
            }

            if (showToConsole)
            {
                Console.WriteLine(message);
            }

            NLog.Fatal(message);
        }

        /// <summary>
        /// Writes the diagnostic message and exception at the <c>Error</c> level.
        /// </summary>
        /// <param name="exception">An exception to be logged.</param>
        /// <param name="message">A <see langword="string" /> to be written.</param>
        /// <param name="showToConsole">Checked if you want show message to console window.</param>
        public void Error(Exception exception, string message = "", bool showToConsole = false)
        {
            if (exception == null || string.IsNullOrEmpty(message))
            {
                return;
            }

            if (showToConsole)
            {
                Console.WriteLine(message);
            }

            NLog.Error(exception, message);
        }

        /// <summary>
        /// Writes the diagnostic message and exception at the <c>Error</c> level.
        /// </summary>
        /// <param name="message">A <see langword="string" /> to be written.</param>
        /// <param name="showToConsole">Checked if you want show message to console window.</param>
        public void Error(string message = "", bool showToConsole = false)
        {
            if (string.IsNullOrEmpty(message))
            {
                return;
            }

            if (showToConsole)
            {
                Console.WriteLine(message);
            }

            NLog.Error(message);
        }

        /// <summary>
        /// Writes the diagnostic message and exception at the <c>Warn</c> level.
        /// </summary>
        /// <param name="exception">An exception to be logged.</param>
        /// <param name="message">A <see langword="string" /> to be written.</param>
        /// <param name="showToConsole">Checked if you want show message to console window.</param>
        public void Warn(Exception exception, string message = "", bool showToConsole = false)
        {
            if (exception == null || string.IsNullOrEmpty(message))
            {
                return;
            }

            if (showToConsole)
            {
                Console.WriteLine(message);
            }

            NLog.Warn(exception, message);
        }

        /// <summary>
        /// Writes the diagnostic message and exception at the <c>Warn</c> level.
        /// </summary>
        /// <param name="message">A <see langword="string" /> to be written.</param>
        /// <param name="showToConsole">Checked if you want show message to console window.</param>
        public void Warn(string message = "", bool showToConsole = false)
        {
            if (string.IsNullOrEmpty(message))
            {
                return;
            }

            if (showToConsole)
            {
                Console.WriteLine(message);
            }

            NLog.Warn(message);
        }

        /// <summary>
        /// Writes the diagnostic message and exception at the <c>Info</c> level.
        /// </summary>
        /// <param name="exception">An exception to be logged.</param>
        /// <param name="message">A <see langword="string" /> to be written.</param>
        /// <param name="showToConsole">Checked if you want show message to console window.</param>
        public void Info(Exception exception, string message = "", bool showToConsole = false)
        {
            if (exception == null || string.IsNullOrEmpty(message))
            {
                return;
            }

            if (showToConsole)
            {
                Console.WriteLine(message);
            }

            NLog.Info(exception, message);
        }

        /// <summary>
        /// Writes the diagnostic message and exception at the <c>Info</c> level.
        /// </summary>
        /// <param name="message">A <see langword="string" /> to be written.</param>
        /// <param name="showToConsole">Checked if you want show message to console window.</param>
        public void Info(string message = "", bool showToConsole = false)
        {
            if (string.IsNullOrEmpty(message))
            {
                return;
            }

            if (showToConsole)
            {
                Console.WriteLine(message);
            }

            NLog.Info(message);
        }

        /// <summary>
        /// Writes the diagnostic message and exception at the <c>Debug</c> level.
        /// </summary>
        /// <param name="exception">An exception to be logged.</param>
        /// <param name="message">A <see langword="string" /> to be written.</param>
        /// <param name="showToConsole">Checked if you want show message to console window.</param>
        public void Debug(Exception exception, string message = "", bool showToConsole = false)
        {
            if (exception == null || string.IsNullOrEmpty(message))
            {
                return;
            }

            if (showToConsole)
            {
                Console.WriteLine(message);
            }

            NLog.Debug(exception, message);
        }

        /// <summary>
        /// Writes the diagnostic message and exception at the <c>Debug</c> level.
        /// </summary>
        /// <param name="message">A <see langword="string" /> to be written.</param>
        /// <param name="showToConsole">Checked if you want show message to console window.</param>
        public void Debug(string message = "", bool showToConsole = false)
        {
            if (string.IsNullOrEmpty(message))
            {
                return;
            }

            if (showToConsole)
            {
                Console.WriteLine(message);
            }

            NLog.Debug(message);
        }

        /// <summary>
        /// Writes the diagnostic message and exception at the <c>Trace</c> level.
        /// </summary>
        /// <param name="exception">An exception to be logged.</param>
        /// <param name="message">A <see langword="string" /> to be written.</param>
        /// <param name="showToConsole">Checked if you want show message to console window.</param>
        public void Trace(Exception exception, string message = "", bool showToConsole = false)
        {
            if (exception == null || string.IsNullOrEmpty(message))
            {
                return;
            }

            if (showToConsole)
            {
                Console.WriteLine(message);
            }

            NLog.Trace(exception, message);
        }

        /// <summary>
        /// Writes the diagnostic message and exception at the <c>Trace</c> level.
        /// </summary>
        /// <param name="message">A <see langword="string" /> to be written.</param>
        /// <param name="showToConsole">Checked if you want show message to console window.</param>
        public void Trace(string message = "", bool showToConsole = false)
        {
            if (string.IsNullOrEmpty(message))
            {
                return;
            }

            if (showToConsole)
            {
                Console.WriteLine(message);
            }

            NLog.Trace(message);
        }
    }
}
