﻿using DeskAlerts.AutoBuilder.Dto;
using DeskAlerts.AutoBuilder.Interfaces;
using DeskAlerts.AutoBuilder.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Text;

namespace DeskAlerts.AutoBuilder.Controllers
{
    public class PackageBuilder
    {
        private static readonly Logger Logger = new Logger();
        public const string CustomerConfigurationFile = "CustomerConfigurationFile.json";
        private static readonly string ServerDistributionFileName = Path.Combine(Path.GetFullPath(Configuration.ServerDirectoryPath), "ServerInstallation.exe");

        private static BuilderParametersDto _builderParametersDto;

        public static void CreatePackage(string companyName)
        {
            if (string.IsNullOrEmpty(companyName))
            {
                Program.ChangeConsoleColorError();
                Console.WriteLine($"File \"{CustomerConfigurationFile}\" not found.");
                return;
            }

            Program.ChangeConsoleColorOk();
            Logger.Info($"{DateTime.Now:yyyy-MM-dd hh:mm:ss} Build \"{companyName}\" is started.", true);
            Program.ChangeConsoleColorReset();

            Git.ClearSolutions();
            var configurationFileCount = new DirectoryInfo(companyName).GetFiles(CustomerConfigurationFile).Length == 1;
            if (!configurationFileCount)
            {
                Logger.Info($"File \"{Path.Combine(companyName, CustomerConfigurationFile)}\" doesn't found.");
                return;
            }

            var clientJson = ParseClientInformation(companyName);
            _builderParametersDto = new BuilderParametersDto(clientJson);
            
            Git.ApplyGitPatch(companyName);
            BuildServerPackage(clientJson);
            Git.ClearSolutions();

            Program.ChangeConsoleColorOk();
            Logger.Info($"{DateTime.Now:yyyy-MM-dd hh:mm:ss} Build \"{companyName}\" is finished.", true);
            Program.ChangeConsoleColorReset();
        }

        public static IClient ParseClientInformation(string companyName)
        {
            if (string.IsNullOrEmpty(companyName))
            {
                Logger.Info($"Parameter \"{nameof(companyName)}\" is null.");
                throw new ArgumentNullException(nameof(companyName));
            }

            var configurationFileName = Path.Combine(companyName, CustomerConfigurationFile);

            if (!File.Exists(configurationFileName))
            {
                var message = $"File \"{configurationFileName}\" doesn't found.";
                Logger.Info(message);
                throw new IOException(message);
            }

            var json = JObject.Parse(File.ReadAllText(configurationFileName));

            json = ReplaceStringToCorrectDatetime(json, "ContractDate");
            json = ReplaceStringToCorrectDatetime(json, "LicenseDate");

            var result = JsonConvert.DeserializeObject<Client>(json.ToString());

            return result;
        }

        public static JObject ReplaceStringToCorrectDatetime(JObject json, string dateType)
        {
            if (json == null || string.IsNullOrEmpty(dateType))
            {
                Logger.Info($"Parameter \"{json}\" or \"{dateType}\" is null.");
                throw new ArgumentNullException($"\"{nameof(json)}\" or \"{nameof(dateType)}\".");
            }

            var dateAsString = json.SelectToken(dateType).Value<string>();
            json[dateType] = DateTime.Parse(dateAsString).ToString("O");

            return json;
        }
        
        public static void BuildServerPackage(IClient clientConfiguration)
        {
            var serverDirectoryPath = Path.GetFullPath(Configuration.ServerDirectoryPath);

            var nugetInterpreter = Path.Combine(serverDirectoryPath, @".nuget\nuget.exe");
            var developerEnvironment = Configuration.DeveloperEnvironment;
            IsExistDeveloperEnvironment(developerEnvironment);

            #region restore nuget packages
            var restoreNugetPackagesInServer = $@"restore {Path.Combine(serverDirectoryPath, @"DeskAlerts.Web\DeskAlerts.Web.csproj")} -PackagesDirectory packages";
            Console.WriteLine(Utils.ExecuteCommand(nugetInterpreter, restoreNugetPackagesInServer));

            var restoreNugetPackagesInInstaller = $@"restore {Path.Combine(serverDirectoryPath, @"NewInstaller\NewInstaller.sln")} -PackagesDirectory {Path.Combine(serverDirectoryPath, @"NewInstaller\packages")}";
            Console.WriteLine(Utils.ExecuteCommand(nugetInterpreter, restoreNugetPackagesInInstaller));
            #endregion

            #region build server
            var rebuildDaServerSln = $"devenv \"{Path.Combine(serverDirectoryPath, "DeskAlerts.Server.sln")}\" /rebuild \"Release 9|AnyCPU\"";
            Console.WriteLine(Utils.ExecuteCommand(developerEnvironment, rebuildDaServerSln));
            #endregion

            #region Update database scripts

            UpdateDatabaseDate("Custom_contract_date.sql", "contract_date", clientConfiguration.ContractDate);
            UpdateDatabaseDate("Set_license_expire_date.sql", "license_expire_date", clientConfiguration.LicenseDate);

            #endregion

            #region Delete old files

            DeleteExistsFiles(@"NewInstaller\InstallertInterface\Resources\Attributes.txt");
            DeleteExistsFiles(@"NewInstaller\Msi\Attributes.txt");
            DeleteExistsFiles(@"NewInstaller\Msi\setup.msi");
            DeleteExistsFiles(@"NewInstaller\InstallertInterface\bin\Release\WpfSetup.exe");

            #endregion

            #region Add installer parameters
            
            WriteToBuilderFile(@"NewInstaller\Msi\Attributes.txt", _builderParametersDto.ToString(), false, true);
            WriteToBuilderFile(@"NewInstaller\InstallertInterface\Resources\Attributes.txt", _builderParametersDto.ToString(), false, true);
            
            #endregion

            #region installer
            
            var rebuildNewInstallerSln = $"devenv \"{Path.Combine(serverDirectoryPath, @"NewInstaller\NewInstaller.sln")}\" /rebuild \"Release|Any CPU\"";
            Console.WriteLine(Utils.ExecuteCommand(developerEnvironment, rebuildNewInstallerSln));

            CopyFiles(Path.Combine(serverDirectoryPath, @"NewInstaller\InstallertInterface\bin\Release\WpfSetup.exe"), ServerDistributionFileName);
            
            #endregion

            #region sign tool
            
            var signToolInpterpreter = Path.Combine(serverDirectoryPath, @"installer\signtool.exe");
            var signToolTimeStamp = $@"{Path.Combine(serverDirectoryPath, @"installer\signtool.exe")} sign /tr http://timestamp.comodoca.com/?td=sha256 /td sha256 /fd sha256 /f {Path.Combine(serverDirectoryPath, @"installer\CodeSignCertificate.pfx")} /p DeskAlerts /v {ServerDistributionFileName}";
            Console.WriteLine(Utils.ExecuteCommand(signToolInpterpreter, signToolTimeStamp));
            
            #endregion

            RenameServerDistribution(clientConfiguration.SingleInstance, clientConfiguration.CompanyName);
        }

        public static void IsExistDeveloperEnvironment(string environment)
        {
            if (string.IsNullOrEmpty(environment))
            {
                throw new ArgumentNullException(nameof(environment));
            }

            if (!File.Exists(environment))
            {
                throw new FileNotFoundException(environment);
            }
        }

        private static void CopyFiles(string sourceFileName, string destinationFileName)
        {
            if (string.IsNullOrEmpty(sourceFileName) || string.IsNullOrEmpty(destinationFileName))
            {
                throw new ArgumentNullException();
            }

            if (!File.Exists(sourceFileName))
            {
                throw new FileNotFoundException(sourceFileName);
            }

            if (File.Exists(destinationFileName))
            {
                File.Delete(destinationFileName);
            }

            File.Copy(sourceFileName, destinationFileName, true);
        }

        private static void DeleteExistsFiles(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                throw new ArgumentNullException(nameof(fileName));
            }

            var fullFilePath = Path.Combine(Path.GetFullPath(Configuration.ServerDirectoryPath), fileName);

            if (File.Exists(fullFilePath))
            {
                File.Delete(fullFilePath);
            }
        }

        public static void WriteToBuilderFile(string fileName, string data, bool appendText = true, bool createNewFile = false)
        {
            if (string.IsNullOrEmpty(data))
            {
                throw new ArgumentNullException(nameof(data));
            }

            if (string.IsNullOrEmpty(fileName))
            {
                throw new ArgumentNullException(nameof(fileName));
            }

            var fileFullPath = Path.Combine(Path.GetFullPath(Configuration.ServerDirectoryPath), fileName);

            if (File.Exists(fileFullPath) && createNewFile)
            {
                File.Delete(fileFullPath);
            }

            using (var streamWriter = new StreamWriter(fileFullPath, appendText, Encoding.UTF8))
            {
                streamWriter.WriteLine(data);
            }
        }

        public static void UpdateDatabaseDate(string sqlFileName, string field, DateTime date)
        {
            if (string.IsNullOrEmpty(sqlFileName))
            {
                throw new ArgumentNullException(nameof(sqlFileName));
            }

            if (string.IsNullOrEmpty(field))
            {
                throw new ArgumentNullException(nameof(field));
            }

            var startDate = DateTime.Now.AddDays(-7);
            var endDate = new DateTime(3000, 1, 1);

            if (date < startDate || date > endDate)
            {
                var logMessage = $"Date \"{field}\" can be between {startDate} and {endDate}.";
                Console.WriteLine(logMessage);
                Git.ClearSolutions();
                Console.WriteLine("Press any key to continue.");
                Console.ReadKey();
                Environment.Exit(1);
            }

            var sqlFilesPath = Path.Combine(Path.GetFullPath(Configuration.ServerDirectoryPath), @"NewInstaller\Msi\SqlScripts");

            if (!Directory.Exists(sqlFilesPath))
            {
                throw new DirectoryNotFoundException($"\"{sqlFilesPath}\" not exist.");
            }

            var sqlFileFullPath = Path.Combine(sqlFilesPath, sqlFileName);

            if (File.Exists(sqlFileFullPath))
            {
                File.Delete(sqlFileFullPath);
            }

            using (var stream = new StreamWriter(sqlFileFullPath, true, Encoding.UTF8))
            {
                stream.WriteLine("USE [%dbname_hook%];");
                stream.WriteLine("SET LANGUAGE british;");
                stream.WriteLine($"UPDATE version SET {field} = cast( \r\n'{date:yyyyMMdd}'\r\nAS datetime)");
            }
        }

        public static void RenameServerDistribution(bool isSingle, string companyName)
        {
            if (!Directory.Exists(Path.GetFullPath(Configuration.OutputDirectoryPath)))
            {
                Directory.CreateDirectory(Path.GetFullPath(Configuration.OutputDirectoryPath));
            }

            var clientOutPutDirectory = Path.Combine(Path.GetFullPath(Configuration.OutputDirectoryPath), companyName);

            if (!Directory.Exists(clientOutPutDirectory))
            {
                Directory.CreateDirectory(clientOutPutDirectory);
            }

            var gitCommitHash = Git.GetGitCommitHash();

            var newServerName = isSingle
                ? $"DeskAlerts.Server.v{_builderParametersDto.ProductVersion}.{gitCommitHash}.{companyName}.exe"
                : $"DeskAlerts.Server.v{_builderParametersDto.ProductVersion}.{gitCommitHash}.{companyName}__MultiInstance.exe";

            var newServerFullName = Path.Combine(clientOutPutDirectory, newServerName);

            if (!File.Exists(ServerDistributionFileName))
            {
                throw new FileNotFoundException(ServerDistributionFileName);
            }

            DeleteExistsFiles(newServerFullName);

            File.Move(ServerDistributionFileName, newServerFullName);
        }
    }
}
