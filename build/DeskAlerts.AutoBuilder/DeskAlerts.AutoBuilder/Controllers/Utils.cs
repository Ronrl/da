﻿using DeskAlerts.AutoBuilder.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace DeskAlerts.AutoBuilder.Controllers
{
    public static class Utils
    {
        private static readonly Logger Logger = new Logger();

        public static void CreateTemplate()
        {
            var clientTemplate = new Client();
            var jsonObject = JsonConvert.SerializeObject(clientTemplate);

            if (!Directory.Exists(Configuration.OutputDirectoryPath))
            {
                Directory.CreateDirectory(Configuration.OutputDirectoryPath);
            }

            var templateFullPath = Path.Combine(Path.GetFullPath(Configuration.OutputDirectoryPath), PackageBuilder.CustomerConfigurationFile);

            if (File.Exists(templateFullPath))
            {
                File.Delete(templateFullPath);
            }

            using (var fileStream = File.Create(templateFullPath))
            {
                var data = new UTF8Encoding(true).GetBytes(jsonObject);
                fileStream.Write(data, 0, data.Length);
            }
        }

        public static string ExecuteCommand(string interpreter, string command)
        {
            var process = new Process();
            var startInfo = new ProcessStartInfo
            {
                WindowStyle = ProcessWindowStyle.Normal,
                FileName = interpreter,
                Arguments = command,
                RedirectStandardInput = false,
                RedirectStandardOutput = true,
                CreateNoWindow = true,
                UseShellExecute = false,
                WorkingDirectory = Path.GetFullPath(Configuration.ServerDirectoryPath)
            };
            process.StartInfo = startInfo;
            process.Start();

            //process.WaitForExit();
            return process.StandardOutput.ReadToEnd();
        }

        public static string GetCustomersPath(string companyName)
        {
            if (string.IsNullOrEmpty(companyName))
            {
                throw new ArgumentNullException(nameof(companyName));
            }

            var clientDirectory = Path.Combine(Path.GetFullPath(Configuration.CustomersDirectoryPath), companyName);

            if (!Directory.Exists(clientDirectory))
            {
                throw new IOException($"Directory \"{clientDirectory}\" doesn't exist.");
            }
            else
            {
                return clientDirectory;
            }
        }

        public static IEnumerable<string> GetCompanyList()
        {
            var result = new List<string>();
            var clientsDirectoryRelativePath = Path.GetFullPath(Configuration.CustomersDirectoryPath);

            if (string.IsNullOrEmpty(clientsDirectoryRelativePath))
            {
                Logger.Info($"Argument \"{clientsDirectoryRelativePath}\" is null.");
                return result;
            }

            var clientsDirectoryPath = Path.GetFullPath(clientsDirectoryRelativePath);

            if (!Directory.Exists(clientsDirectoryPath))
            {
                Logger.Info($"Directory \"{clientsDirectoryRelativePath}\" doesn't found.");
                return result;
            }

            var directories = Directory.GetDirectories(clientsDirectoryPath);

            result.AddRange(directories);

            return result;
        }

        public static bool CheckApplicationConfig()
        {
            if (string.IsNullOrEmpty(Configuration.ServerDirectoryPath))
            {
                return false;
            }

            if (string.IsNullOrEmpty(Configuration.CustomersDirectoryPath))
            {
                return false;
            }

            if (string.IsNullOrEmpty(Configuration.OutputDirectoryPath))
            {
                return false;
            }

            if (string.IsNullOrEmpty(Configuration.ProductVersionPrefix))
            {
                return false;
            }

            return true;
        }
    }
}
