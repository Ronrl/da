﻿using System;
using System.Text.RegularExpressions;
using DeskAlerts.AutoBuilder.Interfaces;
using DeskAlerts.AutoBuilder.Models;

namespace DeskAlerts.AutoBuilder.Dto
{
    public class BuilderParametersDto
    {
        private bool _smpp;
        private bool _sms;

        public string ProductVersion { get; set; }
        public bool FiveServer => true;
        public bool SixServer => true;
        public bool SevenServer => true;
        public bool EightServer => true;
        public bool NineServer => true;
        public bool Demo { get; set; }
        public bool Encryption { get; set; }
        public bool Ad { get; set; }
        public bool Sso { get; set; }
        public bool Video { get; set; }
        public bool Email { get; set; }
        public bool Twitter { get; set; }
        public bool LinkedIn { get; set; }
        public bool Yammer { get; set; }
        public bool TextToCall { get; set; }
        public bool Ticker { get; set; }
        public bool ScreenSaver { get; set; }
        public bool Wallpaper { get; set; }
        public bool Lockscreen { get; set; }
        public bool Rss { get; set; }
        public bool InstantMessenger { get; set; }
        public bool Fullscreen { get; set; }
        public bool Mobile { get; set; }
        public bool DigitalSignage { get; set; }
        public bool Statistics { get; set; }
        public bool Surveys { get; set; }
        public bool Smpp
        {
            get => _smpp;
            set
            {
                _smpp = value;
                if (_smpp)
                {
                    _sms = true;
                }
            }
        }
        public bool Sms
        {
            get => _smpp || _sms;
            set => _sms = _smpp || value;
        }
        public bool Campaign { get; set; }
        public bool MultipleAlerts { get; set; }
        public bool MultiLanguage { get; set; }
        public bool RestApi { get; set; }
        public bool Approve { get; set; }
        public bool SingleInstance { get; set; }
        public bool BulkUploadOfRecipients { get; set; }
        public bool Blog => true;
        public bool WidgetStat => true;
        public bool WebPlugin => true;
        public int Licenses { get; set; }
        public int MaxPublishersCount { get; set; }
        public int Trial { get; set; }

        public override string ToString()
        {
            var result = string.Empty;

            var pattern = $"{Configuration.ProductVersionPrefix}.(\\d{{1,2}}).(\\d{{1,2}})";
            var regexp = new Regex(pattern, RegexOptions.IgnoreCase);

            if (!regexp.IsMatch(ProductVersion))
            {
                throw new ArgumentException("Product version is invalid.");
            }

            if (Licenses < 0)
            {
                throw new ArgumentException("The number of licenses cannot be less than 0.");
            }

            if (MaxPublishersCount < 1)
            {
                throw new ArgumentException("The number of publishers cannot be less than 0.");
            }

            if (Trial < 0)
            {
                throw new ArgumentException("Trial cannot be less than 0.");
            }

            var space = " ";

            result += !string.IsNullOrEmpty(ProductVersion) ? $"{space}/DPRODUCT_VERSION={ProductVersion}" : string.Empty;
            result += FiveServer ? $"{space}/DfiveServer" : string.Empty;
            result += SixServer ? $"{space}/DsixServer" : string.Empty;
            result += SevenServer ? $"{space}/DsevenServer" : string.Empty;
            result += EightServer ? $"{space}/DeightServer" : string.Empty;
            result += NineServer ? $"{space}/DnineServer" : string.Empty;
            result += Demo ? $"{space}/DisDemo" : string.Empty;
            result += Encryption ? $"{space}/DisENCRYPTAddon" : string.Empty;
            result += Ad ? $"{space}/DisADAddon" : string.Empty;
            result += Sso ? $"{space}/DisSSOAddon" : string.Empty;
            result += Video ? $"{space}/DisVIDEOAddon" : string.Empty;
            result += Email ? $"{space}/DisEMAILAddon" : string.Empty;
            result += Twitter ? $"{space}/DisTWITTERAddon" : string.Empty;
            result += LinkedIn ? $"{space}/DisLINKEDINAddon" : string.Empty;
            result += Yammer ? $"/{space}DisYAMMERAddon" : string.Empty;
            result += TextToCall ? $"{space}/DisTEXTCALLAddon" : string.Empty;
            result += Ticker ? $"{space}/DisTICKERAddon" : string.Empty;
            result += ScreenSaver ? $"{space}/DisSCREENSAVERAddon" : string.Empty;
            result += Wallpaper ? $"{space}/DisWALLPAPERAddon" : string.Empty;
            result += Lockscreen ? $"{space}/DisLOCKSCREENAddon" : string.Empty;
            result += Rss ? $"{space}/DisRSSAddon" : string.Empty;
            result += InstantMessenger ? $"{space}/DisIMAddon" : string.Empty;
            result += Fullscreen ? $"{space}/DisFULLSCREENAddon" : string.Empty;
            result += Mobile ? $"{space}/DisMOBILEAddon" : string.Empty;
            result += DigitalSignage ? $"{space}/DisDIGSIGNAddon" : string.Empty;
            result += Statistics ? $"{space}/DisSTATISTICSAddon" : string.Empty;
            result += Surveys ? $"{space}/DisSURVEYSAddon" : string.Empty;
            result += Smpp ? $"{space}/DisSMPPAddon" : string.Empty;
            result += Sms ? $"{space}/DisSMSAddon" : string.Empty;
            result += Campaign ? $"{space}/DisCAMPAIGNAddon" : string.Empty;
            result += MultipleAlerts ? $"{space}/DisMultipleAlerts" : string.Empty;
            result += MultiLanguage ? $"{space}/DisMULTILANGUAGEAddon" : string.Empty;
            result += RestApi ? $"{space}/DisRESTAPIAddon" : string.Empty;
            result += Approve ? $"{space}/DisAPPROVEAddon" : string.Empty;
            result += SingleInstance ? $"{space}/DisSingleInstance" : string.Empty;
            result += BulkUploadOfRecipients ? $"{space}/DisBulkUploadOfRecipients" : string.Empty;
            result += Blog ? $"{space}/DisBLOGAddon" : string.Empty;
            result += WidgetStat ? $"{space}/DisWIDGETSTATAddon" : string.Empty;
            result += WebPlugin ? $"{space}/DisWEBPLUGINAddon" : string.Empty;
            result += Licenses > 0 
                ? $"{space}/Dlicenses={Licenses}"
                : $"{space}/Dlicenses=unlimited";
            result += $"{space}/Dmaxpublisherscount={MaxPublishersCount}";
            result += Trial > 0
                ? $"{space}/Dtrial={Trial}"
                : $"{space}";

            result = result.TrimStart();

            return result;
        }

        public BuilderParametersDto(IClient clientInformation)
        {
            var currentDateTime = DateTime.Now;
            var productVersion = $"{Configuration.ProductVersionPrefix}.{currentDateTime.Month}.{currentDateTime.Day}";

            ProductVersion = productVersion;
            Demo = clientInformation.Demo;
            Encryption = clientInformation.Modules.Encryption;
            Ad = clientInformation.Modules.ActiveDirectory;
            Sso = clientInformation.Modules.SingleSignOn;
            Video = clientInformation.Modules.VideoAlert;
            Email = clientInformation.Modules.EMail;
            Twitter = clientInformation.Modules.Twitter;
            LinkedIn = clientInformation.Modules.LinkedIn;
            Yammer = clientInformation.Modules.Yammer;
            TextToCall = clientInformation.Modules.TextToCall;
            Ticker = clientInformation.Modules.Ticker;
            ScreenSaver = clientInformation.Modules.ScreenSaver;
            Wallpaper = clientInformation.Modules.Wallpaper;
            Lockscreen = clientInformation.Modules.Lockscreen;
            Rss = clientInformation.Modules.Rss;
            InstantMessenger = clientInformation.Modules.InstantMessenger;
            Fullscreen = clientInformation.Modules.Fullscreen;
            Mobile = clientInformation.Modules.MobileSendingModule;
            DigitalSignage = clientInformation.Modules.DigitalSignage;
            Statistics = clientInformation.Modules.ExtendedStatistics;
            Surveys = clientInformation.Modules.Survey;
            Smpp = clientInformation.Modules.SmppSms;
            Sms = clientInformation.Modules.Sms;
            Campaign = clientInformation.Modules.Campaigns;
            MultipleAlerts = clientInformation.Modules.MultipleAlerts;
            MultiLanguage = clientInformation.Modules.MultiLanguageAlerts;
            RestApi = clientInformation.Modules.RestApi;
            Approve = clientInformation.Modules.Approval;
            SingleInstance = clientInformation.SingleInstance;
            BulkUploadOfRecipients = clientInformation.Modules.BulkUploadOfRecipients;
            Licenses = clientInformation.LicensesCount;
            MaxPublishersCount = clientInformation.PublishersCount;
            Trial = clientInformation.Trial;
        }
    }
}
