from django.shortcuts import get_object_or_404
from django.http import HttpResponse, JsonResponse

from .models import *


def index(request):
  return HttpResponse("Hello, world")


def build_info(request, pk):
  build = get_object_or_404(Build, pk=pk)
  modules = dict(map(lambda m: (m.build_alias, False), Module.objects.all()))
  modules.update(dict(map(lambda m: (m.build_alias, True), build.quote.modules.all())))
  return JsonResponse({
    'CompanyName': build.quote.customer.title,
    'ContractDate': build.quote.contract_date,
    'LicenseDate': build.quote.license_date,
    'Version': build.version,
    'LicensesCount': build.quote.licenses_count,
    'PublishersCount': build.quote.publishers_count,
    'SingleInstance': True,
    'Trial': build.trial,
    'Demo': False,
    'Modules': modules,
  }, json_dumps_params={'indent': 2})
