from django.apps import AppConfig


class DaClientsConfig(AppConfig):
    name = 'da_clients'
    verbose_name = 'DeskAlerts Customers'
