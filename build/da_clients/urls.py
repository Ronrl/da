from django.urls import path
from . import views

urlpatterns = [
  path('', views.index, name='index'),
  path('build/<int:pk>/', views.build_info, name='build')
]
