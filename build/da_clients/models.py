from django.db import models
from django.contrib.auth.models import User

class Employee(models.Model):
  class Role(models.IntegerChoices):
    ENGINEER = 1
    SALES = 2
    PARTNER = 3

  name = models.CharField(max_length=200)
  email = models.CharField(max_length=200, blank=True, null=True)
  role = models.IntegerField(choices=Role.choices)

  def __str__(self):
    if self.email:
      return '{} <{}>'.format(self.name, self.email)

    return self.name


class Customer(models.Model):
  title = models.CharField(max_length=200)
  comment = models.TextField(blank=True)

  sales = models.ForeignKey(Employee, blank=True, null=True, on_delete=models.SET_NULL, related_name='+', limit_choices_to={'role': Employee.Role.SALES})
  engineer = models.ForeignKey(Employee, blank=True, null=True, on_delete=models.SET_NULL, related_name='+', limit_choices_to={'role': Employee.Role.ENGINEER})
  partner = models.ForeignKey(Employee, blank=True, null=True, on_delete=models.SET_NULL, related_name='+', limit_choices_to={'role': Employee.Role.PARTNER})

  def __str__(self):
    return self.title


class Module(models.Model):
  title = models.CharField(max_length=200)
  build_alias = models.CharField(max_length=200)

  def __str__(self):
    return self.title


class Quote(models.Model):
  customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
  created_at = models.DateTimeField(auto_now_add=True)

  perpetual = models.BooleanField(default=False)
  cloud = models.BooleanField(default=False)

  contract_date = models.DateField()
  license_date = models.DateField()
  publishers_count = models.IntegerField(default=0)
  licenses_count = models.IntegerField(default=0)
  modules = models.ManyToManyField(Module)

  document = models.FileField(blank=True, null=True)

  comment = models.TextField(blank=True, null=True)

  def __str__(self):
    return "{} quote #{}".format(self.customer, self.id, )

  def modules_list(self):
    return ", ".join(str(m) for m in self.modules.all())

  def latest_build(self):
    try:
      return self.build_set.all().order_by('-created_at')[0]
    except IndexError:
      return None


class Build(models.Model):
  quote = models.ForeignKey(Quote, on_delete=models.CASCADE)
  created_at = models.DateTimeField(auto_now_add=True)

  trial = models.BooleanField(default=False)
  version = models.CharField(max_length=20)

  comment = models.CharField(max_length=200, blank=True, null=True)

  def __str__(self):
    return 'v{} for {}'.format(self.version, self.quote)

