import datetime

from django.contrib import admin
from django.urls import reverse
from django.utils import timezone
from django.utils.http import urlencode
from django.utils.translation import gettext_lazy as _
from django.utils.html import escape, mark_safe
from reversion.admin import VersionAdmin

from .models import *


class QuoteDateFieldListFilter(admin.FieldListFilter):
    def __init__(self, field, request, params, model, model_admin, field_path):
        self.field_generic = '%s__' % field_path
        self.date_params = {k: v for k, v in params.items() if k.startswith(self.field_generic)}

        now = timezone.now()
        # When time zone support is enabled, convert "now" to the user's time
        # zone so Django's definition of "Today" matches what the user expects.
        if timezone.is_aware(now):
            now = timezone.localtime(now)

        if isinstance(field, models.DateTimeField):
            today = now.replace(hour=0, minute=0, second=0, microsecond=0)
        else:       # field is a models.DateField
            today = now.date()
        tomorrow = today + datetime.timedelta(days=1)
        if today.month == 12:
            next_month = today.replace(year=today.year + 1, month=1, day=1)
        else:
            next_month = today.replace(month=today.month + 1, day=1)
        next_year = today.replace(year=today.year + 1, month=1, day=1)

        self.lookup_kwarg_since = '%s__gte' % field_path
        self.lookup_kwarg_until = '%s__lt' % field_path
        self.links = (
            (_('Any date'), {}),
            (_('Past 30 days'), {
                self.lookup_kwarg_since: str(today - datetime.timedelta(days=30)),
                self.lookup_kwarg_until: str(tomorrow),
            }),
            (_('Within 1 month'), {
                self.lookup_kwarg_since: str(today),
                self.lookup_kwarg_until: str(today + datetime.timedelta(days=30)),
            }),
            (_('Within 3 monthes'), {
                self.lookup_kwarg_since: str(today),
                self.lookup_kwarg_until: str(today + datetime.timedelta(days=90)),
            }),
        )
        if field.null:
            self.lookup_kwarg_isnull = '%s__isnull' % field_path
            self.links += (
                (_('No date'), {self.field_generic + 'isnull': 'True'}),
                (_('Has date'), {self.field_generic + 'isnull': 'False'}),
            )
        super().__init__(field, request, params, model, model_admin, field_path)

    def expected_parameters(self):
        params = [self.lookup_kwarg_since, self.lookup_kwarg_until]
        if self.field.null:
            params.append(self.lookup_kwarg_isnull)
        return params

    def choices(self, changelist):
        for title, param_dict in self.links:
            yield {
                'selected': self.date_params == param_dict,
                'query_string': changelist.get_query_string(param_dict, [self.field_generic]),
                'display': title,
            }


def custom_titled_filter(title):
    class Wrapper(admin.FieldListFilter):
        def __new__(cls, *args, **kwargs):
            instance = admin.FieldListFilter.create(*args, **kwargs)
            instance.title = title
            return instance
    return Wrapper


class BuildInline(admin.TabularInline):
  model = Build
  extra = 1
  ordering = ('-created_at', )

class QuoteInline(admin.TabularInline):
  model = Quote
  extra = 1
  ordering = ('-created_at', )


@admin.register(Module)
class ModuleAdmin(admin.ModelAdmin):
  list_display = ('title', 'build_alias')


@admin.register(Employee)
class EmployeeAdmin(admin.ModelAdmin):
  list_display = ('name', 'role', 'email')
  list_filter = ('role', )
  search_fields = ('name', 'email')


@admin.register(Customer)
class CustomerAdmin(VersionAdmin):
  def quote_link(self, obj):
    link = reverse('admin:da_clients_quote_changelist')
    link = '{}?{}'.format(link, urlencode({'customer': obj.id}))
    return mark_safe(f'<a href="{link}">quotes</a>')

  list_display = ('title', 'quote_link', 'engineer', 'sales', 'partner', 'comment')
  search_fields = ('title', 'engineer__name', 'sales__name', 'partner__name')
  inlines = [QuoteInline]


@admin.register(Build)
class BuildAdmin(VersionAdmin):
  def quote_link(self, obj):
    link = reverse('admin:da_clients_quote_change', args=[obj.quote.id])
    return mark_safe(f'<a href="{link}">{obj.quote}</a>')

  def json(self, obj):
    link = reverse('build', args=[obj.id])
    return mark_safe(f'<a href="{link}" target="_blank">JSON</a>')

  list_display = ('__str__', 'quote_link', 'created_at', 'version', 'comment', 'json')
  list_filter = ('quote__customer__title',)
  search_fields = ('quote__customer__title', 'version', 'comment')


@admin.register(Quote)
class QuoteAdmin(VersionAdmin):
  def builds(self, obj):
    link = reverse('admin:da_clients_build_changelist')
    link = '{}?{}'.format(link, urlencode({'quote': obj.id}))
    version = obj.latest_build()
    if version is None:
        version = '-'
    return mark_safe(f'<a href="{link}">{version}</a>')

  list_display = ('__str__', 'created_at', 'perpetual', 'cloud', 'builds', 'contract_date', 'license_date', 'publishers_count', 'licenses_count', 'modules_list', 'comment')
  list_filter = (
    ('license_date', QuoteDateFieldListFilter),
    ('contract_date', QuoteDateFieldListFilter),
    ('modules__title', custom_titled_filter('module')),
  )
  search_fields = ('customer__title', 'modules__title')
  inlines = [BuildInline]
