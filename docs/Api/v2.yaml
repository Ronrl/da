openapi: 3.0.0

servers:
  - description: SwaggerHub API Auto Mocking
    url: https://demo.deskalerts.com/descalerts/api/v2
info:
  description: DeskAlters server API version 2.0
  version: "2.0.0"
  title: DeskAlerts server API
  contact:
    email: 2could@gmail.com
tags:
  - name: v2
    description: API 2 версии
paths:
  /user/contents:
    get:
      security:
        - bearerAuth: []
      tags:
        - v2
      summary: Возвращает информацию о доступных для отображения алертов разных видов
      operationId: GetUserContents
      description: |
        Метод возвращает хэши алертов по типам. При изменении хэша 
        клиентское приложение дожно запросить достпные для отображения алерты
        конкретного типа.

      parameters:
        - in: query
          name: timezone
          description: Разница во времени с поясом +0 в секундах.
          example: 3600
          schema:
            type: integer

      responses:
        '200':
          description: |
            Если Status == 0, то запрос выполнен успешно и поле Data содержит результат
            выполнения запроса. В противном случае поле Data не содержит данных
          content:
            application/json:
              schema:
                properties:
                  Status:
                    $ref: '#/components/schemas/StatusCode'
                  ErrorMessage:
                    type: string
                    description:  Сообщение об ошибке
                  Data:
                    $ref: '#/components/schemas/GetUserContentsApiResponseData'

  /user/content/alerts:
    get:
      security:
        - bearerAuth: []
      tags:
        - v2
      summary: Возвращает
      operationId: GetContentAlerts
      description: |
        Возвращает активные алерты, которые не отображались пользователю
      responses:
        '200':
          description: |
            Список алертов
          content:
            application/json:
              schema:
                properties:
                  Status:
                    $ref: '#/components/schemas/StatusCode'
                  ErrorMessage:
                    type: string
                    example: 0
                    description:  Сообщение об ошибке
                  Data:
                    $ref: '#/components/schemas/Alert'

  /user/content/active:
    get:
      security:
        - bearerAuth: []
      tags:
        - v2
      summary: Возвращает
      operationId: GetContentActive
      description: |
        Возвращает все активные алерты пользователя (всех типов)
      responses:
        '200':
          description: |
            Список алертов
          content:
            application/json:
              schema:
                properties:
                  Status:
                    $ref: '#/components/schemas/StatusCode'
                  ErrorMessage:
                    type: string
                    description:  Сообщение об ошибке
                  Data:
                    $ref: '#/components/schemas/Alert'



  /content/tickers:
    get:
      security:
        - bearerAuth: []
      tags:
        - v2
      summary: Возвращает
      operationId: GetContentTikers
      description: |
        Возвращает страницу для отображения всех актвных тикеров пользователя

      responses:
        '200':
          description: |
            Подготовленный контент (text/html) для отображения всех активных тикеров пользователя
        '500':
          description: |
            Произошла ошибка сервера, отображение контента невозможно


  /api/v2/content/history:
    get:
      security:
        - bearerAuth: []
      tags:
        - v2
      summary: Возвращает записи истории алертов
      operationId: GetContentHistory
      description: |
        Возвращает записи истории алертов 

      parameters:
        - in: query
          name: from
          schema:
            type: integer
            default: 0
          description: Порядковый номер записи, с которой нужно возвращать испорию
          example: 12

        - in: query
          name: count
          schema:
            type: integer
            default: 20
          description: Максимальное количество записей в результате. Если не указано, то 20
          example: 12

        - in: query
          name: query
          schema:
            type: string
          description: |
            Строка, которую должны содержать возвращаемые алерты.
            Метод должен обеспечивать регистронезависимый поиск.
          example: "опрос"

        - in: query
          name: type
          schema:
            type: array
            items:
              type: integer

          description: Типы возвращаемых алертов, если не указано, то все
          example: 1

        - in: query
          name: order
          description: Порядок сортировки результата
          schema:
            type: string
            enum: [NONE, ASC, DESC]
            description: |
              NONE: Нет сортировки, поведение по умолчанию, 
              ASC: По возрастанию,
              DESC: По убыванию

      responses:
        '200':
          description: |
            Список алертов
          content:
            application/json:
              schema:
                properties:
                  Status:
                    $ref: '#/components/schemas/StatusCode'
                  ErrorMessage:
                    type: string
                    description:  Сообщение об ошибке
                  Data:
                    $ref: '#/components/schemas/Alert'




components:
  securitySchemes:
    bearerAuth:
      type: http
      scheme: bearer
      bearerFormat: JWT

  schemas:
    StatusCode:
      type: integer
      enum: [ 0, 1, 2, 3, 4]
      example: 0
      description: |
        0 - Success,
        1 - Fail,
        2 - Error,
        3 - NoContent,
        4 - NA

    GetUserContentsApiResponseData:
      type: object
      description: Ответ на запрос пользователя о новом контенте
      properties:
        AlertsHash:
          type: string
          description: |
            Набор символов, рассчитываемый из идентификаторов всех активных алертов,
            отображающиъся в окне. Система гарантирует, что при изменении активных 
            алертов строка изменится. 
            Если строка пустая, то нет актиных алертов для отображения
          example: 42d43dc8635edec58с3068392bea88b9
        WallpapersHash:
          type: string
          description: |
            Набор символов, рассчитываемый из идентификаторов всех активных алертов,
            устанавливающих обои на рабочий стол. Система гарантирует, что при изменении активных алертов
            строка изменится.
            Если строка пустая, то нет актиных алертов для отображения.
          example: 81d43dc8635edec58d3068392bea88b9
        LockscreensHash:
          type: string
          description: |
            Набор символов, рассчитываемый из идентификаторов всех активных алертов,
            устанавливающих изображене на экране блокировки. Система гарантирует, что при изменении активных алертов
            строка изменится
            Если строка пустая, то нет актиных алертов для отображения.
          example: 5cd8f9ce9860574284967d549dd12854
        ScreensaversHash:
          description: |
            Набор символов, рассчитываемый из идентификаторов всех активных алертов,
            устанавливающих хранитель. Система гарантирует, что при изменении активных алертов
            строка изменится
            Если строка пустая, то нет актиных алертов для отображения.
          type: string
          example: 9a356e9194bbdace5612693c65b60e24
        TikersHash:
          description: |
            Набор символов, рассчитываемый из идентификаторов всех активных алертов,
            отображаемых в виде бегущей строки. Система гарантирует, что при изменении активных алертов
            строка изменится.
            Если строка пустая, то нет актиных алертов для отображения.
          type: string
          example: be41cc6f7d5295b996344e072365a510

    Alert:
      type: object
      description: Праметры алерта
      properties:
        Id:
          type: string
          description: Идентификатор алерта
          example: 42
        Title:
          type: string
          description: Заголовок алерта
          example: Обращение генерального директора

        Class:
          type: integer
          enum: [0, 1, 2, 3, 4, 5, 6, 7, 8, 16, 32, 64, 128, 256, 2048, 4096]
          description: >
            - 0: Undefined
            - 1: SimpleAlert
            - 2: ScreenSaver
            - 3: Ticker
            - 4: RssFeed
            - 5: RssAlert
            - 6: Rsvp
            - 8: Wallpaper
            - 16: EmergencyAlert
            - 32: VideoAlert
            - 64: SimpleSurvey
            - 128: SurveyQuiz
            - 256: SurveyPoll
            - 2048: DigitalSignature
            - 4096: LockScreen

        Urgent:
          type: integer
          default: 0

        Width:
          type: integer

        Height:
          type: integer

        Resizable:
          type: boolean

        Autoclose:
          type: boolean

        SelfDeletable:
          type: boolean

        Position:
          type: string 
          enum: ["fullscreen", "left-top", "right-top", "center", "left-bottom", "right-bottom", "top", "middle", "bottom"]
          description:
            Если позиция не опредлеена, то отображаем в правом нижнем углу.

    