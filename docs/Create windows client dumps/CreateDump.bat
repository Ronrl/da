@Echo off & SetLocal EnableDelayedExpansion
rem Accept procdump.exe eula.
procdump.exe -accepteula

rem Get DeskAlerts pids.
md "DeskAlertsDumps"
set "PID="
for /f "tokens=2" %%A in ('tasklist ^| findstr /i "deskalerts.exe" 2^>NUL') do @Set "PID=!PID! %%A"
rem if defined PID Echo %PID:~1%

rem Delete first space
set ids=%PID:~1%

rem Create dumps.
set index=0
for %%A in (%ids%) do (
    set Array[!index!] = %%A
    set /a index += 1
	procdump.exe -mm %%A "%cd%\DeskAlertsDumps"
)


rem Get DeskAlertsService pid.
set "PID="
for /f "tokens=2" %%A in ('tasklist ^| findstr /i "DeskAlertsService.exe" 2^>NUL') do @Set "PID=!PID! %%A"
rem if defined PID Echo %PID:~1%

rem Delete first space
set ids=%PID:~1%

rem Create dumps.
set index=0
for %%A in (%ids%) do (
    set Array[!index!] = %%A
    set /a index += 1
	procdump.exe -mm %%A "%cd%\DeskAlertsDumps"
)


rem Rename dumps.
cd "%cd%\DeskAlertsDumps"
ren deskalerts.exe_*.dmp %computername%-deskalerts.exe_*.dmp
ren DeskAlertsService.exe_*.dmp %computername%-DeskAlertsService.exe_*.dmp

rem Copy programData folder.
md %computername%-programdata
xcopy %ProgramData%\DeskAlerts %computername%-programdata /s /e /h /y
		
rem Create tasklist and ipconfig.
tasklist /v > %computername%-tasklist.txt
ipconfig /all > %computername%-ipconfig.txt

rem Сatalog archiving.
cd ..
7z\7za.exe a %computername%-DeskAlertsDumps.zip .\DeskAlertsDumps\

rem Sent archive to server
curl\curl.exe -k -v -X POST --data-binary @%computername%-DeskAlertsDumps.zip http://localhost/api/v1/error/client/win

rem Delete folder and files.
del /F /S /Q .\DeskAlertsDumps\
rd /S /Q .\DeskAlertsDumps\
del /F /S /Q %computername%-DeskAlertsDumps.zip