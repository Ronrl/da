#Создание дампов windows clien app (C++)#

Для создания дампов необходимо:

1. Скачать утилиту procdump с официального сайта microsoft [https://docs.microsoft.com/en-us/sysinternals/downloads/procdump](https://docs.microsoft.com/en-us/sysinternals/downloads/procdump "Sysinternals ProcDump")
2. Расположить файлы **CreateDump.bat** и разархивированную утилиту **procdump.exe**, **procdump64.exe**, **Eula.txt** в одном каталоге, в имени которого нет спецсимволов и пробелов.
3. В файле **CreateDump.bat** на **строке 58 изменить имя сервера** на то, на который необходимо переслать архивы с дампами.
4. Запустить **CreateDump.bat** и дождаться завершения работы скрипта.

Формат выходных файлов, после загрузки на сервер, имеет следующих вид: %ClientIPAddress%_%GUID%.dmp.

Результат работы скрипта (файлы .dmp) можно будет забрать в серверном каталоге в папке %serverFolder%\clientCrashDumps\Windows, например: ***c:\inetpub\wwwroot\DeskAlerts\clientCrashDumps\Windows\***.

Для прочтения необходимо переименовать файл %ClientIPAddress%_%GUID%.**dmp** в %ClientIPAddress%_%GUID%.**zip** и распаковать. 