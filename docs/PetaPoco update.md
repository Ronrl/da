#Обновление nuget пакета [petapoco](https://github.com/CollaboratingPlatypus/PetaPoco).

Начиная с 2020-07-02 ([Pull request 68](https://github.com/deskalerts/deskalerts/pull/68)) при обновлении petapoco обязательно должны быть произведены следующие правки в классе PetaPoco.cs:

1. Добавить private field объект для блокировки: 
```object SharedConnectionLocker = new object();```
2. В методах 
```
public void OpenSharedConnection()
``` и 
```public void CloseSharedConnection()
```
необходимо добавить блокировку на весь метод:
```
lock (SharedConnectionLocker)
{
...
}
```