DELETE FROM surveys_answers_stat 
WHERE EXISTS (SELECT sas.*
				FROM surveys_answers_stat AS sas
					LEFT JOIN users AS u ON u.id = sas.user_id
				WHERE u.id IS NULL)

DELETE FROM surveys_custom_answers_stat 
WHERE EXISTS (SELECT scass.*
				FROM surveys_custom_answers_stat AS scass 
					LEFT JOIN users AS u ON scass.user_id = u.id
				WHERE u.id IS NULL)