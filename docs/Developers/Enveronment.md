# Настройка рабочего места разработчика
## Общая информация
Сборка сервера и клиентского приложения под Windows осуществляется
с использованием средств разработки компании Microsoft. Файлы проектов и решений
созданы с использованием Visual Studio 2017. Допускается так же использование среды
Visual Studio 2019 любой редакции, в том числе Community, без преобразрования
файлов проектов и решений в новый формат.

Важно: сборка и работа сервера возможна только под Windows и только с использованием 
.NET Framework версии 4.7.2. Сборка и запуск сервера с использованием .NET Core или Mono
не поддерживается.

## Рекомендуемое ПО
Для работы над проектом потребуется:
1. Среда разработки [Visual Studio](https://visualstudio.microsoft.com/ru/) версии 2017 или новее
2. База данных [MS SQL Server](https://www.microsoft.com/ru-ru/sql-server/sql-server-downloads) (Express, Developer)

Дополнительно рекомендуется использование следющего ПО:
- Инструмент для мониторинга http трафика. Мы используем [Fiddler](https://www.telerik.com/fiddler).
- Инстументы для работы с базой данных. Хорошим выбором будет являться 
  [SQL Server Management Studio](https://docs.microsoft.com/ru-ru/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver15) 
  (наш выбор) или 
  [Azure Data Studio](https://docs.microsoft.com/ru-ru/sql/azure-data-studio/download-azure-data-studio?view=sql-server-ver15)
- Инструменты для .NET разработки от компании JEtBrains.
  Это в первую очередь [Resharper](https://www.jetbrains.com/resharper/)
  или же среда разработки [Rider](https://www.jetbrains.com/rider/),
  а так же полезные инструменты для анализа
  [dotMemory](https://www.jetbrains.com/dotmemory/) и [dotTrace](https://www.jetbrains.com/profiler/)
- GIT клиент. Многие из наших разработчиков испольтзуют [GitKraken](http://gitkraken.com/)
  
Наша система должна работать в браузерах Internet Explorer и 
[Google Chrome](https://www.google.com/intl/ru_ru/chrome/), так что убедитесь, то они установлены у вас.

## Минимальные системные требования
Минимальные системные требования к рабочему месту разработчика определены как:
  * Операционная система Windows 8.1+ или Windows Server 2003+
  * Процессов Intel Core i3 не менее чем с четырмя ядрами и новее
  * Оперативная память не менее 8Gb
  * HDD не менее 300Gb (рекомендуется установка SSD дисков)

## Установка среды разработки
Установку системы рассмотрим на примере Visual Studio Community 2019.
Установка VS версии 2017 будет аналогичной, за исключением того, что 
Windows 8.1 SDK включен в состав дистрибутива и может быть установлен
вместе со средой разработки.

Для начала необходимо скачать инсталятор [здесь](https://visualstudio.microsoft.com/ru/thank-you-downloading-visual-studio/?sku=Community&rel=16).
После запуска скаченного файла инсталятор предолжит скачать необходимые для работы компоненты, соглашаемся.

На вкладке "Рабочие нагрузки" выбираем:
  * ASP.NET и разработка Web приложений
  * Разработка классический приложений .NET
  * Разработка классических приложений на C++
 
В недалеком будущем мы планируем разрадотку новых версий приложений
на платфор ме Xamarin, так что можете выбрать пункт 
"Разработка мобильных приложений на .NET", хотя прямо сейчас это не обязательно.

На вкладке "Отдельные компоненты" для сборки сервера необходимо добавить:
  * Пакет SDK для .NET Framwwork 4.7.2
  * Windows Communication Foundation
  
 Для сборки клиента на вкладке "Отдедльные компоненты" необходимо добавить:
  
  * Windows SDK 10.0.17134.0
  * Windows SDK 10.0.17763.0
  * Поддержка Windows XP на C++ для инструментов VP 2017 (версия 141) 
  * MSVS версии 141 - средства сборки С++ для VS 2017 я x64 или x86 (версия 14.16)
  * ATL C++ для средств сборки версии 141 (x86 и x64)
  * MFC C++ для средств сборки версии 141 (x86 и x64)
  
Дополнительно необходимо скачать и установить [Windows 8.1 SDK] (http://download.microsoft.com/download/B/0/C/B0C80BA3-8AD6-4958-810B-6882485230B5/standalonesdk/sdksetup.exe)
 
На вкладке "Языковые пакеты" обязательно уберите русский и добавьте английский язык.
Visual Studio генерирует очень много файлов, и в этих файлах пишет очень много
комментариев с использованием языка интерфейса по умолчанию. Мы международная
компания, по этому мы стараемся не использовать русский язык в проекте. А еще, постоянная замена
файлов в git, которые не создаются напрямую разработчикм и в которых английские комментарии
меняются на русские и обратно - это самое глупое, что можно сделать.



## Сборка и развертывание сервера
Исходные тексты системы расположены в Git репозитарии по адресу 
[https://github.com/deskalerts/deskalerts] (https://github.com/deskalerts/deskalerts)


### Сборка
deskalerts/apps/servers/legacy/DeskAlerts.Server.sln 


### Настройка
### Запуск с использованием IIS
### Запуск с использованием IIS Express

## Сборка и развертывание Windows клиента
deskalerts/apps/clients/agents/Windows/DeskAlerts.sln
TODO: Описать ручную установку клиента
