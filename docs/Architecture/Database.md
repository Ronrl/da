# Описание структуры базы данных
## Перечень таблиц
Название                    | Описание
--------------------------- | -------------
alert_captions              | 1
alert_param_selects         | 1
alert_param_template_forms  | 1
alert_param_templates       | 1
alert_param_values          | 1
alert_parameters            | 1
alerts                      | 1
alerts_cache_comps          | 1
alerts_cache_ips            | 1
alerts_cache_users          | 1
alerts_comp_stat            | 1
alerts_escalate             | 1
alerts_group_stat           | 1
alerts_langs                | 1
alerts_ou_stat              | 1
alerts_read                 | 1
alerts_received             | 1
alerts_received_recur       | 1
alerts_sent                 | 1
alerts_sent_comp            | 1
alerts_sent_deskbar         | 1
alerts_sent_group           | 1
alerts_sent_iprange         | 1
alerts_sent_ou              | 1
alerts_sent_stat            | 1
archive_alerts              | 1
archive_alerts_comp_stat    | 1
archive_alerts_group_stat   | 1
archive_alerts_read         | 1
archive_alerts_received     | 1
archive_alerts_sent         | 1
archive_alerts_sent_comp    | 1
archive_alerts_sent_group   | 1
archive_alerts_sent_stat    | 1
archive_settings            | 1
banners                     | 1
blogs                       | 1
campaigns                   | 1
client_versions             | 1
color_codes                 | 1
Comp_synch                  | 1
computers                   | 1
computers_groups            | 1
ContentSettings             | 1
digsign_links               | 1
domains                     | 1
edir_context                | 1
email_sent                  | 1
feedbacks                   | 1
group_message_templates     | 1
Group_synch                 | 1
groups                      | 1
groups_groups               | 1
groups_policy               | 1
icalendars                  | 1
instant_messages            | 1
ip_groups                   | 1
ip_range_groups             | 1
jwt_sessions                | 1
languages_list              | 1
Logs                        | 1
menu_items                  | 1
message_templates_groups    | 1
multiple_alerts             | 1
OU                          | 1
OU_comp                     | 1
OU_DOMAIN                   | 1
OU_Group                    | 1
ou_groups                   | 1
OU_hierarchy                | 1
OU_synch                    | 1
OU_User                     | 1
parameters                  | 1
policy                      | 1
policy_computer             | 1
Policy_ContentSettings      | 1
policy_editor               | 1
policy_group                | 1
policy_ip_group             | 1
policy_list                 | 1
policy_ou                   | 1
policy_tgroups              | 1
policy_user                 | 1
policy_viewer               | 1
PolicySkins                 | 1
push_sent                   | 1
recurrence                  | 1
referrals                   | 1
rss                         | 1
settings                    | 1
Skins                       | 1
SkinsProperties             | 1
SkinsTemplates              | 1
sms_sent                    | 1
social_media                | 1
statistics                  | 1
surveys_answers             | 1
surveys_answers_stat        | 1
surveys_custom_answers_stat | 1
surveys_main                | 1
surveys_questions           | 1
synchronizations            | 1
tb_install_stat             | 1
tb_uninstall_stat           | 1
templates                   | 1
templates_preview           | 1
text_templates              | 1
text_to_call_sent           | 1
toolbars                    | 1
unobtrusives                | 1
url_stat                    | 1
urls                        | 1
user_instance_computers     | 1 
user_instance_ip            | 1
user_instances              | 1
User_synch                  | 1
users                       | 1
users_deskbar               | 1
users_groups                | 1
version                     | 1
widgets                     | 1
widgets_editors             | 1



## Действующие таблицы
### alert_captions

Поле      | Тип                 | Описание
----------|---------------------|-----------
id        |uniqueidentifier, pk |
name      |nvarchar(max),, null |
file_name |nvarchar(max),, null |


### alert_param_selects

Поле      | Тип                 | Описание
----------|---------------------|-----------
id        |uniqueidentifier     |
param_id  |uniqueidentifier     |
name      |nvarchar(255)        |
value     |nvarchar(max)        |
type      |tinyint              |
order     |bigint, null         |

### alert_param_template_forms

Поле                 | Тип                 | Описание
---------------------|--------------------|-----------
id                   |uniqueidentifier    |
name                 |nvarchar(255)       |
html                 |nvarchar(max)       |
order                |bigint              |
show_condition       |nvarchar(max), null |
next_button_name     |nvarchar(max), null |
previous_button_name |nvarchar(max), null |
close_button_name    |nvarchar(max), null |
param_temp_id        |uniqueidentifier    |

### alert_param_templates

Поле      | Тип                 | Описание
----------|---------------------|-----------
id        |uniqueidentifier     |
name      |nvarchar(255)        |
html      |nvarchar(max)        |

### alert_param_values

Поле      | Тип                 | Описание
----------|---------------------|-----------
id        |bigint IDENTITY(1,1) |
alert_id  |bigint               |
param_id  |uniqueidentifier     |
value     |nvarchar(max)        |


### alert_parameters

Поле      | Тип                 | Описание
----------|---------------------|-----------
id        |uniqueidentifier     |
temp_id   |uniqueidentifier     |
short_name|varchar(255)         |
name      |nvarchar(255)        |
type      |tinyint              |
order     |smallint             |
default   |nvarchar(max), null  |
visible   |bit                  |
action    |smallint, null       |
required  |bit                  |

### alerts

Поле                  | Тип                  | Описание
----------------------|----------------------|-----------
id                    |bigint IDENTITY(1,1)  |
alert_text            |nvarchar(max), null   |
title                 |nvarchar(255), null   |
type                  |varchar(1), null      |
group_type            |varchar(1), null      |
create_date           |datetime, null        |
sent_date             |datetime, null        |
type2                 |varchar(1), null      |
from_date             |datetime, null        |
to_date               |datetime, null        |
schedule              |varchar(1), null      |
schedule_type         |varchar(1), null      |
recurrence            |varchar(1), null      | 
urgent                |varchar(1), null      |
toolbarmode           |varchar(1), null      |
deskalertsmode        |varchar(1), null      |
sender_id             |bigint, null          |
template_id           |bigint, null          |
group_id              |bigint, null          |
aknown                |varchar(1), null      |
ticker                |varchar(1), null      |
fullscreen            |varchar(1), null      |
ticker_speed          |varchar(1), null      |
anonymous_survey      |varchar(1), null      |
alert_width           |int, null             |
alert_height          |int, null             |
email_sender          |nvarchar(255), null   |
email                 |varchar(1), null      |
sms                   |varchar(1), null      |
desktop               |varchar(1), null      |
escalate              |varchar(1), null      |
escalate_hours        |int, null             |
escalate_count        |int, null             |
escalate_step         |int, null             |
escalate_hours_initial|int, null             |
autoclose             |bigint, null          |
param_temp_id         |uniqueidentifier, null|
caption_id            |uniqueidentifier, null|
class                 |bigint                |
parent_id             |bigint, null          |
resizable             |bit, null             |
post_to_blog          |bit, null             |
social_media          |bit, null             |
self_deletable        |bit, null             |
text_to_call          |varchar(1), null      |
campaign_id           |bigint                |
video                 |bit                   |
approve_status        |int, null             |
reject_reason         |nvarchar(1000), null  |
lifetime              |int, null             |
terminated            |int, null             |
color_code            |uniqueidentifier, null|
rss_allow_content     |int, null             |
device                |int, null             |
is_repeatable_template|bit                   |
parent_template_id    |bigint, null          |


### alerts_cache_comps

Поле      | Тип             | Описание
----------|-----------------|-----------
id        |uniqueidentifier |
alert_id  |bigint           |
recur_id  |bigint           |
comp_id   |bigint           |
from_date |datetime         |
to_date   |datetime         |


### alerts_cache_ips

Поле      | Тип                 | Описание
---------------------------|-----------
id        |uniqueidentifier|
alert_id  |bigint          |
recur_id  |bigint          |
ip_from   |bigint          |
ip_to     |bigint          |
from_date |datetime        |
to_date   |datetime        |


### alerts_cache_users

Поле      | Тип             | Описание
----------|-----------------|-----------
id        |uniqueidentifier |
alert_id  |bigint           |
recur_id  |bigint           |
user_id   |bigint           |
from_date |datetime         |
to_date   |datetime         |

### alerts_comp_stat

Поле      | Тип                 | Описание
----------|---------------------|-----------
id        |bigint IDENTITY(1,1) |
alert_id  |bigint, null         |
comp_id   |bigint, null         |
date      |datetime, null       |

### alerts_escalate

Поле       | Тип                 | Описание
-----------|---------------------|-----------
id         |bigint IDENTITY(1,1) |
id_initial |bigint               |
id_escalate|bigint               |

### alerts_group_stat

Поле      | Тип                 | Описание
----------|---------------------|-----------
id        |bigint IDENTITY(1,1) |
alert_id  |bigint, null         |
group_id  |bigint, null         |
date      |datetime             |


### alerts_langs

Поле         | Тип                 | Описание
-------------|---------------------|-----------
id           |bigint IDENTITY(1,1) |
name         |nvarchar(50)         |
abbreviatures|nvarchar(50)         |
is_default   |bit                  |


### alerts_ou_stat

Поле      | Тип                 | Описание
----------|---------------------|-----------
id        |bigint IDENTITY(1,1) |
alert_id  |bigint, null         |
ou_id     |bigint, null         |
date      |datetime, null       |

### alerts_read

Поле      | Тип                 | Описание
----------|---------------------|-----------
id        |bigint IDENTITY(1,1) |
alert_id  |bigint, null         |
user_id   |bigint, null         |
closetime |varchar(55), null    |
read      |char(10), null       |
date      |datetime, null       |

 ### alerts_received

Поле       | Тип                 | Описание
-----------|---------------------|-----------
id         |bigint IDENTITY(1,1) |
alert_id   |bigint, null         |
user_id    |bigint, null         |
clsid      |varchar(255), null   |
date       |datetime, null       |
computer_id|bigint, null         |
vote       |int, null            |
occurrence |bigint, null         |
ip_address |nvarchar(255), null  |
status     |tinyint, null        |
terminated |tinyint, null        |


### alerts_received_recur

Поле      | Тип                 | Описание
----------|---------------------|-----------
id        |bigint IDENTITY(1,1) |
alert_id  |bigint               |
user_id   |bigint               |
date      |datetime             |
occurrence|bigint, null         |

### alerts_sent

Поле      | Тип                 | Описание
----------|---------------------|-----------
id        |bigint IDENTITY(1,1) |
alert_id  |bigint, null         |
user_id   |bigint, null         |

### alerts_sent_comp

Поле      | Тип                 | Описание
----------|---------------------|-----------
id        |bigint IDENTITY(1,1) |
alert_id  |bigint, null         |
comp_id   |bigint, null         |

### alerts_sent_deskbar

Поле      | Тип                 | Описание
----------|---------------------|-----------
id        |bigint IDENTITY(1,1) |
alert_id  |int, null            |
desk_id   |int, null            |

### alerts_sent_group

Поле      | Тип                 | Описание
----------|---------------------|-----------
id        |bigint IDENTITY(1,1) |
alert_id  |bigint, null         |
group_id  |bigint, null         |

### alerts_sent_iprange

Поле             | Тип                 | Описание
-----------------|---------------------|-----------
id               |bigint IDENTITY(1,1) |
alert_id         |bigint, null         |
ip_range_group_id|bigint, null         |
ip_from          |bigint, null         |
ip_to            |bigint, null         |

### alerts_sent_ou


Поле      | Тип                 | Описание
----------|---------------------|-----------
id|bigint IDENTITY(1,1) |
alert_id|bigint, null |
ou_id|bigint, null |

### alerts_sent_stat

Поле      | Тип                 | Описание
----------|---------------------|-----------
id        |bigint IDENTITY(1,1) |
alert_id  |bigint, null         |
user_id   |bigint, null         |
date      |datetime, null       |

### archive_alerts

Поле                  | Тип                 | Описание
----------------------|---------------------|-----------
id                    |bigint               |
alert_text            |nvarchar(max), null  |
title                 |nvarchar(255), null  |
type                  |varchar(1), null     |
group_type            |varchar(1), null     |
create_date           |datetime, null       |
sent_date             |datetime, null       |
type2                 |varchar(1), null     |
from_date             |datetime, null       |
to_date               |datetime, null       |
schedule              |varchar(1), null     |
schedule_type         |varchar(1), null     |
recurrence            |varchar(1), null     |
urgent                |varchar(1), null     |
toolbarmode           |varchar(1), null     |
deskalertsmode        |varchar(1), null     |
sender_id             |bigint, null         |
template_id           |bigint, null         |
group_id              |bigint, null         |
aknown                |varchar(1), null     |
ticker                |varchar(1), null     |
ticker_speed          |varchar(1), null     |
anonymous_survey      |varchar(1), null     |
fullscreen            |varchar(1), null     |
alert_width           |int, null            |
alert_height          |int, null            |
email_sender          |nvarchar(255), null  |
email                 |varchar(1), null     |
sms                   |varchar(1), null     |
desktop               |varchar(1), null     |
escalate              |varchar(1), null     |
escalate_hours        |int, null            |
escalate_count        |int, null            |
escalate_step         |int, null            |
escalate_hours_initial|int, null            |
autoclose             |bigint, null         |
param_temp_id         |uniqueidentifier,null|
caption_id            |uniqueidentifier,null|
class                 |bigint               |
parent_id             |bigint, null         |
resizable             |bit, null            |
post_to_blog          |bit, null            |
social_media          |bit, null            |
self_deletable        |bit, null            |
text_to_call          |varchar(1), null     |
campaign_id           |bigint               |
video                 |bit                  |
approve_status        |int, null            |
reject_reason         |nvarchar(1000), null |
lifetime              |int, null            |
terminated            |int, null            |
color_code            |uniqueidentifier,null|
rss_allow_content     |int, null            |
device                |int, null            |
is_repeatable_template|bit                  |
parent_template_id    |bigint, null         |

### archive_alerts_comp_stat

Поле      | Тип             | Описание
----------|-----------------|-----------
id        |bigint           |
alert_id  |bigint, null     |
comp_id   |bigint, null     |
date      |datetime, null   |

### archive_alerts_group_stat

Поле      | Тип           | Описание
----------|---------------|-----------
id        |bigint         |
alert_id  |bigint, null   |
group_id  |bigint, null   |
date      |datetime, null |

### archive_alerts_read

Поле      | Тип              | Описание
----------|------------------|-----------
id        |bigint            |
alert_id  |bigint, null      |
user_id   |bigint, null      |
closetime |varchar(55), null |
read      |char(10), null    |
date      |datetime, null    |

### archive_alerts_received

Поле      | Тип               | Описание
----------|-------------------|-----------
id        |bigint             |
alert_id  |bigint, null       |
user_id   |bigint, null       |
clsid     |varchar(255), null |
date      |datetime, null     |
vote      |int, null          |
occurrence|bigint, null       |

### archive_alerts_sent

Поле      | Тип          | Описание
----------|--------------|-----------
id        |bigint        |
alert_id  |bigint, null  |
user_id   |bigint, null  |

### archive_alerts_sent_comp

Поле      | Тип          | Описание
----------|--------------|-----------
id        |bigint        |
alert_id  |bigint, null  |
comp_id   |bigint, null  |

### archive_alerts_sent_group

Поле      | Тип         | Описание
----------|-------------|-----------
id        |bigint       |
alert_id  |bigint, null |
group_id  |bigint, null |

### archive_alerts_sent_stat

Поле      | Тип           | Описание
----------|---------------|-----------
id        |bigint         |
alert_id  |bigint, null   |
user_id   |bigint, null   |
date      |datetime, null |

### archive_settings

Поле           | Тип                 | Описание
---------------|---------------------|-----------
id             |bigint IDENTITY(1,1) |
move_type      |varchar(1), null     |
move_number    |int, null            |
move_date_type |varchar(1), null     |
clear_type     |varchar(1), null     |
clear_number   |int, null            |
clear_date_type|varchar(1), null     |

### banners

Поле                | Тип               | Описание
--------------------|-------------------|-----------
id                  |int IDENTITY(1,1)  |
name                |varchar(50), null  |
date                |datetime, null     |
width               |int, null          |
width_unit          |varchar(3), null   |
max_width           |int, null          |
max_width_unit      |varchar(3), null   |
min_width           |int, null          |
min_width_unit      |varchar(3), null   |
height              |int, null          |
height_unit         |varchar(3), null   |
max_height          |int, null          |
max_height_unit     |varchar(3), null   |
min_height          |int, null          |
min_height_unit     |varchar(3), null   |
background_color    |varchar(7), null   |
transparent         |bit, null          |
font_color          |varchar(7), null   |
font_size           |int, null          |
font_size_unit      |varchar(3), null   |
font_weight         |int, null          |
font_weight_unit    |varchar(3), null   |
text_align          |varchar(10), null  |
direction           |varchar(20), null  |
unicode_bidi        |varchar(20), null  |
border_weight       |int, null          |
border_weight_unit  |varchar(3), null   |
border_style        |varchar(30), null  |
border_color        |varchar(7), null   |
padding_top         |int, null          |
padding_top_unit    |varchar(30), null  |
padding_right       |int, null          |
padding_right_unit  |varchar(30), null  |
padding_bottom      |int, null          |
padding_bottom_unit |varchar(30), null  |
padding_left        |int, null          |
padding_left_unit   |varchar(30), null  |
margin_top          |int, null          |
margin_top_unit     |varchar(30), null  |
margin_right        |int, null          |
margin_right_unit   |varchar(30), null  |
margin_bottom       |int, null          |
margin_bottom_unit  |varchar(30), null  |
margin_left         |int, null          |
margin_left_unit    |varchar(30), null  |
display             |varchar(30), null  |
position            |varchar(30), null  |
top                 |int, null          |
top_unit            |varchar(3), null   |
right               |int, null          |
right_unit          |varchar(3), null   |
bottom              |int, null          |
bottom_unit         |varchar(3), null   |
left                |int, null          |
left_unit           |varchar(3), null   |

### blogs

Поле        | Тип               | Описание
------------|-------------------|-----------
id          |uniqueidentifier   |
name        |nvarchar(50)       |
url         |nvarchar(500)      |
username    |nvarchar(50)       |
password    |nvarchar(50)       |
blogID      |int                |
draft       |bit, null          |
comments    |bit, null          |
postAsPage  |bit, null          |
author      |nvarchar(50), null |

### campaigns

Поле        | Тип                 | Описание
------------|---------------------|-----------
id          |bigint IDENTITY(1,1) |
name        |nvarchar(255), null  |
create_date |datetime, null       |
start_date  |datetime, null       |
end_date    |datetime, null       |
is_active   |bit, null            |
sender_id   |bigint, null         |

### client_versions

Поле      | Тип                 | Описание
----------|---------------------|-----------
id        |bigint IDENTITY(1,1) |
user_id   |bigint, null         |
winver    |varchar(255), null   |
iever     |varchar(255), null   |
spver     |varchar(255), null   |
date      |datetime, null       |

### color_codes

Поле      | Тип                   | Описание
----------|-----------------------|-----------
id        |uniqueidentifier, null |
name      |nvarchar(50), null     |
color     |varchar(6), null       |

### Comp_synch

Поле    | Тип                 | Описание
--------|---------------------|------------
SId     |bigint IDENTITY(1,1) |
Id_comp |bigint               |
UID     |varchar(128), null   |

### computers

Поле        | Тип                | Описание
------------|--------------------|-----------
id          |bigint IDENTITY(1,1)|
name        |nvarchar(255), null |
last_date   |datetime, null      |
reg_date    |datetime, null      |
last_request|datetime, null      |
next_request|varchar(255), null  |
domain_id   |int, null           |
context_id  |int, null           |
standby     |int, null           |
disabled    |int, null           |
was_checked |int, null           |

### computers_groups

Поле        | Тип                 | Описание
------------|---------------------|-----------
id          |bigint IDENTITY(1,1) |
group_id    |bigint, null         |
comp_id     |bigint, null         |
was_checked |int, null            |

### ContentSettings

Поле  | Тип                 | Описание
------|---------------------|-----------
id    |bigint IDENTITY(1,1) |
Name  |nvarchar(100)        |

### digsign_links

Поле                | Тип                | Описание
--------------------|--------------------|-----------
id                  |int IDENTITY(1,1)   |
name                |nvarchar(255), null |
link                |nvarchar(255), null |
creation_date       |datetime, null      |
slide_time_value    |int, null           |
slide_time_factor   |int, null           |

### domains

Поле       | Тип                 | Описание
-----------|---------------------|-----------
id         |bigint IDENTITY(1,1) |
name       |nvarchar(255), null  |
all_groups |int, null            |
all_ous    |int, null            |
was_checked|int, null            |

### edir_context

Поле       | Тип                 | Описание
-----------|---------------------|-----------
id         |bigint IDENTITY(1,1) |
name       |nvarchar(max), null  |
domain_id  |int, null            |
was_checked|int, null            |

### email_sent

Поле      | Тип                 | Описание
----------|---------------------|-----------
id        |int IDENTITY(1,1)    |  
user_id   |int                  |
alert_id  |int                  |
sent_date |datetime NOT, null   |

### feedbacks

Поле      | Тип                 | Описание
----------|---------------------|-----------
id        |int IDENTITY(1,1)    |
client    |nvarchar(255)        |
text      |nvarchar(max), null  |
date      |datetime, null       |
readed    |bit                  |

### group_message_templates

Поле       | Тип                 | Описание
-----------|---------------------|-----------
id         |int IDENTITY(1,1)    |
group_id   |int                  |
template_id|int NOT, null        |

### Group_synch

Поле      | Тип                 | Описание
----------|---------------------|------------
SId       |bigint IDENTITY(1,1) |
Id_group  |bigint               |
UID       |varchar(128), null   |

### groups

Поле        | Тип                 | Описание
------------|---------------------|-----------
id          |bigint IDENTITY(1,1) |
name        |nvarchar(255), null  |
display_name|varchar(255), null   |
alert_id    |int, null            |
type        |varchar(1), null     |
special_id  |varchar(50), null    |
domain_id   |int, null            |
context_id  |int, null            |
rss_url     |varchar(255), null   |
was_checked |int, null            |
root_group  |int, null            |
custom_group|int, null            |
policy_id   |bigint, null         |
dn          |varchar(255), null   |

### groups_groups

Поле                | Тип                 | Описание
--------------------|---------------------|-----------
id                  |bigint IDENTITY(1,1) |
container_group_id  |bigint               |
group_id            |bigint               |
was_checked         |int, null            |

### groups_policy

Поле       | Тип                 | Описание
-----------|---------------------|-----------
id         |bigint IDENTITY(1,1) |
policy_id  |bigint, null         |
group_id   |bigint, null         |
was_checked|int, null            |

### icalendars

Поле            | Тип                 | Описание
----------------|---------------------|-----------
id              |uniqueidentifier     |
calendar_id     |uniqueidentifier     |
summary         |nvarchar(max)        |
location        |nvarchar(max)        |
description     |nvarchar(max)        |
sender_id       |bigint               |
organizer       |nvarchar(255), null  |
organizer_email |nvarchar(255), null  |
create_date     |datetime             |
modify_date     |datetime             |
class           |tinyint              |
priority        |tinyint              |
sequence        |int                  |
status          |tinyint              |
start_date      |datetime, null       |
end_date        |datetime, null       |
alarm           |datetime, null       |

### instant_messages

Поле      | Тип                 | Описание
----------|---------------------|-----------
id        |uniqueidentifier     |
alert_id  |bigint               |

### ip_groups

Поле      | Тип                 | Описание
----------|---------------------|-----------
id        |bigint IDENTITY(1,1) |
name      |nvarchar(255), null  |

### ip_range_groups

Поле      | Тип                 | Описание
----------|---------------------|-----------
id        |bigint IDENTITY(1,1) |
group_id  |bigint, null         | 
from_ip   |varchar(255), null   |
to_ip     |varchar(255), null   |

### jwt_sessions

Поле         | Тип                 | Описание
-------------|---------------------|-----------
id           |int IDENTITY(1,1)    |
user_id      |nvarchar(255), null  |
creation_time|nvarchar(255), null  |
token_value  |nvarchar(255), null  |

### languages_list

Поле      | Тип                 | Описание
----------|---------------------|-----------
id        |bigint IDENTITY(1,1) |
name      |varchar(255)         |
code      |varchar(10), null    |

### Logs

Поле            | Тип              | Описание
----------------|------------------|------------
LogId           |int IDENTITY(1,1) |
DateTimeUTC     |datetime          |
IPAddress       |varchar(max)      |
Level           |nvarchar(max)     |
CallSite        |nvarchar(max)     |
Message         |nvarchar(max)     |
ExType          |nvarchar(max)     |
ExMessage       |nvarchar(max)     |
ExStackTrace    |nvarchar(max)     |
ExInnerException|nvarchar(max)     |

### menu_items

Поле            | Тип               | Описание
----------------|-------------------|-----------
id              |int IDENTITY(0,1)  |
name            |nvarchar(50)       |
loc_key         |nvarchar(200)      |
image           |nvarchar(max)      |
link            |nvarchar(200)      |
enabled         |int                |
parent_id       |int                |
default_index   |int                |
index_in_parent |int NOT, null      |

### message_templates_groups

Поле    | Тип               | Описание
--------|-------------------|-----------
id      |int IDENTITY(1,1)  |
name    |nvarchar(50), null |

### multiple_alerts

Поле         | Тип              | Описание
-------------|------------------|-----------
id           |int IDENTITY(1,1) |
alert_id     |int               |
part_id      |int               |
part_content |nvarchar(max)     |
confirmed    |int               |
stat_alert_id|nvarchar(200)     |

### OU

Поле       | Тип                 | Описание
-----------|---------------------|-----------
id         |bigint IDENTITY(1,1) |
Id_domain  |bigint, null         |
Name       |nvarchar(255), null  |
OU_PATH    |nvarchar(max), null  |
Was_checked|varchar(1), null     |
type       |varchar(155), null   |

### OU_comp

Поле    | Тип                 | Описание
--------|---------------------|------------
PId     |bigint IDENTITY(1,1) |
Id_comp |bigint               |
Id_OU   |bigint               |

### OU_DOMAIN

Поле      | Тип                 | Описание
----------|---------------------|-----------
id_domain |bigint, null         |
Id_ou     |bigint               |

### OU_Group

Поле    | Тип                 | Описание
--------|---------------------|------------
PId     |bigint IDENTITY(1,1) |
Id_group|bigint               |
Id_OU   |bigint               |

### ou_groups

Поле       | Тип                 | Описание
-----------|---------------------|-----------
id         |bigint IDENTITY(1,1) |
group_id   |bigint               |
ou_id      |bigint               |
was_checked|int                  |

### OU_hierarchy

Поле      | Тип                 | Описание
----------|---------------------|------------
PId|bigint IDENTITY(1,1) |
Id_parent|bigint |
Id_child|bigint |

### OU_synch

Поле      | Тип                 | Описание
----------|---------------------|------------
SId|bigint IDENTITY(1,1) |
Id_OU|bigint |
UID|varchar(128), null |

### OU_User
Поле      | Тип                 | Описание
----------|---------------------|------------
PId|bigint IDENTITY(1,1) |
Id_user|bigint |
Id_OU|bigint |

### parameters

Поле      | Тип                 | Описание
----------|---------------------|------------
paramName|varchar(255), null |
paramValue|varchar(255), null

### policy

Поле      | Тип                 | Описание
----------|---------------------|-----------
id|bigint IDENTITY(1,1) |
name|nvarchar(255), null |
type|varchar(1), null |
full_skin_access|bit |
full_contentSettings_access|bit |

### policy_computer

Поле      | Тип                 | Описание
----------|---------------------|-----------
id|bigint IDENTITY(1,1) |
policy_id|bigint, null |
comp_id|bigint, null |
was_checked|int, null |

### Policy_ContentSettings

Поле      | Тип                 | Описание
----------|---------------------|-----------
id|bigint IDENTITY(1,1) |
Policy_Id|bigint |
ContentSetting_Id|bigint |

### policy_editor

Поле      | Тип                 | Описание
----------|---------------------|-----------
id|bigint IDENTITY(1,1) |
policy_id|bigint, null |
editor_id|bigint, null |
group_id|bigint, null |
was_checked|int, null |

### policy_group

Поле      | Тип                 | Описание
----------|---------------------|-----------
id|bigint IDENTITY(1,1) |
policy_id|bigint, null |
group_id|bigint, null |
was_checked|int, null |

### policy_ip_group

Поле      | Тип                 | Описание
----------|---------------------|-----------
id|bigint IDENTITY(1,1) |
policy_id|bigint, null |
group_id|bigint, null |
was_checked|int, null |

### policy_list

Поле      | Тип                 | Описание
----------|---------------------|-----------
id|bigint IDENTITY(1,1) |
policy_id|bigint, null |
alerts_val|varchar(8), null |
emails_val|varchar(8), null |
sms_val|varchar(8), null |
surveys_val|varchar(8), null |
users_val|varchar(8), null |
groups_val|varchar(8), null |
templates_val|varchar(8), null |
text_templates_val|varchar(8), null |
statistics_val|varchar(8), null |
ip_groups_val|varchar(8), null |
rss_val|varchar(8), null |
screensavers_val|varchar(8), null |
wallpapers_val|varchar(8), null |
lockscreens_val|varchar(8), null |
im_val|varchar(8), null |
text_to_call_val|varchar(8), null |
feedback_val|varchar(8), null |
cc_val|varchar(8), null |
webplugin_val|varchar(8), null |
video_val|varchar(8), null |
campaign_val|varchar(8), null |

### policy_ou

Поле      | Тип                 | Описание
----------|---------------------|-----------
id|bigint IDENTITY(1,1) |
policy_id|bigint, null |
ou_id|bigint, null |
was_checked|int, null |

### policy_tgroups

Поле      | Тип                 | Описание
----------|---------------------|-----------
id|int IDENTITY(1,1) |
tgroup_id|bigint |
policy_id|bigint |
was_checked|int NOT, null

### policy_user

Поле      | Тип                 | Описание
----------|---------------------|-----------
id|bigint IDENTITY(1,1) |
policy_id|bigint, null |
user_id|bigint, null |
was_checked|int, null |

### policy_viewer

Поле      | Тип                 | Описание
----------|---------------------|-----------
id|bigint IDENTITY(1,1) |
policy_id|bigint, null |
editor_id|bigint, null |

### PolicySkins

Поле      | Тип                 | Описание
----------|---------------------|-----------
id|bigint IDENTITY(1,1) |
PolicyId|bigint |
SkinId|bigint |

### push_sent

Поле      | Тип                 | Описание
----------|---------------------|-----------
id|int IDENTITY(1,1) |
alert_id|int |
sent_date|datetime |
status|int |
try_count|int |
user_instance_id|uniqueidentifier |
user_id|int, null |

### recurrence

Поле      | Тип                 | Описание
----------|---------------------|-----------
id|bigint IDENTITY(1,1) |
alert_id|bigint, null |
pattern|varchar(1), null |
number_days|int, null |
number_week|int, null |
week_days|int, null |
month_day|int, null |
number_month|int, null |
weekday_place|int, null |
weekday_day|int, null |
month_val|int, null |
dayly_selector|varchar(50), null |
monthly_selector|varchar(50), null |
yearly_selector|varchar(50), null |
end_type|varchar(50), null |
occurences|int, null |

### referrals

Поле      | Тип                 | Описание
----------|---------------------|-----------
id|int IDENTITY(1,1) |
visit_date|datetime |
email|nvarchar(255), null |

### rss

Поле      | Тип                 | Описание
----------|---------------------|-----------
id|bigint IDENTITY(1,1) |
broad_url|varchar(255), null |

### settings

Поле      | Тип                 | Описание
----------|---------------------|-----------
id|bigint IDENTITY(1,1) |
name|varchar(255), null |
val|nvarchar(max), null |
s_type|varchar(1), null |
s_part|varchar(1), null |

### Skins

Поле      | Тип                 | Описание
----------|---------------------|-----------
id|bigint IDENTITY(1,1) |
Template|bigint |
Name|nvarchar(256) |
SkinIdOld|uniqueidentifier |
Directory|nvarchar(256) |
Enabled|bit |
CreateDate|datetime |
CreateAuthor|bigint |
EditDate|datetime |
EditAuthor|bigint |

### SkinsProperties

Поле      | Тип                 | Описание
----------|---------------------|-----------
id|bigint IDENTITY(1,1) |test
SkinId|bigint |
DisplayName|nvarchar(256) |
Name|nvarchar(256) |
Type|int |
Component|int |
Value|nvarchar(max), null |
ValueBinary|varbinary(max), null |


### SkinsTemplates

Поле      | Тип                 | Описание
----------|---------------------|-----------
id|bigint IDENTITY(1,1) |
Name|nvarchar(256) |
Directory|nvarchar(256) |
Enabled|bit |

### sms_sent

Поле      | Тип                 | Описание
----------|---------------------|-----------
id|int IDENTITY(1,1) |
user_id|int |
alert_id|int |
sent_date|datetime NOT, null

### social_media

Поле      | Тип                 | Описание
----------|---------------------|-----------
id|uniqueidentifier ROWGUIDCOL  |
type|tinyint |
name|nvarchar(250), null |
field1|nvarchar(500), null |
field2|nvarchar(500), null |
field3|nvarchar(500), null |
field4|nvarchar(500), null |

### statistics

Поле      | Тип                 | Описание
----------|---------------------|-----------
id|bigint IDENTITY(1,1) |
send|int, null |
view|int, null |
clicks|int, null |
date|datetime, null |
alert_id|int, null |
user_id|text, null |

### surveys_answers

Поле      | Тип                 | Описание
----------|---------------------|-----------
id|bigint IDENTITY(1,1) |
answer|nvarchar(max), null |
survey_id|bigint, null |
votes|bigint, null |
question_id|bigint, null |
correct|bit, null |

### surveys_answers_stat

Поле      | Тип                 | Описание
----------|---------------------|-----------
id|bigint IDENTITY(1,1) |
answer_id|bigint, null |
user_id|bigint, null |
date|datetime, null |
clsid|varchar(255), null |

### surveys_custom_answers_stat

Поле      | Тип                 | Описание
----------|---------------------|-----------
id|bigint IDENTITY(1,1) |
question_id|bigint, null |
user_id|bigint, null |
date|datetime, null |
clsid|varchar(255), null |
answer|nvarchar(max), null |

### surveys_main

Поле      | Тип                 | Описание
----------|---------------------|-----------
id|bigint IDENTITY(1,1) |
name|nvarchar(255), null |
question|nvarchar(max), null |
type|varchar(1), null |
group_type|varchar(1), null |
create_date|datetime, null |
closed|varchar(1), null |
sender_id|bigint, null |
template_id|bigint, null |
from_date|datetime, null |
to_date|datetime, null |
schedule|varchar(1), null |
anonymous_survey|varchar(1), null |
stype|smallint |

### surveys_questions

Поле           | Тип                 | Описание
---------------|---------------------|-----------
id             |bigint IDENTITY(1,1) |
question       |nvarchar(max), null  |
survey_id      |bigint, null         |
question_number|bigint, null         |
question_type  |varchar(1), null     |

### synchronizations

Поле            | Тип                 | Описание
----------------|---------------------|-----------
id              |uniqueidentifier     |
name            |nvarchar(max), null  |
sync_data       |nvarchar(max), null  |
last_sync_time  |datetime, null       |
start_time      |datetime, null       |
recur_id        |bigint, null         |

### tb_install_stat

Поле      | Тип                 | Описание
----------|---------------------|-----------
id        |bigint IDENTITY(1,1) |
toolbar_id|bigint, null         |
date      |datetime, null       |

### tb_uninstall_stat

Поле      | Тип                 | Описание
----------|---------------------|-----------
id        |bigint IDENTITY(1,1) |
toolbar_id|bigint, null         |
date      |datetime, null       |

### templates

Поле      | Тип                 | Описание
----------|---------------------|-----------
id        |bigint IDENTITY(1,1) |
top       |nvarchar(max), null  |
bottom    |nvarchar(max), null  |
name      |nvarchar(255), null  |
sender_id |bigint, null         |

### templates_preview

Поле         | Тип                 | Описание
-------------|---------------------|-----------
id           |bigint IDENTITY(1,1) |
top          |nvarchar(max), null  |
bottom       |nvarchar(max), null  |
text_template|nvarchar(max), null  |
name         |varchar(255), null   |

### text_templates

Поле         | Тип                 | Описание
-------------|---------------------|-----------
id           |bigint IDENTITY(1,1) |
name         |nvarchar(255), null  |
template_text|nvarchar(max), null  |

### text_to_call_sent

Поле      | Тип              | Описание
----------|------------------|-----------
id        |int IDENTITY(1,1) |
user_id   |int               |
alert_id  |int               |
sent_date |datetime NOT, null|

### toolbars

Поле                 | Тип                | Описание
--------------------|---------------------|-----------
id                  |bigint IDENTITY(1,1) |
tbid                |varchar(100), null   |
user_id             |bigint, null         |
after_install_page  |varchar(255), null   |
after_uninstall_page|varchar(255), null   |
name                |varchar(255), null   |

### unobtrusives

Поле    | Тип               | Описание
--------|-------------------|-----------
id      |int IDENTITY(1,1)  |
from    |nvarchar(50)       |
to      |nvarchar(50)       |
days    |nvarchar(50), null |
enabled |int                |
user_id |int, null          |

### url_stat

Поле        | Тип                 | Описание
------------|---------------------|-----------
id          |bigint IDENTITY(1,1) |
toolbar_id  |bigint, null         |
url_id      |bigint, null         |
date        |datetime, null       |

### urls

Поле      | Тип                 | Описание
----------|---------------------|-----------
id        |bigint IDENTITY(1,1) |
url       |varchar(255), null   |
toolbar_id|bigint, null         |
search    |int, null            |
keywords  |varchar(255), null   |

### user_instance_computers

Поле                        | Тип              | Описание
----------------------------|------------------|-----------
id                          |uniqueidentifier  |
user_instance_guid          |uniqueidentifier  |
user_instance_computer_id   |bigint, null      |

### user_instance_ip

Поле                | Тип                 | Описание
--------------------|---------------------|-----------
id                  |uniqueidentifier     |
user_instance_guid  |uniqueidentifier     |
user_instance_ip    |nvarchar(255), null  |

### user_instances

Поле            | Тип                   | Описание
----------------|-----------------------|-----------
id              |uniqueidentifier       |
user_id         |bigint                 |
clsid           |uniqueidentifier, null |
device_type     |int                    |
device_id       |varchar(512)           |
android_sdk_int |int, null              |

### User_synch

Поле     | Тип                | Описание
--------|---------------------|------------
SId     |bigint IDENTITY(1,1) |
Id_user |bigint               |
UID     |varchar(128), null   |

### users

Поле                            | Тип                | Описание
--------------------------------|--------------------|-----------
id                              |bigint IDENTITY(1,1)|
name                            |nvarchar(255), null |
display_name                    |nvarchar(255), null |
pass                            |varchar(255), null  |
deskbar_id                      |varchar(255), null  |
alert_id                        |text, null          |
last_date                       |datetime, null      |
role                            |varchar(1), null    |
group_id                        |int, null           |
last_rss                        |text, null          |
rss_url                         |varchar(255), null  |
last_broad                      |text, null          |
reg_date                        |datetime, null      |
last_request                    |datetime, null      |
next_request                    |varchar(255), null  |
last_standby_request            |datetime, null      |
next_standby_request            |varchar(255), null  |
mobile_phone                    |varchar(50), null   |
email                           |nvarchar(255), null |
domain_id                       |int, null           |
context_id                      |int, null           |
standby                         |int, null           |
disabled                        |int, null           |
was_checked                     |int, null           |
rss_modify                      |varchar(255), null  |
rss_broad_modify                |varchar(255), null  |
uhash                           |varchar(255), null  |
dis                             |int, null           |
update_date                     |datetime, null      |
expire_date                     |datetime, null      |
fname                           |varchar(255), null  |
sname                           |varchar(255), null  |
only_personal                   |int, null           |
only_templates                  |int, null           |
only_alerts                     |int, null           |
client_version                  |varchar(255), null  |
start_page                      |varchar(255), null  |
guide                           |varchar(5), null    |
dis_guide                       |varchar(100), null  |
send_logs                       |int, null           |
publisher_status                |varchar(255), null  |
publisher_last_online_datetime  |datetime, null      |
language_id                     |int, null           |
time_zone                       |int, null           |

### users_deskbar

Поле      | Тип                 | Описание
----------|---------------------|-----------
id        |bigint IDENTITY(1,1) |
user_id   |int, null            |
deskbar_id|varchar(255), null   |

### users_groups


Поле       | Тип                 | Описание
-----------|---------------------|-----------
id         |bigint IDENTITY(1,1) |
group_id   |bigint, null         |
user_id    |bigint, null         |
last_rss   |text, null           |
was_checked|int, null            |
rss_modify |varchar(255), null   |

### version

Поле                | Тип                 | Описание
--------------------|---------------------|-----------
id                  |bigint, IDENTITY(1,1)|
version             |varchar(50), null    |
ul                  |varchar(max), null   |
ul_date             |datetime, null       |
ul_logins           |int, null            |
contract_date       |datetime, null       |
license_expire_date |datetime, null       |
used_prolong_keys   |varchar(max), null   |

### widgets

Поле      | Тип                 | Описание
----------|---------------------|-----------
id        |uniqueidentifier, pk |
title     |varchar(50),, null   |
href      |varchar(50),, null   |
priority  |int,, null           |

### widgets_editors

Поле      | Тип                 | Описание
----------|---------------------|-----------
id        |uniqueidentifier, pk |
editor_id |bigint               |
widget_id |uniqueidentifier     |
parameters|varchar(500),, null  |
page      |varchar(1),, null    |

# Типовые запросы
